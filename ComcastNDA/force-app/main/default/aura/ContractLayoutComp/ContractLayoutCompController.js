({
    doInit : function(component, event, helper) {
        // Action to fetch the user role
        var actionUserProfile = component.get("c.getUserProfile");
        actionUserProfile.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.UserProfile", response.getReturnValue());
                console.log('user profile -'+response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
		$A.enqueueAction(actionUserProfile);
        
        //action to fetch current contract status
        var fetchContractStatus = component.get("c.getContractStatus");
        fetchContractStatus.setParams({
            "ContractId" : component.get("v.recordId")
        });
        fetchContractStatus.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.ContractStatus", response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(fetchContractStatus);
        
        //action to fetch the contract manager public group
        var fetchCMBoolean = component.get("c.getIsCMGroupMember");
        fetchCMBoolean.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.IsCMGroupMember", response.getReturnValue());
                console.log('group mem?-'+response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    console.log(JSON.parse(JSON.stringify(event.getParams())));
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(fetchCMBoolean);
        
        //action to fetch the contract Attroney public group
        var fetchABoolean = component.get("c.getIsAGroupMember");
        fetchABoolean.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.IsAGroupMember", response.getReturnValue());
                console.log('group mem?-'+response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    console.log(JSON.parse(JSON.stringify(event.getParams())));
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(fetchABoolean);
        
        //action to fetch assigned attorney
        var fetchAssignedAttorney = component.get("c.getIsAssignedAttorney");
        fetchAssignedAttorney.setParams({
            "ContractId" : component.get("v.recordId")
        });
        fetchAssignedAttorney.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.IsAssignedAttorney", response.getReturnValue());
                console.log('Is assigned attorney-'+response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(fetchAssignedAttorney);
        
    },
    gotoAssignContract : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        var url = '/apex/ChangeOwnerDetailNewPage?id=' + recordId;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    gotoReviewAssignment : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        var url = '/apex/ReviewContractsNewDetailPage?id=' + recordId;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();	
	},
    gotoCloseContract : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        var url = '/apex/ContractCloseNewPage?id=' + recordId;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();	
	},
    openOutLook : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var action = component.get("c.Mailsendmethod");
        action.setParams({
            recordId : component.get("v.recordId")
        });
    
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                console.log(a.getReturnValue());
                //window.open("http://"+a.getReturnValue());
                document.getElementById("approve").href = a.getReturnValue();
                document.getElementById("approve").click();
                //window.location=a.getReturnValue();
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
    
        $A.enqueueAction(action);
    },
	openOutLookContractRequester : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var action = component.get("c.MailsendmethodContractRequester");
        action.setParams({
            recordId : component.get("v.recordId")
        });
    
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                console.log(a.getReturnValue());
                document.getElementById("approve").href = a.getReturnValue();
                document.getElementById("approve").click();
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
    
        $A.enqueueAction(action);
    },
	ResumeLegalReview : function(component, event, helper){
        var recordId = component.get("v.recordId");
        var action = component.get("c.methodResumeLegalReview");
        action.setParams({
            recordId : component.get("v.recordId")
        });
    
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                //document.getElementById("approve").href = a.getReturnValue();
                //document.getElementById("approve").click();
                $A.get('e.force:refreshView').fire();
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": recordId
                });
                navEvt.fire();
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
    
        $A.enqueueAction(action);
    }
})