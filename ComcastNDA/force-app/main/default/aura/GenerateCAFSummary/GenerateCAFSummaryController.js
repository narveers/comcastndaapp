({
	gotoCAFtoPDF : function(component, event, helper) {
  
        var recordId = component.get("v.recordId");
        var url = '/apex/ContracttoPDF?id=' + recordId;
        var urlEvent = $A.get("e.force:navigateToURL");
        window.open(url,'_blank');   
        
        var action = component.get('c.saveCAFSummary'); 
        action.setParams({
            "contractId" : recordId
        });
        action.setCallback(this, function(a){
            var state = a.getState(); 
            console.log(state);
            $A.get("e.force:closeQuickAction").fire();
        });
        $A.enqueueAction(action);
	}
})