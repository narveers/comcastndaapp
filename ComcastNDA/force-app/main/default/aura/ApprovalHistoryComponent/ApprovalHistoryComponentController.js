({
	LoadApprovalProcessAction : function(component, event, helper) {
		var action = component.get("c.getApprovalHistory");
        action.setCallback(this,function(response){
            component.set("v.ApprovalHistory", response.getReturnValue());
        });
        $A.enqueueAction(action);
	}
})