({
	doInit : function(component, event, helper) {
        //action to fetch current contract status
        var fetchContractStatus = component.get("c.getNDAStatus");
        fetchContractStatus.setParams({
            "ContractId" : component.get("v.recordId")
        });
        fetchContractStatus.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.NDAStatus", response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(fetchContractStatus);
        
        //action to fetch the contract manager public group
        var fetchCMBoolean = component.get("c.getIsADMGroupMember");
        fetchCMBoolean.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.IsADMGroupMember", response.getReturnValue());
                console.log('group mem?-'+response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    console.log(JSON.parse(JSON.stringify(event.getParams())));
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(fetchCMBoolean);
        
        //action to fetch the contract manager public group
        var fetchLegalRBoolean = component.get("c.getIsLegalRGroupMember");
        fetchLegalRBoolean.setCallback(this, function(response) {
        	var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.IsLegalReviewGroupMember", response.getReturnValue());
                console.log('group mem?-'+response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    console.log(JSON.parse(JSON.stringify(event.getParams())));
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(fetchLegalRBoolean);
    },
    gotoNDALegalReview : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        var url = '/apex/NDAAssignLegalReview?id=' + recordId;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    } ,
    gotoCancelNDALegalReview : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        var url = '/apex/DocuSignCancelDocument?id=' + recordId;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    gotoRegenrateNDALegalReview : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        var url = '/apex/DocuSignRegenrateDocument?id=' + recordId;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    gotoSendNDALegalReview : function(component, event, helper) {
		var recordId = component.get("v.recordId");
        var url = '/apex/DocuSignSendDocument?id=' + recordId;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    }
    
})