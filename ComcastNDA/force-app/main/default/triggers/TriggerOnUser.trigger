trigger TriggerOnUser on User (before insert,after Insert) {
    if(Trigger.isBefore && Trigger.isInsert){
        UserTriggerHandler.onBeforeInsert(Trigger.new);
    }else if(Trigger.isAfter && Trigger.isInsert){
        UserTriggerHandler.onAfterInsert(Trigger.new);
    }
}