/*
    Name             : ContractStatusTrigger
    Author           : Narveer S.
    Date             : 07/16/2018
    Description      : Trigger We are using to update the Status
*/
Trigger ContractStatusTrigger on Contract__c (after insert,after update) {

    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            ContractStatusTriggerHandler.StatusUpdate(trigger.new,trigger.old,trigger.newMap,trigger.oldMap);
        }
    }
}