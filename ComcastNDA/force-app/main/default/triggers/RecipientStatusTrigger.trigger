trigger RecipientStatusTrigger on dsfs__DocuSign_Recipient_Status__c (After Update) {
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            RecipientStatusTriggerHandler.onAfterUpdate(Trigger.new,Trigger.oldMap);
        }
            
    }
}