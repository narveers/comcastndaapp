trigger TriggerOnApprovalStepApproverInst on REVVY__MnStepApproverInst__c ( after insert, before delete,after delete, after update) {
    if(Trigger.isInsert && Trigger.isAfter){
        // Check the duplicates and remove them 
         ApprovalStepApproverInstTriggerHandler.RemoveDuplicates(Trigger.new);
         ApprovalStepApproverInstTriggerHandler.onInsert(trigger.newMap.keySet());
         ApprovalStepApproverInstTriggerHandler.manuallyShareContract(trigger.newMap.keySet());
       
    }
    if(Trigger.isDelete && Trigger.isBefore){
       ApprovalStepApproverInstTriggerHandler.deleteContractAccess(Trigger.oldMap.keySet());
   }
   
   if(Trigger.isUpdate && Trigger.isAfter){
       ApprovalStepApproverInstTriggerHandler.updateApproveronContract(Trigger.newMap.keySet());
   }
  
    
    if(Trigger.isDelete && Trigger.isAfter){
       
        Set<Id> Stepids = new  Set<Id>(); 
        For ( REVVY__MnStepApproverInst__c apr : Trigger.Old){
         Stepids.add(apr.REVVY__StepInst__c);
         }
        System.debug('---Vikram1---'+Stepids);
        ApprovalStepApproverInstTriggerHandler.onAfterDelete(Stepids);
        
    }
  
}