trigger TriggerOnContractIntakeForm on Contract_Intake_Form__c (after update) {
    List<Contract_Intake_Form__c> listToUpdate = new List<Contract_Intake_Form__c>();
    for(Contract_Intake_Form__c form : Trigger.new){
        Contract_Intake_Form__c newForm = new Contract_Intake_Form__c();
        if(String.valueOf(form.ownerId).startsWith('005') && !String.valueOf(Trigger.oldMap.get(form.id).ownerId).startsWith('005')){
            newForm.status__C = 'Assigned to Attorney';newForm.id=form.Id;listToUpdate.add(newForm);
        }
    }
    update listToUpdate;
}