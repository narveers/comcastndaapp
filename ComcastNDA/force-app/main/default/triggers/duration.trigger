trigger duration on Contract__c (before update) {
    
    for(Contract__c CR: Trigger.new){

        Contract__c oldCR = Trigger.oldMap.get(CR.Id);
            
        if(CR.Contract_Intake_Status__c  != oldCR.Contract_Intake_Status__c){
            
            switch on CR.Contract_Intake_Status__c{
                when 'Contract Manager'{
                    CR.CM_Start_Date__c= datetime.now();
                }    
                when 'Pending Assignment' {
                    CR.PA_Start_Date__c = datetime.now();
                }
                when 'Legal Review Complete' {
                    CR.LRC_Start_Date__c = datetime.now();
                }
             /*   when 'Pending Legal Assignment'{
                    CR.PLA_Start_Date__c = datetime.now(); 
                }
                when 'Pending Legal Reassignment'{
                    CR.PLR_Start_Date__c = datetime.now(); 
                } */
            }                
        } 
    }  
}