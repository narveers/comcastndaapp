/* 
    Name : NDATrigger
    Auther : Narveer S.
    Date : 03/04/2018
    Description : NDA Trigger for DocuSign Template
*/

trigger NDATrigger on NDA__c(after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    if(trigger.isInsert) {
        if(trigger.isAfter) {
            NDATriggerHandler.onAfterInsert(trigger.new);
        }
        if(trigger.isBefore) {
            NDATriggerHandler.onBeforeInsert(trigger.new);
        }
    }
    
    if(trigger.isAfter){
        if(trigger.isUpdate) {
            NDATriggerHandler.onAfterUpdate(Trigger.new,trigger.oldMap);
        }
    }
}