trigger MnContractBefore on REVVY__MnContract__c (before insert, before update) {
       if('Y'.equalsIgnoreCase(Label.CM_EnableTrigger)) {
           ContractUtility.sendEmail(Trigger.new);        
       }
   /*
   if(trigger.isAfter && trigger.isUpdate){
       system.debug('MnContractBefore - isAfter');
       for(REVVY__MnContract__c contract: trigger.new){
          String oldPhase = Trigger.oldMap.get(contract.Id).REVVY__Phase__c;
          String newPhase = contract.REVVY__Phase__c;
          // List<REVVY__MnLifeCycle__c> test = [SELECT ID,Name,(SELECT ID FROM REVVY__Contracts__r WHERE Id=:contract.Id LIMIT 1) FROM REVVY__MnLifeCycle__c WHERE REVVY__RecordType__c='Request_CAF' ORDER BY Createddate DESC Limit 1];
           //system.debug('lifecycle - '+test[0].Name);
           if(oldPhase=='Approval' && newPhase=='Signature'){
               ContracttoPDFController.saveToContract(contract.Id, contract.Name);
           }
       }
   } */
   
   map<string,Procurement_User__c> procurementUserMap = new map<string,Procurement_User__c>();
           procurementUserMap = Procurement_User__c.getAll();
   
           
           
   if(trigger.isbefore && trigger.isinsert)
   {
   
   
   
   for(REVVY__MnContract__c con: trigger.new)
       {
         if(!procurementUserMap.isEmpty() && procurementUserMap.containsKey(con.Requester_Formula__c))
         {
         con.Procurement__c = 'Yes';
         }
       
       }
}

}