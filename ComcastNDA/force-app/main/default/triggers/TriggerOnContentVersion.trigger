trigger TriggerOnContentVersion on ContentVersion (after insert) {
    if(Trigger.isAfter && Trigger.isInsert){
        ContentVersionTriggerHandler.onAfterInsert(Trigger.new,Trigger.newMap);
    }
}