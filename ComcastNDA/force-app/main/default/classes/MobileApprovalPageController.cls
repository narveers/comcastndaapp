public class MobileApprovalPageController {
    public String Comments{get;set;}
    public Id StepInstanceId;
    public String FromAddress;
    public String Source;
    public void Save(){
        StepInstanceId = ApexPages.currentPage().getParameters().get('Id');
        FromAddress = ApexPages.currentPage().getParameters().get('from');
        Source = ApexPages.currentPage().getParameters().get('source');
            if(!String.isBlank(StepInstanceId) && !String.isBlank(FromAddress) && !String.isBlank(Source)){
                sendEmail();
            }
    }
    public void sendEmail(){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //To address
        List<String> sendTo = new List<String>();
        EmailServicesAddress EmailService = [SELECT EmailDomainName,LocalPart FROM EmailServicesAddress WHERE DeveloperName ='AdvancedApprovalsEmail'];
        String ToAddress = EmailService.LocalPart+'@'+EmailService.EmailDomainName;
        //SendTo.add('sandeep@cmentor.com');
        mail.setToAddresses(sendTo);
        //From address
        mail.setReplyTo(FromAddress);
        //bcc address
        List<String> ccTo = new List<String>();
        ccTo.add('CMS_Notifications@comcast.com');
        ccTo.add('sandeep@cmentor.com');
        mail.setBccAddresses(ccTo);
        //subject
        mail.setSubject('Re:Contract Name');
        //Body
        String Body;
        if(Source == 'Approve'){
            Body = 'Yes';
        }
        else{
            Body = 'No';
        }
        Body += '\n'+Comments+'%0D ----------------------- %0DIf you would like to add any comments to this approval, please enter them on the line after '
            	+Body+
      			'.%0DPlease do not modify the reference id.\n<stepInstance>'+StepInstanceId+'</stepInstance>';
        mail.setPlainTextBody(EncodingUtil.urlDecode(Body, 'UTF-8'));
        mails.add(mail);
        Messaging.sendEmail(mails);
    }
}