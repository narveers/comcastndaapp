/*
Name             : DocuSignSignerPageCtrl
Author           : Cloud Mentor
Date             : 02/21/2019
Description      : Docu Signer Controller
*/
public class DocuSignSignerPageCtrl {
    
    public static String EnvId{get;set;}
    public static String SourceObjectId{get;set;}
    public static String EnvDocuSignId{get;set;}
    public static String TaggerUrl{get;set;}
    public Static String NewWindow{get;set;}
    public Static Boolean isLightningEnabled{get;set;}
    
    public DocuSignSignerPageCtrl() {
        TaggerUrl = 'https://ccms--full--dsfs.visualforce.com/apex/dsfs__docusign_tagenvelope';
        EnvId = 'a2e0x0000007zzS';
        SourceObjectId = 'a410x000001aPkvAAE';
        EnvDocuSignId = '6c60eb64-1bdf-422e-bd55-83fb36d50366';
        NewWindow = '1';
        isLightningEnabled = False;
        //EnvId = apexpages.currentpage().getparameters().get('EnvId');
        //SourceObjectId = apexpages.currentpage().getparameters().get('SourceObjectId');
        //EnvDocuSignId = apexpages.currentpage().getparameters().get('EnvDocuSignId');
    }
}