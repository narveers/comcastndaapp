public class ContractUtility {
    
    public static void sendEmail(List<REVVY__MnContract__c> contractList) {
    	
		// First, reserve email capacity for the current Apex transaction to ensure
		// that we won't exceed our daily email limits when sending email after
		// the current transaction is committed.
		Messaging.reserveSingleEmailCapacity(200);
		
		List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();


		Set<String> groupNameSet = new Set<String>();
		Set<String> emailTemplateNameSet = new Set<String>();
		for(REVVY__MnContract__c con:contractList) {		
			if(!String.isEmpty(con.Internal_Client_Group__c)
			&& con.Send_Email__c) {
				groupNameSet.add(con.Internal_Client_Group__c);
			}
			if(con.Send_Email__c
			&& !String.isEmpty(con.Template_Unique_Name__c)) {
				emailTemplateNameSet.add(con.Template_Unique_Name__c);
			}
		}
		
		Map<String,EmailTemplate> emailTemplateMap = new Map<String,EmailTemplate>();
		for(EmailTemplate et:[Select Name, Id, DeveloperName 
		                        From EmailTemplate 
		                       where DeveloperName in :emailTemplateNameSet]) {
			emailTemplateMap.put(et.DeveloperName, et);	                       	
		}
		
		
		
		Map<String,Group> groupMap = new Map<String,Group>();
		Set<Id> userIdSet = new Set<Id>();
		for(Group g:[Select Name, Id, Email
		                 , (Select UserOrGroupId From GroupMembers) 
		               From Group
		              Where Name in :groupNameSet
		            ]) {
			if(g.GroupMembers!=null && g.GroupMembers.size()>0) {
				for(GroupMember gm:g.GroupMembers) {
					userIdSet.add(gm.UserOrGroupId);
				}
			}		            	
			groupMap.put(g.Name,g);
		}
		
		Map<Id,User> userMap = new Map<Id,User>([Select Id, Username, Email, FirstName, LastName From User where Id in :userIdSet]);
		Map<String,Set<String>> groupEmailMap = new Map<String,Set<String>>();
		Map<String,Contact> userContactIdMap = new Map<String,Contact>();
		for(String s:groupMap.keySet()) {
			Group g = groupMap.get(s);
			Set<String> emailSet = new Set<String>();
			if(g.GroupMembers!=null && g.GroupMembers.size()>0) {
				for(GroupMember gm:g.GroupMembers) {
					if(userMap.containsKey(gm.UserOrGroupId)) {
						emailSet.add(userMap.get(gm.UserOrGroupId).Email);	
						User user = userMap.get(gm.UserOrGroupId);
						Contact ct = new Contact(email = user.email, firstName = user.firstName, lastName = user.lastName);
						userContactIdMap.put((s+'_'+user.email).toLowerCase(),ct);				
					}			
				}
			}		            				
			groupEmailMap.put(s,emailSet);
		}
		for(REVVY__MnContract__c con:contractList) {		
			if(con.Send_Email__c
			&& !String.isEmpty(con.Template_Unique_Name__c)) {
				
				//Email Contract Owner
				Contact ctOwner = new Contact(email = con.Contract_Owner_Email__c, firstName = con.Contract_Owner_FirstName__c, lastName = con.Contract_Owner_LastName__c); 
				userContactIdMap.put(ctOwner.email.toLowerCase(),ctOwner);
				
			}
		}
		insert userContactIdMap.values();
		
		String orgWideEmailAddressId = Label.OrgWideEmailAddressId ;
		
		for(REVVY__MnContract__c con:contractList) {		
		
            System.debug('>>> 111 con.Template_Unique_Name__c='+ con.Template_Unique_Name__c);
			if(con.Send_Email__c
			&& !String.isEmpty(con.Template_Unique_Name__c)) {

				Set<String> toAddresses = new Set<String>();
				if(!String.isEmpty(con.Internal_Client_Group__c) && !'null'.equalsIgnoreCase(con.Internal_Client_Group__c)) {
					toAddresses = groupEmailMap.get(con.Internal_Client_Group__c);
				}


				/*
				for(String toEmailAddr:toAddresses) {

					// Processes and actions involved in the Apex transaction occur next,
					// which conclude with sending a single email.
					
					// Now create a new single email message object
					// that will send out a single email to the addresses in the To, CC & BCC list.
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	
					// Assign the addresses for the To and CC lists to the mail object.
					mail.setToAddresses(new List<String>{toEmailAddr});
					//mail.setCcAddresses(ccAddresses);
					
					// Specify the address used when the recipients reply to the email. 
					//mail.setReplyTo('support@acme.com');
					// Specify the name used as the display name.
					//mail.setSenderDisplayName('Salesforce Support');
					mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
					mail.setWhatId(con.Id);
					System.debug('>>> con.Internal_Client_Group__c_toEmailAddr='+ con.Internal_Client_Group__c+'_'+toEmailAddr);
					Contact ct = userContactIdMap.get((con.Internal_Client_Group__c+'_'+toEmailAddr).toLowerCase());
					mail.setTargetObjectId(ct.Id);
                    System.debug('>>> 222 con.Template_Unique_Name__c='+ con.Template_Unique_Name__c);
					mail.setTemplateId(emailTemplateMap.get(con.Template_Unique_Name__c).Id);
					
					
					// Set to True if you want to BCC yourself on the email.
					mail.setBccSender(false);
					
					// Optionally append the salesforce.com email signature to the email.
					// The email address of the user executing the Apex Code will be used.
					mail.setUseSignature(false);
					mail.setSaveAsActivity(false);
					
					// Specify the text content of the email.
					//mail.setPlainTextBody('Your Case: ' + case.Id +' has been created.');
					//
					//mail.setHtmlBody('Your case:<b> ' + case.Id +' </b>has been created.<p>'+
					//     'To view your case <a href=https://***yourInstance***.salesforce.com/'+case.Id+'>click here.</a>');
					
					mailList.add(mail);
				}
				
				//Email Contract Owner
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setToAddresses(new List<String>{con.Contract_Owner_Email__c.toLowerCase()});
				//Additional Recipient.
				if(!String.isEmpty(con.Additional_Reminder_Recipient__c) && !'null'.equalsIgnoreCase(con.Additional_Reminder_Recipient__c)) {
					mail.setCcAddresses(new List<String>{con.Additional_Reminder_Recipient__c.toLowerCase()});
				}								
				mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
				mail.setWhatId(con.Id);
                System.debug('>>> 333 con.Contract_Owner_Email__c='+ con.Contract_Owner_Email__c.toLowerCase());
				Contact ct = userContactIdMap.get(con.Contract_Owner_Email__c.toLowerCase());
                System.debug('>>> 333 con.ct='+ ct);
				mail.setTargetObjectId(ct.Id);
                System.debug('>>> 333 con.Template_Unique_Name__c='+ con.Template_Unique_Name__c);
                System.debug('>>> 333 emailTemplateMap='+ emailTemplateMap);
				mail.setTemplateId(emailTemplateMap.get(con.Template_Unique_Name__c).Id);
				mail.setBccSender(false);
				mail.setUseSignature(false);
				mail.setSaveAsActivity(false);
				mailList.add(mail);
								
				
				
				*/
				
				
				List<String> toEmailAddrList = new List<String>();
				for(String toEmailAddr:toAddresses) {
					toEmailAddrList.add(toEmailAddr);
				}
				toEmailAddrList.add(con.Contract_Owner_Email__c.toLowerCase());

				
				//Email Contract Owner
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setToAddresses(toEmailAddrList);
				//Additional Recipient.
				if(!String.isEmpty(con.Additional_Reminder_Recipient__c) && !'null'.equalsIgnoreCase(con.Additional_Reminder_Recipient__c)) {
					mail.setCcAddresses(new List<String>{con.Additional_Reminder_Recipient__c.toLowerCase()});
				}								
                mail.setBccAddresses(new List<String>{'Michael_Iacovelli@comcast.com'});
				mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
				mail.setWhatId(con.Id);
                System.debug('>>> 333 con.Contract_Owner_Email__c='+ con.Contract_Owner_Email__c.toLowerCase());
				Contact ct = userContactIdMap.get(con.Contract_Owner_Email__c.toLowerCase());
                System.debug('>>> 333 con.ct='+ ct);
				mail.setTargetObjectId(ct.Id);
                System.debug('>>> 333 con.Template_Unique_Name__c='+ con.Template_Unique_Name__c);
                System.debug('>>> 333 emailTemplateMap='+ emailTemplateMap);
				mail.setTemplateId(emailTemplateMap.get(con.Template_Unique_Name__c).Id);
				mail.setBccSender(false);
				mail.setUseSignature(false);
				mail.setSaveAsActivity(false);
				mailList.add(mail);
				
				
				con.Send_Email__c=false;
				con.Template_Unique_Name__c=null;
				
			}
		}
		
		if(mailList.size()>0) {
			// Send the email you have created.
			System.debug('>>> mailingList='+mailList);
			Messaging.sendEmail(mailList);    	
		}
		delete userContactIdMap.values();
    }
    
    /*********************************************************************************************
    * @description Build an SOQL query based on custom fileds, custom objects, and where clause
    * @param String fields
    * @param String objectName
    * @param String criteria
    * @return String
    **********************************************************************************************/	
    public static String buildSQLStatement(String fields, String objectName, String criteria) {
        String sql = 'Select ' + fields + ' from ' + objectName + (criteria != null && !''.equals(criteria) ? criteria : '');
        return sql;
    }
    /*********************************************************************************************
    * @description Add fields to an SOQL query
    * @param Map<String, Schema.SObjectField> fieldsMap
    * @return String
    **********************************************************************************************/
    public static String getObjectFields(Map<String, Schema.SObjectField> fieldsMap) {
        String ssql = '';
        for(String fieldName:fieldsMap.keySet()) {
            //Schema.Describefieldresult dField = fieldsMap.get(fieldName).getDescribe();
            //if((dField.isNameField() && !dField.isAutoNumber()) || (dField.isCustom() && dField.isUpdateable())) {
               ssql += ',' + fieldName;
            //}
        }
        ssql = ssql.replaceFirst(',','');
        return ssql;
    }
    
}