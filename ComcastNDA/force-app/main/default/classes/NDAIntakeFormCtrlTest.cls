@isTest public class NDAIntakeFormCtrlTest{
    @isTest static void testMethod1(){
        NDA__c  nda = new NDA__c();
        nda.NDA_Type__c = 'One Way NBCU';
        nda.Date_Of_Prior_Discussion__c = system.today();
        nda.Counterparty_Country__c = 'United States';
        nda.Intent_to_pay_parties__c = 'No';
        nda.Discussions_with_this_firm__c = 'No';
        nda.Is_there_intent_to_disclose_PII__c = 'No'; 
        nda.Intent_to_disclose_information__c = 'No';
        nda.Have_discussions_already_taken_place__c = 'No';
        nda.Aware_of_an_existing_agreement__c = 'No';
        nda.Term_Duration_years__c = '2';
        nda.intent_for_NBCU_to_disclose_HW_SW_Tech__c = 'No';
       // insert nda;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(nda);
        NDAIntakeFormCtrl obj = new NDAIntakeFormCtrl(sc);
        List<SelectOption> NBCUSignerList = obj.NBCUSignerList;
        obj.currentStage = 'Review and Submit';
        obj.previousStage();
        obj.previousStage();
        obj.previousStage();
        obj.currentStage = 'Contact Details';
        obj.nextStage();
        obj.showPopUpFunc();
        obj.HidePopUpFunc();
        obj.reRenderPageFunc();
        obj.informationOutsideDisclose();
        obj.informationAlreadyTaken();
        obj.EffectiveDateOnPriorDiscussion();
        obj.UserValues();
        obj.SetUser();
        obj.ExportData();
        obj.objNDA = nda;
        obj.saveRecord();
    }
}