/*
    Name             : OutforSignatureCounterPartyCtrl
    Author           : Narveer S.
    Date             : 03/20/2019
    Description      : Controller class we are using for Out for Signature CounterParty
*/
public class OutforSignatureCounterPartyCtrl {
    public Id emailContractId{get;set;}
    
    public NDA__c getNDARec() {
        List<NDA__c> ListNDA = New List<NDA__c>();
        if(emailContractId!=null) { 
            ListNDA = [Select ID,Name,NDA_Type__c,Counterparty__c,Submitted_on_behalf_of_Name__c FROM NDA__c where id =: emailContractId LIMIT 1];
            if(ListNDA.size() > 0) {
                NDA__c objNDA = ListNDA[0];
                return objNDA;
            } else {
                return new NDA__c();
            }
        }
        return new NDA__c(); 
    }
    
}