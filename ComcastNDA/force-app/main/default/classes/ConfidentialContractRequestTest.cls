@isTest
public class ConfidentialContractRequestTest{
   
    @isTest static void testMethod1(){
    
        Account objAccount = new Account();
        objAccount.Name = 'test';
        objAccount.BillingStreet = 'PA';
        objAccount.BillingCity = 'Allentown';
        objAccount.BillingPostalCode = '123456';
        objAccount.BillingState = 'Dallas';
        objAccount.BillingCountry = 'US';
        objAccount.ShippingStreet = 'PA';
        objAccount.ShippingCity = 'Allentown';
        objAccount.ShippingState = 'Dallas';
        objAccount.ShippingPostalCode = '123456';
        objAccount.ShippingCountry = 'US';
            
        insert objAccount;
        
        Contact con = new contact(LastName = 'Test', AccountId = objAccount.id);
        insert con;
        
        User usr = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US'
           //  UserRoleId = r.Id
        );
        insert usr;
    
        Contract__c objCon = new Contract__c();
        objCon.Assigned_Attorney__c = usr.Id;
        objCon.Contract_Intake_Status__c = 'Pending Assignment';
        objCon.Confidential_Submitter__c = usr.Id;
        insert objCon;
      
        Opportunity objOpp = New Opportunity();
        objOpp.AccountId = objAccount.Id;
        objOpp.Name = 'Testing';
        objOpp.CloseDate = system.today();
        objOpp.StageName = 'Qualified';
        insert objOpp;
        
        Test.startTest();
        
        PageReference pg = Page.ConfidentialContractRequest;
        pg.getParameters().put('id',objCon.id);
        Test.setCurrentPageReference(pg);
        
        Attachment attach = TestData.createAttachment(objCon.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objCon);
        ConfidentialContractRequestController objCCReq = new ConfidentialContractRequestController(sc);
        List<SelectOption> AttorneyList = objCCReq.AttorneyList;
        objCCReq.redirectToDashboard();
        objCCReq.saveContractRecord();
        objCCReq.reRenderPageFunc();
        objCCReq.savecontent();
        ConfidentialContractRequestController.callSharing();
        objCCReq.deleteAttachment();
        objCCReq.attachId = attach.Id;
        objCCReq.attachFileFunction();
        objCCReq.nextStage();
        objCCReq.currentStage = 'Brief Summary';
        objCCReq.nextStage();
        objCCReq.previousStage();
        objCCReq.currentStage = 'Contract Request';
        objCCReq.saveContractRecord();
        objCCReq.currentStage = 'Review and Submit';
        objCCReq.saveContractRecord();
        objCCReq.savecontent();
        objCCReq.previousStage();
        objCCReq.previousStage();
        objCCReq.previousStage();
        objCCReq.currentStage = 'General Information';
        objCCReq.nextStage();
        objCCReq.currentStage='File Upload';
        objCCReq.nextStage();
        objCCReq.currentStage='Smart Approval';
        objCCReq.nextStage();
        
        Test.stopTest();
    }
}