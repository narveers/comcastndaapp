/*
    Name             : ContractStatusTriggerHandler
    Author           : Narveer S.
    Date             : 07/16/2018
    Description      : Trigger We are using to update the Status
*/
public class ContractStatusTriggerHandler {
    public static void StatusUpdate(List<Contract__c> newList, List<Contract__c> oldList, Map<Id, Contract__c> newMap, Map<Id, Contract__c> oldMap) {
        List<Status_Update__c> lstStatusUpdate = new List<Status_Update__c>();    
        for(Contract__c objContract: newList) {
            if(newMap.get(objContract.Id).Status_Update__c != Null && newMap.get(objContract.Id).Status_Update__c != oldMap.get(objContract.Id).Status_Update__c) {
                Status_Update__c objStatusUpdate = new Status_Update__c();
                objStatusUpdate.Contract__c = objContract.Id;
                objStatusUpdate.Status_Text_Update__c = objContract.Status_Update__c;
                if(oldMap.ContainsKey(objContract.Id)) {
                    objStatusUpdate.Previous_Status__c = oldMap.get(objContract.Id).Status_Update__c;
                }
                objStatusUpdate.Status_Updated_On__c = System.now();
                objStatusUpdate.Updated_By__c = UserInfo.getName();
                lstStatusUpdate.add(objStatusUpdate);           
            }
        }
        if(!lstStatusUpdate.IsEmpty()) {
            insert lstStatusUpdate;
        }
    }
}