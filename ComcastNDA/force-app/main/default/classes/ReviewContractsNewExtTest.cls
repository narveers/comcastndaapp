@isTest
public class ReviewContractsNewExtTest{

    @isTest static void testMethod1(){
    
        Test.startTest();
        
        //Creating Group
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        //Creating QUEUE
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Contract_Intake_Form__c');
            insert testQueue;
        }

        Account acc = new account(name = 'test');
        insert acc;
        
        Contact con = new contact(LastName = 'Test', AccountId = acc.id);
        insert con;
        
        Contract__c reqContract = new Contract__c();
        reqContract.Name = 'test';
        reqContract.Attorney__c = 'Alison Mansfield';
        reqContract.Requester_Name__c = 'test';
        reqContract.Business_Unit__c = 'Business Unit - HQ';
        reqContract.Status_Update__c = 'test';
        reqContract.Contract_Intake_Status__c = 'Pending Assignment';
        reqContract.Counterparty__c = 'test';
       
        insert reqContract;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(reqContract);
        ReviewContractsNewExt rce = new ReviewContractsNewExt(sc);
        rce.actualValue = '1';
        rce.rowNumber = 'test';
        rce.valueSelection();
        rce.save();
        
        Test.stopTest();
    
    }
}