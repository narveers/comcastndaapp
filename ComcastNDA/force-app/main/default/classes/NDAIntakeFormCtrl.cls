/* 
    Name : NDAIntakeFormCtrl
    Auther : Narveer S.
    Date : 09/05/2018
    Description : NDA Intake Form Controller For NDA Record Creation
*/
public class NDAIntakeFormCtrl {
    
    public NDA__c objNDA{get;set;}
    public String error{get;set;}
    Id contractId;
    public String currentStage{get;set;}
    public boolean subOther{get;set;}
    public boolean InDisclose{get;set;}
    public boolean discussionsAlready{get;set;}
    public boolean showPopUp{get;set;}
    public string ExpoType{get;set;}
    Public Map<String,String> MapNBCUNameEmail;
    public String NdaRecrdId{get;set;}
    public List<NBCU_Signer_List__c> UserList{get;set;}
    public String searchString{get;set;}
    
    public NDAIntakeFormCtrl(ApexPages.StandardController controller) {
        NdaRecrdId ='';
        MapNBCUNameEmail = new Map<String,String>();
        currentStage = 'Contact Details';
        contractId = controller.getId();
        objNDA = new NDA__c();
        subOther = false;
        InDisclose =false;
        discussionsAlready = false;
        showPopUp=false;
        if(contractId != null) {
            
        } else {
            objNDA = new NDA__c();    
            objNDA.Name = userInfo.getName();
            objNDA.Requester_Email__c = userInfo.getUserEmail();
            objNDA.NDA_Status__c = 'Admin Review';
            objNDA.Term_Duration_years__c = '2';
        }  
        
    }
    
    public void previousStage(){
        
        error = '';
        if(currentStage == 'Disclosure Details'){
            currentStage = 'Contact Details';
        }else if(currentStage == 'Review and Submit'){
            currentStage = 'Disclosure Details';
        }else if(currentStage == 'Contact Details'){
            currentStage = 'Requestor Information';
        }
    }
    public PageReference nextStage(){
        error = '';
        if(currentStage == 'Contact Details'){
            currentStage = 'Disclosure Details';
        }else if(currentStage == 'Disclosure Details'){
            currentStage = 'Review and Submit';
            
            String str = objNDA.Purpose__c.stripHtmlTags();
            objNDA.Purpose__c =str;
            
            if(objNDA.Date_Of_Prior_Discussion__c != null){
            objNDA.Termination_Date__c = Date.newInstance(objNDA.Effective_Date__c.year()+Integer.valueOf(objNDA.Term_Duration_years__c), objNDA.Effective_Date__c.month(), objNDA.Effective_Date__c.Day()-1);
        }
        }
        return null;
    }
    
    public List<SelectOption> NBCUSignerList {
        get
        {
            
            NBCUSignerList = new List<SelectOption>();
            NBCUSignerList.add(new SelectOption('','None'));
            for(NBCU_Signer_List__c objNBCUSign : [SELECT Name,NBCU_Signer_Email__c FROM NBCU_Signer_List__c Order By CreatedDate Limit 50]) {
                NBCUSignerList.add(new SelectOption(objNBCUSign.Name,objNBCUSign.Name));
                MapNBCUNameEmail.put(objNBCUSign.Name,objNBCUSign.NBCU_Signer_Email__c);
            }       
            return NBCUSignerList; 
        }
        set;
    }
    public void UserValues(){
        searchString = '%'+searchString+'%';
        UserList = [select id,name,NBCU_Signer_Email__c from NBCU_Signer_List__c  where name LIKE :searchString Order By CreatedDate limit 1000];
        
        for(NBCU_Signer_List__c objNBCUSign : UserList ) {
                MapNBCUNameEmail.put(objNBCUSign.Name,objNBCUSign.NBCU_Signer_Email__c);
        }
    }
    
    public void SetUser(){
        objNDA.NBCU_Signer_Name__c = searchString  ;
    }
    public PageReference saveRecord() {
    
        system.debug('================'+objNDA.Aware_of_an_existing_agreement__c);
        for(NBCU_Signer_List__c objNBCUSign : [SELECT Name,NBCU_Signer_Email__c FROM NBCU_Signer_List__c]) {
            MapNBCUNameEmail.put(objNBCUSign.Name,objNBCUSign.NBCU_Signer_Email__c);
        }       
        if(!MapNBCUNameEmail.IsEmpty() && MapNBCUNameEmail.ContainsKey(objNDA.NBCU_Signer_Name__c)) {
            objNDA.Comcast_Signer_Email__c = MapNBCUNameEmail.get(objNDA.NBCU_Signer_Name__c);
        }
        objNDA.DocuSign_Status__c = 'Out for Signature – NBCU';
        insert objNDA;
        return new PageReference('/apex/NDARequesterDashboard');
    }
    public void reRenderPageFunc(){
        
    }
    public void informationOutsideDisclose(){
        if(objNDA.Intent_to_disclose_information__c == 'yes'){
          InDisclose = true;
        }else{
            InDisclose = false;
            objNDA.intent_for_NBCU_to_disclose_HW_SW_Tech__c = '';
        }
    }
    public void informationAlreadyTaken(){
        if(objNDA.Have_discussions_already_taken_place__c == 'yes'){
          discussionsAlready = true;
        }else{
            discussionsAlready = false;
            objNDA.Date_Of_Prior_Discussion__c = null;
            objNDA.Effective_Date__c = null;
       }
    }
    public void EffectiveDateOnPriorDiscussion(){
        if(objNDA.Date_Of_Prior_Discussion__c != null)
            objNDA.Effective_Date__c = objNDA.Date_Of_Prior_Discussion__c;
    }
    public void showPopUpFunc(){
        showPopUp = true;
    }
    public void HidePopUpFunc(){
        showPopUp = false;
    }
    public PageReference ExportData(){
        if(objNDA.NDA_Type__c == 'One Way NBCU'){
            return Page.ExportOneWayPdf;
        }else if(objNDA.NDA_Type__c == 'One way Counterparty'){
            return Page.ExportOneWayCountPdf;
        }else if(objNDA.NDA_Type__c == 'Mutual'){
            return Page.ExportMutualPdf;
        }
        return null;
    }
}