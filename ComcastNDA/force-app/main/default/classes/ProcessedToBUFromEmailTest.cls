@isTest public class ProcessedToBUFromEmailTest {
    static testMethod void Method1() {
        
        Contract__c con = new Contract__c();
        con.Name = 'Test Contract Request';
        con.Contract_Intake_Status__c = 'Contract Manager';
        con.Requester_Name__c = 'username';
        con.Requester_Email__c = 'legal_help@comcast.com';
        insert con;
        
        // Create a new email, envelope object and Attachment
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        email.subject = 'Legal Review Complete ref: '+String.ValueOf(con.Id);
        email.fromName = 'test test';
        email.plainTextBody = 'Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
        
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.txt';
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
        Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        email.binaryAttachments = binaryAttachments ;
        envelope.fromAddress = 'test@gmail.com';
            
            
        // setup controller object
        ProcessedToBUFromEmail catcher = new ProcessedToBUFromEmail();
        Messaging.InboundEmailResult result = catcher.handleInboundEmail(email, envelope);
        System.assertEquals( result.success  ,true);  
    }
}