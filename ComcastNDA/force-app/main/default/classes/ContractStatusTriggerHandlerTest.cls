@isTest
public class ContractStatusTriggerHandlerTest {
    @isTest static void Method1() {
        List<Contract__c> lstContract = new List<Contract__c>();
        Map<Id, Contract__c> newMap = new Map<Id, Contract__c>();
        List<Contract__c> lstContract2 = new List<Contract__c>();
        Map<Id, Contract__c> newMap2 = new Map<Id, Contract__c>();
        
        Contract__c con = new Contract__c();
        con.Name = 'Test Contract Request';
        con.Contract_Intake_Status__c = 'Contract Manager';
        con.Requester_Name__c = 'username';
        con.Requester_Email__c = 'legal_help@comcast.com';
        con.Status_Update__c  = 'test';
        insert con;
        
        newMap.put(con.id,con);
        lstContract.add(con);
        con.Status_Update__c  = 'test1';
        update con;
        newMap2.put(con.id,con);
        lstContract2.add(con);
        ContractStatusTriggerHandler.StatusUpdate(lstContract2,lstContract,newMap2,newMap);
    
    }
}