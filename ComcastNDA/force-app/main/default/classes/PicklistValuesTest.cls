@isTest
Private Class PicklistValuesTest{
    @isTest Static void testmethod1(){
        Test.setMock(HttpCalloutMock.class, new PicklistValuesMockResponse());
        List<String> res = PicklistValues.getValues('CAF_Type__c');
        System.assertEquals('test', res[0]);
    }
}