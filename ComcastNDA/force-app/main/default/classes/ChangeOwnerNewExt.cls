public class ChangeOwnerNewExt {
    private ApexPages.StandardController controller;
    public List<Contract__c> listContracts{get;set;}
    public Id ownerId{get;set;}
    public String ownerName{get;set;}
    public String assignmentName{get;set;}
    public ChangeOwnerNewExt(ApexPages.StandardController controller) {
        this.controller = controller;
        listContracts = new List<Contract__c>();
        if(controller!=null && controller.getId()!=null){
            List<Contract__c> selectedContracts = [Select ID,Name,Attorney__c,Assigned_Attorney__c,AttorneyContact__c,Business_Unit__c,Status_Update__c,Contract_Intake_Status__c from Contract__c where id = : controller.getId()] ;
            for(Contract__c contract: selectedContracts) {
                listContracts.add(contract);
            }
        }
    }
    public List<SelectOption> getdynamiclist() {
        List<SelectOption> AttorneyList = new List<SelectOption>();
        AttorneyList.add(new SelectOption('','None'));
        for( User usr: [SELECT Name, Id FROM USER WHERE Id IN (SELECT UserOrGroupId FROM GroupMember where group.DeveloperName='CI_Attorney')])
        {
            AttorneyList.add(new SelectOption(usr.Id,usr.Name));
        }       
        return AttorneyList; 
    }
    public PageReference save() {
        for(Contract__c contract: listContracts){
            if(contract.Contract_Intake_Status__c != 'Pending Assignment') {
                contract.Contract_Intake_Status__c = 'Pending Assignment';
            } else {
                contract.Contract_Intake_Status__c = 'Pending Attorney Reassignment';
            }
        }
        update listContracts;
        if(controller!=null){
            return new PageReference('/'+listContracts[0].id);
        }else {
            return null;
        }
    }
    @RemoteAction
    public static List<sObject> getLookupRecord(String likeString , String objectName) {
        if(String.IsNotBlank(likeString) && String.IsNotBlank(objectName)){
            likeString = '%'+likeString+'%';
            String query = '';
            String typeString = 'Queue';
            if(objectName == 'Group'){
                query = 'SELECT Id, Name FROM '+objectName+' WHERE Name LIKE : likeString AND Type =: typeString LIMIT 50';
            }else{
                query = 'SELECT Id, Name FROM '+objectName+' WHERE Name LIKE : likeString LIMIT 50';
            }
            
            System.debug(query);
            return database.query(query);
        }
        return null;
    }
}