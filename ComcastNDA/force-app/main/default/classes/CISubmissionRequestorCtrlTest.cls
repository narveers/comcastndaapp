@isTest
public class CISubmissionRequestorCtrlTest {
    @isTest static void Method1() {
        Contract__c testcontract = TestData.createContractRequest();
        
        ContentVersion cvlist = new Contentversion(); 
        cvlist.Title = 'CZDSTOU'; 
        cvlist.PathOnClient = 'test'; 
        cvlist.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
        List<ContentVersion> cvl = new List<ContentVersion>();  
        cvl.add(cvlist); 
        insert cvl;
       
        
        CISubmissionRequestorCtrl objClAss = new CISubmissionRequestorCtrl();
        objClAss.emailContractId = testcontract.id;
        objClAss.getContract(); 
        objClAss.getContent();    
    }
}