global class CMTermSheetBatch implements Database.Batchable<sObject>,Database.Stateful {
    
    global static String PHASE = 'Active';
    global static String SUBPHASE = 'In Effect';
    global String testId {get;set;}
    global Map<String, RecordTypeTermSheet__mdt> recordTypeMdt {get;set;}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        recordTypeMdt = new Map<String,RecordTypeTermSheet__mdt>();
        //Getting RecordTypeId
        Set<String> recordTypeNameMDTSet = new Set<String>();       
        for(RecordTypeTermSheet__mdt rtts:[Select Id, MasterLabel, DeveloperName, Display_Approval__c From RecordTypeTermSheet__mdt limit 100]) {
            recordTypeNameMDTSet.add(rtts.DeveloperName);
            recordTypeNameMDTSet.add(rtts.MasterLabel);
            recordTypeMdt.put(rtts.DeveloperName, rtts);
            recordTypeMdt.put(rtts.MasterLabel, rtts);
        }
        
        Set<Id> recordTypeIdSet = new Set<Id>();
        for(RecordType rt:[Select Id, Name From RecordType where SobjectType = 'REVVY__MnContract__c' 
                              and (DeveloperName in :recordTypeNameMDTSet OR Name in :recordTypeNameMDTSet)]) {
            recordTypeIdSet.add(rt.Id); 
        }
        
        DateTime dtPast90 = System.now().addDays(-90);
        String query = '';
        
        query = 'Select Id, Name, TermSheet_Created__c, TermSheet_CreatedDate__c, OwnerId, RecordTypeId, RecordType.Name '
                     + ' From REVVY__MnContract__c Where lastModifiedDate >= :dtPast90 And TermSheet_Created__c = false '
                     + '  And REVVY__Phase__c = :PHASE And REVVY__SubPhase__c = :SUBPHASE ';

        if(recordTypeIdSet.size()>0) {
            query += ' And RecordTypeId in :recordTypeIdSet';
        }                     
        
        //Override for test
        if(!String.isEmpty(testId)) {
            //'a0s0x000000IGkH\'
            query = 'Select Id, Name, TermSheet_Created__c, TermSheet_CreatedDate__c, OwnerId, RecordTypeId, RecordType.Name From REVVY__MnContract__c Where Id = :testId';
        }
        System.debug('>>> Termsheet Query='+ query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
        System.debug('>>> scope size='+scope.size());
        for(SObject so:scope) {
            REVVY__MnContract__c mnContract = (REVVY__MnContract__c) so;
            PageReference pg = Page.CMTermSheet;
            pg.getParameters().put('sid',mnContract.Id);
            
            RecordTypeTermSheet__mdt rtts = recordTypeMdt.get(mnContract.RecordType.Name);
            if(rtts!=null && rtts.Display_Approval__c !=null && rtts.Display_Approval__c) {
                pg.getParameters().put('dap','Y');
            } 
                        
            String title = mnContract.Name + ' Term Sheet.pdf';
           
            
            ContentVersion cv = new ContentVersion();
            cv.REVVY__APPLYTOORIGINAL__c = true;
            cv.IsMajorVersion = true;
            cv.Title = title;
            cv.Description = title;
            cv.PathOnClient = '.pdf';
            cv.FIRSTPUBLISHLOCATIONID = mnContract.Id;
            cv.Origin = 'H';
            cv.ContentLocation = 'S';
            cv.Revvy__Status__c = 'DRAFT';
            cv.OwnerId = mnContract.OwnerId;
            if(Test.isRunningTest()) {
                cv.VersionData = Blob.valueOf('THIS IS TEST');
            } else {
                cv.VersionData = pg.getContentAsPDF();  
            }       
            insert cv;
            
            System.debug('>>> cv='+cv.id);
            
            
            mnContract.TermSheet_Created__c = true;
            mnContract.TermSheet_CreatedDate__c = System.now();
            if(!Test.IsRunningTest()) {
                update mnContract;
            }
            
            System.debug('>>> update mnContract='+mnContract.id);
            
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}