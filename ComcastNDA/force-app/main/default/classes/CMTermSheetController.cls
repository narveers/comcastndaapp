public class CMTermSheetController {
    
    public String sid {get;set;}
    public String approvalObjId {get;set;}
    
    public List<REVVY__MnApproverActionLog__c> actionLogList {get;set;}
    
    public String recordTypeName {get;set;}
    public String recordTypeDevName {get;set;}
    public String logoURL {get;set;}
    public REVVY__MnContract__c mncontract {get;set;}
    public Map<String,String> labelMap {get;set;}
    public Boolean bdisplayApprovalComponent {get;set;}

    public CMTermSheetController() {
        sid = ApexPages.currentPage().getParameters().get('sid');
    }
    
    public void init() {
    	actionLogList = new List<REVVY__MnApproverActionLog__c>();
    	bdisplayApprovalComponent=false;
        String sdap = ApexPages.currentPage().getParameters().get('dap');
        if('Y'.equalsIgnoreCase(sdap)) {
        	bdisplayApprovalComponent = true;
        }
        
        labelMap = new Map<String,String>();
        Map<String, Schema.SObjectField> contractFieldsMap = Schema.SobjectType.REVVY__MnContract__c.fields.getMap();
        String scontractFields = ContractUtility.getObjectFields(contractFieldsMap) + ', RecordType.DeveloperName, RecordType.Name ';
        String scontractCriteria = ' where Id = :sid';
        String contractQuery = ContractUtility.buildSQLStatement(scontractFields, 'REVVY__MnContract__c', scontractCriteria);

        List<REVVY__MnContract__c> mnContractList = Database.query(contractQuery);
        if(mnContractList!=null && mnContractList.size() >0) {
            mncontract = mnContractList[0];
            logoURL = mncontract.Logo_URL__c;
            recordTypeName = mnContract.RecordType.Name;
            recordTypeDevName = mnContract.RecordType.DeveloperName;
        }

        //Label
		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Schema.SObjectType contractSchema = schemaMap.get('REVVY__MnContract__c');
		Map<String, Schema.SObjectField> contractFieldMap = contractSchema.getDescribe().fields.getMap();

        for (String fieldName: contractFieldMap.keySet()) {
	        System.debug('##Field API Name= '+fieldName);// list of all field API name
	        System.debug('##Field Label Name= ' + contractFieldMap.get(fieldName).getDescribe().getLabel());//It provides to get the object fields label.
            labelMap.put(fieldName,contractFieldMap.get(fieldName).getDescribe().getLabel());
        }     
        
        //Get the approval instance
        if(bdisplayApprovalComponent) {
        	for(REVVY__MnApprovalInst__c mnai:[Select Id, REVVY__ApprovalObjId__c 
        										 From REVVY__MnApprovalInst__c 
        										where REVVY__Contract__c = :sid order by lastModifiedDate desc]) {
        		approvalObjId = mnai.REVVY__ApprovalObjId__c;
        		break;
        	}   
        	if(String.isEmpty(approvalObjId)) {
        		bdisplayApprovalComponent = false;
        	} else {
        		
        		for(REVVY__MnApproverActionLog__c log:[SELECT Id, Name, REVVY__ApprovalInst__c, REVVY__ActionDate__c
        		                                            , REVVY__Action__c, REVVY__Actor__c, REVVY__ActualApproverFormula__c, REVVY__AssignedToFormula__c
        		                                            , REVVY__AssignedTo__c, REVVY__Comments__c, REVVY__StepApproverInst__c, REVVY__OriginalApproverFormula__c
        		                                            , REVVY__StepInst__c, REVVY__SubmitterFormula__c, ApproverUniquenessCheck__c 
        		                                         FROM REVVY__MnApproverActionLog__c 
        		                                        where Revvy__ApprovalInst__c = :approvalObjId order by Revvy__ActionDate__c desc]) {
        			actionLogList.add(log);                                        	
        		}
        	}    	
        }
        
    }
    
    public List<Schema.FieldSetMember> getTermSheetFields() {
        
		//fieldset        
        System.debug('>>> fieldSetName MAP= '+Schema.SObjectType.REVVY__MnContract__c.fieldSets.getMap());
        Schema.FieldSet fs1 = Schema.SObjectType.REVVY__MnContract__c.fieldSets.getMap().get(recordTypeName);
        if(fs1 == null) {
            fs1 = Schema.SObjectType.REVVY__MnContract__c.fieldSets.getMap().get(recordTypeDevName);
            if(fs1 == null) {
        		fs1 = Schema.SObjectType.REVVY__MnContract__c.fieldSets.getMap().get('TermSheet');  
            }
        }
        
        List<Schema.FieldSetMember> fsMemberList = fs1.getFields();
        return fsMemberList;
    }
    
}