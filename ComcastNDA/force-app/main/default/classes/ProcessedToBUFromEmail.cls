global class ProcessedToBUFromEmail implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Try{
            String subject = email.subject;
            String subToCompare = 'Legal Review Complete';
            String BMessage='';
            String TMessage='';
            if(email.subject.containsIgnoreCase(subToCompare)) {
                String ContractId = subject.split('ref: ')[1];
                Contract__c objCon;
                if(String.isNotBlank(ContractId)){
                    system.debug('contract id - '+ContractId);
                    objCon = [Select Id, Contract_Intake_Status__c From Contract__c Where Id = :ContractId];
                    //System.debug(objCon);
                    objCon.Contract_Intake_Status__c = 'Legal Review Complete';
                    Update objCon;
                }
                //Save email to files
                ContentVersion emailText = new ContentVersion();
                String EmailData = 'From:'+ email.fromAddress +'\n'+'To:'+email.toAddresses+'\n';
                if(email.ccAddresses != NULL){
                    EmailData += 'cc:'+email.ccAddresses+'\n';
                }
                EmailData +='Subject:'+email.subject+'\n';
                if(String.isNotBlank(email.plainTextBody)){
                    EmailData +='Body:'+email.plainTextBody;
                }       
                emailText.VersionData = Blob.ValueOf(EmailData);
                emailText.FirstPublishLocationId = objCon.id;
                emailText.Title = email.subject;
                emailText.PathOnClient = email.subject+'.txt';
                insert emailText;
                
                // Save attachments, if any
                //System.debug(email.textAttachments);
                List<ContentVersion> FilesList = New List<ContentVersion>();
                if(email.textAttachments != Null) {
                    for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments){
                        ContentVersion cv = new ContentVersion();  cv.versionData = Blob.ValueOf(tAttachment.body); cv.FirstPublishLocationId = objCon.id;
                        cv.title = tAttachment.fileName; TMessage += tAttachment.fileName; cv.pathOnClient = tAttachment.fileName;  FilesList.add(cv);
                    }
                }
                //System.debug(email.binaryAttachments);
                //Save any Binary Attachment
                if(email.binaryAttachments != Null) {
                    for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
                        ContentVersion cv = new ContentVersion(); cv.versionData = bAttachment.body;  cv.FirstPublishLocationId = objCon.id;  cv.title = bAttachment.fileName;
                        BMessage += bAttachment.fileName;  cv.pathOnClient = bAttachment.fileName;  FilesList.add(cv);
                    }
                }
                //Save email as attachment
                if(!FilesList.isEmpty()) {
                    insert FilesList;
                }
            }
            //result.message = 'email.binaryAttachments - '+email.binaryAttachments+' email.textAttachments - '+email.textAttachments;
            result.success = true;
        }
        Catch(Exception ex){
            result.message = ex.getStackTraceString();
        }
        return result;
    }
}