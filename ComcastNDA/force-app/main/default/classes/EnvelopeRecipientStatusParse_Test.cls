@IsTest
public class EnvelopeRecipientStatusParse_Test {
    
    static testMethod void testParse() {
        String json = '{'+
        '    \"signers\": ['+
        '        {'+
        '            \"creationReason\": \"sender\",'+
        '            \"isBulkRecipient\": \"false\",'+
        '            \"name\": \"Test Contact\",'+
        '            \"email\": \"narveer@cmentor.com\",'+
        '            \"recipientId\": \"1\",'+
        '            \"recipientIdGuid\": \"33891bc3-5027-4ce0-9493-d0f70f3772a8\",'+
        '            \"requireIdLookup\": \"false\",'+
        '            \"userId\": \"ca85f150-dd94-4ea1-8e2a-e0b53e478197\",'+
        '            \"routingOrder\": \"1\",'+
        '            \"status\": \"completed\",'+
        '            \"signedDateTime\": \"2019-05-27T14:46:57.7500000Z\",'+
        '            \"deliveredDateTime\": \"2019-05-27T14:46:51.0930000Z\"'+
        '        },'+
        '        {'+
        '            \"creationReason\": \"sender\",'+
        '            \"isBulkRecipient\": \"false\",'+
        '            \"recipientSuppliesTabs\": \"true\",'+
        '            \"name\": \"Narveer Singh\",'+
        '            \"email\": \"narveer@cmentor.com\",'+
        '            \"recipientId\": \"2\",'+
        '            \"recipientIdGuid\": \"95a0407e-3792-42fd-8ce1-d2405bd23b19\",'+
        '            \"requireIdLookup\": \"false\",'+
        '            \"userId\": \"ca85f150-dd94-4ea1-8e2a-e0b53e478197\",'+
        '            \"routingOrder\": \"2\",'+
        '            \"status\": \"sent\"'+
        '        }'+
        '    ],'+
        '    \"agents\": [],'+
        '    \"editors\": [],'+
        '    \"intermediaries\": [],'+
        '    \"carbonCopies\": [],'+
        '    \"certifiedDeliveries\": [],'+
        '    \"inPersonSigners\": [],'+
        '    \"seals\": [],'+
        '    \"witnesses\": [],'+
        '    \"recipientCount\": \"2\",'+
        '    \"currentRoutingOrder\": \"2\"'+
        '}';
        EnvelopeRecipientStatusParse obj = EnvelopeRecipientStatusParse.parse(json);
        System.assert(obj != null);
    }
}