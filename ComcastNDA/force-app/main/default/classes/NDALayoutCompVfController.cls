public class NDALayoutCompVfController {
    public NDALayoutCompVfController(){
        
    }
	public PageReference nextPage(  ){
        String RecId =  ApexPages.currentPage().getParameters().get('id');
        System.debug('RecId'+ RecId);
        String docuSignUrl;
        NDA__c objNDA = [Select Id,Comcast_Signer_Email__c,NBCU_Signer_Name__c FROM NDA__c WHERE ID =: RecId LIMIT 1];
        System.debug(objNDA);
        List<ContentVersion> lstContentVersion = [Select id,title,FirstPublishLocationId,ContentDocumentId from ContentVersion where FirstPublishLocationId =: RecId AND DocuSign_Required__c = True LIMIT 1];
        
        docuSignUrl = '/apex/dsfs__DocuSign_CreateEnvelope?SourceID='+RecId+'&CRL=';
        
        docuSignUrl += 'Email~'+objNDA.Comcast_Signer_Email__c+';Role~Signer1;FirstName~'+objNDA.NBCU_Signer_Name__c+';RoutingOrder~1;';
        docuSignUrl += 'SignNow~1,LoadDefaultContacts~1&CCRM=Decision Maker~Signer 1&CCTM=Decision Maker~Signer&DST=&LA=0&LF=0&OCO=Send&LF=0&FILES='+lstContentVersion[0].ContentDocumentId;
        System.debug('docuSignUrl'+ docuSignUrl);
        return new PageReference(docuSignUrl);
    }
}