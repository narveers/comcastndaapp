public class AccountHierarchyChartController{

// Variable to create a string and pass it to google charts.
public String ContactData{get;set;}
    
    public AccountHierarchyChartController(ApexPages.StandardController controller){
        ContactData = '';
        for(User con :[Select Name,Title,Manager.Name from User where ManagerId!=null ]){
            ContactData = ContactData + '[{v:\'';
            ContactData += con.Name;
            ContactData += '\',';
            ContactData += ' f:\''+ con.Name +'<div style="color:red; font-style:italic">'+ '' +'</div>\'}';
            ContactData += ',\'';
            ContactData += con.Manager.Name != null ? con.Manager.Name : '';       
            ContactData += '\',\'' + '' + '\'],';
        }
    }
}