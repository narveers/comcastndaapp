global class GetDocuSignStatusBatch implements Database.Batchable<dsfs__DocuSign_Status__c>, Database.AllowsCallouts, Database.stateFul{
    global Docu_Sign__mdt docSign;
    
    global GetDocuSignStatusBatch(){
        docSign = new Docu_Sign__mdt();
        
        docSign = [Select MasterLabel,DocuSign_URL__c,DocuSign_Account_ID__c,DocuSign_IntegratorKey__c,DocuSign_UserName__c,
                       DocuSign_Password__c,DocuSign_Base_URL__c from Docu_Sign__mdt WHERE MasterLabel = 'DocuSign Details'];
    }
    
    // Start Method
    global iterable<dsfs__DocuSign_Status__c> start(Database.BatchableContext BC){
        return [SELECT id,createdDate,dsfs__DocuSign_Envelope_ID__c,dsfs__Envelope_Status__c,NDA__c FROM dsfs__DocuSign_Status__c WHERE 
                       dsfs__Envelope_Status__c != 'Completed' AND dsfs__Envelope_Status__c != 'Voided' AND dsfs__Envelope_Status__c != 'Canceled'];
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<dsfs__DocuSign_Status__c> docuSignStatusList){
        list<dsfs__DocuSign_Status__c> docuSignUpdateList = new list<dsfs__DocuSign_Status__c>();
        map<string,List<EnvelopeRecipientStatusParse.signers>> enevelopeIdWithSignersListMap = new map<string,List<EnvelopeRecipientStatusParse.signers>>();
        set<string> envelopeIdSet = new set<string>();
        
        try{
            for(dsfs__DocuSign_Status__c docuSignStatus : docuSignStatusList){
                envelopeIdSet.add(docuSignStatus.dsfs__DocuSign_Envelope_ID__c);
            }
            
            for(string envelopeId : envelopeIdSet){
                Http http = new Http();
            
                string endPoint = docSign.DocuSign_Base_URL__c+'v2/accounts/'+docSign.DocuSign_Account_ID__c+'/envelopes/'+envelopeId+'/recipients';
                
                HttpRequest request = new HttpRequest();
                request.setEndpoint(endPoint);
                request.setHeader('X-DocuSign-Authentication','{"Username":"'+docSign.DocuSign_UserName__c+'","Password":"'+docSign.DocuSign_Password__c+'","IntegratorKey": "'+docSign.DocuSign_IntegratorKey__c+'"}');
                request.setHeader('Content-Type','application/json');
                request.setMethod('GET');
                
                HttpResponse response = http.send(request);
                
                if(response.getStatusCode() == 200){
                    EnvelopeRecipientStatusParse ersp = EnvelopeRecipientStatusParse.parse(response.getBody());
                    
                    enevelopeIdWithSignersListMap.put(envelopeId.toUpperCase(),ersp.signers);
                    
                    system.debug('enevelopeIdWithSignersListMap**'+enevelopeIdWithSignersListMap);
                }
            }
            
            map<string,EnvelopeRecipientStatusParse.signers> signerIdWithSignersDataMap = new map<string,EnvelopeRecipientStatusParse.signers>();
            map<string,boolean> enevelopeIdWithSignerStatusMap = new map<string,boolean>();
            
            for(string envelopeId : enevelopeIdWithSignersListMap.keySet()){
                for(EnvelopeRecipientStatusParse.signers signer: enevelopeIdWithSignersListMap.get(envelopeId.toUpperCase())){
                    signerIdWithSignersDataMap.put(signer.recipientIdGuid.toUpperCase(),signer);
                    
                    system.debug('signer==='+signer);
                    system.debug('enevelopeIdWithSignerStatusMap'+enevelopeIdWithSignerStatusMap);
                    
                    if(!enevelopeIdWithSignerStatusMap.isEmpty() && enevelopeIdWithSignerStatusMap.containsKey(envelopeId.toUpperCase()) && enevelopeIdWithSignerStatusMap.get(envelopeId.toUpperCase()) != False && signer.status != 'Completed'){
                       system.debug('inner***'+enevelopeIdWithSignerStatusMap.get(envelopeId.toUpperCase()));
                    
                        enevelopeIdWithSignerStatusMap.put(envelopeId.toUpperCase(),False);
                    }else{
                        if(signer.status == 'Completed')
                            enevelopeIdWithSignerStatusMap.put(envelopeId.toUpperCase(),True);
                        if(signer.status != 'Completed')
                            enevelopeIdWithSignerStatusMap.put(envelopeId.toUpperCase(),False);
                    }
                        
                }
            }
            
            system.debug('enevelopeIdWithSignerStatusMap***'+enevelopeIdWithSignerStatusMap);
            
            map<string,string> envelopeIdWithNDAIdMap = new map<string,string>();
            for(dsfs__DocuSign_Status__c docuSignStatus : docuSignStatusList){
                if(!enevelopeIdWithSignerStatusMap.isEmpty() && enevelopeIdWithSignerStatusMap.containsKey(docuSignStatus.dsfs__DocuSign_Envelope_ID__c.toUpperCase())
                    && enevelopeIdWithSignerStatusMap.get(docuSignStatus.dsfs__DocuSign_Envelope_ID__c.toUpperCase()) != False){
                    docuSignStatus.dsfs__Envelope_Status__c = 'Completed';
                    
                    envelopeIdWithNDAIdMap.put(docuSignStatus.dsfs__DocuSign_Envelope_ID__c,docuSignStatus.NDA__c);
                    docuSignUpdateList.add(docuSignStatus);
                }
            } 
            
            list<dsfs__DocuSign_Recipient_Status__c> recipientStatusList = new list<dsfs__DocuSign_Recipient_Status__c>();
            for(dsfs__DocuSign_Recipient_Status__c recpStatus : [SELECT id,dsfs__DocuSign_Recipient_Id__c,dsfs__Recipient_Status__c,dsfs__Parent_Status_Record__r.NDA__c,dsfs__DocuSign_Routing_Order__c FROM dsfs__DocuSign_Recipient_Status__c
                                                                        WHERE dsfs__DocuSign_Recipient_Id__c IN: signerIdWithSignersDataMap.keySet()]){
                EnvelopeRecipientStatusParse.signers signer = signerIdWithSignersDataMap.get(recpStatus.dsfs__DocuSign_Recipient_Id__c.toUpperCase());
                
                if(recpStatus.dsfs__Recipient_Status__c != signer.status){
                    recpStatus.dsfs__Recipient_Status__c = signer.status;
                    
                    recipientStatusList.add(recpStatus);
                }
            }
            
            if(!envelopeIdWithNDAIdMap.isEmpty()) {
                map<string,blob> enevelopeIdWithDocumentMap = new map<string,blob>();
                    map<id,NDA__c> NDAMap = new map<id,NDA__c>([SELECT Id,NDA_Type__c,Agreement_Number__c FROM NDA__c WHERE Id IN: envelopeIdWithNDAIdMap.values()]);
                    List<ContentVersion> lstContent = new List<ContentVersion>();
                    
                    for(string envelopeId : envelopeIdWithNDAIdMap.keySet()){
                        Http http = new Http();
                
                        string endPoint = docSign.DocuSign_Base_URL__c+'v2/accounts/'+docSign.DocuSign_Account_ID__c+'/envelopes/'+envelopeId+'/documents/1';
                        
                        HttpRequest request = new HttpRequest();
                        request.setEndpoint(endPoint);
                        request.setHeader('X-DocuSign-Authentication','{"Username":"'+docSign.DocuSign_UserName__c+'","Password":"'+docSign.DocuSign_Password__c+'","IntegratorKey": "'+docSign.DocuSign_IntegratorKey__c+'"}');
                        request.setHeader('Content-Type','application/json');
                        request.setMethod('GET');
                        
                        HttpResponse response = http.send(request);
                        
                        if(response.getStatusCode() == 200){
                            enevelopeIdWithDocumentMap.put(envelopeId,response.getBodyAsBlob());
                            
                            NDA__c objNDARec = new NDA__c();
                            objNDARec = NDAMap.get(envelopeIdWithNDAIdMap.get(envelopeId));
                            
                            ContentVersion cv = new ContentVersion();
                            cv.versionData = response.getBodyAsBlob();
                            cv.title = objNDARec.Agreement_Number__c +' '+objNDARec.NDA_Type__c +' Signed';
                            cv.FirstPublishLocationId = objNDARec.id;
                            cv.pathOnClient = objNDARec.Agreement_Number__c +' '+objNDARec.NDA_Type__c+'.pdf';
                            cv.DocuSign_Required__c = True;
                           
                            lstContent.add(cv);
                        }
                    }
                    if(!lstContent.isEmpty())
                        insert lstContent;
            }
            if(!recipientStatusList.isEmpty())
                update recipientStatusList;
                
            if(!docuSignUpdateList.isEmpty()){
                update docuSignUpdateList;
            }
        }Catch(Exception e){
            system.debug('Error=='+e.getMessage()+'===Line no=='+e.getlinenumber());
        }
    }
    
    global void finish(Database.BatchableContext BC){
        // Logic to be Executed at finish
        string T = system.now().format('kk:mm:ss');
        List<string> timeList = t.split(':');
        string hours = timeList[0];
        string minutes = timeList[1];
        string seconds = timeList[2];
        
        list<string> dateVal = string.valueOf(date.Today()).split('-');
        string year = dateVal[0];
        string month = dateVal[1];
        string day = dateVal[2];
        
        string scheduleTime = seconds + ' ' + string.valueOf(integer.valueOf(minutes) + 1) + ' ' + hours + ' ' +  day + ' ' +
                              ' ' + month + ' ? ' + year;
        
        string scheduleName = 'Update DocuSign Status'+ ' '+ system.now().format('kk:mm:ss') + ' ' +string.valueOf(date.Today());
        system.schedule(scheduleName,scheduleTime,new GetDocuSignStatusBatchSchedule());
    }
}