@isTest
private class UserHandlerTest {
    static testMethod void testCreateUser() {
        Zip_Code__c zip = new Zip_Code__c();
        zip.Timezone__c = '-5';
        zip.City__c = 'Big Stone Gap';
        zip.DST__c = '1';
        zip.Latitude__c = '36.851953';
        zip.Longitude__c = '-82.77056';
        zip.Name = '24219';
        insert zip;
        String pid =[Select Id from Profile Where name ='Standard User'].Id ;
        Map<string,String> tempUser=  new Map<String, String>
        {
            'COMCAST_LNAME'=>'asdjhgasjhd',
                'COMCAST_FNAME' => 'blahblah',
                'MAIL'=>'askdasgdhgas@sdjkfd.com',
                'ZIPCODE' => '24219'
                };
                
        UserHandler UHObj = new UserHandler();
        User u = new User();
        u = UHObj.createUser(null, null, null, 'federationIdentifier', tempUser, null);
    }
    @isTest
    Private static void updateUserTest(){
        final Id samlSsoProviderId = '0LE000000000000';
        final Id communityId = '0DB000000000000';
        final Id portalId = '0DB000000000000';
        final String federationIdentifier = 'testfederationIdentifier';
        final String assertion = 'testassertion';
        final Map<String, String> attributes = new Map<String, String>();
        User user = new User();
        user.Email = 'test@comcast.com';
        user.FederationIdentifier = 'testFedId';
        user.Username = 'test@comcast.com.test';
        user.Role_Name__c = Label.JITRole;
        user.Profile_Name__c = Label.JITProfile;
        user.localesidkey='en_US';
        user.emailencodingkey='UTF-8';
        user.languagelocalekey='en_US';
        user.TimeZoneSidKey = 'America/New_York';
        user.LastName = 'LName';
        user.FirstName= 'FName';
        user.Alias = 'FNLNa';
        insert user;
        test.startTest();
        UserHandler UHObj = new UserHandler();
        UHObj.createAlias(user.FirstName,user.LastName);
        UHObj.updateUser(user.Id, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        test.stopTest();
    }
}