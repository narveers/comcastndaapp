@isTest
public class TriggerOnApprovalStepApproverInstTest {
     @testSetup static void setup() {
     
       REVVY__MnContract__c testcontract = TestData.CreateCAFRequest();
        REVVY__MnApprovalDef__c mad = new REVVY__MnApprovalDef__c(REVVY__MainObject__c = 'REVVY__MnContract__c',REVVY__Order__c = 100);
        insert mad;
        
        REVVY__MnApprovalInst__c mai = new REVVY__MnApprovalInst__c(REVVY__ApprovalDef__c = mad.id, REVVY__ApprovalObjId__c = 'Text',REVVY__Contract__c= testcontract.Id);
        insert mai;
        
        REVVY__MnApprovalStepDef__c asd = new REVVY__MnApprovalStepDef__c(REVVY__ApprovalDef__c = mad.id, REVVY__StepSequence__c = 100);
        insert asd;
        
        REVVY__MnApprovalStepInst__c asi = new REVVY__MnApprovalStepInst__c(REVVY__ApprovalInst__c = mai.id, REVVY__ApprovalStepDef__c = asd.id, REVVY__Step_Sequence__c = 100,
                                                                           REVVY__StepStatus__c='Pending Approval',REVVY__IsParentStep__c = false);
        insert asi;
        
        
        
     }
    
     static testMethod void testMnStepApproverInstInsert() {
     Test.startTest();
     
      List<REVVY__MnApprovalStepInst__c > asi = [ Select id from REVVY__MnApprovalStepInst__c ];
       
       REVVY__MnStepApproverInst__c SIA = new REVVY__MnStepApproverInst__c(REVVY__StepInst__c=asi[0].Id,REVVY__Status__c='Pending Approval');
        insert SIA;
     Test.stopTest();  
     
     }
     
     static testMethod void testMnStepApproverInstDelete() {
     
     Test.startTest();
      List<REVVY__MnApprovalStepInst__c > asi = [ Select id from REVVY__MnApprovalStepInst__c ];
       
       REVVY__MnStepApproverInst__c SIA = new REVVY__MnStepApproverInst__c(REVVY__StepInst__c=asi[0].Id,REVVY__Status__c='Pending Approval');
        insert SIA;
        
        Delete SIA;
      Test.stopTest(); 
     
     }
     
     static testMethod void testMnStepApproverInstUpdate() {
     
      Test.startTest();
      List<REVVY__MnApprovalStepInst__c > asi = [ Select id from REVVY__MnApprovalStepInst__c ];
       
       REVVY__MnStepApproverInst__c SIA = new REVVY__MnStepApproverInst__c(REVVY__StepInst__c=asi[0].Id,REVVY__Status__c='Pending Approval');
       insert SIA;
       SIA.REVVY__Status__c = 'Approved';
       Update SIA;
      Test.stopTest();  
        
       
     
     }
     
    }