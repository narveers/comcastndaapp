@isTest
public class BUHandoffRequestorCtrlTest {
    @isTest static void Method1() {
        Contract__c testcontract = TestData.createContractRequest();
        
        ContentVersion cvlist = new Contentversion(); 
        cvlist.Title = 'CZDSTOU'; 
        cvlist.PathOnClient = 'test'; 
        cvlist.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
        List<ContentVersion> cvl = new List<ContentVersion>(); 
        cvl.add(cvlist); 
        insert cvl; 
        BUHandoffRequestorCtrl objBU = new BUHandoffRequestorCtrl ();
        objBU.emailContractId = testcontract.id;
        objBU.getContract(); 
        objBU.getContent();    
    }
}