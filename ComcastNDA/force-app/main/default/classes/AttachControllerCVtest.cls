@isTest
public class AttachControllerCVtest {
           
        @isTest static void testMethod1(){
        Test.StartTest();
        
        Account acc = new account(name = 'See Counterparty (Legal Entity)');
        insert acc;
        
        Contact con = new contact(LastName = 'TestLname', AccountId = acc.id);
        insert con;
        
        Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
        
        REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
        lifeCycle.Name = 'Test Lifecycle';
        lifeCycle.REVVY__ApplyTo__c = 'Original';
        lifeCycle.REVVY__Object__c = 'MnContract__c';
        lifeCycle.REVVY__RecordType__c = 'Request_CAF';
        lifeCycle.REVVY__Status__c = 'Draft';
        insert lifeCycle;
        
        REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
        stage.name = 'Test';
        stage.REVVY__LifeCycle__c = lifeCycle.Id;
        stage.REVVY__Phase__c = 'Draft';
        stage.REVVY__SubPhase__c = 'Draft';
        stage.REVVY__Editable__c = true;
        insert stage;
        
        lifeCycle.REVVY__InitialStage__c = stage.Id;
        lifeCycle.REVVY__Status__c = 'Active';
        update lifeCycle;
        
        
        REVVY__MnContract__c objMnContract = new REVVY__MnContract__c(Name='Test contract viv',REVVY__Account__c = acc.id, recordTypeId=recTypeId,Brief_Summary__c = 'test summary by viv',
                                                           REVVY__Contact__c = con.id,Contract_Value__c=20000,REVVY__LifecycleConfig__c = lifeCycle.Id,Business_Unit__c='test');
        insert objMnContract;
        
        // generateNewContentVersionVersion(objMnContract.Id);
        //ContentVersion cont = new ContentVersion();
          //  cont.ContentDocumentId = objMnContract.Id;
         //   cont.Title = 'Title for this contentVersion';
         //   cont.PathOnClient = 'file_' + Datetime.now().getTime() + '.txt';
          //  cont.VersionData = Blob.valueOf('My Content in file_' + Datetime.now().getTime() + '.txt');
          //  cont.Origin = 'C';
          //  cont.ContentLocation = 'S';
           // insert cont;

        
        AttachControllerCV  ahc = new AttachControllerCV();
        ahc.Attachments();
        ahc.getTotal_size();
        ahc.getPageNumber();
        ahc.getTotalPages();
        ahc.getDisableNext();
        ahc.getDisablePrevious();
        ahc.Beginning();
        Integer counter = 0;
        Integer total_size = 555;
        Integer list_size = 25;
        ahc.Next();
        ahc.Previous();       
        ahc.end();
        
        Test.stopTest();
        
      }
          
}