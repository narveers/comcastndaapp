global class CMnLifeCycleUploadFeedAttachment extends REVVY.CMnLifeCycleActionScript {

    global override Map<String, String> doAction(Map<String,String> values){
		Id contractId = values.get('objectId');

		Map<String,String> pageRefMap = new Map<String,String>();
		pageRefMap.put('PageReference', '/apex/c__PMnUploadFeedDocument?Id='+contractId);
		return pageRefMap;
    }

}