@isTest
public class NDATriggerHandlerTest {
    
    @isTest
    private static void testMethod1(){
        
        Test.startTest();
        
        NDA__c nda = new NDA__c();
        nda.NDA_Type__c = 'One Way NBCU';
        nda.Date_Of_Prior_Discussion__c = system.today();
        nda.Counterparty_Country__c = 'United States';
        nda.Intent_to_pay_parties__c = 'No';
        nda.Discussions_with_this_firm__c = 'No';
        nda.Is_there_intent_to_disclose_PII__c = 'No'; 
        nda.Intent_to_disclose_information__c = 'No';
        nda.Have_discussions_already_taken_place__c = 'No';
        nda.Aware_of_an_existing_agreement__c = 'No';
        nda.Term_Duration_years__c = '2';
        nda.intent_for_NBCU_to_disclose_HW_SW_Tech__c = 'No';
        insert nda;
        
        nda.NDA_Status__c = 'Signed/Executed';
        update nda;
        
        system.assertEquals('One Way NBCU',nda.NDA_Type__c);
        
        Test.stopTest();
    }
    
    
    
}