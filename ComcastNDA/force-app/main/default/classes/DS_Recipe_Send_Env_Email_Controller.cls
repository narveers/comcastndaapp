/* 
    Name : DS_Recipe_Send_Env_Email_Controller
    Auther : Narveer S.
    Date : 02/07/2019
    Description : Docu Sign Automation class
*/
Public class DS_Recipe_Send_Env_Email_Controller {
    
    // SETTINGS
    Private static string integration_key = '52c45a24-0855-4301-bb23-ef8571855f58';
    Private static string account_id = '8928d498-e771-4630-b34b-06693437049d'; 
     
    Public Static string signer_email;
    Public Static string signer_name;
    Public Static String counterpartyName;
    Public Static string email_message;
    Public Static string output;
    public Static Id NDARecordId;
    Public Static string envelope_id;
    Public Static string error_code {get;set;} // Null means no error
    Public Static string error_message {get;set;}
    public static string oid;
    Public static String NdaStatus;

    Private static string ds_server = 'https://demo.docusign.net/api/3.0/dsapi.asmx';
    
    Private static string trace_value = 'SFDC_004_SOAP_email_send'; // Used for tracing API calls
    Private static string trace_key = 'X-ray';
    Private Static DocuSignTK.APIServiceSoap api_sender = new DocuSignTK.APIServiceSoap();
    
    Public PageReference send() {
        NDARecordId = ApexPages.currentPage().getParameters().get('id');
        System.debug(NDARecordId);
        if(NDARecordId != Null) {
        } 
        return null;
    }
    
    public Static  PageReference doCancel() { 
        Id NDARecordId1 = ApexPages.currentPage().getParameters().get('id');
        PageReference demoPage = new pagereference('/' + NDARecordId1);
        demoPage.setRedirect(true);
        return demoPage;
    }
    
    Public static void send(Id NDARecordId) {
        System.debug('In DocuSign');
        oid = NDARecordId;
        ApexPages.Message myMsg;
        if( oid != null && String.isNotBlank( oid )) {
            List<Contact> oppList = new List<Contact>();
        }
    }
    
    Private static void configure_sender() {
    
       // api_sender.endpoint_x = ds_server;
        String username = 'narveer@cmentor.com';
        String password = 'Apple@1234';
       // api_sender.inputHttpHeaders_x = new Map<String, String>();
        String auth =   '<DocuSignCredentials><Username>'+ username +'</Username>' 
                      + '<Password>'+ password +'</Password>' 
                      + '<IntegratorKey>' + integration_key + '</IntegratorKey></DocuSignCredentials>';

     //   api_sender.inputHttpHeaders_x.put('X-DocuSign-Authentication', auth);
    
    }
    Private static void do_send() {
    
        // Check input
        if (String.isBlank(signer_email) || String.isBlank(signer_name) || !signer_email.contains('@')) {
            error_message = 'Please Fill Contact-Role email to send email';
            error_code = 'INPUT_PROBLEM';
            return;
        }
        DocuSignTK.Recipient recipient;
        DocuSignTK.Document document;
        blob pdf;
        
        if(pdf != Null) {
            document = new DocuSignTK.Document();
            document.ID = 1;
            if(NdaStatus != Null && NdaStatus.equals('Mutual Basic')) {
                document.Name = 'Mutual Contract';
            } else if(NdaStatus != Null && NdaStatus.equals('One Way Basic')) {
                document.Name = 'One Way Contract';
            } else if(NdaStatus != Null && NdaStatus.equals('One Way International')) {
                document.Name = 'One Way CounterParty';
            }
            
            document.FileExtension = 'pdf';
            document.pdfBytes = EncodingUtil.base64Encode(pdf);
    
            recipient = new DocuSignTK.Recipient();
            recipient.Email = signer_email;
            recipient.UserName = signer_name;
            
            recipient.ID = 1;
            recipient.Type_x = 'Signer';
            recipient.RoutingOrder = 1;
        } else{
            error_message = 'Please select a Valid Doc Attachment!';
            error_code = 'INPUT_PROBLEM';
            return;
        }
        
        // The signer tab...
        DocuSignTK.Tab signHereTab = new DocuSignTK.Tab();
        signHereTab.Type_x = 'SignHere';
        signHereTab.AnchorTabItem = new DocuSignTK.AnchorTab();
        signHereTab.AnchorTabItem.AnchorTabString = 'By:N';
        signHereTab.AnchorTabItem.XOffset = 40;
        signHereTab.RecipientID = 1;
        signHereTab.DocumentID = 1;
        signHereTab.Name = 'Please sign here';
        signHereTab.ScaleValue = 1;
        signHereTab.TabLabel = '{Authorized Signer’s Name}';
        
       // DocuSignTK.Tab tab2 = new DocuSignTK.Tab();
       // tab2.Type_x = 'DateSigned';
       // tab2.RecipientID = 1;
       // tab2.DocumentID = 1;
       // tab2.AnchorTabItem = new DocuSignTK.AnchorTab();
       // tab2.AnchorTabItem.AnchorTabString = 'Dated:';
       // tab2.AnchorTabItem.XOffset = 60;
        //tab2.ScaleValue = 1;
        //tab2.TabLabel = '<<Date>>';
        
        // Create an envelope and fill it in
        DocuSignTK.Envelope envelope = new DocuSignTK.Envelope();
        if(NdaStatus != Null && NdaStatus.equals('Mutual Basic')) {
            envelope.Subject = 'Please sign Document: Mutual Contract';
        } else if(NdaStatus != Null && NdaStatus.equals('One Way Basic')) {
            envelope.Subject = 'Please sign Document: One Way Contract';
        } else if(NdaStatus != Null && NdaStatus.equals('One Way International')) {
            envelope.Subject = 'Please sign Document: One Way CounterParty';
        }
        envelope.AccountId  = account_id; 
        envelope.Tabs = new DocuSignTK.ArrayOfTab();
        envelope.Tabs.Tab = new DocuSignTK.Tab[2];  
        //envelope.Tabs.Tab[0] = signHereTab;        
        //envelope.Tabs.Tab[1] = tab2; 
        
        
        envelope.CustomFields = new DocuSignTK.ArrayOfCustomField();
        envelope.CustomFields.CustomField = new DocuSignTK.CustomField[1];
        DocuSignTK.CustomField env_customfield = new DocuSignTK.CustomField();
        
        env_customfield.Name = 'DSFSSourceObjectId';
        env_customfield.Show = 'false';
        env_customfield.Required = 'false';
        env_customfield.Value = oid;
        env_customfield.CustomFieldType = 'Text';
        env_customfield.ListItems = '';       
        envelope.CustomFields.CustomField[0] = env_customfield;
        
        System.debug(envelope);
        
        envelope.Tabs.Tab.add(signHereTab);
      //  envelope.Tabs.Tab.add(tab2);
        
        envelope.Recipients = new DocuSignTK.ArrayOfRecipient();
        envelope.Recipients.Recipient = new DocuSignTK.Recipient[1];
        envelope.Recipients.Recipient.add(recipient);
        
        envelope.Documents = new DocuSignTK.ArrayOfDocument();
        envelope.Documents.Document = new DocuSignTK.Document[1];
        envelope.Documents.Document.add(document);
        
        if (String.isNotBlank(email_message)) {
            envelope.EmailBlurb = email_message;
        }
        
        System.debug(envelope);
        
        // Make the call
        try {
            if(!Test.isRunningTest()){
                DocuSignTK.EnvelopeStatus result = api_sender.CreateAndSendEnvelope(envelope);
                envelope_id = result.EnvelopeID;
                System.debug('@@@@@@envelope_id = '+  envelope_id);
            }
        } catch ( CalloutException e) {
            System.debug('Exception - ' + e );
            error_code = 'Problem: ' + e;
            error_message = error_code;
        }    
    
    }
    Private static Boolean no_error() {
        return (String.isEmpty(error_code));
    }
}