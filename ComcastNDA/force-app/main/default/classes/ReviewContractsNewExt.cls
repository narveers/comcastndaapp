public class ReviewContractsNewExt {
    private ApexPages.StandardController controller;
    public String actualValue{get;set;}
    public String rowNumber{get;set;}
    public List<Contract__c> listContracts{get;set;}
    public ReviewContractsNewExt(ApexPages.StandardController controller) {
        this.controller = controller;
        listContracts = new List<Contract__c>();
        if(controller!=null && controller.getId()!=null){
            List<Contract__c> selectedContracts = [Select ID,Name,Attorney__c,Requester_Name__c,Business_Unit__c,Status_Update__c,Contract_Intake_Status__c,Counterparty__c from Contract__c where id = : controller.getId()] ;
            for(Contract__c contract: selectedContracts){
                if(contract.Contract_Intake_Status__c =='Pending Assignment' || contract.Contract_Intake_Status__c =='Pending Attorney Reassignment'){
                   listContracts.add(contract);
                }
            }
        }
    }
   
    public void valueSelection(){
        System.debug(ApexPages.currentPage().getParameters().get('valindex'));
       // listContracts[Integer.valueOf(ApexPages.currentPage().getParameters().get('valindex'))].Review_Status__c = ApexPages.currentPage().getParameters().get('value');
    }
    public PageReference save(){
        for(Contract__c contract: listContracts){
                contract.Contract_Intake_Status__c ='Legal Review';
        }
        update listContracts;
        if(controller!=null){
            return new PageReference('/'+listContracts[0].id);
        }else {
            return null;
        }
    }
}