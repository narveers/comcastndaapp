public class CAFrecUpd {

    public static void updateCAFrecords(){
    
        //REVVY__MnContract__c[] ConUpdate = new List<REVVY__MnContract__c>();  // to store the contracts that are changed for bulk update
    
		List<REVVY__MnContract__c> Contracts = [SELECT id, Current_Step_ID__c  FROM REVVY__MnContract__c  where Current_Step_ID__c = null LIMIT 10]; // get all the existing contracts
        system.debug(Contracts.size());
            
        for (REVVY__MnContract__c ContractRec : Contracts) {
               // get the current step instatnce id for the contract (calling the Model-N API)
               List<REVVY__MnApprovalStepInst__c> approverList = REVVY.CMnAdvApprovalRemoter.getApprovalStepsByObjId(ContractRec.Id);
               if(approverList != null){
	               ContractRec.Current_Step_ID__c = approverList[0].id;    //store the step id in the contract (Fieldname: Current_Step_ID__c)
                   // ConUpdate.add(ContractRec);
                   system.debug(ContractRec.id);
               }                
        }    
		update Contracts;        
		//update ConUpdate;       
    }   
}