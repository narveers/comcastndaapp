public class AddUserOnPublicGroup{
    
    @future
    public static void AddToGroups(Set<Id> userIds){
        
        //Get the groups that the user should be added to
        Group corpVP = [SELECT id FROM Group WHERE developername = 'CAF_Corp_VP_or_Above' limit 1];
        Group corpSVP = [SELECT id FROM Group WHERE developername = 'CAF_Corp_SVP_or_Above' limit 1];
        Group cabVP = [SELECT id FROM Group WHERE developername = 'CAFCAB_SVPs_Above_Approvers' limit 1];
        Group cabSVP = [SELECT id FROM Group WHERE developername = 'CAFCAB_VP_Above_Approvers' limit 1];
        List<PermissionSetAssignment> Permissions = new List<PermissionSetAssignment>();
        List< PermissionSet > PermissionSetList = [SELECT Id FROM PermissionSet WHERE Name IN ('Approver','CMSalesReps','CMStandardObj','Editors','Submitter')];  
        List<User> users=[Select Id,Name,Job_Level__c,CompanyName from user Where Id IN :userIds];
        system.debug('users-->' + users);
        List<GroupMember>listGroupMember =new List<GroupMember>();  
        
               
        for( User u : users){
            
            if( u.Job_Level__c == 'Vice President 1' || u.Job_Level__c == 'Vice President 2' ){
                GroupMember GM = new GroupMember();
                if( u.CompanyName == 'Cable' ){
                    GM.GroupId = cabVP.id;
                }
                if( u.CompanyName == 'Corporate' ){
                    GM.GroupId = corpVP.id;
                }
                
                GM.UserOrGroupId = U.Id;
                listGroupMember.add(GM);  
            }
            if( u.Job_Level__c == 'Vice President 3' || u.Job_Level__c == 'Vice President 4' ){
                GroupMember GM = new GroupMember();
                if( u.CompanyName == 'Cable' ){
                    GM.GroupId = cabSVP.id;
                }
                if( u.CompanyName == 'Corporate' ){
                    GM.GroupId = corpSVP.id;
                }
                GM.UserOrGroupId = U.Id;
                listGroupMember.add(GM);  
            }
            
            for(PermissionSet PS: PermissionSetList){
                PermissionSetAssignment PSA = new PermissionSetAssignment();
                PSA.AssigneeId=u.Id;
                PSA.PermissionSetId = PS.Id;   
                Permissions.add(PSA);
            }
            
        }
        
        insert listGroupMember;
        
        Database.SaveResult[] srList1 = Database.insert(Permissions, false);
        

       /*  for(Integer j=0; j<srList1.size();j++){ 
            if(!srList1[j].isSuccess()){
                String str1 = '';
                for(Database.Error err : srList1[j].getErrors()) { 
                    str1 += err.getMessage();
                }
            }else {
               
            }
        }*/
    }
    @future
    public static void sendEmailToUser( String Emailbody ){
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        String email = 'Narendra@cmentor.com';
        message.toAddresses = new String[] {email};
        message.subject = 'Subject Test User';
        
       	message.plainTextBody = Emailbody ;
        emails.add(message);
        Messaging.sendEmail(emails);

        
    }
}