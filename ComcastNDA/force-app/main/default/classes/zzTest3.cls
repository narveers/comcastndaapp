public class zzTest3 {

    public static void vivTest() {
        //Account acc = new account(name = 'See Counterparty (Legal Entity)');
        //insert acc;
        Account acc = [SELECT Id, Name from account where name='See Counterparty (Legal Entity)' LIMIT 1];
        system.debug('account id --' + acc.Id);
           
        Contact con = new contact(LastName = 'TestLname', AccountId = acc.id);
        insert con;
        //Contact con = [SELECT Id, LastName from Contact where LastName = 'TestLname' LIMIT 1];
        system.debug('contact id --' + con.Id + con.lastname);
    
        Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
        system.debug('REVVY  record type id --' + recTypeId);
    
        REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
        lifeCycle.Name = 'Test Lifecycle';
        lifeCycle.REVVY__ApplyTo__c = 'Original';
        lifeCycle.REVVY__Object__c = 'MnContract__c';
        lifeCycle.REVVY__RecordType__c = 'Request_CAF';
        lifeCycle.REVVY__Status__c = 'Draft';
        insert lifeCycle;
        //REVVY__MnLifeCycle__c lifeCycle = [SELECT Id, Name from REVVY__MnLifeCycle__c where name='Test Lifecycle' LIMIT 1];
        system.debug('REVVY  lifecycle name --' + lifecycle.name);
    
        REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
        stage.name = 'Test';
        stage.REVVY__LifeCycle__c = lifeCycle.Id;
        stage.REVVY__Phase__c = 'Draft';
        stage.REVVY__SubPhase__c = 'Draft';
        stage.REVVY__Editable__c = true;
        insert stage;
        //REVVY__MnLifeCycleStage__c stage = [SELECT Id, Name from REVVY__MnLifeCycleStage__c Where Name ='Test' LIMIT 1];
    system.debug('REVVY  stage id --' + stage.Id);
    
        //lifeCycle.REVVY__InitialStage__c = stage.Id;
        //lifeCycle.REVVY__Status__c = 'Active';
        //update lifeCycle;
//system.debug('REVVY   lifecycle updated --' + lifecycle.REVVY__status__c);        
        
        REVVY__MnContract__c objMnContract = new REVVY__MnContract__c(Name='Test contract viv',REVVY__Account__c = acc.id, recordTypeId=recTypeId,Brief_Summary__c = 'test summary by viv',
                                                           REVVY__Contact__c = con.id,Contract_Value__c=20000,REVVY__LifecycleConfig__c = lifeCycle.Id,Business_Unit__c='test');
        insert objMnContract;
        system.debug('REVVY   Contract id --' + objMnContract.Id);
    system.debug('REVVY   Contract name --' + objMnContract.Name);
    
        // generateNewContentVersionVersion(objMnContract.Id);
        ContentVersion cont = new ContentVersion();
            cont.ContentDocumentId = objMnContract.Id;
            cont.Title = 'Title for this contentVersion';
            cont.PathOnClient = 'file_' + Datetime.now().getTime() + '.txt';
            cont.VersionData = Blob.valueOf('My Content in file_' + Datetime.now().getTime() + '.txt');
            cont.Origin = 'C';
            cont.ContentLocation = 'S';
            insert cont;
    system.debug('ContentVersion id --' + cont.Id);
    }
}