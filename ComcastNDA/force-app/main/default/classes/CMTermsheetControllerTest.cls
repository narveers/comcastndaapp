@isTest(SeeAllData=true)
private class CMTermsheetControllerTest {
    
    static testMethod void myUnitTest() {
        Account acc = new Account(Name='TestData'+System.now());
        insert acc;
        
        RecordType rt = [Select Id, Name, DeveloperName from RecordType where SObjectType = 'REVVY__MnContract__c' and DeveloperName='Fandango'];

        REVVY__MnLifeCycle__c lc = [Select Id From REVVY__MnLifeCycle__c where REVVY__RecordType__c = 'Fandango' and REVVY__Status__c='Active'];
        System.debug('>>> lc='+lc);
        System.debug('>>> rt='+rt);
        
        
REVVY__MnContract__c con = new REVVY__MnContract__c();
        con.Name='Test';
        con.Contract_Value__c = 100;
        con.REVVY__Account__c = acc.Id;
        con.Internal_Client_Group__c = 'Model N';
        con.Template_Unique_Name__c='EMnContractExpireNotif';
        con.Expiration_Date__c = System.today().addDays(30);
        con.RecordTypeId = rt.Id;
        con.REVVY__LifecycleConfig__c = lc.Id;
        con.REVVY__Phase__c = 'Active';
        con.REVVY__SubPhase__c = 'In Effect';
        insert con;
                
        Test.startTest();
        PageReference pg = Page.CMTermSheet;
        pg.getParameters().put('sid',con.Id);
        Test.setCurrentPage(pg);
        CMTermSheetController controller = new CMTermSheetController();
        List<Schema.FieldSetMember> schemaFieldSet = controller.getTermSheetFields();
        controller.init();
        
        System.assert(controller.mncontract !=null);
        
        Test.stopTest();
    }

}