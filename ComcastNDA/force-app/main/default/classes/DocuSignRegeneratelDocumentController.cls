/* 
    Name : DocuSignRegeneratelDocumentController
    Auther : Narveer S.
    Date : 04/05/2019
    Description : DocuSignCancelDocumentController for Regenrate Document Via DocuSign
*/
Public class DocuSignRegeneratelDocumentController {
    
    public Static Id NDARecordId;
    public static string oid;
    Public static String NdaStatus;
    
    Public PageReference send() {
        NDARecordId = ApexPages.currentPage().getParameters().get('id');
        if(NDARecordId != Null) {
             NDA__c objNDA = [Select Id,Counterparty_Country__c,Discussions_with_this_firm__c,Is_there_intent_to_disclose_PII__c,NDA_Type__c,
                            Intent_to_pay_parties__c,Intent_to_disclose_information__c,intent_for_NBCU_to_disclose_HW_SW_Tech__c,Counterparty_Contact_Name__c,
                            Have_discussions_already_taken_place__c,Aware_of_an_existing_agreement__c,Term_Duration_years__c FROM NDA__c WHERE ID =: NDARecordId];
            
            /*if(!(objNDA.Counterparty_Country__c.equals('United States') &&
                    objNDA.Discussions_with_this_firm__c.equals('No') && objNDA.Is_there_intent_to_disclose_PII__c.equals('No') &&
                    objNDA.Intent_to_pay_parties__c.equals('No') && objNDA.Intent_to_disclose_information__c.equals('No') &&
                    objNDA.intent_for_NBCU_to_disclose_HW_SW_Tech__c.equals('No') && objNDA.Have_discussions_already_taken_place__c.equals('No') &&
                    objNDA.Aware_of_an_existing_agreement__c.equals('No') && objNDA.Term_Duration_years__c.equals('2'))) {*/
                    Blob content;
                    Blob pdf;
                    if(objNDA.NDA_Type__c.equals('One Way NBCU')) {
                        PageReference pr = page.OneWayPdf;
                        pr.getParameters().put('id',NDARecordId);
                        if (Test.IsRunningTest()){
                            content=Blob.valueOf('UNIT.TEST');
                        } else {
                            pdf = pr.getContentAsPDF();
                        }
                    } else if(objNDA.NDA_Type__c.equals('One way Counterparty')) {
                        PageReference pr = page.OneWayCountPdf;
                        pr.getParameters().put('id',objNDA.id);
                        if (Test.IsRunningTest()){
                            content=Blob.valueOf('UNIT.TEST');
                        } else {
                            pdf = pr.getContentAsPDF();
                        }    
                    } else if(objNDA.NDA_Type__c.equals('Mutual')) {
                        PageReference pr = page.MutualPdf;
                        pr.getParameters().put('id',objNDA.id);
                        if (Test.IsRunningTest()){
                            content=Blob.valueOf('UNIT.TEST');
                        } else {
                            pdf = pr.getContentAsPDF();
                        }    
                    }
                    
                    System.debug(pdf);
                    DS_Recipe_Send_Env_Email_Controller.send(objNDA.Id);
                    
                    Attachment NDAFormAttachment = new Attachment();
                    NDAFormAttachment.Body = pdf;
                    NDAFormAttachment.Name = objNDA.NDA_Type__c+ ' - Contract1.pdf';
                    NDAFormAttachment.ParentId = objNDA.Id;
                    
                    insert NDAFormAttachment;
                    
                    List<dsfs__DocuSign_Status__c> lstDocuSignStatus = [Select Id,dsfs__Envelope_Status__c,NDA__c FROM dsfs__DocuSign_Status__c WHERE NDA__c =: NDARecordId AND dsfs__Envelope_Status__c = 'Sent'];
                    for(dsfs__DocuSign_Status__c objDocuSignStatus: lstDocuSignStatus) {
                        objDocuSignStatus.dsfs__Envelope_Status__c = 'Voided';    
                    }
                    if(!lstDocuSignStatus.IsEmpty()) {
                        update lstDocuSignStatus;
                    }
                    
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'NDA Contarct Has been Submitted for Docu Sign');
                    ApexPages.addMessage(myMsg);
                    return null;       
                /*} else {
                    System.debug('Else Part');
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'NDA Record is not Meeting Legal Review Condition');
                    ApexPages.addMessage(myMsg);
                    return null;
                }  */
            
        }
        return Null;
    }
    
    public Static  PageReference doCancel() { 
        Id NDARecordId1 = ApexPages.currentPage().getParameters().get('id');
        PageReference demoPage = new pagereference('/' + NDARecordId1);
        demoPage.setRedirect(true);
        return demoPage;
    }
}