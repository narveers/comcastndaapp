public with sharing class CAFGlobalCalls{

   // Method to get all the approvers associated with the current Contract
   public static List< REVVY__MnApprovalStepInst__c> getApprovers(id ContractId) {
        
     String StepInstid =  getStepInstanceId(ContractId);  
      List< REVVY__MnApprovalStepInst__c  > approverList = [SELECT Name,REVVY__Approver__r.Name,
                                                                   REVVY__StepStatus__c,REVVY__SortField__c FROM REVVY__MnApprovalStepInst__c 
                                                                   WHERE REVVY__ApprovalInst__c = : StepInstid AND REVVY__StepStatus__c != 'Skipped' 
                                                                  ORDER BY REVVY__Step_Sequence__c ASC];
                                                                  
                                                                  
      Return approverList;        
       
   }
   
    public static id getStepInstanceId(id ContractId) {
    
      // Get the Step Id from the List
        String currentStepId;
        String StepInstid;
        List<REVVY__MnContract__c> contracts =  [SELECT Id, Current_Step_ID__c FROM REVVY__MnContract__c WHERE Id =: contractId];
         List<REVVY__MnApprovalInst__c> ApprovInst = New List<REVVY__MnApprovalInst__c>();
         List<REVVY__MnApprovalStepInst__c> ApproverList = New List< REVVY__MnApprovalStepInst__c >();
        If (!contracts.isempty()){
         if ( contracts[0].Current_Step_ID__c != Null) {
          currentStepId = contracts[0].Current_Step_ID__c;
         } else {
          // Added by Vikram on 05/24/2019
           ApprovInst = [ SELECT Id,Name FROM REVVY__MnApprovalInst__c WHERE REVVY__Contract__c=: contractId ORDER BY Createddate DESC LIMIT 1];
           ApproverList = [SELECT Id 
                           FROM REVVY__MnApprovalStepInst__c 
                          WHERE REVVY__ApprovalInst__c in : ApprovInst and REVVY__StepStatus__c != 'Skipped' 
                       ORDER BY REVVY__SortField__c ASC ];
                          
           if(!ApproverList.isEmpty())
              currentStepId = ApproverList[0].id; 
        } 
        } 
       
       List< REVVY__MnApprovalStepInst__c > CurrentStepInst = [Select REVVY__ApprovalInst__c  from REVVY__MnApprovalStepInst__c where id = : currentStepId];
      
      If (!CurrentStepInst.isempty()){
         StepInstid = CurrentStepInst[0]. REVVY__ApprovalInst__c;
      } 
      
      Return StepInstId ; 
    }   
   
   
   

}