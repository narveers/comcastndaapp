public class CableOperationsIntakeFormNewCtrl {
    public Contract__c contract{get;set;}
    public String error{get;set;}
    public String UserFullNameString {get;set;}
    public Map<String,String> UserFullNameMap;
    Id contractId;
    public Attachment attach{get;set;}
    public Id selectedid{get;set;}
    public List<Id> attachmentIdList;
    public List<Attachment> AttachmentList{get;set;}
    public Boolean ContentCreated = false;
    public String currentStage{get;set;}
    public String filename{get;set;}
    public String filenametemp{get;set;}
    public String bodyTemp{get;set;}
    public String body{get;set;}
    public Document doc{get;set;}
    public Id attachId{get;set;}
    public List<Document> docuList{get;set;}
    public String smartApprovalURL{get;set;}
    public String attachDescription{get;set;}
    public String attachType{get;set;}
    public String procurementContact{get;set;} 
    public Boolean ContractValueBoolean{get;set;}
    Private EmailTemplate EmailTemplate;
    Private User User;
    Private String URL;
    public CableOperationsIntakeFormNewCtrl(ApexPages.StandardController controller) {
        URL = ApexPages.currentPage().getHeaders().get('Referer');
        filename = '';
        body = '';
        currentStage = 'Contract Details';
        attach = new Attachment();
        docuList = new List<Document>();
        AttachmentList = new List<Attachment>();
        attachmentIdList = new List<Id>();
        contractId = controller.getId();
        doc= new Document();
        if(contractId != null){//,Attorney_Contact__c
            contract = [select id,Assigned_Attorney__c,AttorneyContact__c,AttorneyContact__r.Name,Existing_NDA__c,PII_and_IP_Tech_Survey__c,Business_Unit_HQ__c,Business_Unit_Division__c,Existing_Master_Agreement__c,
                        Name,Procurement_Contact__c,Requester_Email__c,Prior_Attorney_Engagement__c,Business_Unit__c,Counterparty__c, Has_TPSA_completed_an_assessment_for_thi__c,
                        Projected_Dollar_Amount__c,Projected_due_date__c,Please_Explain__c,
                        Is_Procurement_Engaged__c,Brief_Summary__c from Contract__c where id=: contractId];
            UserFullNameString = contract.AttorneyContact__r.Name;
        }
        else{
            contract = new Contract__c();
        }
    }
    public List<SelectOption> attachmentTypes{
        get
        {
            attachmentTypes = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult =
                ContentVersion.Attachment_Type__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
                attachmentTypes.add(new SelectOption(f.getLabel(), f.getValue()));
            }       
            return attachmentTypes; 
        }
        set;
    }
    public List<SelectOption> AttorneyList{
        get
        {
            UserFullNameMap = new Map<String,String>();
            AttorneyList = new List<SelectOption>();
            AttorneyList.add(new SelectOption('','None'));
            for( User usr: [SELECT Name, Id FROM USER WHERE Id IN (SELECT UserOrGroupId FROM GroupMember where group.DeveloperName='CI_Attorney')])
            {
                AttorneyList.add(new SelectOption(usr.Name,usr.Name));
                UserFullNameMap.put(usr.Id, usr.Name);
            }       
            return AttorneyList; 
        }
        set;
    }
    public void attachFileFunction(){
        error = '';
        if(String.isNotBlank(attachId)){
            attachmentIdList.add(attachId); 
        }
        if(attachmentIdList.size() > 0){
            if(AttachmentList.size()>0){
                AttachmentList.clear();
            }
            AttachmentList = [SELECT Id,Name,Body,ContentType,Description FROM Attachment WHERE Id IN:attachmentIdList];
        }
    }
    public PageReference deleteAttachment(){
        Integer count=0;
        Attachment deleteDocument=null;
        try{
            for(Attachment doc:AttachmentList){
                if(doc.Id == selectedid){
                    deleteDocument = doc;
                    AttachmentList.Remove(count);
                    break;
                }
                count++;
            }
            if (deleteDocument!=null){
                delete deleteDocument;
            } 
        }
        catch(DMLException ex){
            error = ex.getMessage();
        }
        catch(exception ex){
            error = ex.getMessage();
        }
        return null;
    }
    public void previousStage(){
        error = '';
        if(currentStage == 'Review and Submit'){
            currentStage = 'Upload Attachment(s)';
        }else if(currentStage == 'Upload Attachment(s)'){
            currentStage = 'Contract Details';
        }
    }
    public PageReference nextStage(){
        error = '';
        if(currentStage == 'Contract Details'){
            currentStage = 'Upload Attachment(s)';
            contract.Contract_Intake_Status__c = 'Pending Submission';
            saveContractRecord();
        }else if(currentStage == 'Upload Attachment(s)'){
            currentStage = 'Review and Submit'; 
        }
         return null;
    }
    /*
    public void saveAttachments(){
        if(filename != null && body != null && filename != '' && body != ''){
            Try{
                doc.Name=filename;
                body = body.replace(System.URL.getSalesforceBaseURL().toExternalForm()+'/', '');
                System.debug(body);
                doc.Body= EncodingUtil.base64Decode(body);
                if (attachDescription.length() <= 255){
                    doc.Description = attachDescription;
                }
                else{
                    error = 'Description cannot be more than 255 characters';
                    system.debug('got out of exit');
                    return ;
                }
                doc.FolderId = UserInfo.getUserId();
                doc.Keywords = attachType;
                //attach.Name=filename;
                //attach.Body=Blob.valueOf(body);
                //attach.Description = attachDescription;
                body = '';
                insert doc;
                //insert attach;
                //attachmentId = attach.Id;
                attachmentIdList.add(doc.Id);
                //attach = new Attachment();
                doc = new Document();     
                
                if(attachmentIdList.size() > 0){ 
                    if(docuList.size() > 0){
                        system.debug('attachment list size '+ attachmentIdList.size());
                        docuList.clear();
                    }
                    docuList = [select id,Name,Body,Description,Keywords from Document where id IN :attachmentIdList]; 
                    
                }
            }
            Catch(Exception ex){
                error = ex.getMessage();
            }
        }
    }
    */
    public PageReference redirectToDashboard(){
        contract.Contract_Intake_Status__c = 'Contract Manager';
        
        saveContractRecord();
        savecontent();
        return new PageReference('/apex/ContractIntakeRequesterDashboard');
    }
    public PageReference saveContractRecord(){
        SavePoint sp = Database.setSavepoint();
        try{
            contract.RecordTypeId = Schema.SObjectType.Contract__c.getRecordTypeInfosByName().get('Cable Legal').getRecordTypeId();
            contract.Requester_Name__c = UserInfo.getName();
            contract.Requester_Email__c = UserInfo.getUserEmail();
            String userPhone = [SELECT Phone FROM User WHERE Id =: UserInfo.getUserId()].Phone;
            if(userPhone != Null) {
                contract.Requester_Phone_Number__c = userPhone;    
            }
            
            //contract.AttorneyContact__c = UserFullNameString;
            //UserFullNameString = contract.AttorneyContact__c;
            UserFullNameString = UserFullNameMap.get(contract.AttorneyContact__c);
            upsert contract;
            return null;
        }catch(DMLException e){
            Database.rollback(sp);
            for(Integer i=0 ;i < e.getNumDml() ; i++){
                error +=e.getdmlMessage(i);
            }
            return null;
        }catch(Exception e){
            Database.rollback(sp);
            error = e.getMessage();
            return null;
        }
    }
    public void savecontent(){
        if(!AttachmentList.isEmpty() && AttachmentList != null && AttachmentList.size() > 0){
            List<ContentVersion> FilesList = New List<ContentVersion>();
            for(Attachment doc : AttachmentList) {
                ContentVersion cv = new ContentVersion();
                cv.versionData = doc.Body;
                cv.title = doc.name;
                cv.FirstPublishLocationId = contract.id;
                cv.pathOnClient = doc.name;
                cv.Description = doc.Description;
                FilesList.add(cv);
            }
            Try {
                insert FilesList;
                //sendEmailtoContractManager();
                //sendEmailtoRequester();
            }
            Catch (DMLException e){
                error = e.getMessage(); 
            }
            Finally{
                if(AttachmentList.size() > 0 && !AttachmentList.isEmpty()){
                    Database.DeleteResult[] delRes = Database.delete(AttachmentList, false);
                    if (delRes.size() > 0){
                        system.debug('delete result -- '+delRes);
                    }
                }
            }
        }
    }/*
    public void sendEmailtoContractManager(){
        EmailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'CISubmissionContractManager' LIMIT 1];
        User = [SELECT Id FROM User WHERE Username='tanika_velingker@comcast.com.cmfull' LIMIT 1];
        //change the hardcoded username after deciding roles. 
        if(EmailTemplate!=NULL && User!=NULL){
            sendEmail(EmailTemplate.Id,User.Id,contract.Id);
            System.debug('Sent email to contract manager');
        }
    }
    public void sendEmailtoRequester(){
        EmailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'CISubmissionRequestor' LIMIT 1];
        User = [SELECT Id FROM User WHERE Email=:contract.Requester_Email__c LIMIT 1];
        if(String.isNotBlank(contract.Requester_Email__c) && EmailTemplate!=NULL && User!=NULL){
            system.debug('Requester - '+contract.Requester_Email__c);
            sendEmail(EmailTemplate.Id,User.Id,contract.Id);
            System.debug('Sent email to requester');
        }
    }
    public void sendEmail(string EmailTemplate, string Who, String What){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId(EmailTemplate);
        if(String.isNotBlank(Who)){
            mail.setTargetObjectId(Who);
        } 
        mail.setWhatId(What);
        mail.setSenderDisplayName('Legal Technology Operations');
        mail.setBccSender(false); 
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }*/
    public void reRenderPageFunc(){
        system.debug('con bool - '+ContractValueBoolean);
    }
    @InvocableMethod
    public static void assignAttorneyEmail(List<Id> contractId){
        List<Contract__c> con = [SELECT Id,Assigned_Attorney_Email__c,Attorney__c FROM Contract__c WHERE Id IN:contractId];
        User usr = [SELECT Id,Name,Email FROM User WHERE Name=:con[0].Attorney__c AND isActive = TRUE LIMIT 1];
        if(usr.Name == con[0].Attorney__c){
            con[0].Assigned_Attorney_Email__c = usr.Email;
            update con[0];
        }
    }
}