public class ContractstoExcelController {
    Public String Page{get;set;}
    Private Integer Size;
    Public String NowShowing{get;set;}
    Public List<Contract__c> CableContracts{get;set;}
    Public List<REVVY__MnContract__c> MnContracts{get;set;}
    public List<REVVY__MnStepApproverInst__c> ApproverList{get;set;}
    Public ContractstoExcelController(){
        String userName = UserInfo.getName();
        CableContracts = new List<Contract__c>();
        MnContracts = new List<REVVY__MnContract__c>();
        ApproverList = new List<REVVY__MnStepApproverInst__c>();
        String queryString = '';
        String status = '';
        String SubPhase = '';
        String URL = ApexPages.currentPage().getHeaders().get('Referer');
        NowShowing = ApexPages.currentPage().getparameters().get('NowShowing');
        NowShowing = EncodingUtil.urlDecode(NowShowing,'UTF-8');
        if(String.isNotBlank(URL)){
            List<String>URLList = new List<String>();
            URLList = URL.split('/apex/');
            system.debug('URL - '+URLList);
            if(URLList.size() > 0){
                if(String.isNotBlank(URLList[1]) && String.isNotEmpty(URLList[1])){
                    if(URLList[1].startsWith('CableIntakeDashBoard')){
                        Page = 'Management';
                        if(NowShowing == 'Pending Assignment'){
                            status = '\'Pending Attorney\',\'Pending Attorney Reassignment\'';
                        }
                        else if(NowShowing == 'Assigned'){
                            status = '\'Attorney Review\',\'Assigned to BU\'';
                        }
                        else if(NowShowing == 'Closed'){
                            status = '\'closed\'';
                        }else{
                            status = '\'Pending Attorney\',\'Pending Attorney Reassignment\',\'Attorney Review\',\'Assigned to BU\',\'Closed\'';
                        }
                        queryString = 'Select Id,Attorney__c,Business_Unit__c,Name,Counterparty__c,Contract_Intake_Status__c,'+
                            +'Procurement_Contact__c,Createddate,Projected_Dollar_Amount__c FROM Contract__c' 
                            +' WHERE Contract_Intake_Status__c IN (' + status  + ') ORDER BY Createddate DESC';
                        CableContracts = Database.query(queryString);
                    }
                    else if(URLList[1].startsWith('CableIntakeAttorneyDashboard')){
                        Page = 'Attorney';
                        if(NowShowing == 'Pending Assignment'){
                            status = '\'Pending Attorney\'';
                        }
                        else if(NowShowing == 'Assigned'){
                            status = '\'Attorney Review\',\'Assigned to BU\'';
                        }
                        else if(NowShowing == 'Closed'){
                            status = '\'closed\'';
                        }else{
                            status = '\'Pending Attorney\',\'Attorney Review\',\'Assigned to BU\',\'Closed\'';
                        }
                        queryString = 'Select Id,Attorney__c,Business_Unit__c,Name,Counterparty__c,Contract_Intake_Status__c,'+
                        +'Procurement_Contact__c,Createddate,Projected_Dollar_Amount__c FROM Contract__c' 
                        +' WHERE Contract_Intake_Status__c IN (' + status  + ') AND Attorney__c =\'' + userName +'\' ORDER BY Createddate DESC';
                        CableContracts = Database.query(queryString);
                    }
                    else if(URLList[1].startsWith('CableIntakeRequesterDashBoard')){
                        Page = 'Requester';
                        if(NowShowing == 'Pending Assignment'){
                            status = '\'Pending Attorney\',\'Contract Manager\',\'Pending Attorney Reassignment\'';
                        }
                        else if(NowShowing == 'Assigned'){
                            status = '\'Attorney Review\',\'Assigned to BU\'';
                        }
                        else if(NowShowing == 'Closed'){
                            status = '\'closed\'';
                        }else{
                            status = '\'Pending Attorney\',\'Contract Manager\',\'Pending Attorney Reassignment\',\'Attorney Review\',\'Assigned to BU\',\'Closed\'';
                        }
                        queryString = 'Select Id,Attorney__c,Business_Unit__c,Name,Counterparty__c,Contract_Intake_Status__c,'+
                            +'Procurement_Contact__c,Createddate,Projected_Dollar_Amount__c FROM Contract__c'
                            +' WHERE Requester_Name__c =\''+ userName + '\' AND Contract_Intake_Status__c IN (' + status  + ') ORDER BY Createddate DESC';
                        System.debug('query string - '+queryString);
                        CableContracts = Database.query(queryString);
                    }
                    else if(URLList[1].startsWith('ContractManagementDashboard')){
                        Page = 'MnContract';
                        if(NowShowing == 'Pending Approvals'){
                            SubPhase = '\'In Process\',\'Admin Review\',\'CAF Request\',\'CAF Approval\'';
                        }else if(NowShowing == 'Completed Requests'){
                            SubPhase = '\'Signature\',\'Active\',\'InActive\'';
                        }else if(NowShowing == 'Total Requests'){
                            SubPhase = '\'Signature\',\'Active\',\'InActive\',\'Approval\'';
                        }
                        queryString = 'SELECT Id,Name,Counterparty__c,Business_Unit__c,Contract_Value__c,Createddate,'+
                            'REVVY__Phase__c,Current_Approver__c '+
                            'FROM REVVY__MnContract__c WHERE REVVY__SubPhase__c'+
                            ' IN ('+SubPhase+') AND Recordtype.DeveloperName = \'Request_CAF\' ORDER BY Createddate DESC';
                        MnContracts = Database.query(queryString);
                    }
                    else if(URLList[1].startsWith('ContractRequesterDashboard')){
                        Page = 'MnContract';
                        if(NowShowing == 'Pending Approvals'){
                            SubPhase = '\'In Process\',\'Admin Review\',\'CAF Request\',\'CAF Approval\'';
                        }else if(NowShowing == 'Completed Requests'){
                            SubPhase = '\'Signature\',\'Active\',\'InActive\'';
                        }else if(NowShowing == 'Total Requests'){
                            SubPhase = '\'Signature\',\'Active\',\'InActive\',\'Approval\'';
                        }
                        queryString = 'SELECT Id,Name,Counterparty__c,Business_Unit__c,Contract_Value__c,Createddate,'+
                            'REVVY__Phase__c,Current_Approver__c '+
                            'FROM REVVY__MnContract__c WHERE REVVY__Phase__c'+
                            ' IN ('+SubPhase+') AND Recordtype.DeveloperName = \'Request_CAF\' AND Owner.Id = \'' + UserInfo.getUserId() +
                            '\' ORDER BY Current_Approver__c';
                        MnContracts = Database.query(queryString);
                    }
                    else if(URLList[1].startsWith('ContractApprovalDashboard')){
                        Page = 'MnApprover';
                        if(NowShowing == 'Pending Approvals'){
                            SubPhase = '\'In Process\',\'Admin Review\',\'CAF Request\',\'CAF Approval\'';
                             Status = '\'Pending Approval\'';
                        }else if(NowShowing == 'Completed Requests'){
                            Status = '\'Approved\',\'Rejected\'';
                            SubPhase = '\'Signature\',\'Active\',\'InActive\'';
                        }else if(NowShowing == 'Total Requests'){
                            Status = '\'Approved\',\'Rejected\',\'Pending Approval\'';
                            SubPhase = '\'Signature\',\'Active\',\'InActive\',\'Approval\'';
                        }
                        queryString = 'SELECT REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c,'+
                            'REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.Name,'+
                            'REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.Counterparty__c,'+
                            'REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.Business_Unit__c,'+
                            'REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.Contract_Value__c,'+
                            'REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.Createddate,'+
                            'REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.REVVY__Phase__c,'+
                            'REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.Current_Approver__c FROM '+
                            'REVVY__MnStepApproverInst__c WHERE REVVY__AssignedUser__c = \''+UserInfo.getUserId()+'\' AND REVVY__Status__c IN ('+
                            Status+') AND REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.REVVY__Phase__c IN ('+SubPhase+') '+
                            'AND REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.Recordtype.DeveloperName = \'Request_CAF\' '+
                            'ORDER BY REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.Current_Approver__c';
                        ApproverList = Database.query(queryString);
                    }
                    else if(URLList[1].startsWith('ContractAdminDashboard')){
                        Page = 'MnContract';
                        if(NowShowing == 'Pending Approvals'){
                            SubPhase = '\'In Process\',\'Admin Review\',\'CAF Request\',\'CAF Approval\'';
                        }else if(NowShowing == 'Completed Requests'){
                            SubPhase = '\'Signature\',\'Active\',\'InActive\'';
                        }else if(NowShowing == 'Total Requests'){
                            SubPhase = '\'Signature\',\'Active\',\'InActive\',\'Approval\'';
                        }
                        queryString = 'SELECT Id,Name,Counterparty__c,Business_Unit__c,Contract_Value__c,Createddate,'+
                            'REVVY__Phase__c,Current_Approver__c '+
                            'FROM REVVY__MnContract__c WHERE REVVY__Phase__c'+
                            ' IN ('+SubPhase+') AND Recordtype.DeveloperName = \'Request_CAF\' ORDER BY Current_Approver__c';
                        MnContracts = Database.query(queryString);
                    }
                }
            }
        }
    }
}