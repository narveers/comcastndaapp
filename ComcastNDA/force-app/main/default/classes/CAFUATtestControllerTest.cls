@isTest public class CAFUATtestControllerTest {
    @isTest static void testMethod1(){
        Account acc = new account(name = 'See Counterparty (Legal Entity)');
        insert acc;
        
        Contact con = new contact(LastName = 'Test', AccountId = acc.id);
        insert con;
        
        Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
        
        REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
        lifeCycle.Name = 'Test Lifecycle';
        lifeCycle.REVVY__ApplyTo__c = 'Original';
        lifeCycle.REVVY__Object__c = 'MnContract__c';
        lifeCycle.REVVY__RecordType__c = 'Request_CAF';
        lifeCycle.REVVY__Status__c = 'Draft';
        insert lifeCycle;
        
        REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
        stage.name = 'Test';
        stage.REVVY__LifeCycle__c = lifeCycle.Id;
        stage.REVVY__Phase__c = 'Draft';
        stage.REVVY__SubPhase__c = 'Draft';
        stage.REVVY__Editable__c = true;
        insert stage;
        
        lifeCycle.REVVY__InitialStage__c = stage.Id;
        lifeCycle.REVVY__Status__c = 'Active';
        update lifeCycle;
        
        REVVY__MnContract__c objMnContract = new REVVY__MnContract__c(Name='Test contract',REVVY__Account__c = acc.id, recordTypeId=recTypeId,Brief_Summary__c = 'test',
                                                           REVVY__Contact__c = con.id,Contract_Value__c=80000,REVVY__LifecycleConfig__c = lifeCycle.Id,Business_Unit__c='test');
        insert objMnContract;
        
        PageReference pageRef = Page.CAFUATtest;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objMnContract);
        CAFUATtestController objCon = new CAFUATtestController(sc);
        objCon.attachId = TestData.createAttachment(objMnContract.Id).Id;
        objCon.attachmentIdList.add(objCon.attachId);
        objCon.AttachmentList.add(TestData.createAttachment(objMnContract.Id));
        List<SelectOption> attachmentTypes = objCon.attachmentTypes;
        List<SelectOption> BusinessUnits = objCon.BusinessUnits;
        List<SelectOption> CAFType = objCon.CAFType;
        List<SelectOption> contractcurrency = objCon.contractcurrency;
        objCon.gotoDashboard();
        objCon.previousStage();
        objCon.previousStage();
        objCon.previousStage();
        objCon.previousStage();
        objCon.nextStage();
        objCon.nextStage();
        objCon.nextStage();
        objCon.nextStage();
        objCon.attachFileFunction();
        objCon.isApproverAdded();
        objCon.saveContractRecord();
        objCon.savecontent();
        objCon.selectedid = objCon.AttachmentList[0].id;
        objCon.deleteAttachment();
        objCon.CAFTypeString = 'Cable Division';
        objCon.reRenderPage();
        objCon.CAFTypeString = 'Cable Division - Preferred Vendor';
        objCon.reRenderPage();
        objCon.CAFTypeString = 'CAF - Leisure Arts';
        objCon.reRenderPage();
        objCon.CAFTypeString = 'Cable Division - Comcast India';
        objCon.reRenderPage();
        objCon.currentStage = 'Review and Submit';
        objCon.ApproverResultBoolean = true;
        objCon.nextStage();
        objCon.currentStage = 'Review and Submit';
        objCon.ApproverResultBoolean = false;
        objCon.nextStage();
        objCon.currentStage = 'Smart Approval';
        objCon.nextStage();
        objCon.AttachmentList.add(TestData.createAttachment(objMnContract.Id));
        objCon.currentStage = 'File Upload';
        objCon.nextStage();
    }
}