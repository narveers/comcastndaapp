public class ReviewContractsExt {
    private ApexPages.StandardSetController standardController;
    private ApexPages.StandardController controller; public String actualValue{get;set;} public String rowNumber{get;set;}
    public List<Contract_Intake_Form__c> listContracts{get;set;}
	public ReviewContractsExt(ApexPages.StandardSetController standardController){
        this.standardController = standardController;
    }
    public ReviewContractsExt(ApexPages.StandardController controller){
        this.controller = controller;
    }
    public PageReference init(){
        listContracts = new List<Contract_Intake_Form__c>();
        if(standardController!=null && standardController.getSelected().size()>0){
            List<Contract_Intake_Form__c> selectedContracts = [select id,name,Client_Name_and_business_unit__c,Description__c,Status__c,Review_Status__c,Ownerid from Contract_Intake_Form__c where id in : standardController.getSelected()] ;
            for(Contract_Intake_Form__c contract: selectedContracts){ if(contract.Status__c=='Assigned to Attorney'){ listContracts.add(contract);
                }
            }
        }else if(controller!=null && controller.getId()!=null){
            List<Contract_Intake_Form__c> selectedContracts = [select id,name,Client_Name_and_business_unit__c,Description__c,Status__c,Review_Status__c,Ownerid from Contract_Intake_Form__c where id = : controller.getId()] ;
            for(Contract_Intake_Form__c contract: selectedContracts){
                if(contract.Status__c=='Assigned to Attorney'){
                    listContracts.add(contract);
                }
            }
        }
        return null;
    }
    public void valueSelection(){
        System.debug(ApexPages.currentPage().getParameters().get('valindex'));
        listContracts[Integer.valueOf(ApexPages.currentPage().getParameters().get('valindex'))].Review_Status__c = ApexPages.currentPage().getParameters().get('value');
    }
    public PageReference save(){
        for(Contract_Intake_Form__c contract: listContracts){
            if(contract.Review_Status__c == null || contract.Review_Status__c == 'Accepted'){
                contract.Review_Status__c = 'Accepted';
                contract.Status__c ='In Progress';
            }else if(contract.Review_Status__c == 'Rejected'){   contract.Status__c ='Pending Assignment to Attorney'; contract.OwnerId ='00G0x000000VFQn';
            }else if(contract.Review_Status__c == 'Need More Info'){              contract.Status__c ='Need More Info';  contract.OwnerId ='00G0x000000VFQn';
            }
        }
        update listContracts;
        if(standardController!=null){   return standardController.cancel();
        }else if(controller!=null){
            return controller.cancel();
        }else {
            return null;
        }
    }
}