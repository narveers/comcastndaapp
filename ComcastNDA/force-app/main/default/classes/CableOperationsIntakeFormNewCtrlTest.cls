@isTest
public class CableOperationsIntakeFormNewCtrlTest {
    static testmethod void testMethod1(){
    List<id> lstContr = new List<id>();
       // Contract__c con = TestData.createContractRequest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='Aliza Reder');
        
        Contract__c con = new Contract__c();
        con.Name = 'Test Contract Request';
        con.Contract_Intake_Status__c = 'Contract Manager';
        con.Requester_Name__c = 'username';
        con.Requester_Email__c = 'legal_help@comcast.com';
        con.Attorney__c = 'Aliza Reder';
        insert con;
        lstContr.add(con.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        CableOperationsIntakeFormNewCtrl CIF = new CableOperationsIntakeFormNewCtrl(sc);
        List<SelectOption> TestAttType = CIF.attachmentTypes;
        CIF.attachId = TestData.createAttachment(con.Id).Id;
        CIF.attachmentIdList.add(CIF.attachId);
        CIF.attachFileFunction();
        CIF.selectedid = CIF.attachId;
        CIF.deleteAttachment();
        CIF.nextStage();
        CIF.nextStage();
        CIF.previousStage();
        CIF.previousStage();
        CIF.AttachmentList.add(TestData.createAttachment(con.Id));
        CIF.redirectToDashboard();
        CIF.savecontent();
        CIF.reRenderPageFunc();
        CableOperationsIntakeFormNewCtrl.assignAttorneyEmail(lstContr);
        
    }
    static testmethod void testmethod2(){
        Contract__c con = new Contract__c();
        con.Name = 'Test Contract Request';
        con.Contract_Intake_Status__c = 'Contract Manager';
        con.Requester_Name__c = 'username';
        con.Requester_Email__c = 'legal_help@comcast.com';
        con.Attorney__c = 'Aliza Reder';
        insert con;
        PageReference pgref = Page.ConfidentialContractRequest;
        pgref.getParameters().put('Id', con.Id);
        Test.setCurrentPage(Page.ConfidentialContractRequest);
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        ConfidentialContractRequestController CCRC = new ConfidentialContractRequestController(sc);
        CCRC.attachId = TestData.createAttachment(con.Id).Id;
        CCRC.attachmentIdList.add(CCRC.attachId);
        CCRC.AttachmentList.add(TestData.createAttachment(con.Id));
        ccrc.savecontent();
        CCRC.attachFileFunction();
        CCRC.nextStage();
        CCRC.nextStage();
        CCRC.previousStage();
        CCRC.previousStage();
        CCRC.selectedid = CCRC.attachId;
        CCRC.deleteAttachment();
        CCRC.redirectToDashboard();
        CCRC.reRenderPageFunc();
        Test.startTest();
        ConfidentialContractRequestController.isShareable = true;
        ConfidentialContractRequestController.callSharing();
        Test.stopTest();
        
        ccrc.currentStage = 'Review and Submit';
        ccrc.saveContractRecord();
        ccrc.previousStage();
        ccrc.previousStage();
        ccrc.nextStage();
        ccrc.nextStage();
    }
}