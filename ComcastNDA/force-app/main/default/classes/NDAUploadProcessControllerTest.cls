@isTest
public class NDAUploadProcessControllerTest{
@isTest
private static void testMethod1(){
      
      Test.startTest();
      
   NDA_Master__c objNDA  = new NDA_Master__c ();
     // objNDA.LastName = 'test';
     objNDA.Third_Party__c = 'test';
     objNDA.Entity_Entities__c = 'NBCU LE 1';
     objNDA .Agreement_Type__c = 'C - Confidentiality and/or Non-Disclosure Agreement  ';
     objNDA.Type_of_Confidentiality__c = 'A - One way - Comcast as Receiving Party';
     objNDA.NBCU_Business_Person_s__c = 'Business Person 1';
     objNDA.NBCU_Approving_Attorney_s__c = 'Attorney 1';
     objNDA.Agreement_Contact_Person_s__c = 'Contact Person 1';
          insert objNDA ;
         
          Apexpages.StandardController sc = new Apexpages.StandardController(objNDA);

        NDAUploadProcessController objUpload = new NDAUploadProcessController(sc);
        List<SelectOption> selOpts=objUpload.NBCUBusinessPerson;
        List<SelectOption> selOpts1=objUpload.NBCUApprovingAttorney;
        List<SelectOption> selOpts2=objUpload.AgreementContactPerson;
       // objUpload.currentStageString ='Disclosure Details';
       // objUpload.currentStageString = 'File Upload';
                    objUpload.attachFileFunction();
                    objUpload.deleteAttachment(); 
                    objUpload.nextStage();
                    //objUpload.currentStageString ='Disclosure Details';
                     objUpload.saveMasterNDA();
                     objUpload.previousStage();
    NDA_Master__c objNDA1  = new NDA_Master__c ();                 
     objNDA1.Third_Party__c = 'test';
     objNDA1.Entity_Entities__c = 'NBCU LE 1';
     objNDA1.Agreement_Type__c = 'C - Confidentiality and/or Non-Disclosure Agreement  ';
     objNDA1.Type_of_Confidentiality__c = 'A - One way - Comcast as Receiving Party';
     objNDA1.NBCU_Business_Person_s__c = 'Business Person 1';
     objNDA1.NBCU_Approving_Attorney_s__c = 'Attorney 2';
     objNDA1.Agreement_Contact_Person_s__c = 'Contact Person 1';
          insert objNDA1 ;
          
          Apexpages.StandardController sc1 = new Apexpages.StandardController(objNDA1);

        NDAUploadProcessController objUpload1 = new NDAUploadProcessController(sc1);
                     //objUpload1.nextStage();
                    //objUpload1.previousStage();
                    //objUpload1.saveMasterNDA();
                   // objUpload1.deleteAttachment();
                   objUpload1.currentStageString ='Disclosure Details';
                    objUpload1.nextStage();
                   
     NDA_Master__c objNDA2  = new NDA_Master__c ();
     // objNDA.LastName = 'test';
     objNDA2.Third_Party__c = 'test';
     objNDA2.Entity_Entities__c = 'NBCU LE 1';
     objNDA2.Agreement_Type__c = 'C - Confidentiality and/or Non-Disclosure Agreement  ';
     objNDA2.Type_of_Confidentiality__c = 'A - One way - Comcast as Receiving Party';
     objNDA2.NBCU_Business_Person_s__c = 'Business Person 1';
     objNDA2.NBCU_Approving_Attorney_s__c = 'Attorney 1';
     objNDA2.Agreement_Contact_Person_s__c = 'Contact Person 1';
          insert objNDA2;
         
          Apexpages.StandardController sc2 = new Apexpages.StandardController(objNDA2);

        NDAUploadProcessController objUpload2 = new NDAUploadProcessController(sc2);               
                   
                    objUpload2.currentStageString = 'File Upload';
                     objUpload2.nextStage();
                   
                   
  NDA_Master__c objNDA3  = new NDA_Master__c ();
     // objNDA.LastName = 'test';
     objNDA3.Third_Party__c = 'test';
     objNDA3.Entity_Entities__c = 'NBCU LE 1';
     objNDA3.Agreement_Type__c = 'C - Confidentiality and/or Non-Disclosure Agreement  ';
     objNDA3.Type_of_Confidentiality__c = 'A - One way - Comcast as Receiving Party';
     objNDA3.NBCU_Business_Person_s__c = 'Business Person 1';
     objNDA3.NBCU_Approving_Attorney_s__c = 'Attorney 1';
     objNDA3.Agreement_Contact_Person_s__c = 'Contact Person 1';
          insert objNDA3;
         
          Apexpages.StandardController sc3 = new Apexpages.StandardController(objNDA3);

        NDAUploadProcessController objUpload3 = new NDAUploadProcessController(sc3);               
                   
                    objUpload3.currentStageString = 'Review and Submit';
                    objUpload3.nextStage();
                    
 NDA_Master__c objNDA4  = new NDA_Master__c ();
     // objNDA.LastName = 'test';
     objNDA4.Third_Party__c = 'test';
     objNDA4.Entity_Entities__c = 'NBCU LE 1';
     objNDA4.Agreement_Type__c = 'C - Confidentiality and/or Non-Disclosure Agreement  ';
     objNDA4.Type_of_Confidentiality__c = 'A - One way - Comcast as Receiving Party';
     objNDA4.NBCU_Business_Person_s__c = 'Business Person 1';
     objNDA4.NBCU_Approving_Attorney_s__c = 'Attorney 1';
     objNDA4.Agreement_Contact_Person_s__c = 'Contact Person 1';
          insert objNDA4;
         
          Apexpages.StandardController sc4 = new Apexpages.StandardController(objNDA4);

        NDAUploadProcessController objUpload4 = new NDAUploadProcessController(sc4);               
                   
                    objUpload4.currentStageString = 'File Upload';
                     objUpload4.previousStage();                   
                    
                    
   NDA_Master__c objNDA5  = new NDA_Master__c ();
     // objNDA.LastName = 'test';
     objNDA5.Third_Party__c = 'test';
     objNDA5.Entity_Entities__c = 'NBCU LE 1';
     objNDA5.Agreement_Type__c = 'C - Confidentiality and/or Non-Disclosure Agreement  ';
     objNDA5.Type_of_Confidentiality__c = 'A - One way - Comcast as Receiving Party';
     objNDA5.NBCU_Business_Person_s__c = 'Business Person 1';
     objNDA5.NBCU_Approving_Attorney_s__c = 'Attorney 1';
     objNDA5.Agreement_Contact_Person_s__c = 'Contact Person 1';
          insert objNDA5;
         
          Apexpages.StandardController sc5 = new Apexpages.StandardController(objNDA5);

        NDAUploadProcessController objUpload5 = new NDAUploadProcessController(sc5);               
                   
                    objUpload5.currentStageString = 'Review and Submit';
                     objUpload5.previousStage();  
                     
         
                    
             Test.stopTest();
       }

}