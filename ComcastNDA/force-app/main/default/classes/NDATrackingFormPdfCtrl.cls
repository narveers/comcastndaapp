/*
    Name             : NDATrackingFormPdfCtrl
    Author           : Narveer S.
    Date             : 04/11/2019
    Description      : Controller class for NDA Tracking form
*/
public class NDATrackingFormPdfCtrl { 
    
    Public NDA__c objNDARec{get;set;}
    public Id NDARecId;
    
    public NDATrackingFormPdfCtrl() {
        objNDARec = new NDA__c();
        NDARecId = ApexPages.currentPage().getParameters().get('id');
        if(NDARecId != null){
        objNDARec = [Select ID,Counterparty_Entity_Name__c,NBCU_Legal_Entity_Name__c,Purpose__c,Effective_Date__c,Public_Company__c,NBCU_Signer_Name__c,Assigned_Attorney__r.Name,Termination_Date__c,No_Hire_Non_Solicit__c,NDA_Type__c,No_Hire_Non_Solicit_Termination_Date__c,Standstill__c,Standstill_Termination_Date__c,Non_Compete__c,Non_Compete_Termination_Date__c,General_Comments__c FROM NDA__c WHERE ID =:NDARecId];    
                    }
        System.debug(objNDARec);
    }
}