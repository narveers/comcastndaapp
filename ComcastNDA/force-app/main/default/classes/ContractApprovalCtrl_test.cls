@isTest
public class ContractApprovalCtrl_test {
    
    
    static testMethod void testCreateContract() {
        ContractApprovalCtrl CAF = new ContractApprovalCtrl(new ApexPages.StandardController(TestData.CreateCAFRequest()));
        REVVY__MnContract__c ConNull = new REVVY__MnContract__c();
        ContractApprovalCtrl CAF1 = new ContractApprovalCtrl(new ApexPages.StandardController(ConNull));
    }
    testmethod static void runTest0(){
            
            Account acc = new account(name = 'See Counterparty (Legal Entity)');
            insert acc;
            
            Contact con = new contact(LastName = 'Test', AccountId = acc.id);
            insert con;
            
            Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
            
            REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
            lifeCycle.Name = 'Test Lifecycle';
            lifeCycle.REVVY__ApplyTo__c = 'Original';
            lifeCycle.REVVY__Object__c = 'MnContract__c';
            lifeCycle.REVVY__RecordType__c = 'Request_CAF';
            lifeCycle.REVVY__Status__c = 'Draft';
            insert lifeCycle;
            
            REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
            stage.name = 'Test';
            stage.REVVY__LifeCycle__c = lifeCycle.Id;
            stage.REVVY__Phase__c = 'Draft';
            stage.REVVY__SubPhase__c = 'Draft';
            stage.REVVY__Editable__c = true;
            insert stage;
            
            lifeCycle.REVVY__InitialStage__c = stage.Id;
            lifeCycle.REVVY__Status__c = 'Active';
            update lifeCycle;
            
            REVVY__MnContract__c mnc = new REVVY__MnContract__c(Name='Test contract',REVVY__Account__c = acc.id, recordTypeId=recTypeId,
                                                                REVVY__Contact__c = con.id,Contract_Value__c=80000,Brief_Summary__c = ' ',
                                                               REVVY__Phase__c = 'Approval');
            
            insert mnc;
        
        TestData.createAttachment(mnc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(mnc);
        ContractApprovalCtrl ctr = new ContractApprovalCtrl(sc);
        
        PageReference myVfPage = Page.ContractApproval;
        Test.setCurrentPage(myVfPage);
       // ApexPages.currentPage().getParameters().put('stage','File Upload');
        ctr.currentStage = 'General Information';
        ctr.CAFTypeString = 'LTO Test';
        List<SelectOption> testOpt = ctr.attachmentTypes;
        List<SelectOption> testOp1t =ctr.BusinessUnits;
        List<SelectOption> testOpt2 =ctr.CAFType;
        List<SelectOption> testOp3t =ctr.contractcurrency;
        ctr.nextStage();
        ctr.nextStage();
        ctr.nextStage();
        ctr.saveContractRecord();
        ctr.previousStage();
        ctr.previousStage();
        ctr.previousStage();
     /*   ctr.isApproverAdded();
        ctr.bringmeApproverDetails();
        ctr.savecontent();
        ctr.bringmeApproverDetails();
*/       
        ctr.currentStage = 'General Information';
        ctr.attachFileFunction();
        ctr.ApproverResultBoolean = false;
        ctr.currentStage = 'Review and Submit';
        ctr.nextStage();
        ctr.reRenderPage();
        ctr.gotoDashboard();
        ctr.deleteAttachment();
        ctr.CAFTypeString = 'Cable Division';
        ctr.contractvalue = '-10';
        ctr.reRenderPage();
        ctr.CAFTypeString = 'Cable Division';
        ctr.contractvalue = '-10';
        ctr.reRenderPage();
        ctr.reRenderPage();
        ctr.CAFTypeString ='Cable Division - Comcast India';
        ctr.reRenderPage();
        ctr.CAFTypeString ='Cable Division - Preferred Vendor';
        ctr.reRenderPage();
        ctr.CAFTypeString ='Cable Division - Preferred Vendor';
        ctr.refreshFunc();
    }
    static testMethod void testDeleteAttachment() {
        REVVY__MnContract__c testcontract = TestData.CreateCAFRequest();
        REVVY__MnApprovalDef__c mad = new REVVY__MnApprovalDef__c(REVVY__MainObject__c = 'REVVY__MnContract__c',REVVY__Order__c = 100);
        insert mad;
        
        REVVY__MnApprovalInst__c mai = new REVVY__MnApprovalInst__c(REVVY__ApprovalDef__c = mad.id, REVVY__ApprovalObjId__c = 'Text',REVVY__Contract__c= testcontract.Id);
        insert mai;
        
        REVVY__MnApprovalStepDef__c asd = new REVVY__MnApprovalStepDef__c(REVVY__ApprovalDef__c = mad.id, REVVY__StepSequence__c = 100);
        insert asd;
        
        REVVY__MnApprovalStepInst__c asi = new REVVY__MnApprovalStepInst__c(REVVY__ApprovalInst__c = mai.id, REVVY__ApprovalStepDef__c = asd.id, REVVY__Step_Sequence__c = 100,
                                                                           REVVY__StepStatus__c='Pending Approval',REVVY__IsParentStep__c = false);
        insert asi;
        
        REVVY__MnStepApproverInst__c SIA = new REVVY__MnStepApproverInst__c(REVVY__StepInst__c=asi.Id,REVVY__Status__c='Pending Approval');
        insert SIA;
        List<REVVY__MnStepApproverInst__c> SIAList = new List<REVVY__MnStepApproverInst__c>();
        SIAList.add(SIA);
        Attachment attach = TestData.createAttachment(testcontract.Id);
        ContractApprovalCtrl CAF2 = new ContractApprovalCtrl(new ApexPages.StandardController(testcontract));
        ContractApprovalCtrl.ApprovalWrapper wrapp = new ContractApprovalCtrl.ApprovalWrapper(asi,SIAList);
        List<SelectOption> attachmentTypesTest = CAF2.attachmentTypes;
        List<SelectOption> BusinessUnitTest = CAF2.BusinessUnits;
        List<SelectOption> contractcurrencyTest = CAF2.contractcurrency;
        CAF2.contractvalue = '5000';
        CAF2.CAFTypeString = 'Cable Division - Comcast India';
        CAF2.reRenderPage();
        CAF2.contractvalue = '120000';
        CAF2.CAFTypeString = 'Cable Division';
        CAF2.reRenderPage();
        CAF2.CAFTypeString = 'CAF - Leisure Arts';
        CAF2.reRenderPage();
        CAF2.CAFTypeString = 'Cable Division - Preferred Vendor';
        CAF2.reRenderPage();
        CAF2.CAFTypeString = 'test';
        CAF2.reRenderPage();
        CAF2.selectedid = attach.id;
        CAF2.deleteAttachment();
        CAF2.nextStage();
        CAF2.gotoDashboard();
        CAF2.refreshFunc();
        //List<REVVY__MnContract__c> mnclist = RequestCAFContractVFController.getLookupRecord('Test', 'REVVY__MnContract__c', 'test');
    }
    static testMethod void testmethod3() {
        test.startTest();
        Account acc = new account(name = 'See Counterparty (Legal Entity)');
            insert acc;
            
            Contact con = new contact(LastName = 'Test', AccountId = acc.id);
            insert con;
            
            Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
            
            REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
            lifeCycle.Name = 'Test Lifecycle';
            lifeCycle.REVVY__ApplyTo__c = 'Original';
            lifeCycle.REVVY__Object__c = 'MnContract__c';
            lifeCycle.REVVY__RecordType__c = 'Request_CAF';
            lifeCycle.REVVY__Status__c = 'Draft';
            insert lifeCycle;
            
            REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
            stage.name = 'Test';
            stage.REVVY__LifeCycle__c = lifeCycle.Id;
            stage.REVVY__Phase__c = 'Draft';
            stage.REVVY__SubPhase__c = 'Draft';
            stage.REVVY__Editable__c = true;
            insert stage;
            
            lifeCycle.REVVY__InitialStage__c = stage.Id;
            lifeCycle.REVVY__Status__c = 'Active';
            update lifeCycle;
            
            REVVY__MnContract__c mnc = new REVVY__MnContract__c(Name='Test contract',REVVY__Account__c = acc.id, recordTypeId=recTypeId,
                                                                REVVY__Contact__c = con.id,Contract_Value__c=80000,Brief_Summary__c = 'test',
                                                               REVVY__Phase__c = 'Approval',Is_Sourcing_Contract__c = 'Yes');
            insert mnc;
        
        Attachment attach = TestData.createAttachment(mnc.Id);
        ContractApprovalCtrl CAF2 = new ContractApprovalCtrl(new ApexPages.StandardController(mnc));        
        CAF2.AttachmentList = [SELECT Id,Name,Description,Body,ContentType FROM Attachment WHERE ParentId =:mnc.Id];
        
        List<SelectOption> TestCAFType = CAF2.CAFType;
        List<SelectOption> contractcurrency = CAF2.contractcurrency;
        List<SelectOption> BusinessUnits = CAF2.BusinessUnits;
        List<SelectOption> attachmentTypes = CAF2.attachmentTypes;
        CAF2.CAFTypeString = 'Cable Division';
        CAF2.reRenderPage();
        CAF2.gotoDashboard();
        
        CAF2.attachId = attach.Id;
        CAF2.attachFileFunction();
        CAF2.nextStage();
        CAF2.currentStage = 'Brief Summary';
        CAF2.nextStage();
        test.stopTest();
        CAF2.previousStage();
        CAF2.currentStage = 'Review and Submit';
        CAF2.saveContractRecord();
        CAF2.savecontent();
        CAF2.previousStage();
        CAF2.previousStage();
        CAF2.previousStage();
        CAF2.currentStage = 'General Information';
        CAF2.nextStage();
        CAF2.currentStage='File Upload';
        CAF2.nextStage();
        CAF2.currentStage='Smart Approval';
        CAF2.nextStage();
        CAF2.refreshFunc();
    }
    
}