@isTest
public class CAFApproverDashboardControllerTest{
@isTest
private static void testMethod1(){
    Test.startTest();
    
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
    
    User objUser = new user();
    //objUser.Name = 'test';
    objUser.UserName = 'asd.test@gmail.com';
    objUser.LastName = 'lName';
    objUser.Email = 'abc@bh.com';
    objUser.Alias = 'tsamp';
    objUser.CommunityNickname = 'ghjk';
    objUser.TimeZoneSidKey = 'America/Los_Angeles';
    objUser.LocaleSidKey = 'en_US';
    objUser.EmailEncodingKey = 'UTF-8';
    objUser.ProfileId = p.Id;
    objUser.LanguageLocaleKey = 'en_US';
    
    insert objUser;
    Account acc = new account(name = 'test');
        insert acc;
        
        Contact con = new contact(LastName = 'Test', AccountId = acc.id);
        insert con;
        
        Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
        
        REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
        lifeCycle.Name = 'Test Lifecycle';
        lifeCycle.REVVY__ApplyTo__c = 'Original';
        lifeCycle.REVVY__Object__c = 'MnContract__c';
        lifeCycle.REVVY__RecordType__c = 'Request_CAF';
        lifeCycle.REVVY__Status__c = 'Draft';
        insert lifeCycle;
        
        REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
        stage.name = 'Test';
        stage.REVVY__LifeCycle__c = lifeCycle.Id;
        stage.REVVY__Phase__c = 'Draft';
        stage.REVVY__SubPhase__c = 'Draft';
        stage.REVVY__Editable__c = true;
        insert stage;
        
        lifeCycle.REVVY__InitialStage__c = stage.Id;
        lifeCycle.REVVY__Status__c = 'Active';
        update lifeCycle;
        
        REVVY__MnContract__c objMnContract = new REVVY__MnContract__c(Name='Test contract',REVVY__Account__c = acc.id, recordTypeId=recTypeId,
                                                           REVVY__Contact__c = con.id,Contract_Value__c=80000,REVVY__LifecycleConfig__c = lifeCycle.Id);
        insert objMnContract;
        
        REVVY__MnApprovalDef__c mad = new REVVY__MnApprovalDef__c(REVVY__MainObject__c = 'REVVY__MnContract__c',REVVY__Order__c = 100);
        insert mad;
        
        REVVY__MnApprovalInst__c mai = new REVVY__MnApprovalInst__c(REVVY__ApprovalDef__c = mad.id, REVVY__ApprovalObjId__c = 'Text');
        insert mai;
        
        REVVY__MnApprovalStepDef__c asd = new REVVY__MnApprovalStepDef__c(REVVY__ApprovalDef__c = mad.id, REVVY__StepSequence__c = 100);
        insert asd;
        
        REVVY__MnApprovalStepInst__c asi = new REVVY__MnApprovalStepInst__c(REVVY__ApprovalInst__c = mai.id, REVVY__ApprovalStepDef__c = asd.id, REVVY__Step_Sequence__c = 100,
                                                                           REVVY__StepStatus__c='Pending Approval',REVVY__IsParentStep__c = false);
        insert asi;
    
 
    
    REVVY__MnApprovalInst__c objMnApprovalInst = new REVVY__MnApprovalInst__c();
    objMnApprovalInst.REVVY__Contract__c = objMnContract.Id;
    // objMnApprovalInst.REVVY__AssignedUser__c = objUser.Id;
    objMnApprovalInst.REVVY__ApprovalDef__c = mad.id;
    objMnApprovalInst.REVVY__ApprovalObjId__c='test';
    insert objMnApprovalInst;
    
    REVVY__MnApprovalStepInst__c objMnApprovalStepInst = new REVVY__MnApprovalStepInst__c();
    
    objMnApprovalStepInst.Name = 'test2';
    objMnApprovalStepInst.REVVY__ApprovalInst__c = objMnApprovalInst.Id;
    objMnApprovalStepInst.REVVY__ApprovalStepDef__c = asd.id;
    objMnApprovalStepInst.REVVY__Step_Sequence__c = 100;
    insert objMnApprovalStepInst;
    
    // REVVY__MnApprovalStepInst__c.objMnApprovalStepInst1 = new  REVVY__MnApprovalStepInst__c();
    
    
    REVVY__MnStepApproverInst__c objMnStepApproverIns = new REVVY__MnStepApproverInst__c ();  //=====4
    objMnStepApproverIns.REVVY__Status__c = 'Pending Approval';
    objMnStepApproverIns.REVVY__AssignedUser__c = objUser.Id;
    objMnStepApproverIns.REVVY__StepInst__c = asi.id;
    insert objMnStepApproverIns;
    
    REVVY__MnStepApproverInst__c objMnStepApproverInst1 = new REVVY__MnStepApproverInst__c ();          
    objMnStepApproverInst1.REVVY__ActionDate__c = System.today();
    objMnStepApproverInst1.Name = 'test';
    objMnStepApproverInst1.REVVY__Status__c =  'NotStarted';
    objMnStepApproverInst1.REVVY__StepInst__c = asi.id;
    insert objMnStepApproverInst1;
    
    ContentVersion cvlist = new Contentversion(); 
    cvlist.Title = 'CZDSTOU'; 
    cvlist.PathOnClient = 'test'; 
    cvlist.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
    List<ContentVersion> cvl = new List<ContentVersion>(); 
    cvl.add(cvlist); 
    insert cvl; 
    
    PageReference pageRef = Page.ReviewCAF;
    pageRef.getParameters().put('Id', String.valueOf(objMnContract.Id));
    Test.setCurrentPage(pageRef);

    ApexPages.CurrentPage().getparameters().put('ContractManagementDashboard','ContractManagementDashboard');
    
    
    ApexPages.StandardController sc = new ApexPages.StandardController(objMnContract);
    CAFApproverDashboardController objCAF = new CAFApproverDashboardController(sc);
    objCAF.contractsearchstring='test search';
    objCAF.displayContractApprovals();
    //objCAF.refresh();
    objCAF.Search();
    objCAF.contractNameASC();
    objCAF.CounterpartyASC();
    objCAF.BusinessunitASC();
    objCAF.contractValueASC();
    objCAF.submissionDateASC();
    objCAF.ApproverASC();
    objCAF.setPendingStatus();
    objCAF.setInprocessStatus();
    objCAF.setCompletedStatus();
    objCAF.setTotalRequests();
    objCAF.getContractDetails();
    objCAF.getContent();
    objCAF.getApprovalDetails();
    objCAF.openManageApprovals();
    //objCAF.goBack();
    objCAF.movePage();
    //objCAF.goForward();
    //objCAF.Newer();
    //objCAF.Older();
    //objCAF.last();
    objCAF.getApprovals();
    objCAF.approveContract();
    objCAF.rejectContract();
    objCAF.reviewContract();
    CAFApproverDashboardController.getContractModal(objMnContract.id);
    CAFApproverDashboardController.getContentModal(cvl[0].id);
    CAFApproverDashboardController.getApprovalLogModal(objMnContract.id);
    
    CAFApproverDashboardController.Approvals objApprov = new CAFApproverDashboardController.Approvals(objMnContract.id,'Test contract','Active','test',1000,'Approve','test',system.now(),'check');
    Test.stopTest();
    }
    
@isTest
private static void testMethod2(){
    Test.startTest();
    
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
    
    User objUser = new user();
    //objUser.Name = 'test';
    objUser.UserName = 'asd.test@gmail.com';
    objUser.LastName = 'lName';
    objUser.Email = 'abc@bh.com';
    objUser.Alias = 'tsamp';
    objUser.CommunityNickname = 'ghjk';
    objUser.TimeZoneSidKey = 'America/Los_Angeles';
    objUser.LocaleSidKey = 'en_US';
    objUser.EmailEncodingKey = 'UTF-8';
    objUser.ProfileId = p.Id;
    objUser.LanguageLocaleKey = 'en_US';
    
    insert objUser;
    Account acc = new account(name = 'test');
        insert acc;
        
        Contact con = new contact(LastName = 'Test', AccountId = acc.id);
        insert con;
        
        Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
        
        REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
        lifeCycle.Name = 'Test Lifecycle';
        lifeCycle.REVVY__ApplyTo__c = 'Original';
        lifeCycle.REVVY__Object__c = 'MnContract__c';
        lifeCycle.REVVY__RecordType__c = 'Request_CAF';
        lifeCycle.REVVY__Status__c = 'Draft';
        insert lifeCycle;
        
        REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
        stage.name = 'Test';
        stage.REVVY__LifeCycle__c = lifeCycle.Id;
        stage.REVVY__Phase__c = 'Draft';
        stage.REVVY__SubPhase__c = 'Draft';
        stage.REVVY__Editable__c = true;
        insert stage;
        
        lifeCycle.REVVY__InitialStage__c = stage.Id;
        lifeCycle.REVVY__Status__c = 'Active';
        update lifeCycle;
        
        REVVY__MnContract__c objMnContract = new REVVY__MnContract__c(Name='Test contract',REVVY__Account__c = acc.id, recordTypeId=recTypeId,
                                                           REVVY__Contact__c = con.id,Contract_Value__c=80000,REVVY__LifecycleConfig__c = lifeCycle.Id);
        insert objMnContract;
        
        REVVY__MnApprovalDef__c mad = new REVVY__MnApprovalDef__c(REVVY__MainObject__c = 'REVVY__MnContract__c',REVVY__Order__c = 100);
        insert mad;
        
        REVVY__MnApprovalInst__c mai = new REVVY__MnApprovalInst__c(REVVY__ApprovalDef__c = mad.id, REVVY__ApprovalObjId__c = 'Text');
        insert mai;
        
        REVVY__MnApprovalStepDef__c asd = new REVVY__MnApprovalStepDef__c(REVVY__ApprovalDef__c = mad.id, REVVY__StepSequence__c = 100);
        insert asd;
        
        REVVY__MnApprovalStepInst__c asi = new REVVY__MnApprovalStepInst__c(REVVY__ApprovalInst__c = mai.id, REVVY__ApprovalStepDef__c = asd.id, REVVY__Step_Sequence__c = 100,
                                                                           REVVY__StepStatus__c='Pending Approval',REVVY__IsParentStep__c = false);
        insert asi;
 
    REVVY__MnApprovalInst__c objMnApprovalInst = new REVVY__MnApprovalInst__c();
    objMnApprovalInst.REVVY__Contract__c = objMnContract.Id;
    // objMnApprovalInst.REVVY__AssignedUser__c = objUser.Id;
    objMnApprovalInst.REVVY__ApprovalDef__c = mad.id;
    objMnApprovalInst.REVVY__ApprovalObjId__c='test';
    insert objMnApprovalInst;
    
    REVVY__MnApprovalStepInst__c objMnApprovalStepInst = new REVVY__MnApprovalStepInst__c();
    
    objMnApprovalStepInst.Name = 'test2';
    objMnApprovalStepInst.REVVY__ApprovalInst__c = objMnApprovalInst.Id;
    objMnApprovalStepInst.REVVY__ApprovalStepDef__c = asd.id;
    objMnApprovalStepInst.REVVY__Step_Sequence__c = 100;
    insert objMnApprovalStepInst;
    
    // REVVY__MnApprovalStepInst__c.objMnApprovalStepInst1 = new  REVVY__MnApprovalStepInst__c();
    
    
    REVVY__MnStepApproverInst__c objMnStepApproverIns = new REVVY__MnStepApproverInst__c ();  //=====4
    objMnStepApproverIns.REVVY__Status__c = 'Pending Approval';
    objMnStepApproverIns.REVVY__AssignedUser__c = objUser.Id;
    objMnStepApproverIns.REVVY__StepInst__c = asi.id;
    insert objMnStepApproverIns;
    
    REVVY__MnStepApproverInst__c objMnStepApproverInst1 = new REVVY__MnStepApproverInst__c ();          
    objMnStepApproverInst1.REVVY__ActionDate__c = System.today();
    objMnStepApproverInst1.Name = 'test';
    objMnStepApproverInst1.REVVY__Status__c =  'NotStarted';
    objMnStepApproverInst1.REVVY__StepInst__c = asi.id;
    insert objMnStepApproverInst1;
    
    ContentVersion cvlist = new Contentversion(); 
    cvlist.Title = 'CZDSTOU'; 
    cvlist.PathOnClient = 'test'; 
    cvlist.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
    List<ContentVersion> cvl = new List<ContentVersion>(); 
    cvl.add(cvlist); 
    insert cvl; 
    
    PageReference pageRef = Page.ReviewCAF;
    pageRef.getParameters().put('Id', String.valueOf(objMnContract.Id));
    Test.setCurrentPage(pageRef);

    ApexPages.CurrentPage().getparameters().put('ContractRequesterDashboard','ContractRequesterDashboard');
    
    
    ApexPages.StandardController sc = new ApexPages.StandardController(objMnContract);
    CAFApproverDashboardController objCAF = new CAFApproverDashboardController(sc);
    objCAF.contractsearchstring='test search';
    objCAF.displayContractApprovals();
    objCAF.contractNameASC();
    objCAF.Search();
    objCAF.CounterpartyASC();
    objCAF.BusinessunitASC();
    objCAF.contractValueASC();
    objCAF.submissionDateASC();
    objCAF.ApproverASC();
    objCAF.setPendingStatus();
    objCAF.setInprocessStatus();
    objCAF.setCompletedStatus();
    objCAF.setTotalRequests();
    objCAF.getContractDetails();
    objCAF.getContent();
    objCAF.getApprovalDetails();
    objCAF.openManageApprovals();
        Test.stopTest();
    }
    
    @isTest
private static void testMethod3(){
    Test.startTest();
    
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
    
    User objUser = new user();
    //objUser.Name = 'test';
    objUser.UserName = 'asd.test@gmail.com';
    objUser.LastName = 'lName';
    objUser.Email = 'abc@bh.com';
    objUser.Alias = 'tsamp';
    objUser.CommunityNickname = 'ghjk';
    objUser.TimeZoneSidKey = 'America/Los_Angeles';
    objUser.LocaleSidKey = 'en_US';
    objUser.EmailEncodingKey = 'UTF-8';
    objUser.ProfileId = p.Id;
    objUser.LanguageLocaleKey = 'en_US';
    
    insert objUser;
    Account acc = new account(name = 'test');
        insert acc;
        
        Contact con = new contact(LastName = 'Test', AccountId = acc.id);
        insert con;
        
        Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
        
        REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
        lifeCycle.Name = 'Test Lifecycle';
        lifeCycle.REVVY__ApplyTo__c = 'Original';
        lifeCycle.REVVY__Object__c = 'MnContract__c';
        lifeCycle.REVVY__RecordType__c = 'Request_CAF';
        lifeCycle.REVVY__Status__c = 'Draft';
        insert lifeCycle;
        
        REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
        stage.name = 'Test';
        stage.REVVY__LifeCycle__c = lifeCycle.Id;
        stage.REVVY__Phase__c = 'Draft';
        stage.REVVY__SubPhase__c = 'Draft';
        stage.REVVY__Editable__c = true;
        insert stage;
        
        lifeCycle.REVVY__InitialStage__c = stage.Id;
        lifeCycle.REVVY__Status__c = 'Active';
        update lifeCycle;
        
        REVVY__MnContract__c objMnContract = new REVVY__MnContract__c(Name='Test contract',REVVY__Account__c = acc.id, recordTypeId=recTypeId,
                                                           REVVY__Contact__c = con.id,Contract_Value__c=80000,REVVY__LifecycleConfig__c = lifeCycle.Id);
        insert objMnContract;
        
        REVVY__MnApprovalDef__c mad = new REVVY__MnApprovalDef__c(REVVY__MainObject__c = 'REVVY__MnContract__c',REVVY__Order__c = 100);
        insert mad;
        
        REVVY__MnApprovalInst__c mai = new REVVY__MnApprovalInst__c(REVVY__ApprovalDef__c = mad.id, REVVY__ApprovalObjId__c = 'Text');
        insert mai;
        
        REVVY__MnApprovalStepDef__c asd = new REVVY__MnApprovalStepDef__c(REVVY__ApprovalDef__c = mad.id, REVVY__StepSequence__c = 100);
        insert asd;
        
        REVVY__MnApprovalStepInst__c asi = new REVVY__MnApprovalStepInst__c(REVVY__ApprovalInst__c = mai.id, REVVY__ApprovalStepDef__c = asd.id, REVVY__Step_Sequence__c = 100,
                                                                           REVVY__StepStatus__c='Pending Approval',REVVY__IsParentStep__c = false);
        insert asi;
 
    REVVY__MnApprovalInst__c objMnApprovalInst = new REVVY__MnApprovalInst__c();
    objMnApprovalInst.REVVY__Contract__c = objMnContract.Id;
    // objMnApprovalInst.REVVY__AssignedUser__c = objUser.Id;
    objMnApprovalInst.REVVY__ApprovalDef__c = mad.id;
    objMnApprovalInst.REVVY__ApprovalObjId__c='test';
    insert objMnApprovalInst;
    
    REVVY__MnApprovalStepInst__c objMnApprovalStepInst = new REVVY__MnApprovalStepInst__c();
    
    objMnApprovalStepInst.Name = 'test2';
    objMnApprovalStepInst.REVVY__ApprovalInst__c = objMnApprovalInst.Id;
    objMnApprovalStepInst.REVVY__ApprovalStepDef__c = asd.id;
    objMnApprovalStepInst.REVVY__Step_Sequence__c = 100;
    insert objMnApprovalStepInst;
    
    // REVVY__MnApprovalStepInst__c.objMnApprovalStepInst1 = new  REVVY__MnApprovalStepInst__c();
    
    
    REVVY__MnStepApproverInst__c objMnStepApproverIns = new REVVY__MnStepApproverInst__c ();  //=====4
    objMnStepApproverIns.REVVY__Status__c = 'Pending Approval';
    objMnStepApproverIns.REVVY__AssignedUser__c = objUser.Id;
    objMnStepApproverIns.REVVY__StepInst__c = asi.id;
    insert objMnStepApproverIns;
    
    REVVY__MnStepApproverInst__c objMnStepApproverInst1 = new REVVY__MnStepApproverInst__c ();          
    objMnStepApproverInst1.REVVY__ActionDate__c = System.today();
    objMnStepApproverInst1.Name = 'test';
    objMnStepApproverInst1.REVVY__Status__c =  'NotStarted';
    objMnStepApproverInst1.REVVY__StepInst__c = asi.id;
    insert objMnStepApproverInst1;
    
    ContentVersion cvlist = new Contentversion(); 
    cvlist.Title = 'CZDSTOU'; 
    cvlist.PathOnClient = 'test'; 
    cvlist.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
    List<ContentVersion> cvl = new List<ContentVersion>(); 
    cvl.add(cvlist); 
    insert cvl; 
    
    PageReference pageRef = Page.ReviewCAF;
    pageRef.getParameters().put('Id', String.valueOf(objMnContract.Id));
    Test.setCurrentPage(pageRef);

    ApexPages.CurrentPage().getparameters().put('ContractAdminDashboard','ContractAdminDashboard');
    
    
    ApexPages.StandardController sc = new ApexPages.StandardController(objMnContract);
    CAFApproverDashboardController objCAF = new CAFApproverDashboardController(sc);
    objCAF.contractsearchstring='test search';
    objCAF.displayContractApprovals();
    
    objCAF.Search();
    objCAF.contractNameASC();
    objCAF.CounterpartyASC();
    objCAF.BusinessunitASC();
    objCAF.contractValueASC();
    objCAF.submissionDateASC();
    objCAF.ApproverASC();
    objCAF.setPendingStatus();
    objCAF.setInprocessStatus();
    objCAF.setCompletedStatus();
    objCAF.setTotalRequests();
    objCAF.getContractDetails();
    objCAF.getContent();
    objCAF.getApprovalDetails();
    objCAF.openManageApprovals();
        Test.stopTest();
    }
    
    @isTest
private static void testMethod4(){
    Test.startTest();
    
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
    
    User objUser = new user();
    //objUser.Name = 'test';
    objUser.UserName = 'asd.test@gmail.com';
    objUser.LastName = 'lName';
    objUser.Email = 'abc@bh.com';
    objUser.Alias = 'tsamp';
    objUser.CommunityNickname = 'ghjk';
    objUser.TimeZoneSidKey = 'America/Los_Angeles';
    objUser.LocaleSidKey = 'en_US';
    objUser.EmailEncodingKey = 'UTF-8';
    objUser.ProfileId = p.Id;
    objUser.LanguageLocaleKey = 'en_US';
    
    insert objUser;
    Account acc = new account(name = 'test');
        insert acc;
        
        Contact con = new contact(LastName = 'Test', AccountId = acc.id);
        insert con;
        
        Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
        
        REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
        lifeCycle.Name = 'Test Lifecycle';
        lifeCycle.REVVY__ApplyTo__c = 'Original';
        lifeCycle.REVVY__Object__c = 'MnContract__c';
        lifeCycle.REVVY__RecordType__c = 'Request_CAF';
        lifeCycle.REVVY__Status__c = 'Draft';
        insert lifeCycle;
        
        REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
        stage.name = 'Test';
        stage.REVVY__LifeCycle__c = lifeCycle.Id;
        stage.REVVY__Phase__c = 'Draft';
        stage.REVVY__SubPhase__c = 'Draft';
        stage.REVVY__Editable__c = true;
        insert stage;
        
        lifeCycle.REVVY__InitialStage__c = stage.Id;
        lifeCycle.REVVY__Status__c = 'Active';
        update lifeCycle;
        
        REVVY__MnContract__c objMnContract = new REVVY__MnContract__c(Name='Test contract',REVVY__Account__c = acc.id, recordTypeId=recTypeId,
                                                           REVVY__Contact__c = con.id,Contract_Value__c=80000,REVVY__LifecycleConfig__c = lifeCycle.Id);
        insert objMnContract;
        
        REVVY__MnApprovalDef__c mad = new REVVY__MnApprovalDef__c(REVVY__MainObject__c = 'REVVY__MnContract__c',REVVY__Order__c = 100);
        insert mad;
        
        REVVY__MnApprovalInst__c mai = new REVVY__MnApprovalInst__c(REVVY__ApprovalDef__c = mad.id, REVVY__ApprovalObjId__c = 'Text');
        insert mai;
        
        REVVY__MnApprovalStepDef__c asd = new REVVY__MnApprovalStepDef__c(REVVY__ApprovalDef__c = mad.id, REVVY__StepSequence__c = 100);
        insert asd;
        
        REVVY__MnApprovalStepInst__c asi = new REVVY__MnApprovalStepInst__c(REVVY__ApprovalInst__c = mai.id, REVVY__ApprovalStepDef__c = asd.id, REVVY__Step_Sequence__c = 100,
                                                                           REVVY__StepStatus__c='Pending Approval',REVVY__IsParentStep__c = false);
        insert asi;
 
    REVVY__MnApprovalInst__c objMnApprovalInst = new REVVY__MnApprovalInst__c();
    objMnApprovalInst.REVVY__Contract__c = objMnContract.Id;
    // objMnApprovalInst.REVVY__AssignedUser__c = objUser.Id;
    objMnApprovalInst.REVVY__ApprovalDef__c = mad.id;
    objMnApprovalInst.REVVY__ApprovalObjId__c='test';
    insert objMnApprovalInst;
    
    REVVY__MnApprovalStepInst__c objMnApprovalStepInst = new REVVY__MnApprovalStepInst__c();
    
    objMnApprovalStepInst.Name = 'test2';
    objMnApprovalStepInst.REVVY__ApprovalInst__c = objMnApprovalInst.Id;
    objMnApprovalStepInst.REVVY__ApprovalStepDef__c = asd.id;
    objMnApprovalStepInst.REVVY__Step_Sequence__c = 100;
    insert objMnApprovalStepInst;
    
    // REVVY__MnApprovalStepInst__c.objMnApprovalStepInst1 = new  REVVY__MnApprovalStepInst__c();
    
    
    REVVY__MnStepApproverInst__c objMnStepApproverIns = new REVVY__MnStepApproverInst__c ();  //=====4
    objMnStepApproverIns.REVVY__Status__c = 'Pending Approval';
    objMnStepApproverIns.REVVY__AssignedUser__c = objUser.Id;
    objMnStepApproverIns.REVVY__StepInst__c = asi.id;
    insert objMnStepApproverIns;
    
    REVVY__MnStepApproverInst__c objMnStepApproverInst1 = new REVVY__MnStepApproverInst__c ();          
    objMnStepApproverInst1.REVVY__ActionDate__c = System.today();
    objMnStepApproverInst1.Name = 'test';
    objMnStepApproverInst1.REVVY__Status__c =  'NotStarted';
    objMnStepApproverInst1.REVVY__StepInst__c = asi.id;
    insert objMnStepApproverInst1;
    
    ContentVersion cvlist = new Contentversion(); 
    cvlist.Title = 'CZDSTOU'; 
    cvlist.PathOnClient = 'test'; 
    cvlist.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
    List<ContentVersion> cvl = new List<ContentVersion>(); 
    cvl.add(cvlist); 
    insert cvl; 
    
    PageReference pageRef = Page.ReviewCAF;
    pageRef.getParameters().put('Id', String.valueOf(objMnContract.Id));
    Test.setCurrentPage(pageRef);

    ApexPages.CurrentPage().getparameters().put('ContractApprovalDashboard','ContractApprovalDashboard');
    
    
    ApexPages.StandardController sc = new ApexPages.StandardController(objMnContract);
    CAFApproverDashboardController objCAF = new CAFApproverDashboardController(sc);
    objCAF.contractsearchstring='test search';
    objCAF.displayContractApprovals();
    
    objCAF.Search();
    objCAF.contractNameASC();
    objCAF.CounterpartyASC();
    objCAF.BusinessunitASC();
    objCAF.contractValueASC();
    objCAF.submissionDateASC();
    objCAF.ApproverASC();
    objCAF.setPendingStatus();
    objCAF.setInprocessStatus();
    objCAF.setCompletedStatus();
    objCAF.setTotalRequests();
    objCAF.getContractDetails();
    objCAF.getContent();
    objCAF.getApprovalDetails();
    objCAF.openManageApprovals();
        Test.stopTest();
    }
    
}