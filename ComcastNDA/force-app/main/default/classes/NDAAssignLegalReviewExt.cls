/*
Name             : NDAAssignLegalReviewExt
Author           : Narveer S.
Date             : 03/01/2019
Description      : Legal Review Assign Page
*/
public class NDAAssignLegalReviewExt {
    
    private ApexPages.StandardController controller;
    public List<NDA__c> listNDARecs{get;set;}
    public Id ownerId{get;set;}
    public String ownerName{get;set;}
    public String assignmentName{get;set;}
    public NDAAssignLegalReviewExt(ApexPages.StandardController controller) {
        this.controller = controller;
        listNDARecs = new List<NDA__c>();
        if(controller!=null && controller.getId()!=null){
            List<NDA__c> selectedNDARec = [Select ID,Name,Attorney__c,Assigned_Attorney__c,AttorneyContact__c,NDA_Status__c,Status_Update_Comment__c from NDA__c where id = : controller.getId()] ;
            for(NDA__c objNDA: selectedNDARec) {
                listNDARecs.add(objNDA);
            }
        }
    }
    public List<SelectOption> getdynamiclist() {
        List<SelectOption> AttorneyList = new List<SelectOption>();
        AttorneyList.add(new SelectOption('','None'));
        for( User usr: [SELECT Name, Id FROM USER WHERE Id IN (SELECT UserOrGroupId FROM GroupMember where group.DeveloperName='NDA_Legal_Reviewer')])
        {
            AttorneyList.add(new SelectOption(usr.Id,usr.Name));
        }       
        return AttorneyList; 
    }
    
    public PageReference save() {
        for(NDA__c objNDARec: listNDARecs){
            objNDARec.NDA_Status__c = 'Legal Review';
        }
        update listNDARecs;
        if(controller!=null){
            return new PageReference('/'+listNDARecs[0].id);
        }else {
            return null;
        }
    }
    
    @RemoteAction
    public static List<sObject> getLookupRecord(String likeString , String objectName) {
        if(String.IsNotBlank(likeString) && String.IsNotBlank(objectName)){
            likeString = '%'+likeString+'%';
            String query = '';
            String typeString = 'Queue';
            if(objectName == 'Group'){
                query = 'SELECT Id, Name FROM '+objectName+' WHERE Name LIKE : likeString AND Type =: typeString LIMIT 50';
            }else{
                query = 'SELECT Id, Name FROM '+objectName+' WHERE Name LIKE : likeString LIMIT 50';
            }
            
            System.debug(query);
            return database.query(query);
        }
        return null;
    }    
}