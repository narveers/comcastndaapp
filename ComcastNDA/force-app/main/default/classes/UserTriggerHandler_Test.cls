@isTest
public class UserTriggerHandler_Test {
	static testMethod void testUserCreation(){
        Test.startTest();
        
        User usr = new User();
        usr.username ='test@test.com.full.cmentor9';
        usr.LASTNAME ='User';
        usr.FIRSTNAME ='Test';
        usr.ALIAS ='test';
        usr.COMMUNITYNICKNAME ='test';
        usr.EMAIL ='testr@test.com.cmentor9';
        usr.FEDERATIONIDENTIFIER ='test@test.com.cmentor9';
        usr.COUNTRY ='US';
        usr.COMPANYNAME ='Test';
        usr.ISACTIVE =TRUE;
        usr.TIMEZONESIDKEY ='America/New_York';
        usr.LOCALESIDKEY ='en_US';
        usr.EMAILENCODINGKEY ='ISO-8859-1';
        usr.LANGUAGELOCALEKEY ='en_US';
        usr.Profile_Name__c ='CLA Legal';
        usr.Role_Name__c ='Cable Ops';
        
        insert usr;
        
        Test.stopTest();
    }
}