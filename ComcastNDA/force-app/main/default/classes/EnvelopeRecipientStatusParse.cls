public class EnvelopeRecipientStatusParse {

    public List<Signers> signers;
    public List<Agents> agents;
    public List<Agents> editors;
    public List<Agents> intermediaries;
    public List<Agents> carbonCopies;
    public List<Agents> certifiedDeliveries;
    public List<Agents> inPersonSigners;
    public List<Agents> seals;
    public List<Agents> witnesses;
    public String recipientCount;
    public String currentRoutingOrder;

    public class Signers {
        public String creationReason;
        public String isBulkRecipient;
        public String name;
        public String email;
        public String recipientId;
        public String recipientIdGuid;
        public String requireIdLookup;
        public String userId;
        public String routingOrder;
        public String status;
        public String signedDateTime;
        public String deliveredDateTime;
        public String recipientSuppliesTabs;
    }

    public class Agents {
    }

    
    public static EnvelopeRecipientStatusParse parse(String json) {
        return (EnvelopeRecipientStatusParse) System.JSON.deserialize(json, EnvelopeRecipientStatusParse.class);
    }
}