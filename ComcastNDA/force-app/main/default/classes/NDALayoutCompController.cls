/*
Name             : NDALayoutCompController
Author           : Narveer S.
Date             : 02/28/2019
Description      : NDALayoutCompController for Buttons on NDA record
*/
public class NDALayoutCompController {
    
    @AuraEnabled
    public static Boolean getIsADMGroupMember() {
        Boolean ResultBoolean = False;
        try{
            Group gr = [SELECT id FROM Group WHERE developername='NDA_Admin'];
            for(GroupMember gm: [SELECT groupId FROM GroupMember WHERE UserOrGroupId=:UserInfo.getUserId()]){
                if(gm.GroupId == gr.Id){
                    ResultBoolean = true;
                    break;
                }
            }
            return ResultBoolean;
        }
        Catch(Exception ex){
            system.debug('error - '+ex.getStackTraceString() +' msg -'+ex.getMessage());
            return false;
        }
    }
    
    @AuraEnabled
    public static Boolean getIsLegalRGroupMember() {
        Boolean ResultBoolean = False;
        try{
            Group gr = [SELECT id FROM Group WHERE developername='NDA_Legal_Reviewer'];
            for(GroupMember gm: [SELECT groupId FROM GroupMember WHERE UserOrGroupId=:UserInfo.getUserId()]){
                if(gm.GroupId == gr.Id){
                    ResultBoolean = true;
                    break;
                }
            }
            return ResultBoolean;
        }
        Catch(Exception ex){
            system.debug('error - '+ex.getStackTraceString() +' msg -'+ex.getMessage());
            return false;
        }
    }
    
    @AuraEnabled
    public static String getNDAStatus(Id ContractId){
        NDA__c objNDA;
        String ResultString = NULL;
        if(String.isNotBlank(ContractId)){
          objNDA = [SELECT NDA_Status__c FROM NDA__c WHERE Id=:ContractId];
          ResultString = objNDA.NDA_Status__c;
        }
        System.debug(ResultString);
        return ResultString;
    }
    @AuraEnabled
    public static String setDocuSignUrl(Id NDAId) {
        String docuSignUrl;
        
        NDA__c objNDA = [Select Id,Comcast_Signer_Email__c,NBCU_Signer_Name__c FROM NDA__c WHERE ID =: NDAId LIMIT 1];
        System.debug(objNDA);
        List<ContentVersion> lstContentVersion = [Select id,title,FirstPublishLocationId,ContentDocumentId from ContentVersion where FirstPublishLocationId =: NDAId AND DocuSign_Required__c = True LIMIT 1];
        
        docuSignUrl = '/apex/dsfs__DocuSign_CreateEnvelope?DSEID='+0+'&SourceID='+NDAId+'&CRL=';
        
        
        docuSignUrl += 'Email~'+objNDA.Comcast_Signer_Email__c+';Role~Signer1;FirstName~'+objNDA.NBCU_Signer_Name__c+';RoutingOrder~1;';
        docuSignUrl += 'SignNow~1,LoadDefaultContacts~1&CCRM=Decision Maker~Signer 1&CCTM=Decision Maker~Signer&DST=&LA=0&LF=0&OCO=Tag&LF=0&FILES='+lstContentVersion[0].ContentDocumentId;
        System.debug('docuSignUrl'+ docuSignUrl);
        return docuSignUrl;
    }
    /*
    @AuraEnabled
    public static Boolean getIsAGroupMember(){
        Boolean ResultBoolean = False;
        try{
            Group gr = [SELECT id FROM Group WHERE developername='CI_Attorney'];
            for(GroupMember gm: [SELECT groupId FROM GroupMember WHERE UserOrGroupId=:UserInfo.getUserId()]){
                if(gm.GroupId == gr.Id){
                    ResultBoolean = true;
                    break;
                }
            }
            return ResultBoolean;
        }
        Catch(Exception ex){
            system.debug('error - '+ex.getStackTraceString() +' msg -'+ex.getMessage());
            return false;
        }
    }
    
    @AuraEnabled
    public static String getContractStatus(Id ContractId){
        Contract__c contract;
        String ResultString = NULL;
        if(String.isNotBlank(ContractId)){
          contract = [SELECT Contract_Intake_Status__c FROM Contract__c WHERE Id=:ContractId];
          ResultString = contract.Contract_Intake_Status__c;
        }
        return ResultString;
    }
    @AuraEnabled
    public static String Mailsendmethod(Id recordId) {
        System.debug('REC**'+recordId);
        try{
            EmailServicesAddress EmailService = [SELECT EmailDomainName,LocalPart FROM EmailServicesAddress WHERE DeveloperName ='AssignedToBU'];
            String emailStr = 'mailto:?cc='+ EmailService.LocalPart+'@'+EmailService.EmailDomainName;
            Contract__c Contract = [Select Id,Contract_Intake_Status__c,Name,Requester_Name__c,Status_Update__c,
                                    LastModifiedBy.Name,Reason_Of_Rejection__c FROM Contract__c WHERE Id =: recordId];
            system.debug('Contract - '+Contract);
            if(Contract!=null) {
                emailStr += '&subject=Re:Legal Review Complete ' + Contract.Name +' ref: '+recordId;
                emailStr += '&body=Dear '+Contract.Requester_Name__c+',%0A%0A';
                emailStr +='The Cable Law Department has completed its review of your contract request. Please review the comments below for additional information. The Law Department member who assisted you will deliver the contract to you via email.%0A';
                emailStr +=Contract.LastModifiedBy.Name+':%0A%0A%0A'+Contract.Reason_Of_Rejection__c;
                emailStr += '[NOTE: do not remove the text in the CC line.]';
                emailStr +='You must now obtain the required approvals before execution of the contract for approval. Please review the Contract Standards and Approval Policy for approval requirements.%0A%0A</body></html>';
                System.debug(emailStr);
            }
            return emailStr; 
        }
        Catch(Exception ex){
            return ex.getMessage()+'%0A'+ex.getStackTraceString(); 
        }    
    }
    @AuraEnabled
    public static String MailsendmethodContractRequester(Id recordId) {
        System.debug('REC**'+recordId);
        try{
            EmailServicesAddress EmailService = [SELECT EmailDomainName,LocalPart FROM EmailServicesAddress WHERE DeveloperName ='RequesterContactedEmail'];
            String emailStr = 'mailto:?cc='+ EmailService.LocalPart+'@'+EmailService.EmailDomainName;
            Contract__c Contract = [Select Id,Contract_Intake_Status__c,Name,Requester_Name__c,Status_Update__c,
                                    LastModifiedBy.Name FROM Contract__c WHERE Id =: recordId];
            if(Contract != Null) {
              Contract.Contract_Intake_Status__c = 'Requester Contacted';
              update Contract;    
            }
            system.debug('Contract - '+Contract);
            if(Contract!=null) {
                emailStr += '&subject=Re:Requester Contacted ' + Contract.Name +' ref: '+recordId;
                emailStr += '&body=Dear '+Contract.Requester_Name__c+',%0A%0A';
                emailStr +='[Insert your message here. NOTE: do not remove the text in the CC line.]';
                System.debug(emailStr);
            }
            return emailStr; 
        }
        Catch(Exception ex){
            return ex.getMessage()+'%0A'+ex.getStackTraceString(); 
        }    
    }
    @AuraEnabled
    public static String methodResumeLegalReview(Id recordId) {
        System.debug('REC**'+recordId);
        try{
            Contract__c Contract = [Select Id,Contract_Intake_Status__c,Name,Requester_Name__c,Status_Update__c,
                                    LastModifiedBy.Name FROM Contract__c WHERE Id =: recordId];
            if(Contract != Null) {
              Contract.Contract_Intake_Status__c = 'Legal Review';
              update Contract;    
            }
            return null; 
        }
        Catch(Exception ex){
            return ex.getMessage()+'%0A'+ex.getStackTraceString(); 
        }    
    }
    */
}