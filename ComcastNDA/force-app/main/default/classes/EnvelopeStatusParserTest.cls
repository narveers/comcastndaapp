@IsTest
public class EnvelopeStatusParserTest {
    
    static testMethod void testParse() {
        String json = '{'+
        '    \"resultSetSize\": \"17\",'+
        '    \"totalSetSize\": \"17\",'+
        '    \"startPosition\": \"0\",'+
        '    \"endPosition\": \"16\",'+
        '    \"nextUri\": \"\",'+
        '    \"previousUri\": \"\",'+
        '    \"envelopes\": ['+
        '        {'+
        '            \"status\": \"sent\",'+
        '            \"documentsUri\": \"/envelopes/1c80593d-171a-4017-a4ab-7c6274a519b2/documents\",'+
        '            \"recipientsUri\": \"/envelopes/1c80593d-171a-4017-a4ab-7c6274a519b2/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/1c80593d-171a-4017-a4ab-7c6274a519b2/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/1c80593d-171a-4017-a4ab-7c6274a519b2\",'+
        '            \"envelopeId\": \"1c80593d-171a-4017-a4ab-7c6274a519b2\",'+
        '            \"customFieldsUri\": \"/envelopes/1c80593d-171a-4017-a4ab-7c6274a519b2/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/1c80593d-171a-4017-a4ab-7c6274a519b2/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T11:53:01.8670000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/1c80593d-171a-4017-a4ab-7c6274a519b2/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/1c80593d-171a-4017-a4ab-7c6274a519b2/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/1c80593d-171a-4017-a4ab-7c6274a519b2/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"completed\",'+
        '            \"documentsUri\": \"/envelopes/04749ef4-3791-470c-8d42-0cff7ad815c5/documents\",'+
        '            \"recipientsUri\": \"/envelopes/04749ef4-3791-470c-8d42-0cff7ad815c5/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/04749ef4-3791-470c-8d42-0cff7ad815c5/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/04749ef4-3791-470c-8d42-0cff7ad815c5\",'+
        '            \"envelopeId\": \"04749ef4-3791-470c-8d42-0cff7ad815c5\",'+
        '            \"customFieldsUri\": \"/envelopes/04749ef4-3791-470c-8d42-0cff7ad815c5/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/04749ef4-3791-470c-8d42-0cff7ad815c5/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T13:15:52.1730000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/04749ef4-3791-470c-8d42-0cff7ad815c5/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/04749ef4-3791-470c-8d42-0cff7ad815c5/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/04749ef4-3791-470c-8d42-0cff7ad815c5/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"completed\",'+
        '            \"documentsUri\": \"/envelopes/9ec984d2-b472-4aef-9f24-5a511fd94a6c/documents\",'+
        '            \"recipientsUri\": \"/envelopes/9ec984d2-b472-4aef-9f24-5a511fd94a6c/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/9ec984d2-b472-4aef-9f24-5a511fd94a6c/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/9ec984d2-b472-4aef-9f24-5a511fd94a6c\",'+
        '            \"envelopeId\": \"9ec984d2-b472-4aef-9f24-5a511fd94a6c\",'+
        '            \"customFieldsUri\": \"/envelopes/9ec984d2-b472-4aef-9f24-5a511fd94a6c/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/9ec984d2-b472-4aef-9f24-5a511fd94a6c/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T13:55:19.1870000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/9ec984d2-b472-4aef-9f24-5a511fd94a6c/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/9ec984d2-b472-4aef-9f24-5a511fd94a6c/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/9ec984d2-b472-4aef-9f24-5a511fd94a6c/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"sent\",'+
        '            \"documentsUri\": \"/envelopes/71ef4ec1-2db7-4824-947e-ceadb9198ba3/documents\",'+
        '            \"recipientsUri\": \"/envelopes/71ef4ec1-2db7-4824-947e-ceadb9198ba3/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/71ef4ec1-2db7-4824-947e-ceadb9198ba3/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/71ef4ec1-2db7-4824-947e-ceadb9198ba3\",'+
        '            \"envelopeId\": \"71ef4ec1-2db7-4824-947e-ceadb9198ba3\",'+
        '            \"customFieldsUri\": \"/envelopes/71ef4ec1-2db7-4824-947e-ceadb9198ba3/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/71ef4ec1-2db7-4824-947e-ceadb9198ba3/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T15:59:49.7730000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/71ef4ec1-2db7-4824-947e-ceadb9198ba3/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/71ef4ec1-2db7-4824-947e-ceadb9198ba3/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/71ef4ec1-2db7-4824-947e-ceadb9198ba3/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"sent\",'+
        '            \"documentsUri\": \"/envelopes/4bf5ee9d-7ca1-482d-a993-a7c5a0c8782d/documents\",'+
        '            \"recipientsUri\": \"/envelopes/4bf5ee9d-7ca1-482d-a993-a7c5a0c8782d/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/4bf5ee9d-7ca1-482d-a993-a7c5a0c8782d/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/4bf5ee9d-7ca1-482d-a993-a7c5a0c8782d\",'+
        '            \"envelopeId\": \"4bf5ee9d-7ca1-482d-a993-a7c5a0c8782d\",'+
        '            \"customFieldsUri\": \"/envelopes/4bf5ee9d-7ca1-482d-a993-a7c5a0c8782d/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/4bf5ee9d-7ca1-482d-a993-a7c5a0c8782d/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T16:01:58.9170000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/4bf5ee9d-7ca1-482d-a993-a7c5a0c8782d/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/4bf5ee9d-7ca1-482d-a993-a7c5a0c8782d/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/4bf5ee9d-7ca1-482d-a993-a7c5a0c8782d/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"sent\",'+
        '            \"documentsUri\": \"/envelopes/3e993166-51bd-44a7-b7b1-6edffd2195d5/documents\",'+
        '            \"recipientsUri\": \"/envelopes/3e993166-51bd-44a7-b7b1-6edffd2195d5/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/3e993166-51bd-44a7-b7b1-6edffd2195d5/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/3e993166-51bd-44a7-b7b1-6edffd2195d5\",'+
        '            \"envelopeId\": \"3e993166-51bd-44a7-b7b1-6edffd2195d5\",'+
        '            \"customFieldsUri\": \"/envelopes/3e993166-51bd-44a7-b7b1-6edffd2195d5/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/3e993166-51bd-44a7-b7b1-6edffd2195d5/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T16:11:22.8400000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/3e993166-51bd-44a7-b7b1-6edffd2195d5/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/3e993166-51bd-44a7-b7b1-6edffd2195d5/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/3e993166-51bd-44a7-b7b1-6edffd2195d5/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"sent\",'+
        '            \"documentsUri\": \"/envelopes/17a9435d-a243-4975-a0ce-3cf9ff916e2c/documents\",'+
        '            \"recipientsUri\": \"/envelopes/17a9435d-a243-4975-a0ce-3cf9ff916e2c/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/17a9435d-a243-4975-a0ce-3cf9ff916e2c/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/17a9435d-a243-4975-a0ce-3cf9ff916e2c\",'+
        '            \"envelopeId\": \"17a9435d-a243-4975-a0ce-3cf9ff916e2c\",'+
        '            \"customFieldsUri\": \"/envelopes/17a9435d-a243-4975-a0ce-3cf9ff916e2c/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/17a9435d-a243-4975-a0ce-3cf9ff916e2c/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T16:25:22.1130000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/17a9435d-a243-4975-a0ce-3cf9ff916e2c/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/17a9435d-a243-4975-a0ce-3cf9ff916e2c/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/17a9435d-a243-4975-a0ce-3cf9ff916e2c/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"completed\",'+
        '            \"documentsUri\": \"/envelopes/4ae90484-ec05-4513-a68e-ee68f668cdbf/documents\",'+
        '            \"recipientsUri\": \"/envelopes/4ae90484-ec05-4513-a68e-ee68f668cdbf/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/4ae90484-ec05-4513-a68e-ee68f668cdbf/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/4ae90484-ec05-4513-a68e-ee68f668cdbf\",'+
        '            \"envelopeId\": \"4ae90484-ec05-4513-a68e-ee68f668cdbf\",'+
        '            \"customFieldsUri\": \"/envelopes/4ae90484-ec05-4513-a68e-ee68f668cdbf/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/4ae90484-ec05-4513-a68e-ee68f668cdbf/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T16:35:47.8030000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/4ae90484-ec05-4513-a68e-ee68f668cdbf/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/4ae90484-ec05-4513-a68e-ee68f668cdbf/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/4ae90484-ec05-4513-a68e-ee68f668cdbf/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"voided\",'+
        '            \"documentsUri\": \"/envelopes/241c5790-fe91-40ae-8186-832b3af9e077/documents\",'+
        '            \"recipientsUri\": \"/envelopes/241c5790-fe91-40ae-8186-832b3af9e077/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/241c5790-fe91-40ae-8186-832b3af9e077/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/241c5790-fe91-40ae-8186-832b3af9e077\",'+
        '            \"envelopeId\": \"241c5790-fe91-40ae-8186-832b3af9e077\",'+
        '            \"customFieldsUri\": \"/envelopes/241c5790-fe91-40ae-8186-832b3af9e077/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/241c5790-fe91-40ae-8186-832b3af9e077/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T16:43:46.9070000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/241c5790-fe91-40ae-8186-832b3af9e077/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/241c5790-fe91-40ae-8186-832b3af9e077/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/241c5790-fe91-40ae-8186-832b3af9e077/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"completed\",'+
        '            \"documentsUri\": \"/envelopes/741ec070-a716-450c-978e-793f5b067952/documents\",'+
        '            \"recipientsUri\": \"/envelopes/741ec070-a716-450c-978e-793f5b067952/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/741ec070-a716-450c-978e-793f5b067952/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/741ec070-a716-450c-978e-793f5b067952\",'+
        '            \"envelopeId\": \"741ec070-a716-450c-978e-793f5b067952\",'+
        '            \"customFieldsUri\": \"/envelopes/741ec070-a716-450c-978e-793f5b067952/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/741ec070-a716-450c-978e-793f5b067952/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T16:57:22.1170000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/741ec070-a716-450c-978e-793f5b067952/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/741ec070-a716-450c-978e-793f5b067952/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/741ec070-a716-450c-978e-793f5b067952/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"completed\",'+
        '            \"documentsUri\": \"/envelopes/209d1ce9-16e1-4357-9e01-e50c86ee47d0/documents\",'+
        '            \"recipientsUri\": \"/envelopes/209d1ce9-16e1-4357-9e01-e50c86ee47d0/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/209d1ce9-16e1-4357-9e01-e50c86ee47d0/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/209d1ce9-16e1-4357-9e01-e50c86ee47d0\",'+
        '            \"envelopeId\": \"209d1ce9-16e1-4357-9e01-e50c86ee47d0\",'+
        '            \"customFieldsUri\": \"/envelopes/209d1ce9-16e1-4357-9e01-e50c86ee47d0/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/209d1ce9-16e1-4357-9e01-e50c86ee47d0/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T21:06:45.0430000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/209d1ce9-16e1-4357-9e01-e50c86ee47d0/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/209d1ce9-16e1-4357-9e01-e50c86ee47d0/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/209d1ce9-16e1-4357-9e01-e50c86ee47d0/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"sent\",'+
        '            \"documentsUri\": \"/envelopes/34f8578e-9e72-463c-badb-60f04cd9e73a/documents\",'+
        '            \"recipientsUri\": \"/envelopes/34f8578e-9e72-463c-badb-60f04cd9e73a/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/34f8578e-9e72-463c-badb-60f04cd9e73a/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/34f8578e-9e72-463c-badb-60f04cd9e73a\",'+
        '            \"envelopeId\": \"34f8578e-9e72-463c-badb-60f04cd9e73a\",'+
        '            \"customFieldsUri\": \"/envelopes/34f8578e-9e72-463c-badb-60f04cd9e73a/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/34f8578e-9e72-463c-badb-60f04cd9e73a/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-24T22:11:02.1900000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/34f8578e-9e72-463c-badb-60f04cd9e73a/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/34f8578e-9e72-463c-badb-60f04cd9e73a/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/34f8578e-9e72-463c-badb-60f04cd9e73a/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"voided\",'+
        '            \"documentsUri\": \"/envelopes/b407a489-f95b-4706-9b92-376dc4f13886/documents\",'+
        '            \"recipientsUri\": \"/envelopes/b407a489-f95b-4706-9b92-376dc4f13886/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/b407a489-f95b-4706-9b92-376dc4f13886/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/b407a489-f95b-4706-9b92-376dc4f13886\",'+
        '            \"envelopeId\": \"b407a489-f95b-4706-9b92-376dc4f13886\",'+
        '            \"customFieldsUri\": \"/envelopes/b407a489-f95b-4706-9b92-376dc4f13886/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/b407a489-f95b-4706-9b92-376dc4f13886/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-25T04:46:01.7100000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/b407a489-f95b-4706-9b92-376dc4f13886/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/b407a489-f95b-4706-9b92-376dc4f13886/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/b407a489-f95b-4706-9b92-376dc4f13886/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"completed\",'+
        '            \"documentsUri\": \"/envelopes/b5052989-705f-466e-a180-008b8dbb795c/documents\",'+
        '            \"recipientsUri\": \"/envelopes/b5052989-705f-466e-a180-008b8dbb795c/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/b5052989-705f-466e-a180-008b8dbb795c/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/b5052989-705f-466e-a180-008b8dbb795c\",'+
        '            \"envelopeId\": \"b5052989-705f-466e-a180-008b8dbb795c\",'+
        '            \"customFieldsUri\": \"/envelopes/b5052989-705f-466e-a180-008b8dbb795c/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/b5052989-705f-466e-a180-008b8dbb795c/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-25T11:11:09.0730000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/b5052989-705f-466e-a180-008b8dbb795c/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/b5052989-705f-466e-a180-008b8dbb795c/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/b5052989-705f-466e-a180-008b8dbb795c/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"voided\",'+
        '            \"documentsUri\": \"/envelopes/8a34de56-d14b-4844-b43f-2e54947abe63/documents\",'+
        '            \"recipientsUri\": \"/envelopes/8a34de56-d14b-4844-b43f-2e54947abe63/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/8a34de56-d14b-4844-b43f-2e54947abe63/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/8a34de56-d14b-4844-b43f-2e54947abe63\",'+
        '            \"envelopeId\": \"8a34de56-d14b-4844-b43f-2e54947abe63\",'+
        '            \"customFieldsUri\": \"/envelopes/8a34de56-d14b-4844-b43f-2e54947abe63/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/8a34de56-d14b-4844-b43f-2e54947abe63/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-26T04:51:06.0100000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/8a34de56-d14b-4844-b43f-2e54947abe63/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/8a34de56-d14b-4844-b43f-2e54947abe63/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/8a34de56-d14b-4844-b43f-2e54947abe63/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"completed\",'+
        '            \"documentsUri\": \"/envelopes/b583a240-c6ab-413b-9120-cebdaad19373/documents\",'+
        '            \"recipientsUri\": \"/envelopes/b583a240-c6ab-413b-9120-cebdaad19373/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/b583a240-c6ab-413b-9120-cebdaad19373/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/b583a240-c6ab-413b-9120-cebdaad19373\",'+
        '            \"envelopeId\": \"b583a240-c6ab-413b-9120-cebdaad19373\",'+
        '            \"customFieldsUri\": \"/envelopes/b583a240-c6ab-413b-9120-cebdaad19373/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/b583a240-c6ab-413b-9120-cebdaad19373/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-26T10:52:54.7230000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/b583a240-c6ab-413b-9120-cebdaad19373/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/b583a240-c6ab-413b-9120-cebdaad19373/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/b583a240-c6ab-413b-9120-cebdaad19373/templates\"'+
        '        },'+
        '        {'+
        '            \"status\": \"sent\",'+
        '            \"documentsUri\": \"/envelopes/f43e003a-643e-471b-bb85-d48f83694cd0/documents\",'+
        '            \"recipientsUri\": \"/envelopes/f43e003a-643e-471b-bb85-d48f83694cd0/recipients\",'+
        '            \"attachmentsUri\": \"/envelopes/f43e003a-643e-471b-bb85-d48f83694cd0/attachments\",'+
        '            \"envelopeUri\": \"/envelopes/f43e003a-643e-471b-bb85-d48f83694cd0\",'+
        '            \"envelopeId\": \"f43e003a-643e-471b-bb85-d48f83694cd0\",'+
        '            \"customFieldsUri\": \"/envelopes/f43e003a-643e-471b-bb85-d48f83694cd0/custom_fields\",'+
        '            \"notificationUri\": \"/envelopes/f43e003a-643e-471b-bb85-d48f83694cd0/notification\",'+
        '            \"statusChangedDateTime\": \"2019-05-26T15:52:24.0200000Z\",'+
        '            \"documentsCombinedUri\": \"/envelopes/f43e003a-643e-471b-bb85-d48f83694cd0/documents/combined\",'+
        '            \"certificateUri\": \"/envelopes/f43e003a-643e-471b-bb85-d48f83694cd0/documents/certificate\",'+
        '            \"templatesUri\": \"/envelopes/f43e003a-643e-471b-bb85-d48f83694cd0/templates\"'+
        '        }'+
        '    ]'+
        '}';
        EnvelopeStatusParser obj = EnvelopeStatusParser.parse(json);
        System.assert(obj != null);
    }
}