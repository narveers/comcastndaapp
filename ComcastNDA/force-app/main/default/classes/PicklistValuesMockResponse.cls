@isTest
global class PicklistValuesMockResponse implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        String URL = 'callout:UI_API_Named_Credential/services/data/v41.0/ui-api/object-info/REVVY__MnContract__c/picklist-values/';
        Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
        URL =   URL+recTypeId+'/'+'CAF_Type__c';
        System.assertEquals(URL, req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"values":[{"label":"test"}]}');
        res.setStatusCode(200);
        return res;
  }
}