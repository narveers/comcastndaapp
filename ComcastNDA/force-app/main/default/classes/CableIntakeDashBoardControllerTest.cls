@isTest
public class CableIntakeDashBoardControllerTest {
    @isTest static void Method1(){
        Contract__c con = new Contract__c();
        con.Name = 'Test Contract Request';
        con.Contract_Intake_Status__c = 'Contract Manager';
        con.Requester_Name__c = 'username';
        con.Requester_Email__c = 'legal_help@comcast.com';
        insert con;
        
        PageReference myVfPage = Page.ContractIntakeRequesterDashboard;
        test.setcurrentpage(myVfPage );
        ApexPages.CurrentPage().getparameters().put('ContractIntakeRequesterDashboard','ContractIntakeRequesterDashboard');
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        CableIntakeDashBoardController objCable = new CableIntakeDashBoardController(sc);
        objCable.contractsearchstring='test';
        objCable.getContractDetails();
      //  objCable.displayContractApprovals();
    }
    
    @isTest static void Method2(){
        Contract__c con = new Contract__c();
        con.Name = 'Test Contract Request';
        con.Contract_Intake_Status__c = 'Contract Manager';
        con.Requester_Name__c = 'username';
        con.Requester_Email__c = 'legal_help@comcast.com';
        insert con;
        
        PageReference myVfPage = Page.ContractIntakeRequesterDashboard;
        test.setcurrentpage(myVfPage );
        ApexPages.CurrentPage().getparameters().put('ContractIntakeAttorneyDashboard','ContractIntakeAttorneyDashboard');
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        CableIntakeDashBoardController objCable = new CableIntakeDashBoardController(sc);
        
        objCable.contractsearchstring='test';
        objCable.displayContractApprovals();
        objCable.getApprovals();
        objCable.setPendingStatus();
        objCable.setAssignedStatus();
        objCable.setClosedStatus();
        objCable.setTotalRequests();
        objCable.refresh();
        objCable.Search();
        objCable.contractNameASC();
        objCable.createdDateASC();
        objCable.CounterpartyASC();
        objCable.BusinessunitASC();
        objCable.contractValueASC();
        objCable.procurementvalueASC();
        objCable.assignedattorneyASC();
        objCable.goBack();
        objCable.movePage();
        objCable.goForward();
        objCable.Newer();
        objCable.Older();
        objCable.last();
        CableIntakeDashBoardController.getContentModal(con.id);
        CableIntakeDashBoardController.getContractModal(con.id);
        CableIntakeDashBoardController.getContractHistoryModal(con.id);
    }
    
    @isTest static void Method3(){
        Contract__c con = new Contract__c();
        con.Name = 'Test Contract Request';
        con.Contract_Intake_Status__c = 'Contract Manager';
        con.Requester_Name__c = 'username';
        con.Requester_Email__c = 'legal_help@comcast.com';
        insert con;
        
        PageReference myVfPage = Page.ContractIntakeRequesterDashboard;
        test.setcurrentpage(myVfPage );
        ApexPages.CurrentPage().getparameters().put('ContractIntakeRequesterDashboard','ContractIntakeRequesterDashboard');
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        CableIntakeDashBoardController objCable = new CableIntakeDashBoardController(sc);
        objCable.contractsearchstring='test';
        objCable.displayContractApprovals();
        objCable.getApprovals();
        objCable.setPendingStatus();
        objCable.setAssignedStatus();
        objCable.setClosedStatus();
        objCable.setTotalRequests();
        objCable.refresh();
        objCable.Search();
        objCable.contractNameASC();
        objCable.createdDateASC();
        objCable.CounterpartyASC();
        objCable.BusinessunitASC();
        objCable.contractValueASC();
        objCable.procurementvalueASC();
        objCable.assignedattorneyASC();
        objCable.goBack();
        objCable.movePage();
        objCable.goForward();
        objCable.Newer();
        objCable.Older();
        objCable.last();
        CableIntakeDashBoardController.getContentModal(con.id);
        
    }
    
    @isTest static void Method4(){
        Contract__c con = new Contract__c();
        con.Name = 'Test Contract Request';
        con.Contract_Intake_Status__c = 'Contract Manager';
        con.Requester_Name__c = 'username';
        con.Requester_Email__c = 'legal_help@comcast.com';
        insert con;
        
        PageReference myVfPage = Page.ContractIntakeRequesterDashboard;
        test.setcurrentpage(myVfPage );
        ApexPages.CurrentPage().getparameters().put('ContractIntakeContractManagerDashboard','ContractIntakeContractManagerDashboard');
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        CableIntakeDashBoardController objCable = new CableIntakeDashBoardController(sc);
        objCable.contractsearchstring='test';
        objCable.displayContractApprovals();
        objCable.getApprovals();
        objCable.setPendingStatus();
        objCable.setAssignedStatus();
        objCable.setClosedStatus();
        objCable.setTotalRequests();
        objCable.refresh();
        objCable.Search();
        objCable.contractNameASC();
        objCable.createdDateASC();
        objCable.CounterpartyASC();
        objCable.BusinessunitASC();
        objCable.contractValueASC();
        objCable.procurementvalueASC();
        objCable.assignedattorneyASC();
        objCable.NextPage =2;
        objCable.NumberOfPages = 3;
        objCable.goBack();
        objCable.movePage();
        objCable.goForward();
        objCable.Newer();
        objCable.Older();
        
        objCable.last();
        CableIntakeDashBoardController.getContentModal(con.id);
        
    }
    
    
    
    @isTest static void Method5(){
        Contract__c con = new Contract__c();
        con.Name = 'Test Contract Request';
        con.Contract_Intake_Status__c = 'Contract Manager';
        con.Requester_Name__c = 'username';
        con.Requester_Email__c = 'legal_help@comcast.com';
        insert con;
        
        PageReference myVfPage = Page.ContractIntakeRequesterDashboard;
        test.setcurrentpage(myVfPage );
        ApexPages.CurrentPage().getparameters().put('ContractIntakeManagementDashBoard','ContractIntakeManagementDashBoard');
        ApexPages.StandardController sc = new ApexPages.StandardController(con);
        CableIntakeDashBoardController objCable = new CableIntakeDashBoardController(sc);
        objCable.contractsearchstring='test';
        objCable.displayContractApprovals();
        objCable.getApprovals();
        objCable.setPendingStatus();
        objCable.setAssignedStatus();
        objCable.setClosedStatus();
        objCable.setTotalRequests();
        objCable.refresh();
        objCable.Search();
        objCable.contractNameASC();
        objCable.createdDateASC();
        objCable.CounterpartyASC();
        objCable.BusinessunitASC();
        objCable.contractValueASC();
        objCable.procurementvalueASC();
        objCable.assignedattorneyASC();
        objCable.NextPage =2;
        objCable.NumberOfPages = 3;
        objCable.goBack();
        objCable.movePage();
        objCable.goForward();
        objCable.Newer();
        objCable.Older();
        
        objCable.last();
        CableIntakeDashBoardController.getContentModal(con.id);
        objCable.createPublicPreviewLink();
        objCable.deletePublicPreviewLink();
    }
    
    
}