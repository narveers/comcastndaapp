public class AssignFailedApproversExt{
    public String recId;
    public Boolean profileCheck{get;set;}
    public AssignFailedApproversExt(ApexPages.StandardController cont){
        profileCheck = false;
       recId = cont.getId();
       Profile prof = [SELECT Id FROM Profile WHERE Name='LTO Admin'];
       if(prof.id == UserInfo.getProfileId())
           profileCheck = true;
    }
    
    public PageReference AssignApprovers(){
    if(profileCheck){
    try{
        set<String> cntDocuIdSet = new set<String>();
        for(ContentDocumentLink  cnt : [SELECT Id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId=:recId ]){
            cntDocuIdSet.add(cnt.ContentDocumentId );
        }
        List<ContentVersion> AttachmentList = [SELECT Id,Title,Description,versionData,Attachment_Type__c,ContentDocumentId FROM ContentVersion WHERE ContentDocumentId  =:cntDocuIdSet];
        ContentVersionTriggerHandler.shareFilesForContracts(AttachmentList);
        system.debug('AttachmentList>>.'+AttachmentList.size());
    
            List<Id> ContentLink = new List<Id>();
            Map<Id,Id> contentDocumentids = new Map<Id,Id>();
            List<ContentDocumentLink> linksToCreate = new List<ContentDocumentLink>();
            Set<Id> userIds = new Set<Id>();
            for(ContentVersion cv : AttachmentList ){
                    contentDocumentids.put(cv.Id,cv.ContentDocumentId);
            }
            for(REVVY__MnApprovalStepInst__c step : [select id,REVVY__Approver__r.REVVY__ApproverUser__c from REVVY__MnApprovalStepInst__c where REVVY__Approver__r.REVVY__ApproverUser__c!=null and REVVY__ApprovalInst__r.REVVY__Contract__c =:recId  and REVVY__StepStatus__c!='Skipped']){
                userIds.add(step.REVVY__Approver__r.REVVY__ApproverUser__c);
            }
            for(REVVY__MnStepApproverInst__c approvers : [select id,REVVY__AssignedUser__c from REVVY__MnStepApproverInst__c where REVVY__AssignedUser__c!=null and REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c =:recId  and REVVY__StepInst__r.REVVY__StepStatus__c!='Skipped' ]){
                userIds.add(approvers.REVVY__AssignedUser__c);
            }
            
            Boolean inactiveUser = false;
            for(User usr : [select id,IsActive from User where id in :userIds] ){
                if(!usr.IsActive){
                    inactiveUser = true;
                }
            }
            if(inactiveUser){
               IssueAssigningApprover();
            }else{
                
                for(ContentVersion cv : AttachmentList ){
                    for(Id ids : userIds){
                        ContentDocumentLink cdl = new ContentDocumentLink();
                        cdl.ContentDocumentId = contentDocumentids.get(cv.Id);
                        cdl.LinkedEntityId = ids;
                        cdl.shareType = 'V';
                        cdl.Visibility='AllUsers';
                        linksToCreate.add(cdl);
                    }
                }
                insert linksToCreate;
            }
        }catch(Exception ex){
            system.debug(ex);
            IssueAssigningApprover();
        }
        
        return new PageReference('/'+recId);
        }else{
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Only LTO Admin can process this request.');
            ApexPages.addMessage(errorMsg);

            return null;
        }
    }
    
    public void IssueAssigningApprover(){
        contact objContact = [select id from contact where email != null limit 1];
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
        
        List<String> sendTo = new List<String>();
        sendTo.add('michael_iacovelli@comcast.com');
        mail.setToAddresses(sendTo);
        
        mail.setSubject('Issue assigning the approver');
        
        String body = 'Hi Mike,<br/>The contract '+Label.BaseLabel+'/'+recId +' <br/>had issues assigning the approver, please check.<br/><br/>Thanks,';
        mail.setHtmlBody(body);
        
        mail.setTargetObjectId(objContact .Id);
        mail.setWhatId(recId );
        mails.add(mail);
        Messaging.sendEmail(mails);
  
    }
    
    public PageReference Back(){
        return new PageReference('/'+recId);
    }
}