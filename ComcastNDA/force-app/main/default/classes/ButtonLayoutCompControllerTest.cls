@isTest
public class ButtonLayoutCompControllerTest{
    @isTest
    private static void testMethod1(){
        
        Test.startTest();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        User objUser = new user();
        //objUser.Name = 'test';
        objUser.UserName = 'asd.test@gmail.com';
        objUser.LastName = 'lName';
        objUser.Email = 'abc@bh.com';
        objUser.Alias = 'tsamp';
        objUser.CommunityNickname = 'ghjk';
        objUser.TimeZoneSidKey = 'America/Los_Angeles';
        objUser.LocaleSidKey = 'en_US';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.ProfileId = p.Id;
        objUser.LanguageLocaleKey = 'en_US';
        
        insert objUser;
        
        
        Contract__c objContract = new Contract__c ();
        objContract .Name = 'test1';
        //objContract.name = 'test';
        objContract.Requester_Name__c = 'test3';
        objContract.Status_Update__c = 'asdfg';
        objContract.Contract_Intake_Status__c = 'Contract Manager';
        objContract.Assigned_Attorney__c= objUser.id;
        insert objContract;
        
        ButtonLayoutCompController.getIsAssignedAttorney(objContract.Id);
        
        ButtonLayoutCompController.getIsCMGroupMember();
        
        ButtonLayoutCompController.getIsAGroupMember();
        
        ButtonLayoutCompController.getUserProfile();
        
        ButtonLayoutCompController.getContractStatus(objContract.Id);
        
        ButtonLayoutCompController.Mailsendmethod(objContract.Id);
        ButtonLayoutCompController.MailsendmethodContractRequester(objContract.Id);
        ButtonLayoutCompController.methodResumeLegalReview(objContract.Id);
    }
}