global class ScheduleCMTermSheetBatch implements Schedulable {

    global void execute(SchedulableContext ctx) {
        CMTermSheetBatch c = new CMTermSheetBatch();
        Database.executeBatch(c,1);
    }
    global static void startSchedule() {
    
    String prefix = 'CMTermSheetBatch -';
    if(Test.isRunningTest()) {
      prefix = System.now() + '_' + prefix; 
    }
    
    ScheduleCMTermSheetBatch c = new ScheduleCMTermSheetBatch();
    String sch = '0 0 * * * ?';
    System.schedule(prefix +sch, sch, c);
    
    sch = '0 5 * * * ?';
    System.schedule(prefix +sch, sch, c);

    sch = '0 10 * * * ?';
    System.schedule(prefix +sch, sch, c);

    sch = '0 15 * * * ?';
    System.schedule(prefix +sch, sch, c);

    sch = '0 20 * * * ?';
    System.schedule(prefix +sch, sch, c);

    sch = '0 25 * * * ?';
    System.schedule(prefix +sch, sch, c);

    sch = '0 30 * * * ?';
    System.schedule(prefix +sch, sch, c);

    sch = '0 35 * * * ?';
    System.schedule(prefix +sch, sch, c);

    sch = '0 40 * * * ?';
    System.schedule(prefix +sch, sch, c);

    sch = '0 45 * * * ?';
    System.schedule(prefix +sch, sch, c);

    sch = '0 50 * * * ?';
    System.schedule(prefix +sch, sch, c);
    
    sch = '0 55 * * * ?';
    System.schedule(prefix +sch, sch, c);
  }   
}