public class NDAUploadProcessController {
    public String currentStageString{get;set;}
    public NDA_Master__c NDAMaster {get;set;}
    public String NBCUBusinessPersonString {get;set;}
    public String NBCUApprovingAttorneyString {get;set;}
    public String AgreementContactPersonString {get;set;}
    public String AttachDescriptionString {get;set;}
    public String error {get;set;} 
    public String AttachmentId {get;set;}
    public Id SelectedId{get;set;}
    public List<Id> AttachmentIdList {get;set;} 
    public List<Attachment> AttachmentList {get;set;}
    public NDAUploadProcessController(ApexPages.StandardController controller){
        currentStageString = 'Contract Details';
        error = '';
        NDAMaster = new NDA_Master__c();
        AttachmentList = new List<Attachment>();
        AttachmentIdList = new List<Id>();
    }
    public List<SelectOption> NBCUBusinessPerson{
        get
        {
            NBCUBusinessPerson= new List<SelectOption>();
            NBCUBusinessPerson.add(new SelectOption('', '--None--'));
            Schema.DescribeFieldResult describeResult = NDA_Master__c.NBCU_Business_Person_s__c.getDescribe();
            for( Schema.PicklistEntry f : describeResult.getPicklistValues())
            {
                NBCUBusinessPerson.add(new SelectOption(f.getValue(), f.getLabel()));
            }          
            return NBCUBusinessPerson;
        }
        set;
    }
    public List<SelectOption> NBCUApprovingAttorney{
        get
        {
            NBCUApprovingAttorney= new List<SelectOption>();
            NBCUApprovingAttorney.add(new SelectOption('', '--None--'));
            Schema.DescribeFieldResult describeResult = NDA_Master__c.NBCU_Approving_Attorney_s__c.getDescribe();
            for( Schema.PicklistEntry f : describeResult.getPicklistValues())
            {
                NBCUApprovingAttorney.add(new SelectOption(f.getValue(), f.getLabel()));
            }          
            return NBCUApprovingAttorney;
        }
        set;
    }
    public List<SelectOption> AgreementContactPerson{
        get
        {
            AgreementContactPerson= new List<SelectOption>();
            AgreementContactPerson.add(new SelectOption('', '--None--'));
            Schema.DescribeFieldResult describeResult = NDA_Master__c.Agreement_Contact_Person_s__c.getDescribe();
            for( Schema.PicklistEntry f : describeResult.getPicklistValues())
            {
                AgreementContactPerson.add(new SelectOption(f.getValue(), f.getLabel()));
            }          
            return AgreementContactPerson;
        }
        set;
    }
    public void attachFileFunction(){
        if(String.isNotBlank(AttachmentId)){
            AttachmentIdList.add(AttachmentId); 
        }
        if(AttachmentIdList.size() > 0){
            if(AttachmentList.size()>0){
               AttachmentList.clear();
            }
            AttachmentList = [SELECT Id,Name,Body,ContentType,Description FROM Attachment WHERE Id IN:AttachmentIdList];
        }
    }
    public void deleteAttachment(){
        Integer count=0;
        Attachment deleteDocument=null;
        try{
            for(Attachment doc:AttachmentList){
                if(doc.Id == SelectedId){
                    deleteDocument = doc;
                    AttachmentList.Remove(count);
                    break;
                }
                count++;
            }
            if (deleteDocument!=null){
                delete deleteDocument;
            } 
        }
        catch(DMLException ex){
            error = ex.getMessage();
        }
        catch(exception ex){
            error = ex.getMessage();
        }
    }
    public PageReference nextStage(){
        if(currentStageString == 'Contract Details'){
            currentStageString = 'Disclosure Details';
        }else if(currentStageString == 'Disclosure Details'){
            saveMasterNDA();
            currentStageString = 'File Upload';
        }else if(currentStageString == 'File Upload'){
            currentStageString = 'Review and Submit';
        }else if(currentStageString == 'Review and Submit'){
            return new PageReference('/'+NDAMaster.Id);
        }
        return null;
    }
    public void previousStage(){
        if(currentStageString == 'Disclosure Details'){
            currentStageString = 'Contract Details';
        }else if(currentStageString == 'File Upload'){
            currentStageString = 'Disclosure Details';
        }else if(currentStageString == 'Review and Submit'){
            currentStageString = 'File Upload';
        }
    }
    public void saveMasterNDA(){
        SavePoint sp = Database.setSavepoint();
        try{
            NDAMaster.NBCU_Approving_Attorney_s__c = NBCUApprovingAttorneyString;
            NDAMaster.NBCU_Business_Person_s__c = NBCUBusinessPersonString;
            NDAMaster.Agreement_Contact_Person_s__c = AgreementContactPersonString;
            upsert NDAMaster;				                
        }catch(DMLException e){
            Database.rollback(sp);
            error = e.getdmlMessage(0);
        }catch(Exception e){
            Database.rollback(sp);
            error = e.getMessage();
        }
    }
}