public without sharing class NDAReviewController{
    public NDA__c contract{get;set;}
    public List<ContentVersion> content{get;set;}
    public Id contractId;
    public List<NDA__C> ContractList;
    public List<NDA__History> NDAHistory {get;set;}
    public String previewLink{get;set;}
    public string recordIDforAAp{get;set;}
    public ContentDistribution cd;
    public List<ndaHstryWpr> ndaHstryWprList{get;set;}
    
    public NDAReviewController(ApexPages.StandardController controller){
        ndaHstryWprList = new List<ndaHstryWpr>();
        ContractList = new List<NDA__C>();
        contractId = controller.getId();
        content = new List<ContentVersion>();
        NDAHistory = new List<NDA__History>();
        if(contractId != NULL){
            getApprovalDetails();
            getContractDetails();
            getContent();
        }
   }
    
    public void getContractDetails(){
        contract = [SELECT Id,name,Submitted_on_behalf_of_Name__c,Submitted_on_behalf_of_Email__c,NBCU_Legal_Entity_Name__c,NBCU_Signer_Name__c,NBCU_Signer_Title__c,Counterparty_Entity_Name__c,
                    Counterparty_Country__c,Counterparty_Contact_Name__c,Counterparty_Signer_Email__c,NDA_Type__c,Discussions_with_this_firm__c,Purpose__c,Is_there_intent_to_disclose_PII__c,Aware_of_an_existing_agreement__c,
                    Intent_to_disclose_information__c,intent_for_NBCU_to_disclose_HW_SW_Tech__c,Have_discussions_already_taken_place__c,Date_Of_Prior_Discussion__c,Term_Duration_years__c,Effective_Date__c FROM NDA__C WHERE Id =: contractId];
                    
                   
    }
    public void getContent(){
        set<String> cntDocuIdSet = new set<String>();
        for(ContentDocumentLink  cnt : [SELECT Id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where LinkedEntityId=:contractId]){
            cntDocuIdSet.add(cnt.ContentDocumentId );
        }
        content = [SELECT Id,Title,Attachment_Type__c,Description,FileExtension FROM ContentVersion WHERE ContentDocumentId  =:cntDocuIdSet];
       
      
    }
    public pageReference createPublicPreviewLink(){
        try{
        system.debug('recordIDforAAp'+recordIDforAAp);
            
            cd = new ContentDistribution();
            cd.Name = 'Test';
            cd.ContentVersionId = recordIDforAAp;
            cd.PreferencesAllowViewInBrowser= true;
            cd.PreferencesLinkLatestVersion=true;
            cd.PreferencesNotifyOnVisit=false;
            cd.PreferencesPasswordRequired=false;
            cd.PreferencesAllowOriginalDownload= false;
            cd.PreferencesAllowPDFDownload = false;
            insert cd;
            previewLink = [select id,DistributionPublicUrl from ContentDistribution where id =: cd.id].DistributionPublicUrl;
            system.debug('previewLink '+previewLink );
            PageReference pgContent = new PageReference(previewLink);
            system.debug('pgContent '+pgContent );
            return pgContent;
        }catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return null;
        }
    }
    public void deletePublicPreviewLink(){
        try{
            delete cd;
            previewLink = '' ;
        }catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
        }
    }
    Public void getApprovalDetails(){
        NDAHistory  = [SELECT ParentId, OldValue, NewValue, Field, CreatedById, CreatedDate FROM NDA__History where parentId = :contractId and Field = 'NDA_Status__c' order by CreatedDate desc];
    
        for(NDA__History nda : NDAHistory){
            ndaHstryWprList.add(new ndaHstryWpr(String.valueOf(nda.NewValue),nda.CreatedDate.format('MMMM dd, yyyy HH:mm:ss a','America/New_York')));
        }
    /*
        List< REVVY__MnApprovalInst__c > ApprovalProcess = [SELECT Id,Name FROM REVVY__MnApprovalInst__c WHERE REVVY__Contract__c=: contractId ORDER BY Createddate DESC LIMIT 1];
        ApprovalLog = [SELECT Id,REVVY__StepInst__r.Name,REVVY__AssignedUser__r.Name,REVVY__ActionDate__c,REVVY__Status__c,REVVY__ApprovalComments__c , 
                        REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.Requester_Formula__c, 
                        REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__r.Contract_Owner_Email__c
                       FROM REVVY__MnStepApproverInst__c WHERE REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c =: contractId 
                       and REVVY__StepInst__r.REVVY__ApprovalInst__c in: ApprovalProcess 
                       and REVVY__StepInst__r.REVVY__StepStatus__c != 'Skipped' 
                       ORDER BY REVVY__StepInst__r.REVVY__SortField__c ASC];
                */       
               
    }
    
    public class ndaHstryWpr{
        public String ndaStatus{get;set;}
        public String ndaDate{get;set;}
        
        public ndaHstryWpr(String status,String hstryDate){
            ndaStatus = status;
            ndaDate = hstryDate;
        }
    }
}