public class UnifiedCustomSearchController {
    Public String Searchtext {get;set;}
    Public Boolean hasResult {get;set;} 
    Public Boolean hasmoreCAF {get;set;}
    Public Boolean OnlyCAF {get;set;}
    Public Boolean OnlyCI {get;set;}
    Public Boolean Nomore {get;set;}
    public String CAFCount {get;set;}
    Public List<REVVY__MnContract__c> CAFContractList {get;set;}
    Public List<Contract__c> CIContractList{get;set;}
    public UnifiedCustomSearchController(){
        hasResult = false;
        hasmoreCAF = false;
        Nomore = false;
        OnlyCAF = true;
        OnlyCI = true;
        CAFContractList = new List<REVVY__MnContract__c>();
        CIContractList = new List<Contract__c>();
    }
    public void getResults(){
        String searchQuery = 'FIND \'*' + Searchtext + '*\' IN ALL FIELDS RETURNING REVVY__MnContract__c(Id,Name,Current_Approver__c,RecordType.DeveloperName),Contract__c(Id,Name,Contract_Intake_Status__c) LIMIT 10';
        system.debug('search query - '+searchQuery);
        List<List <sObject>> searchList = search.query(searchQuery);
        CAFContractList = ((List<REVVY__MnContract__c>)searchList[0]);
        CIContractList = ((List<Contract__c>)searchList[1]);
        if(searchList.size()>0){
            hasResult = true;
        }
        if(searchList.size()>5){
            hasmoreCAF = true;
        }
        if(!CAFContractList.isEmpty()){
            if(CAFContractList.size()>25){
                CAFCount = '25+';
            }else{
                CAFCount = CAFContractList.size()+'';
            }
        }
    }
    public void getMoreCAF(){
        OnlyCAF = true;
        OnlyCI = false;
        Nomore = true;
        if(!CAFContractList.isEmpty()){
            CAFContractList.clear();
        } 
        String searchQuery = 'FIND \'*' + Searchtext + '*\' IN ALL FIELDS RETURNING REVVY__MnContract__c(Id,Name,Current_Approver__c,RecordType.DeveloperName)';
		List<List <sObject>> searchList = search.query(searchQuery);
        CAFContractList = ((List<REVVY__MnContract__c>)searchList[0]);
    }
    public void getMoreCI(){
        OnlyCAF = false;
        OnlyCI = true;
        Nomore = true;
        if(!CIContractList.isEmpty()){
            CIContractList.clear();
        } 
        String searchQuery = 'FIND \'*' + Searchtext + '*\' IN ALL FIELDS RETURNING Contract__c(Id,Name,Contract_Intake_Status__c)';
		List<List <sObject>> searchList = search.query(searchQuery);
        CIContractList = ((List<Contract__c>)searchList[0]);
    }
}