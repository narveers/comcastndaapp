/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UserDeactivationBatchTest {

    static testMethod void testUserDeactivationBatch() {
        setupData();
        UserDeactivationBatch batchProcess = new UserDeactivationBatch();
        Database.executebatch(batchProcess);        
    }
    
    static testMethod void testUserDeactivationSchedule() {
        setupData();
        UserDeactivationBatch sch = new UserDeactivationBatch();
        sch.execute(null);
    }
    
	static void setupData() {
		
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      	User u2 = new User(Alias = 'newUser', Email='testu1@testorg.com', 
	        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	        LocaleSidKey='en_US', ProfileId = p.Id, 
	        TimeZoneSidKey='America/Los_Angeles', UserName='testu1@testorg.com');
		insert u2;
		
	}    
    
}