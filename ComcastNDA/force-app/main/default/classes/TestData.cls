public class TestData {
    public static Contract__c createContractRequest(){
        try{
            Contract__c con = new Contract__c();
            con.Name = 'Test Contract Request';
            con.Contract_Intake_Status__c = 'Contract Manager';
            con.Requester_Name__c = 'username';
            con.Requester_Email__c = 'legal_help@comcast.com';
            insert con;
            return con;
        }
        Catch(Exception ex){
            system.debug('Error - '+ex.getStackTraceString());
            return new Contract__c();
        }
    } 
    public static Attachment createAttachment(Id ParentId){
        try{
            Attachment att = new Attachment();
            att.Name = 'Test Attachment';
            att.Body = blob.toPdf('test body');
            att.ParentId = ParentId;
            insert att;
            return att;
        }
        Catch(Exception ex){
            system.debug('Error - '+ex.getStackTraceString());
            return new Attachment();
        }
    }
    public static REVVY__MnContract__c CreateCAFRequest(){
        try{
            Account acc = new account(name = 'See Counterparty (Legal Entity)');
            insert acc;
            
            Contact con = new contact(LastName = 'Test', AccountId = acc.id);
            insert con;
            
            Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
            
            REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
            lifeCycle.Name = 'Test Lifecycle';
            lifeCycle.REVVY__ApplyTo__c = 'Original';
            lifeCycle.REVVY__Object__c = 'MnContract__c';
            lifeCycle.REVVY__RecordType__c = 'Request_CAF';
            lifeCycle.REVVY__Status__c = 'Draft';
            insert lifeCycle;
            
            REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
            stage.name = 'Test';
            stage.REVVY__LifeCycle__c = lifeCycle.Id;
            stage.REVVY__Phase__c = 'Draft';
            stage.REVVY__SubPhase__c = 'Draft';
            stage.REVVY__Editable__c = true;
            insert stage;
            
            lifeCycle.REVVY__InitialStage__c = stage.Id;
            lifeCycle.REVVY__Status__c = 'Active';
            update lifeCycle;
            
            REVVY__MnContract__c mnc = new REVVY__MnContract__c(Name='Test contract',REVVY__Account__c = acc.id, recordTypeId=recTypeId,
                                                                REVVY__Contact__c = con.id,Contract_Value__c=80000,Brief_Summary__c = 'test',
                                                               REVVY__Phase__c = 'Approval');
            insert mnc;
            return mnc;
        }catch(Exception ex){
            system.debug('Error-'+ex.getStackTraceString());
            return new REVVY__MnContract__c();
        }
    }
}