@istest
public class CMentorApprovalInboundEmailHandlerTest {
    
   static testMethod void TestinBoundEmail()
   {
       Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
      
      email.subject = 'Create Contact';
      email.fromAddress = 'someaddress@email.com';
      email.plainTextBody = 'email body test > testring Reference: tested  Comments: testinggg';
      Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
      attachment.body = blob.valueOf('my attachment text');
      attachment.fileName = 'textfileone.txt';
      attachment.mimeTypeSubType = 'text/plain';
      email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
     
      CMentorApprovalInboundEmailHandler  testInbound=new CMentorApprovalInboundEmailHandler ();
      testInbound.handleInboundEmail(email, env);
      testInbound.sendEmail(email.plainTextBody.substringAfter('>'),email.plainTextBody.substringAfter('Reference:'),email.plainTextBody.substringAfter('Comments:'),email.subject);
      testInbound.saveApprovals(email.plainTextBody.substringAfter('Reference:')); 
   }
}