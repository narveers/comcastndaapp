public class Contract_Intake_Form_Ctrl {
    public Contract_Intake_Form__c form{get;set;}
    public String error{get;set;}
    public Attachment attachment{ get { 
                                        if (attachment == null) 
                                            attachment = new Attachment(); 
                                        return attachment; 
                                } 
                                 set; }
    public Contract_Intake_Form_Ctrl(ApexPages.StandardController controller ){
        
        if(controller.getId()!=null){
            form = [SELECT Id, OwnerId, IsDeleted, Name,Business_Unit__c, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, LastViewedDate, LastReferencedDate, Client_Name_and_business_unit__c, What_does_the_software_do__c, SaaS_self_hosted_Private_public_cloud__c, Integration_APIs__c, Source_code_escrow__c, premium_version_of_open_source_software__c, provide_services_to_syndication_partners__c, Explain_provide_services_to_syndication__c, Status__c, Explain_services_advanced_advertising__c, Is_vendor_providing_us_hardware_componen__c, Are_we_providing_vendor_with_hardware__c, professional_services_vendor_providing__c, Crowdsourcing_model__c, Creation_of_custom_development_work_IP__c, Are_we_asking_vendor_to_modify_OSS__c, Explain_asking_vendor_to_modify_OSS__c, Is_vendor_a_foreign_company__c, Where_will_the_work_be_performed__c, vendor_Comcast_shared_access_foreign_nat__c, What_data_will_vendor_product_collecect__c, Who_will_own_what__c, Where_will_data_be_stored__c, Are_we_giving_3_rd_party_data__c, Explain_data_flow__c, Does_the_vendor_require_remote__c, Does_vendor_need_access_Comcast_environm__c, Will_vendor_access_to_Comcast_facilities__c, Explain_vendor_level_of_access__c, License_Term__c, Anticipated_spend__c, Payment_Structure_Subscription__c, deadlines_to_start_using_product__c, Does_BU_have_SVP_authorization__c, Explain_SVP_authorization__c, Anything_we_should_know_about_the_vendor__c, Provide_additional_info_about_product__c, provide_services_to_advanced_advertising__c, Software__c, Hardware__c, Professional_Services__c, Export_Control__c, Data__c, Access__c, Description__c, Review_Status__c, Status_Review__c, Pending_Assignment_to_Attorney_Timestamp__c, Assigned_to_Attorney_Timestamp__c, Need_More_Info_Timestamp__c, In_Progress_Timestamp__c, Pending_Assignment_to_Attorney_Duration__c, Assigned_to_Attorney_Duration__c, Need_More_Info_Duration__c, In_Progress_Duration__c FROM Contract_Intake_Form__c where  id=:controller.getId() ] ;
        }else{
            form = new Contract_Intake_Form__c();
        }
    }
    public pagereference save(){
        try{
            error ='';
            form.OwnerId ='00G0x000000VFQn';
            upsert form;
            //attachment.parentid = form.Id; 
            //insert attachment;
            return new pagereference('/'+String.valueOf(form.Id).left(3));
        }catch(Exception e){
            error =e.getMessage();
        }
        return null;
    }
}