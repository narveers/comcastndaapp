//Get a list of attachments related to Contracts
//4/14/2019 Vivek Mehta  Initial Draft

public class AttachControllerCV {


   private integer counter   =0;  //keeps track of the offset
   public integer list_size =25; //sets the number of rows on the page
   public  integer total_size;    //used to show the total size of the list
   
   //public Set<Id> CVid = new Set<Id>();
   public AttachControllerCV () {
        //set the total size in the constructor
       Set<Id> CVid = new Set<Id>();
       for (REVVY__MnContract__c con : [SELECT Id FROM REVVY__MnContract__c]) {
                 CVid.add(con.Id);
           //for (ContentVersion cv : [SELECT Id, ContentDocumentId FROM ContentVersion]) {
           //        CVid.add(cv.ContentDocumentId );
       }
       
       total_size = [select count() from REVVY__MnContract__c];
   }
   
   
    public List<ContentDocumentLink> attachList { get;set;}
    
    public void Attachments() {
        //integer counter   =0;
        Set<Id> CVid = new Set<Id>();
        for (REVVY__MnContract__c con : [SELECT Id FROM REVVY__MnContract__c]) {
                 CVid.add(con.Id);
        //for (ContentVersion con : [SELECT Id, ContentDocumentId FROM ContentVersion]) {
        //       CVid.add(con.ContentDocumentId );
        }
        attachList = [SELECT ContentDocumentID, LinkedEntity.Type, LinkedEntityId, ContentDocument.Title, ContentDocument.FileType, ContentDocument.ContentSize
                      FROM ContentDocumentLink
                      //WHERE ContentDocumentID IN :CVid ];
                      //WHERE LinkedEntityId = 'a0s0x000000DR6iAAG' ];
                      WHERE LinkedEntityId IN :CVid
                      ORDER BY ContentDocument.Title
                      LIMIT :list_size
                      OFFSET :counter];
               system.debug(counter);
    }
    
   public PageReference Beginning() { //user clicked beginning
      counter = 0;
      Attachments();
      return null;
   }

   public PageReference Previous() { //user clicked previous button
      counter -= list_size;
      Attachments();
      return null;
   }

   public PageReference Next() { //user clicked next button
      counter += list_size;
      Attachments();
      return null;
   }

   public PageReference End() { //user clicked end
      counter = total_size - math.mod(total_size, list_size);
      Attachments();
      return null;
   }

   public Boolean getDisablePrevious() { 
      //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
   }

   public Boolean getDisableNext() { //this will disable the next and end buttons
      if (counter + list_size < total_size) return false; else return true;
   }

   public Integer getTotal_size() {
      return total_size;
   }

   public Integer getPageNumber() {
      return counter/list_size + 1;
   }

   public Integer getTotalPages() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
   }
   
}