public with sharing class RecipientStatusTriggerHandler{
    
    public static void onAfterUpdate(list<dsfs__DocuSign_Recipient_Status__c> newList, Map<id,dsfs__DocuSign_Recipient_Status__c> oldMap){
        Set<Id> SetNDA = new Set<Id>();
        
        Set<Id> SetRecStatus = new Set<Id>();
        
        Set<Id> SetDocAttachedStatus = new Set<Id>();
        
        for(dsfs__DocuSign_Recipient_Status__c recStatus : newList){
            SetRecStatus.add(recStatus.Id);    
        }
        
        List<dsfs__DocuSign_Recipient_Status__c> lstRecStatus = [Select ID,dsfs__Parent_Status_Record__r.NDA__c,dsfs__Envelope_Id__c,dsfs__DocuSign_Routing_Order__c,dsfs__Recipient_Status__c FROM dsfs__DocuSign_Recipient_Status__c WHERE ID IN: SetRecStatus];
        List<ContentVersion> lstContent = new List<ContentVersion>();
         
        for(dsfs__DocuSign_Recipient_Status__c recStatus : lstRecStatus) {
            dsfs__DocuSign_Recipient_Status__c objOldRecStatus = oldMap.get(recStatus.Id);
            if(recStatus.dsfs__DocuSign_Routing_Order__c == 1 && objOldRecStatus.dsfs__Recipient_Status__c != recStatus.dsfs__Recipient_Status__c && recStatus.dsfs__Recipient_Status__c == 'Completed') {
                SetNDA.add(recStatus.dsfs__Parent_Status_Record__r.NDA__c);    
            }  
            if(recStatus.dsfs__DocuSign_Routing_Order__c == 2 && objOldRecStatus.dsfs__Recipient_Status__c != recStatus.dsfs__Recipient_Status__c && recStatus.dsfs__Recipient_Status__c == 'Completed') {
                SetDocAttachedStatus.add(recStatus.Id);   
            }        
        }
        
        if(!SetDocAttachedStatus.IsEmpty()) {
            AttachSignedDocusign(SetDocAttachedStatus);
        }
        
        list<NDA__c> updateNdaList = new list<NDA__c>();
        for(NDA__c ndaObj : [SELECT Id,NDA_Status__c,DocuSign_Status__c FROM NDA__c WHERE Id IN: SetNDA]){
            ndaObj.NDA_Status__c = 'Out for Signature – NBCU';
            ndaObj.DocuSign_Status__c = 'Out for Signature – NBCU';
            updateNdaList.add(ndaObj);
        }
                
        if(!updateNdaList.isEmpty())
            update updateNdaList;
    }
    
    @Future(callout=true)
    public static void AttachSignedDocusign(Set<Id> StatusRecordId) {
        Docu_Sign__mdt docSign = new Docu_Sign__mdt();
        
        docSign = [Select MasterLabel,DocuSign_URL__c,DocuSign_Account_ID__c,DocuSign_IntegratorKey__c,DocuSign_UserName__c,
                       DocuSign_Password__c,DocuSign_Base_URL__c from Docu_Sign__mdt WHERE MasterLabel = 'DocuSign Details'];
        
        List<dsfs__DocuSign_Recipient_Status__c> lstRecStatus = [Select ID,dsfs__Parent_Status_Record__r.NDA__c,dsfs__Envelope_Id__c,dsfs__DocuSign_Routing_Order__c,dsfs__Recipient_Status__c FROM dsfs__DocuSign_Recipient_Status__c WHERE ID IN: StatusRecordId];
        List<ContentVersion> lstContent = new List<ContentVersion>();
        Map<String,Id> MapNDAEnvelope= new Map<String,Id>();
        
        for(dsfs__DocuSign_Recipient_Status__c recStatus : lstRecStatus) {
            MapNDAEnvelope.put(recStatus.dsfs__Envelope_Id__c,recStatus.dsfs__Parent_Status_Record__r.NDA__c);    
        }

        Map<Id,NDA__c> NDAMap = new map<id,NDA__c>([SELECT Id,NDA_Type__c,Agreement_Number__c FROM NDA__c WHERE Id IN: MapNDAEnvelope.values()]);
        
        
        for(String envelopeId : MapNDAEnvelope.keySet()) {
            Http http = new Http();
    
            String endPoint = docSign.DocuSign_Base_URL__c+'v2/accounts/'+docSign.DocuSign_Account_ID__c+'/envelopes/'+envelopeId+'/documents/1';
            
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endPoint);
            request.setHeader('X-DocuSign-Authentication','{"Username":"'+docSign.DocuSign_UserName__c+'","Password":"'+docSign.DocuSign_Password__c+'","IntegratorKey": "'+docSign.DocuSign_IntegratorKey__c+'"}');
            request.setHeader('Content-Type','application/json');
            request.setMethod('GET');
            
            HttpResponse response = http.send(request);
            
            if(response.getStatusCode() == 200) {
                NDA__c objNDARec = new NDA__c();
                if(!MapNDAEnvelope.IsEmpty() && MapNDAEnvelope.containsKey(envelopeId)) {
                    if(!NDAMap.IsEmpty() && NDAMap.containsKey(MapNDAEnvelope.get(envelopeId)))
                    objNDARec = NDAMap.get(MapNDAEnvelope.get(envelopeId));
                }
                
                ContentVersion cv = new ContentVersion();
                cv.versionData = response.getBodyAsBlob();
                cv.title = objNDARec.Agreement_Number__c +' '+objNDARec.NDA_Type__c +' Signed';
                cv.FirstPublishLocationId = objNDARec.id;
                cv.pathOnClient = objNDARec.Agreement_Number__c +' '+objNDARec.NDA_Type__c+'.pdf';
               // cv.DocuSign_Required__c = True;
               
                lstContent.add(cv);
            }
        }
        
        if(!lstContent.isEmpty())
            insert lstContent;
    }
}