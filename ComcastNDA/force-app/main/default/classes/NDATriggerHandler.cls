/**
    Name   : NDATriggerHandler
    Auther : Narveer S.
    Date : 03/04/2018
    Description : NDA TriggerHandler for DocuSign Template and Legal Review
**/
public class NDATriggerHandler {

    public static void onAfterUpdate(List<NDA__c> newListNDARec,Map<Id, NDA__c> mapIdToOldNDARec) {
    
        for(NDA__c objNDARec: newListNDARec) {
            NDA__c objOldNDA = mapIdToOldNDARec.get(objNDARec.Id);
            if(objNDARec.NDA_Status__c == 'Signed/Executed' && objOldNDA.NDA_Status__c != 'Signed/Executed') {
                if(!System.isBatch())
                    saveNDATrackingPDF(objOldNDA.Id);              
            }
        }
    }
    
    @future(callout = true)
    public static void saveNDATrackingPDF(Id NDARecid){
        
        Pagereference Pg = Page.NDATrackingFormPdf;
        Pg.getParameters().put('id',NDARecid);
        //Pg.getParameters().put('renderAs','pdf');
        blob body;
        if(!test.isRunningTest()){
            //body = Pg.getContent();
            body = Pg.getcontentAsPdf();
        } else {
            body = blob.valueof('TEST');
        }
        Attachment atch = new Attachment();
        atch.Name = 'NDATrackingForm.pdf';
        atch.ContentType = 'application/pdf';
        atch.ParentId = NDARecid;
        atch.body = body; 
        insert atch;
    }
    
    public static void onBeforeInsert(List<NDA__c> lstNDARecord) {
        for(NDA__c objNDARecord: lstNDARecord) {
            
            if(objNDARecord.NDA_Type__c.equals('One Way NBCU') || objNDARecord.NDA_Type__c.equals('One way Counterparty')) {
                objNDARecord.Custom_Template__c = 'One Way Basic';
            } else if(objNDARecord.NDA_Type__c.equals('Mutual')) {
                objNDARecord.Custom_Template__c = 'Mutual Basic';    
            } 
            integer numberOfDays = 0;
                if(objNDARecord.Date_Of_Prior_Discussion__c != null && objNDARecord.Effective_Date__c != null)
                    numberOfDays = objNDARecord.Date_Of_Prior_Discussion__c.monthsBetween(objNDARecord.Effective_Date__c);
                    
            System.debug(numberOfDays);
            System.debug(objNDARecord.Discussions_with_this_firm__c);
            System.debug(objNDARecord.Is_there_intent_to_disclose_PII__c);
            System.debug(objNDARecord.Intent_to_disclose_information__c);
            System.debug(objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c);
            System.debug(objNDARecord.Have_discussions_already_taken_place__c);
            System.debug(objNDARecord.Aware_of_an_existing_agreement__c);
            
            if(objNDARecord.Counterparty_Country__c.equals('United States') &&
               objNDARecord.Intent_to_pay_parties__c != Null && objNDARecord.Intent_to_pay_parties__c != '' && objNDARecord.Intent_to_pay_parties__c.equals('No') &&
               objNDARecord.Discussions_with_this_firm__c != Null && objNDARecord.Discussions_with_this_firm__c != '' && objNDARecord.Discussions_with_this_firm__c.equals('No') &&
               objNDARecord.Is_there_intent_to_disclose_PII__c != Null && objNDARecord.Is_there_intent_to_disclose_PII__c != '' && objNDARecord.Is_there_intent_to_disclose_PII__c.equals('No') && 
               objNDARecord.Intent_to_disclose_information__c != Null && objNDARecord.Intent_to_disclose_information__c != '' && objNDARecord.Intent_to_disclose_information__c.equals('No') && 
               objNDARecord.Have_discussions_already_taken_place__c != Null && objNDARecord.Have_discussions_already_taken_place__c != '' && objNDARecord.Have_discussions_already_taken_place__c.equals('No') &&
               objNDARecord.Aware_of_an_existing_agreement__c != Null && objNDARecord.Aware_of_an_existing_agreement__c != '' && objNDARecord.Aware_of_an_existing_agreement__c.equals('No') && 
               objNDARecord.Term_Duration_years__c.equals('2') && 
               ((objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c == '' || objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c == Null) || (objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c != '' && objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c != Null && objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c.equals('No')))) {
                  objNDARecord.NDA_Status__c = 'Out for Signature – Counterparty';
            } else if(objNDARecord.Have_discussions_already_taken_place__c != Null && objNDARecord.Have_discussions_already_taken_place__c != '' && objNDARecord.Have_discussions_already_taken_place__c.equals('Yes') && numberOfDays < 4){
                objNDARecord.NDA_Status__c = 'Out for Signature – Counterparty';    
            } else {
                objNDARecord.NDA_Status__c = 'Admin Review';    
            } 
        }    
    }
    public static void onAfterInsert(List<NDA__c> lstNDARecord) {
        
        Set<Id> SetNDAWithoutLegalReview = new Set<Id>();
        
        for(NDA__c objNDARecord: lstNDARecord) {
            integer numberOfDays = 0;
            if(objNDARecord.Date_Of_Prior_Discussion__c != null && objNDARecord.Effective_Date__c != null)
                numberOfDays = objNDARecord.Date_Of_Prior_Discussion__c.monthsBetween(objNDARecord.Effective_Date__c);
                
            System.debug(numberOfDays);    
            System.debug(objNDARecord.Discussions_with_this_firm__c);
            System.debug(objNDARecord.Is_there_intent_to_disclose_PII__c);
            System.debug(objNDARecord.Intent_to_disclose_information__c);
            System.debug(objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c);
            System.debug(objNDARecord.Have_discussions_already_taken_place__c);
            System.debug(objNDARecord.Aware_of_an_existing_agreement__c);
            
            if(objNDARecord.Counterparty_Country__c.equals('United States') &&
               objNDARecord.Intent_to_pay_parties__c != Null && objNDARecord.Intent_to_pay_parties__c != '' && objNDARecord.Intent_to_pay_parties__c.equals('No') &&
               objNDARecord.Discussions_with_this_firm__c != Null && objNDARecord.Discussions_with_this_firm__c != '' && objNDARecord.Discussions_with_this_firm__c.equals('No') &&
               objNDARecord.Is_there_intent_to_disclose_PII__c != Null && objNDARecord.Is_there_intent_to_disclose_PII__c != '' && objNDARecord.Is_there_intent_to_disclose_PII__c.equals('No') && 
               objNDARecord.Intent_to_disclose_information__c != Null && objNDARecord.Intent_to_disclose_information__c != '' && objNDARecord.Intent_to_disclose_information__c.equals('No') && 
               objNDARecord.Have_discussions_already_taken_place__c != Null && objNDARecord.Have_discussions_already_taken_place__c != '' && objNDARecord.Have_discussions_already_taken_place__c.equals('No') &&
               objNDARecord.Aware_of_an_existing_agreement__c != Null && objNDARecord.Aware_of_an_existing_agreement__c != '' && objNDARecord.Aware_of_an_existing_agreement__c.equals('No') && 
               objNDARecord.Term_Duration_years__c.equals('2') && 
               ((objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c == '' || objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c == Null) || (objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c != '' && objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c != Null && objNDARecord.intent_for_NBCU_to_disclose_HW_SW_Tech__c.equals('No')))) {
                   SetNDAWithoutLegalReview.add(objNDARecord.Id);
            } else if(objNDARecord.Have_discussions_already_taken_place__c != Null && objNDARecord.Have_discussions_already_taken_place__c != '' && objNDARecord.Have_discussions_already_taken_place__c.equals('Yes') && numberOfDays < 4){
                SetNDAWithoutLegalReview.add(objNDARecord.Id);        
            } 
        }
        
        if(!SetNDAWithoutLegalReview.isEmpty()) {
            SendDocusign(SetNDAWithoutLegalReview);    
        }            
    }
    // Sending the Templates to Docusign as per User Selection
    @Future(callout=true)
    public static void SendDocusign(Set<Id> NDARecordId) {
        
        for(Id RecordId: NDARecordId) {
            DS_Recipe_Send_Env_Email_NDA_Controller.send(RecordId);
        }
    }
}