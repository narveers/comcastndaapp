public without sharing class ManageApproverPageCtlr{
Public String ManageApprovalsText{get;set;}
    
    public ManageApproverPageCtlr(ApexPages.StandardController controller){
        
        ManageApprovalsText= ApexPages.currentpage().getParameters().get('EmailSource');
        
    }
}