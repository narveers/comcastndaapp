/*
Name             : NDADashboardController
Author           : Narveer S.
Date             : 03/07/2019
Description      : Controller Class for NDA Dashboard
*/
public without sharing class NDADashboardController {
    public NDA__c NDA {get;set;}
    public Integer PendingContractsCount {get;set;}
    public Integer CompletedContractsCount {get;set;}
    public Integer TotalContractsCount {get;set;}
    public Integer NumberOfPages {get;set;} 
    public Integer PreviousPage {get;set;}
    public Integer CurrentPage {get;set;}
    public Integer NextPage {get;set;}
    public String NowShowing {get;set;}
    public String contractsearchstring {get;set;}
    private Integer Size;
    private String Status;
    private Boolean IsIdSorted;
    private Boolean IsCounterpartySorted;
    private Boolean IsLastactivitySorted;
    private Boolean IsStatusSorted;
    private Boolean IsSubmissiondateSorted;
    private Boolean IsElapseddaysSorted;
    private Boolean IsCurrentClicked;
    public List<NDA__c> NDAList{get;set;}
    public NDADashboardController(){
        PendingContractsCount = 0;
        CompletedContractsCount = 0;
        TotalContractsCount = 0;
        PreviousPage = 1;
        CurrentPage = 2;
        NextPage = 3;
        IsIdSorted = False;
        IsCounterpartySorted = False;
        IsLastactivitySorted = False;
        IsStatusSorted = False;
        IsSubmissiondateSorted = False;
        IsElapseddaysSorted = False;
        IsCurrentClicked = False;
        try{
            NowShowing = Label.NDAPendingRequests;
            NDA = new NDA__c();
            NDAList = new List<NDA__c>();
            if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
                Status = '\'Legal Review\',\'Admin Review\',\'Out for Signature – Counterparty\',\'Out for Signature – NBCU\'';
            } else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')){
                Status = '\'Admin Review\'';    
            } else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')){
                Status = '\'Legal Review\'';     
            }
            getPanelsCount();
            
        }
        Catch(Exception ex)
        {
            system.debug('Exception - '+ ex.getStackTraceString());
        }
    }
    Public ApexPages.StandardSetController setCon{
        get{
            if(setCon == null){
                Try{
                    Size = 20;
                    string queryString = '';
                    if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
                        queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                            'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') AND Requester_Email__c = \''+ 
                            UserInfo.getUserEmail()+'\' ORDER BY CreatedDate DESC';
                    }
                    else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')){
                        queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                            'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY CreatedDate DESC';
                    }
                    else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')){
                        queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                            'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY CreatedDate DESC';
                    }
                    system.debug('query - '+queryString);
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                    setCon.setPageSize(Size);
                    NumberOfPages = (Integer)(setCon.getResultSize()/(Decimal)Size).round(System.RoundingMode.CEILING);
                    system.debug('Number of pages - '+NumberOfPages);
                }
                Catch(Exception ex){
                    System.debug('Exception - '+ex.getStackTraceString());
                }
            }
            return setCon;
        }
        set;
    }
    public List<NDA__c> getNDARequests(){
        Try{
            if(!NDAList.isEmpty()){
                NDAList.clear();
            }
            for(NDA__c nda: (List<NDA__c>)setCon.getRecords()){
                NDAList.add(nda);
                System.debug(NDAList);
            }
        }
        Catch(Exception ex){
            System.debug('Exception - '+ex.getStackTraceString());
        }
        return NDAList;
    }
    public void getPanelsCount(){
        if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
            PendingContractsCount = [SELECT COUNT() FROM NDA__c WHERE Requester_Email__c =: UserInfo.getUserEmail() AND 
                                     NDA_Status__c IN ('Legal Review','Admin Review',
                                                       'Out for Signature - Counterparty',
                                                       'Out for Signature - NBCU')];
            CompletedContractsCount = [SELECT COUNT() FROM NDA__c WHERE Requester_Email__c =: UserInfo.getUserEmail() AND
                                       NDA_Status__c IN ('Signed/Executed',
                                                         'Canceled')];
            TotalContractsCount = [SELECT COUNT() FROM NDA__c WHERE Requester_Email__c =: UserInfo.getUserEmail() AND 
                                   NDA_Status__c IN ('Legal Review','Signed/Executed','Admin Review',
                                                     'Out for Signature - Counterparty',
                                                     'Out for Signature - NBCU',
                                                     'Canceled')];
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')){
            PendingContractsCount = [SELECT COUNT() FROM NDA__c WHERE NDA_Status__c IN ('Admin Review')];
            CompletedContractsCount = [SELECT COUNT() FROM NDA__c WHERE NDA_Status__c IN ('Legal Review','Signed/Executed',
                                                                                      'Out for Signature - NBCU','Out for Signature – Counterparty',
                                                                                      'Canceled')];
            TotalContractsCount = [SELECT COUNT() FROM NDA__c WHERE NDA_Status__c IN ('Legal Review','Signed/Executed',
                                                                                      'Out for Signature - Counterparty',
                                                                                      'Out for Signature - NBCU','Admin Review',
                                                                                      'Canceled')];
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')){
            PendingContractsCount = [SELECT COUNT() FROM NDA__c WHERE NDA_Status__c IN ('Legal Review')];
            CompletedContractsCount = [SELECT COUNT() FROM NDA__c WHERE NDA_Status__c IN ('Signed/Executed',
                                                                                          'Out for Signature - Counterparty',
                                                                                          'Out for Signature - NBCU','Canceled')];
            TotalContractsCount = [SELECT COUNT() FROM NDA__c WHERE NDA_Status__c IN ('Legal Review','Signed/Executed',
                                                                                      'Out for Signature - Counterparty',
                                                                                      'Out for Signature - NBCU',
                                                                                      'Canceled')];
        }
    }
    public void setPendingStatus(){
        setCon = null;
        if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')) {
            Status = '\'Admin Review\',\'Out for Signature – Counterparty\',\'Legal Review\',\'Out for Signature – NBCU\'';
        } else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')) {
            Status = '\'Admin Review\'';
        } else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')) {
            Status = '\'Legal Review\'';
        }
        NowShowing = Label.NDAPendingRequests;
        getNDARequests();  
    } 
    public void setCompletedStatus(){
        setCon = null;
        NowShowing = Label.NDACompletedRequest;
        if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')) {
            Status = '\'Signed/Executed\',\'Canceled\'';
        } else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')) {
            Status = '\'Legal Review\',\'Signed/Executed\',\' Out for Signature - NBCU\',\'Out for Signature – Counterparty\',\'Canceled\'';
        } else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')) {
            Status = '\'Signed/Executed\',\' Out for Signature - NBCU\',\'Out for Signature – Counterparty\',\'Canceled\'';
        }
        getNDARequests();
    }
    public void setTotalRequests(){
        setCon = null;
        NowShowing = Label.NDATotalRequests;
        if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')) {
            Status = '\'Admin Review\',\'Legal Review\',\'Out for Signature – Counterparty\',\'Out for Signature – NBCU\',\'Signed/Executed\',\'Canceled\'';
        } else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')) {
            Status = '\'Admin Review\',\'Legal Review\',\'Out for Signature – Counterparty\',\'Out for Signature – NBCU\',\'Signed/Executed\',\'Canceled\'';
        } else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')) {
            Status = '\'Legal Review\',\'Out for Signature – Counterparty\',\'Out for Signature – NBCU\',\'Signed/Executed\',\'Canceled\'';
        }
        getNDARequests();
    }
    public void refresh(){
        setCon = null;
        getNDARequests();
        setCon.setPageNumber(1);  
    }
    public void Search(){
        string queryString = '';
        try{ 
            if(String.isNotBlank(contractsearchstring) && contractsearchstring.length() > 2){
                if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
                    queryString = 'FIND \''+contractsearchstring+'*\' RETURNING NDA__c(Id, Agreement_Number__c, Counterparty_Entity_Name__c'
                        +', LastModifiedDate, NDA_Status__c, CreatedDate WHERE NDA_Status__c IN ('+Status+') AND Requester_Email__c = \''+ 
                        UserInfo.getUserEmail()+'\' ORDER BY CreatedDate DESC)';
                    setCon = new ApexPages.StandardSetController((List<NDA__c>)Search.query(queryString)[0]);
                    setCon.setPageSize(Size);
                }
            }
            else{
                if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
                    queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                        'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY CreatedDate DESC';
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                    setCon.setPageSize(Size);
                }    
            }
        }
        Catch(Exception ex){
            system.debug('Exception - '+ex.getStackTraceString());
        }
    }
    public void sortId(){
        string queryString='';
        if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') AND Requester_Email__c = \''+ 
                            UserInfo.getUserEmail()+'\' ORDER BY ';
            if(IsIdSorted){
                IsIdSorted = False;
                queryString += 'Agreement_Number__c ASC';
            }else{
                IsIdSorted = True;
                queryString += 'Agreement_Number__c DESC';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsIdSorted){
                IsIdSorted = False;
                queryString += 'Agreement_Number__c ASC';
            }else{
                IsIdSorted = True;
                queryString += 'Agreement_Number__c DESC';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsIdSorted){
                IsIdSorted = False;
                queryString += 'Agreement_Number__c ASC';
            }else{
                IsIdSorted = True;
                queryString += 'Agreement_Number__c DESC';
            }
        }
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
        setCon.setPageSize(Size);
    }
    public void sortCounterparty(){
        string queryString='';
        if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') AND Requester_Email__c = \''+ 
                UserInfo.getUserEmail()+'\' ORDER BY ';
            if(IsCounterpartySorted){
                IsCounterpartySorted = False;
                queryString += 'Counterparty_Entity_Name__c ASC NULLS LAST';
            }else{
                IsCounterpartySorted = True;
                queryString += 'Counterparty_Entity_Name__c DESC NULLS LAST';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsCounterpartySorted){
                IsCounterpartySorted = False;
                queryString += 'Counterparty_Entity_Name__c ASC NULLS LAST';
            }else{
                IsCounterpartySorted = True;
                queryString += 'Counterparty_Entity_Name__c DESC NULLS LAST';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsCounterpartySorted){
                IsCounterpartySorted = False;
                queryString += 'Counterparty_Entity_Name__c ASC NULLS LAST';
            }else{
                IsCounterpartySorted = True;
                queryString += 'Counterparty_Entity_Name__c DESC NULLS LAST';
            }
        }
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
        setCon.setPageSize(Size);
    }
    public void sortLastactivity(){
        string queryString='';
        if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') AND Requester_Email__c = \''+ 
                UserInfo.getUserEmail()+'\' ORDER BY ';
            if(IsLastactivitySorted){
                IsLastactivitySorted = False;
                queryString += 'LastModifiedDate ASC';
            }else{
                IsLastactivitySorted = True;
                queryString += 'LastModifiedDate DESC';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsLastactivitySorted){
                IsLastactivitySorted = False;
                queryString += 'LastModifiedDate ASC';
            }else{
                IsLastactivitySorted = True;
                queryString += 'LastModifiedDate DESC';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsLastactivitySorted){
                IsLastactivitySorted = False;
                queryString += 'LastModifiedDate ASC';
            }else{
                IsLastactivitySorted = True;
                queryString += 'LastModifiedDate DESC';
            }
        }
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
        setCon.setPageSize(Size);
    }
    public void sortStatus(){
        string queryString='';
        if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') AND Requester_Email__c = \''+ 
                UserInfo.getUserEmail()+'\' ORDER BY ';
            if(IsStatusSorted){
                IsStatusSorted = False;
                queryString += 'NDA_Status__c ASC NULLS LAST';
            }else{
                IsStatusSorted = True; 
                queryString += 'NDA_Status__c DESC NULLS LAST';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsStatusSorted){
                IsStatusSorted = False;
                queryString += 'NDA_Status__c ASC NULLS LAST';
            }else{
                IsStatusSorted = True; 
                queryString += 'NDA_Status__c DESC NULLS LAST';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsStatusSorted){
                IsStatusSorted = False;
                queryString += 'NDA_Status__c ASC NULLS LAST';
            }else{
                IsStatusSorted = True; 
                queryString += 'NDA_Status__c DESC NULLS LAST';
            }
        }
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
        setCon.setPageSize(Size);
    }
    public void sortSubmissiondate(){
        string queryString='';
        if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') AND Requester_Email__c = \''+ 
                UserInfo.getUserEmail()+'\' ORDER BY ';
            if(IsSubmissiondateSorted){
                IsSubmissiondateSorted = False;
                queryString += 'CreatedDate ASC';
            }else{
                IsSubmissiondateSorted = True;
                queryString += 'CreatedDate DESC';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsSubmissiondateSorted){
                IsSubmissiondateSorted = False;
                queryString += 'CreatedDate ASC';
            }else{
                IsSubmissiondateSorted = True;
                queryString += 'CreatedDate DESC';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsSubmissiondateSorted){
                IsSubmissiondateSorted = False;
                queryString += 'CreatedDate ASC';
            }else{
                IsSubmissiondateSorted = True;
                queryString += 'CreatedDate DESC';
            }
        }
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
        setCon.setPageSize(Size);
    }
    public void sortElapseddays(){
        string queryString='';
        if(ApexPages.CurrentPage().getUrl().contains('NDARequesterDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') AND Requester_Email__c = \''+ 
                UserInfo.getUserEmail()+'\' ORDER BY ';
            if(IsElapseddaysSorted){
                IsElapseddaysSorted = False;
                queryString += 'CreatedDate ASC';
            }else{
                IsElapseddaysSorted = True;
                queryString += 'CreatedDate DESC';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalAdminDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsElapseddaysSorted){
                IsElapseddaysSorted = False;
                queryString += 'CreatedDate ASC';
            }else{
                IsElapseddaysSorted = True;
                queryString += 'CreatedDate DESC';
            }
        }
        else if(ApexPages.CurrentPage().getUrl().contains('LegalReviewerDashboard')){
            queryString = 'SELECT Id, Agreement_Number__c, Counterparty_Entity_Name__c, LastModifiedDate, NDA_Status__c, '+
                'CreatedDate FROM NDA__c WHERE NDA_Status__c IN ('+Status+') ORDER BY ';
            if(IsElapseddaysSorted){
                IsElapseddaysSorted = False;
                queryString += 'CreatedDate ASC';
            }else{
                IsElapseddaysSorted = True;
                queryString += 'CreatedDate DESC';
            }
        }
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
        setCon.setPageSize(Size);
    }
    public void goBack(){
        if(PreviousPage == 1){
            setCon.setPageNumber(PreviousPage);
        }else if(PreviousPage>1 && setCon.getHasPrevious()){
            CurrentPage=CurrentPage-1;
            NextPage=NextPage-1;
            if(IsCurrentClicked || (setCon.getPageNumber()-PreviousPage)==1 ){
                IsCurrentClicked = false;
                setCon.setPageNumber(setCon.getPageNumber() - 1);
            }else{
                setCon.setPageNumber(setCon.getPageNumber() - 2);
            }
            PreviousPage=PreviousPage-1;
        }
    } 
    public void movePage(){
        try{
            IsCurrentClicked = true;
            setCon.setPageNumber(CurrentPage);
        }Catch(Exception ex){
            system.debug('Exception - '+ex);
        }
    }
    public void goForward(){
        if(setCon.getHasNext()){
            if((NextPage)< NumberOfPages){
                PreviousPage++;
                CurrentPage++;
                NextPage++;
                if(IsCurrentClicked){
                    IsCurrentClicked = false;
                    setCon.setPageNumber(setCon.getPageNumber() + 1);
                }else{
                    setCon.setPageNumber(setCon.getPageNumber() + 2);
                }
            }else if(NextPage == NumberOfPages){
                last();
            }
        }
    } 
    public void Newer(){
        if(setCon.getHasPrevious()){
            setCon.previous();
            if((PreviousPage-1)>=1){
                NextPage=NextPage-1;
                CurrentPage=CurrentPage-1;
                PreviousPage=PreviousPage-1;
            }
        }
    } 
    public void Older(){
        if(setCon.getHasNext()){
            if((NextPage)< NumberOfPages){
                PreviousPage++;
                CurrentPage++;
                NextPage++;   
            }
            setCon.next();
        }
    }
    public void last(){
        setCon.last();
        if(NumberOfPages > 2){
            NextPage = NumberOfPages;
            CurrentPage = NextPage - 1;
            PreviousPage = CurrentPage - 1;
        }
    }
}