public class CafApprovalRequestEmailComponentCtrl {
    public Id emailContractId{get;set;}
    public string BaseURL{get;set;}
    public string contractnameescaped {get;set;}
    public List<REVVY__MnStepApproverInst__c> FromList = new List<REVVY__MnStepApproverInst__c>();
    public String FromAddress{get;set;}
    public String ToAddress{get;set;}
    Public String DocId{get;set;}
    Public String SI{get;set;}
    Public String StepInstanceId{get;set;}
    Public String test{get;set;}
    public String ManageApprovalsPage1{get;set;}
    public String ManageApprovalsPage2{get;set;}
    public String CAFemailHeader{get;set;}
    public CafApprovalRequestEmailComponentCtrl(){
        BaseURL = Label.BaseLabel;
        CAFemailHeader = Label.CAFEmailHeader;
        Document Doc = [SELECT Id,Name FROM Document WHERE DeveloperName =: CAFemailHeader LIMIT 1];
        DocId = Doc.Id;
        
        EmailServicesAddress EmailService = [SELECT EmailDomainName,LocalPart FROM EmailServicesAddress WHERE DeveloperName ='AdvancedApprovalsEmail'];
        ToAddress = EmailService.LocalPart+'@'+EmailService.EmailDomainName;
    }
    public String getReviewURL(){
        String URL = '';
        if (emailContractId!=null){
            REVVY__MnContract__c contract = [SELECT Id,Name FROM REVVY__MnContract__c WHERE Id =:emailContractId LIMIT 1];
            String EmbedPart1 = '/apex/EmbeddedManageApprovals?EmailSource=ReviewContract&resource=approval&entry=approvalHistory#/approval/REVVY__MnContract__c/contract/'+contract.Name+'/'+contract.Id;
            ManageApprovalsPage1 = BaseURL+'/?ec=302&startURL='+EncodingUtil.urlEncode(EmbedPart1, 'UTF-8');
           // String EmbedPart2 = '&resource=approvalbundle&entry=approvalHistory#/approval/contract/';
           // ManageApprovalsPage2 = EncodingUtil.urlEncode(EmbedPart2, 'UTF-8');
            URL = ManageApprovalsPage1;
        }
        return URL;
    }
    public String getApproveURL(){
        String URL = '';
        if (emailContractId!=null){
          REVVY__MnContract__c contract = [SELECT Id,Name FROM REVVY__MnContract__c WHERE Id =:emailContractId LIMIT 1];
            //String redirecturl = '/apex/EmbeddedManageApprovals?EmailSource=Approve&resource=approval&entry=approvalHistory#/approval/REVVY__MnContract__c/contract/'+contract.Name+'/'+contract.Id;
            String EmbedPart1 = '/apex/EmbeddedManageApprovals?EmailSource=Approve&resource=approval&entry=approvalHistory#/approval/REVVY__MnContract__c/contract/'+contract.Name+'/'+contract.Id;
           //String EmbedPart1 = '/apex/EmbeddedApprovalsDashboard?EmailSource=Approve&resource=approval&entry=approvalHistory#/approval/REVVY__MnContract__c/contract/'+contract.Name+'/'+contract.Id;
           
            ManageApprovalsPage1 = BaseURL+'/?ec=302&startURL='+EncodingUtil.urlEncode(EmbedPart1, 'UTF-8');
            //String EmbedPart2 = '&resource=approvalbundle&entry=approvalHistory#/approval/contract/';
            //ManageApprovalsPage2 = EncodingUtil.urlEncode(EmbedPart2, 'UTF-8');
            URL = ManageApprovalsPage1;
        }
        return URL;
        
        
      
    }
    public String getRejectURL(){
        String URL = '';
        if (emailContractId!=null){
            REVVY__MnContract__c contract = [SELECT Id,Name FROM REVVY__MnContract__c WHERE Id =:emailContractId LIMIT 1];
            String EmbedPart1 = '/apex/EmbeddedManageApprovals?EmailSource=Reject&resource=approval&entry=approvalHistory#/approval/REVVY__MnContract__c/contract/'+contract.Name+'/'+contract.Id;
            ManageApprovalsPage1 = BaseURL+'/?ec=302&startURL='+EncodingUtil.urlEncode(EmbedPart1, 'UTF-8');
           // String EmbedPart2 = '&resource=approvalbundle&entry=approvalHistory#/approval/contract/';
           // ManageApprovalsPage2 = EncodingUtil.urlEncode(EmbedPart2, 'UTF-8');
             URL = ManageApprovalsPage1;
        }
        return URL;
    }
    public String getRecallURL(){
        String URL = '';
        if (emailContractId!=null){
           URL = BaseURL+'/apex/ReviewCAF?id='+emailContractId ;
        }
        return URL;
    }
    public REVVY__MnApprovalStepInst__c ApprovalList { 
        get{
            List<REVVY__MnApprovalStepInst__c> ToDisplay = new List<REVVY__MnApprovalStepInst__c>();
            List<REVVY__MnApprovalInst__c> tmpAppIns = [SELECT Id,Name FROM REVVY__MnApprovalInst__c WHERE REVVY__Contract__c=: emailContractId ORDER BY Createddate DESC LIMIT 1];
            ToDisplay  = [SELECT Name,(SELECT name FROM REVVY__StepInstApprovers__r) FROM REVVY__MnApprovalStepInst__c WHERE REVVY__ApprovalInst__c IN :tmpAppIns AND REVVY__StepStatus__c != 'Skipped' ORDER BY REVVY__Step_Sequence__c ASC];
            if (ToDisplay.size() > 0)
            {
                REVVY__MnApprovalStepInst__c APSI = ToDisplay[0];return APSI;
            }
            else{
                return new REVVY__MnApprovalStepInst__c();
            }
        }                        
        set; }
    public REVVY__MnContract__c getContract(){
        List<REVVY__MnContract__c> contractList = New List<REVVY__MnContract__c>();
        if (emailContractId!=null){
            contractList = [select id,REVVY__ContractId__c,Requester_Formula__c,Contract_Owner_Email__c,REVVY__ContractNum__c,CAF_Type__c,Counterparty__c,Brief_Summary__c,Name,CreatedBy.Name,REVVY__Account__r.Name,Contract_Value__c,On_Behalf_Of_Name__c,REVVY__Parent__r.Name,toLabel(Business_Unit__c),REVVY__StartDateContract__c,Evergreen_Contract__c,Total_Contract_Value__c,Expiration_Date__c,Attorney__c,Is_Cable_Law_Dept_or_Corp_General_Counce__c,Record_URL__c,Contract_Type_CAF__c from REVVY__MnContract__c where id=:emailContractId LIMIT 1];
            if(contractList.size() > 0){
                REVVY__MnContract__c contract = contractList[0];
                contractnameescaped = EncodingUtil.urlEncode(contractList[0].Name, 'UTF-8');
                return contract;
            }
            else{
                return new REVVY__MnContract__c();
            }
        }
        else{
            return new REVVY__MnContract__c();
        }
    }
    public REVVY__MnApprovalStepInst__c getStepInstance(){
        List<REVVY__MnApprovalStepInst__c> StepList = new List<REVVY__MnApprovalStepInst__c>();
        if(emailContractId!=null){
            StepList = [select id from REVVY__MnApprovalStepInst__c where REVVY__StepStatus__c='Pending Approval' and REVVY__IsParentStep__c = false and REVVY__ApprovalInst__r.REVVY__Contract__c =:emailContractId];
            if(StepList.size() > 0){REVVY__MnApprovalStepInst__c APSI = StepList[0];
                                    FromList = [SELECT REVVY__AssignedUser__r.email FROM REVVY__MnStepApproverInst__c
                                                  WHERE REVVY__StepInst__c =:StepList[0].Id AND REVVY__Status__c='Pending Approval'
                                                  ORDER BY CreatedDate ASC LIMIT 1];
                                    FromAddress = FromList[0].REVVY__AssignedUser__r.email;
                                    test = 'Approve\nPlease do not modify the reference line\n<stepInstance>'+StepList[0].Id+'</stepInstance>';
                                    //StepInstanceId=APSI.Id;
                                    return APSI;
                                   }
            else{
                return new REVVY__MnApprovalStepInst__c();
            }
        }
        else{
            return new REVVY__MnApprovalStepInst__c();
        }
    }
    public List<REVVY__MnApprovalStepInst__c> getApprover(){
        List<REVVY__MnApprovalStepInst__c> ApprovalInstList = new List<REVVY__MnApprovalStepInst__c>();
        if(emailContractId!=null){
            
            // List<REVVY__MnApprovalInst__c> ApprovalProcess = [SELECT Id,Name FROM REVVY__MnApprovalInst__c WHERE REVVY__Contract__c=: emailContractId ORDER BY Createddate DESC LIMIT 1];
            Id StepInstId  = CAFGlobalCalls.getStepInstanceId(emailContractId); // added 5/16/2019 for multiple approval process (Mike's corner case)
        
            ApprovalInstList = [SELECT Name,
                                 (SELECT name,REVVY__ActionDate__c,
                                  REVVY__ApprovalComments__c,REVVY__Status__c 
                                  FROM REVVY__StepInstApprovers__r) 
                                  FROM REVVY__MnApprovalStepInst__c 
                                  // WHERE REVVY__ApprovalInst__c IN :ApprovalProcess AND REVVY__StepStatus__c != 'Skipped' ORDER BY REVVY__Step_Sequence__c ASC];
                                  WHERE REVVY__ApprovalInst__c = :StepInstId AND REVVY__StepStatus__c != 'Skipped' ORDER BY REVVY__Step_Sequence__c ASC];
            if(ApprovalInstList.size() > 0){
                return ApprovalInstList;
            }
            else{
                return new List<REVVY__MnApprovalStepInst__c>();
            }
        }
        else{
            return new List<REVVY__MnApprovalStepInst__c>();
        }
    }
    public List<contentversion> getContent(){
        List<contentversion> contentList = new List<contentversion>();
        if(emailContractId!=null){
            contentList = [SELECT Id,title,Attachment_Type__c,Description FROM contentversion WHERE FirstPublishLocationId =:emailContractId];
            if(contentList.size()>0){
                return contentList;
            }
            else{
                return new List<contentversion>();
            }
        }
        else{
            return new List<contentversion>();
        }
    }
    public REVVY__MnApproverActionLog__c getActionLog(){
        List<REVVY__MnApproverActionLog__c> ActionLogs = new List<REVVY__MnApproverActionLog__c>();
        if(emailContractId!=null){
            ActionLogs = [SELECT REVVY__AssignedToFormula__c,REVVY__Comments__c FROM REVVY__MnApproverActionLog__c WHERE REVVY__ApprovalInst__r.REVVY__Contract__c =:emailContractId AND REVVY__Action__c = 'Reject' ORDER BY REVVY__ActionDate__c DESC LIMIT 1];
            if(ActionLogs.size()>0){
               return ActionLogs[0];
            }
            else{
                return new REVVY__MnApproverActionLog__c();
            }
        }
        else{
            return new REVVY__MnApproverActionLog__c();
        }
    } 
}