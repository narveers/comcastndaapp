global class UserDeactivationBatch implements Database.Batchable<Sobject>, Database.Stateful, Schedulable{

	global Database.QueryLocator start(Database.BatchableContext BC){
		DateTime dlastLoginDate = System.now().addDays(-180);		
		String query = 'Select Id, IsActive, LastLoginDate from User where IsActive = true AND LastLoginDate < :dlastLoginDate';
		if(Test.isRunningTest()) {
			query ='Select Id, IsActive, LastLoginDate from User where username=\'testu1@testorg.com\'';
		}
		
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<User> scope){

		for(User u:scope) {			
			u.IsActive = false;
		}
		
		update scope;

	}

	global void finish(Database.BatchableContext BC){

	}

    global void execute(SchedulableContext SC) {
        UserDeactivationBatch batchProcess = new UserDeactivationBatch();
        Database.executebatch(batchProcess);
    }

}