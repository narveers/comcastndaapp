/*********************************************************************
Description: Used to Create user from SSO
Author: Cloud Mentor
Date Created: October-10-2018
Original Release: 
Change History:  

**********************************************************************/
global class UserHandler implements Auth.SamlJitHandler {
  private class JitException extends Exception{}
  List<Process_Log__c> ProcessLogs= new List<Process_Log__c>();
      
  private void handleUser(boolean create, User u, Map<String, String> attributes,
                            String federationIdentifier, boolean isStandard) {
                            
  try
  {
   List<User> userlist = new List<User>(); 
   String emailString='';
   // Creating string for the error messages
   String essertionStr = '' ;  
   for( String essertionkeys : attributes.Keyset()){
    
     if ( essertionStr.Length() > 0 ){
      essertionStr = essertionStr  + essertionkeys+ ' :'+ attributes.get(essertionkeys)+ ','; 
     } else {
       essertionStr = essertionkeys+ ' :'+ attributes.get(essertionkeys)+ ','; 

     }  
   }
   system.debug('---Vikram--'+essertionStr );
   // New User 
   if(create) 
   {
      if(attributes.containsKey('MAIL')) 
      {
      
       emailString = attributes.get('MAIL');
       system.debug('---Vick4--'+emailString );
       u.Username = emailString+Label.JITUsernameSuffix;
       u.FederationIdentifier = emailString;
       u.Email = emailString;
      } else {
        ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT-UserHandler ', Method__c = 'handleUser' ,Message_type__c='Error',Description__c= 'Email is Missing in the essertion.', Detail_Description__c = essertionStr)); 
      } 
      String strRole = getRoles(attributes.get('COMPANYNAME'));  
      if ( strRole.Length() > 0  ){  u.Role_Name__c = strRole;
      }else {
        u.Role_Name__c = 'Cable TBA';
        ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT-UserHandler ', Method__c = 'handleUser' ,Message_type__c='Error',Description__c= 'Company not mapped in the settings.', Detail_Description__c = essertionStr)); 
      } 
      u.Profile_Name__c = Label.JITProfile;
      u.localesidkey='en_US';
      u.emailencodingkey='UTF-8';
      u.languagelocalekey='en_US';
      if(attributes.containsKey('ZIPCODE')) {
         u.TimeZoneSidKey = getTimeZone(attributes.get('ZIPCODE'));
      } else {   ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT-UserHandler ', Method__c = 'handleUser' ,Message_type__c='Error',Description__c= 'Zip Code is Missing in the essertion.', Detail_Description__c = essertionStr)); 
      }                                
      if(attributes.containsKey('COMCAST_LNAME')) {
         u.LastName = attributes.get('COMCAST_LNAME');
      } else {   ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT-UserHandler ', Method__c = 'handleUser' ,Message_type__c='Error',Description__c= 'Last Name is Missing in the essertion.', Detail_Description__c = essertionStr)); 
      }  
      if(attributes.containsKey('COMCAST_FNAME')){ 
         u.FirstName = attributes.get('COMCAST_FNAME');
      } else {   ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT-UserHandler ', Method__c = 'handleUser' ,Message_type__c='Error',Description__c= 'First Name is Missing in the essertion.', Detail_Description__c = essertionStr)); 
      }  
      u.Alias = createAlias(attributes.get('COMCAST_FNAME'),attributes.get('COMCAST_LNAME'));
      u.IsActive = true;
      //userlist.add(u);
      PackageLicense pl = [SELECT Id,AllowedLicenses FROM PackageLicense WHERE NamespacePrefix =: Label.PackageNameSpacePrefix];
      //Assign permission set
      List<PermissionSetAssignment> Permissions = new List<PermissionSetAssignment>();
      List< PermissionSet > PermissionSetList = [SELECT Id FROM PermissionSet WHERE Name IN ('Approver','CMSalesReps','CMStandardObj','Editors','Submitter')];    
      Database.SaveResult srList = Database.insert(u, false);
       if(!srList.isSuccess()){ String str = '';   for(Database.Error err : srList.getErrors()) {  str += err.getMessage();   }   ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT-UserHandler ', Method__c = 'handleUser' ,Message_type__c='Error',Description__c= 'Error inserting the user.', Detail_Description__c = str)); 
        }else {
             if(pl.AllowedLicenses > 1 || pl.AllowedLicenses == -1){   UserPackageLicense usepacklic = new UserPackageLicense(UserId = u.id, PackageLicenseId = pl.id);    Database.SaveResult srList2 = Database.insert(usepacklic, false);  if(!srList2.isSuccess()){  ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT-UserHandler ', Method__c = 'handleUser' ,Message_type__c='Error',Description__c= 'Error assigning Model N licenses', Detail_Description__c = '')); 
              } 
               
             }else{
                 ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT-UserHandler ', Method__c = 'handleUser' ,Message_type__c='Error',Description__c= 'Model N Licenses not available.', Detail_Description__c = '')); 
             }
             for(PermissionSet PS: PermissionSetList){
                 PermissionSetAssignment PSA = new PermissionSetAssignment();
                 PSA.AssigneeId=u.Id;
                 PSA.PermissionSetId = PS.Id;   
                 Permissions.add(PSA);
             }
             
             Database.SaveResult[] srList1 = Database.insert(Permissions, false);
              for(Integer j=0; j<srList1.size();j++){ 
               if(!srList1[j].isSuccess()){
                   String str1 = '';
                   for(Database.Error err : srList1[j].getErrors()) { 
                        str1 += err.getMessage();
                    }
                    ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT-UserHandler ', Method__c = 'handleUser' ,Message_type__c='Error',Description__c= 'Error inserting the Permission sets.', Detail_Description__c = str1)); 
                }else {
             
                 // All good
                   ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT-UserHandler ', Method__c = 'handleUser' ,Message_type__c='Success',Description__c= 'User Inserted Successfully from JIT', Detail_Description__c = essertionStr)); 
                              
               }
             } 
          }     
         
          }else if(!create && u!=null) {
                u.IsActive = true;
                PackageLicense pl = [SELECT Id,AllowedLicenses FROM PackageLicense WHERE NamespacePrefix =: Label.PackageNameSpacePrefix];
                Integer UPL = [SELECT COUNT() FROM UserPackageLicense WHERE UserId =:u.Id AND PackageLicenseId =:pl.Id ];
                if(UPL <>1 && UPL <1){
                   if(pl.AllowedLicenses > 1 || pl.AllowedLicenses == -1)  { insert new UserPackageLicense(UserId = u.id, PackageLicenseId = pl.id);
                       }
                }
                update(u);
          }
     } catch(Exception ex){
           system.debug('---VickException'+ ex.getMessage());                     
           ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT- user Handler', Method__c = 'Userhandler' ,Message_type__c='Error',Description__c= ex.getMessage()));  if(ProcessLogs.size()>0){  Insert processlogs;    
           } 
                                
     }
   }
    public string createAlias(string Fname, String Lname){
        String Resultstring = null;
        String Aliasstring = null;
        String Userstring ; 
                    
        try{
            Resultstring = Fname.substring(0, 1)+Lname.substring(0, 2)+ '%';
            List<user> userlist = new List<user>();
            Integer userlength ; 
            userlist = [SELECT Id FROM USER WHERE Alias LIKE :Resultstring];
            if(userlist!=NULL && userlist.size() > 0){
                userlength = String.valueof(userlist.size() + 1).Length();    Aliasstring = Fname.substring(0, 1) + Lname.substring(0, 2) ;   UserString = String.Valueof(userlist.size() + 1);   Aliasstring  = Aliasstring.RightPad(8-userlength , '0') + UserString;
           }
            else{
                 Aliasstring = Fname.substring(0, 1) + Lname.substring(0, 2) ;
                 UserString = String.Valueof(1);
                 Aliasstring  = Aliasstring.RightPad(7 , '0') + UserString;
            }
        }
        Catch (Exception ex){   ProcessLogs.add(new Process_Log__c(Class_Name__c= 'JIT- UserHandler', Method__c = 'Alias Creation' ,Message_type__c='Error',Description__c= ex.getMessage())); if(ProcessLogs.size()>0){  Insert processlogs;    
           }      
           
        }
        return Aliasstring;
    }
    
    public String getTimeZone(String ZIP){
        system.debug('ZIP - '+ZIP);
        String ResultString = NULL;
        If(String.isNotBlank(ZIP)){
            Zip_Code__c objZipCode = [SELECT Timezone__c FROM Zip_Code__c WHERE Name =:ZIP LIMIT 1];
            system.debug('Time zone - '+objZipCode.Timezone__c);
            if(objZipCode!=NULL){
                if(objZipCode.Timezone__c.equals('-10')) {   ResultString = 'Pacific/Honolulu';    
                } else if(objZipCode.Timezone__c.equals('-9')) {  ResultString = 'America/Anchorage';    
                } else if(objZipCode.Timezone__c.equals('-8')) {   ResultString = 'America/Los_Angeles';
                } else if(objZipCode.Timezone__c.equals('-7')) {   ResultString = 'America/Denver';
                } else if(objZipCode.Timezone__c.equals('-6')) {    ResultString = 'America/Chicago';
                } else if(objZipCode.Timezone__c.equals('-5')) {    ResultString = 'America/New_York';
                } else if(objZipCode.Timezone__c.equals('-4')) { ResultString = 'America/Halifax';
                }
                else{ ResultString = 'America/New_York';
                }
            }
            else{ ResultString = 'America/New_York';
            }
        }
        system.debug('result - '+ResultString);
        return ResultString;
    }
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
                           String federationIdentifier, Map<String, String> attributes, String assertion) {
           
           system.debug('--- Vick1--'+ assertion);
           String essertionStr = '' ;  
           for( String essertionkeys : attributes.Keyset()){
    
               if ( essertionStr.Length() > 0 ){
                    essertionStr = essertionStr  + essertionkeys+ ' :'+ attributes.get(essertionkeys)+ ','; 
               } else {
                   essertionStr = essertionkeys+ ' :'+ attributes.get(essertionkeys)+ ','; 

               }  
           }
           system.debug('--Vick3'+essertionStr );
           User u = new User();
           handleUser(true, u, attributes, federationIdentifier, true);
         //  if(ProcessLogs.size()>0){
         //    Insert processlogs;    
         //  }
                    
           return u;
    }
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
                           String federationIdentifier, Map<String, String> attributes, String assertion) {                               
                 if(userId!=null){
                     User u = [SELECT Id FROM User WHERE Id=:userId];
                     handleUser(false, u, attributes, federationIdentifier, true);
                 }
    }
    
    private String getRoles(String companyName){
      Map<String, String> CompanyNames = new Map<String, String>();
      For( JIT_Company__mdt JitRols : [select id,Label,Role__c from JIT_Company__mdt]){
          
       CompanyNames.put( JitRols.Label, JitRols.Role__c );
      }
      
      If(CompanyNames.Containskey(companyName)){   Return CompanyNames.Get(companyName) ; 
      } else{
         Return '';
      }
       
    } 
}