public class BytesToKbMbGbComponentController {
   
    /*
     * Converts a String value that represents the bytes in a file to text such as 2 KB, 4.1 MB, etc
     * @author Tad Aalgaard
     * @Date 2/20/2015
  */
  public BytesToKbMbGbComponentController(AttachControllerCV controller){}
   
    public string controllerValue;
   
    public void setControllerValue (long s) {
        if (s < 1024)
            controllerValue =  string.valueOf(s) + ' Bytes';
        else
            if (s >= 1024 && s < (1024*1024))
        {
            //KB
            Decimal kb = Decimal.valueOf(s);
            kb = kb.divide(1024,2);
            controllerValue = string.valueOf(kb) + ' KB';
        }
        else
            if (s >= (1024*1024) && s < (1024*1024*1024))
        {
            //MB
            Decimal mb = Decimal.valueOf(s);
            mb = mb.divide((1024*1024),2);
            controllerValue = string.valueOf(mb) + ' MB';
        }
        else
        {
            //GB
            Decimal gb = Decimal.valueOf(s);
            gb = gb.divide((1024*1024*1024),2);
           
            controllerValue = string.valueOf(gb) + ' GB';
        }   
       
    }
   
    public String getControllerValue() {
    
    return controllerValue;
        
    }
}