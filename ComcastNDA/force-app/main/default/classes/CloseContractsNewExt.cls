public class CloseContractsNewExt {
    
    private ApexPages.StandardController controller;
    public String actualValue{get;set;}
    public String rowNumber{get;set;}
    public List<Contract__c> listContracts{get;set;}
    public CloseContractsNewExt(ApexPages.StandardController controller){
        this.controller = controller;
        listContracts = new List<Contract__c>();
        if(controller!=null && controller.getId()!=null){
            List<Contract__c> selectedContracts = [select ID,Name,Business_Unit__c,Reason_Of_Rejection__c from Contract__c where id = : controller.getId()] ;
            for(Contract__c contract: selectedContracts){
                listContracts.add(contract);
            }
        }
    }
    
    public PageReference save(){
        for(Contract__c contract: listContracts){
            
            contract.Contract_Intake_Status__c = 'Declined';       
       
        }
        update listContracts;
        if(controller!=null){
            return new PageReference('/'+listContracts[0].id);
        }else {
            return null;
        }
    }
}