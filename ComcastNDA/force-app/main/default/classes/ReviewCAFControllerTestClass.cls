@isTest
private class ReviewCAFControllerTestClass {
    /*static testMethod void testMethod1(){
        List<REVVY__MnContract__c> CAFList = new List<REVVY__MnContract__c>();
        REVVY__MnContract__c CAFContract = TestData.CreateCAFRequest();
        CAFList.add(CAFContract);
        
        Test.startTest();
        PageReference pageRef = Page.ReviewCAF;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('Referer', 'test');
        ApexPages.currentPage().getParameters().put('ContractRequesterDashboard','Test');
        ApexPages.currentPage().getParameters().put('ContractRequesterDashboard','Test');
        
        ReviewCAFController CAF = new ReviewCAFController(new ApexPages.StandardController(CAFContract));
        CAF.displayContractApprovals();
        CAF.setPendingStatus();
        CAF.setInprocessStatus();
        CAF.setCompletedStatus();
        CAF.setTotalRequests();
        CAF.approveContract();
        CAF.rejectContract();
        CAF.reviewContract();
        Test.stopTest();
    }*/
    
    @isTest static void unitTest(){
        Test.startTest();
        
        List<REVVY__MnContract__c> CAFList = new List<REVVY__MnContract__c>();
        REVVY__MnContract__c CAFContract = TestData.CreateCAFRequest();
        CAFList.add(CAFContract);
        
        Account acc = new account(name = 'See Counterparty (Legal Entity)');
        insert acc;
        
        REVVY__MnContract__c mnc = new REVVY__MnContract__c(Name='Test contract',recordTypeId=Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId(),
                                                            Contract_Value__c=80000,Brief_Summary__c = 'test', REVVY__Phase__c = 'Approval',REVVY__Account__c=acc.id, Is_Sourcing_Contract__c = 'Yes');
        insert mnc;
        
        PageReference pageRef = Page.ContractManagementDashboard;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardSetController stdset = new ApexPages.StandardSetController(CAFList);
        stdset.setSelected(CAFList);
        ApexPages.currentPage().getParameters().put('ContractManagementDashboard','Test');
        ReviewCAFController CAF_Management = new ReviewCAFController(new ApexPages.StandardController(CAFContract));
        CAF_Management.setcon = stdset;
        CAF_Management.displayContractApprovals();
        CAF_Management.contractsearchstring = 'test';
        CAF_Management.Search();
        CAF_Management.contractNameASC();
        CAF_Management.CounterpartyASC();
        CAF_Management.RequesterASC();
        CAF_Management.BusinessunitASC();
        CAF_Management.contractValueASC();
        CAF_Management.submissionDateASC();
        CAF_Management.ApproverASC();
        
        PageReference pageRef2 = Page.ContractRequesterDashboard;
        Test.setCurrentPage(pageRef2);
        ApexPages.currentPage().getParameters().put('ContractRequesterDashboard','Test');
        ReviewCAFController CAF_Management2 = new ReviewCAFController(new ApexPages.StandardController(CAFContract));
        CAF_Management2.setcon = stdset;
        CAF_Management2.contractsearchstring = 'test';
        CAF_Management2.displayContractApprovals();
        CAF_Management2.Search();
        CAF_Management2.contractNameASC();
        CAF_Management2.CounterpartyASC();
        CAF_Management2.RequesterASC();
        CAF_Management2.BusinessunitASC();
        CAF_Management2.contractValueASC();
        CAF_Management2.submissionDateASC();
        CAF_Management2.ApproverASC();
        
        PageReference pageRef3 = Page.ContractAdminDashboard;
        Test.setCurrentPage(pageRef3);
        ApexPages.currentPage().getParameters().put('ContractAdminDashboard','Test');
        Test.stopTest();
        ReviewCAFController CAF_Management3 = new ReviewCAFController(new ApexPages.StandardController(CAFContract));
        CAF_Management3.setcon = stdset;
        CAF_Management3.contractsearchstring = 'test';
        
        CAF_Management3.displayContractApprovals();
        CAF_Management3.Search();
        CAF_Management3.contractNameASC();
        CAF_Management3.CounterpartyASC();
        
        CAF_Management3.RequesterASC();
        CAF_Management3.BusinessunitASC();
        CAF_Management3.contractValueASC();
        CAF_Management3.submissionDateASC();
        CAF_Management3.ApproverASC();
        CAF_Management3.openManageApprovals();
        CAF_Management3.resubmitContract();
        CAF_Management3.ApproverAuditReport();
        CAF_Management3.createPublicPreviewLink();
        
    }
    
    
    @isTest static void unitTest2(){
        Test.startTest();
        
        List<REVVY__MnContract__c> CAFList = new List<REVVY__MnContract__c>();
        REVVY__MnContract__c CAFContract = TestData.CreateCAFRequest();
        CAFList.add(CAFContract);
        
        Account acc = new account(name = 'See Counterparty (Legal Entity)');
        insert acc;
        
        REVVY__MnContract__c mnc = new REVVY__MnContract__c(Name='Test contract',recordTypeId=Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId(),
                                                            Contract_Value__c=80000,Brief_Summary__c = 'test', REVVY__Phase__c = 'Approval',REVVY__Account__c=acc.id, Is_Sourcing_Contract__c = 'Yes');
        insert mnc;
        
        
        PageReference pageRef = Page.ContractManagementDashboard;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardSetController stdset = new ApexPages.StandardSetController(CAFList);
        stdset.setSelected(CAFList);
        
        PageReference pageRef4 = Page.ContractApprovalDashboard;
        Test.setCurrentPage(pageRef4);
        ApexPages.currentPage().getParameters().put('ContractApprovalDashboard','Test');
        ReviewCAFController CAF_Management4 = new ReviewCAFController(new ApexPages.StandardController(mnc));
        CAF_Management4.setcon = stdset;
        CAF_Management4.contractsearchstring = 'test';
        CAF_Management4.displayContractApprovals();
        CAF_Management4.Search();
        CAF_Management4.contractNameASC();
        CAF_Management4.CounterpartyASC();
        CAF_Management4.RequesterASC();
        CAF_Management4.BusinessunitASC();
        CAF_Management4.contractValueASC();
        Test.stopTest();
        CAF_Management4.submissionDateASC();
        CAF_Management4.ApproverASC();
        CAF_Management4.setPendingStatus();
        CAF_Management4.setInprocessStatus();
        CAF_Management4.setCompletedStatus();
        CAF_Management4.setTotalRequests();
        ReviewCAFController.getContractModal(mnc.id);
        ReviewCAFController.getContentModal(mnc.id);
        ReviewCAFController.getApprovalLogModal(mnc.id);
        CAF_Management4.movePage();
        CAF_Management4.goForward();
        CAF_Management4.Newer();
        CAF_Management4.Older();
        CAF_Management4.last();
        CAF_Management4.goBack();
        CAF_Management4.refresh();
        CAF_Management4.approveContract();
        CAF_Management4.rejectContract();
        CAF_Management4.reviewContract();
        
        
    }
    
    
    
    @isTest static void testMethod2(){
        List<REVVY__MnContract__c> CAFList = new List<REVVY__MnContract__c>();
        REVVY__MnContract__c CAFContract = TestData.CreateCAFRequest();
        CAFContract.CAF_Type__c = 'LTO Test';
        CAFList.add(CAFContract);
        Test.startTest();
        contact con = new contact();
        con.LastName = 'test';
        con.Email = 'Legal_Technology_Support@comcast.com';
        insert con;
        String email = 'Legal_Technology_Support@comcast.com';
        EmailTemplate et = [Select Id from EmailTemplate where Name = 'CAF - LTO from:user - Recalled Email Notifications'];
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        
        PageReference pageRef = Page.ContractManagementDashboard;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('ContractManagementDashboard','Test');
        ReviewCAFController CAF_Management = new ReviewCAFController(new ApexPages.StandardController(CAFContract));
        CAF_Management.search();
        
        PageReference pageRef2 = Page.ContractRequesterDashboard;
        Test.setCurrentPage(pageRef2);
        ApexPages.currentPage().getParameters().put('ContractRequesterDashboard','Test');
        ReviewCAFController CAF_Management2 = new ReviewCAFController(new ApexPages.StandardController(CAFContract));
        CAF_Management2.search();
        
        
        PageReference pageRef3 = Page.ContractAdminDashboard;
        Test.setCurrentPage(pageRef3);
        ApexPages.currentPage().getParameters().put('ContractAdminDashboard','Test');
        ReviewCAFController CAF_Management3 = new ReviewCAFController(new ApexPages.StandardController(CAFContract));
        CAF_Management3.search();
        
        PageReference pageRef4 = Page.ContractApprovalDashboard;
        Test.setCurrentPage(pageRef4);
        ApexPages.currentPage().getParameters().put('ContractApprovalDashboard','Test');
        ReviewCAFController CAF_Management4 = new ReviewCAFController(new ApexPages.StandardController(CAFContract));
        CAF_Management4.search();
        CAF_Management4.refreshContractDetailFunc();
        CAF_Management4.createPublicPreviewLink();
        
        message.toAddresses = new String[] {email};
        message.setTemplateId(et.id);
        message.setTargetObjectId(con.Id);
        message.setWhatId(CAFContract.Id);
        emails.add(message);
       // CAF_Management4.recallContract();
       // Messaging.sendEmail(emails);
        Test.stopTest();
    }
}