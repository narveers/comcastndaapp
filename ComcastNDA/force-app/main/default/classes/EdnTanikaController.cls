public class EdnTanikaController{
    public REVVY__MnContract__c contract{get;set;}
    public REVVY__MnContract__c ReviewContract{get;set;}
    public String error{get;set;}
    Id contractId;
    public string contid{get;set;}
    public List<Id> attachmentIdList{get;set;}
    public Id selectedid{get;set;}
    public Id selectedcvid{get;set;}
    public Attachment attach{get;set;}
    public String currentStage{get;set;}
    public String filename{get;set;}
    public String body{get;set;}
    public Id attachId{get;set;}
    public List<Document> docuList{get;set;}
    public List<Attachment> AttachmentList{get;set;}
    public List<ContentVersion> ReviewDocs{get;set;}
    public String attachDescription{get;set;}
    public String attachType{get;set;}
    public Document doc{get;set;}
    public String counterpartyName{get;set;}
    public String counterpartyName1{get;set;}
    public String parentContractName1{get;set;}
    public String erpAccNum{get;set;}
    public List<Account> searchedCounterparties{get;set;}
    public REVVY__MnContract__c searchContract{get;set;}
    public List<REVVY__MnContract__c> searchedParentsContracts{get;set;}
    public String accountName{get;set;}
    public String parentName{get;set;}
    public Account acc{get;set;}
    public Boolean optionalMetaData{get;set;}
    public String legalcontract{get;set;}
    public String sourcingContract{get;set;}
    public String BindingCorp{get;set;}
    public String contractvalue{get;set;}
    public List<REVVY__MnApprovalStepInst__c> ApproverList{get;set;}
    public List<REVVY__MnApprovalInst__c> ApprovalProcess{get;set;}
    public Boolean sorted = false;
    public Boolean sortedcont = false;
    public Boolean ContentCreated = false;
    public Boolean Lessthan100kBoolean{get;set;}
    public Boolean ApproverResultBoolean{get;set;}
    public List<ApprovalWrapper> ApprovalWrapperList{get;set;}
    public List<Attachment> tempDocList{get;set;}
    public List<Id> CachedIds{get;set;}
    public Map<REVVY__MnApprovalStepInst__c,List<REVVY__MnStepApproverInst__c>>Approvers {get;set;}
    public EdnTanikaController(ApexPages.StandardController controller) {
        Lessthan100kBoolean = false;
        contractvalue = '';
        contractId = controller.getId();
        ApproverResultBoolean = false;
        string stage = ApexPages.currentPage().getParameters().get('stage');
        attach = new Attachment();
        doc= new Document();
        CachedIds= new List<Id>();
        acc = new Account();
        docuList = new List<Document>();
        attachmentIdList = new List<Id>();
        AttachmentList = new List<Attachment>();
        searchedCounterparties = new List<Account>();
        searchedParentsContracts = new List<REVVY__MnContract__c>();
        List<Attachment> tempDocList = new List<Attachment>();
        searchContract = new REVVY__MnContract__c();
        if(contractId != null){
            contract = [select Id,Counterparty__c,Less_Than_100K__c,Above_statements_applicable_to_Contract__c,
                        Attorney__c ,Are_you_requesting_on_someone_s_behalf__c,On_Behalf_Of_Name__c,On_Behalf_Of_Email__c,Name,Business_Unit__c,REVVY__Account__r.Name,
                        REVVY__Parent__r.Name,REVVY__Currency__c,Contract_Value__c,Evergreen_Contract__c,REVVY__StartDateContract__c,Expiration_Date__c,
                        Contract_Type_CAF__c,Spend_Category__c,Is_Sourcing_Contract__c,Is_Binding_Corporate_Dev_Contract__c,Unchanged_legal_provided_template_used__c ,Payment_Terms__c,Brief_Summary__c from REVVY__MnContract__c where id=: contractId];
            ReviewContract = contract;
            
            if (!string.isempty(stage)){
                currentStage = 'File Upload';
            }else
            {
                bringmeApproverDetails();
                currentStage = 'Review and Submit';
            }
            AttachmentList = [SELECT Id,Name,Description,Body,ContentType FROM Attachment WHERE ParentId =:contractId];
        }else{
            currentStage = 'General Information';
            contract = new REVVY__MnContract__c();
            contract.Unchanged_legal_provided_template_used__c = '';
            contract.Is_Sourcing_Contract__c = '';
        }
    }
    public void bringmeApproverDetails(){
        ApprovalWrapperList = new List<ApprovalWrapper>();
        ApprovalProcess = [SELECT Id,Name FROM REVVY__MnApprovalInst__c WHERE REVVY__Contract__c=: contractId ORDER BY Createddate DESC LIMIT 1];
        
        ApproverList = [SELECT Name,REVVY__Approver__r.Name,REVVY__StepStatus__c,REVVY__SortField__c FROM REVVY__MnApprovalStepInst__c 
                        WHERE REVVY__ApprovalInst__c IN :ApprovalProcess AND REVVY__StepStatus__c != 'Skipped' ORDER BY REVVY__Step_Sequence__c ASC];
        
        for (REVVY__MnApprovalStepInst__c SI : ApproverList){  
            ApprovalWrapperList.add(New ApprovalWrapper(SI,[SELECT Name FROM REVVY__MnStepApproverInst__c Where REVVY__StepInst__c =: SI.Id]));
            
        }
        system.debug('Approvres --- '+ApprovalWrapperList);
    }
    public PageReference deleteAttachment(){
        Integer count=0;
        Attachment deleteDocument=null;
        try{
            for(Attachment doc:AttachmentList){
                if(doc.Id == selectedid){
                    deleteDocument = doc;
                    AttachmentList.Remove(count);
                    break;
                }
                count++;
            }
            if (deleteDocument!=null){
                delete deleteDocument;
            } 
        }
        catch(DMLException ex){
            error = ex.getMessage();
        }
        catch(exception ex){
            error = ex.getMessage();
        }
        return null;
    }
    public PageReference nextStage(){
        system.debug('nextstage');
        error = '';
        if(currentStage == 'General Information'){
            currentStage = 'Brief Summary';
        }/*else if(currentStage == 'Contract Terms'){
            currentStage = 'Contract Terms 1';
        }else if(currentStage == 'Contract Terms 1'){
            currentStage = 'Brief Summary';
        }*/else if(currentStage == 'Brief Summary'){
            if(contract.Brief_Summary__c == null || contract.Brief_Summary__c == ''){
                error = 'Please input Brief Summary';
            }else{
                saveContractRecord();
                currentStage = 'File Upload';
            }
        }else if(currentStage == 'Review and Submit'){
            isApproverAdded();
            if(ApproverResultBoolean){
                savecontent();
                PageReference finalmessagepage = new PageReference('/apex/RequestCAF_ThankYou');
                finalmessagepage.getParameters().put('Id', contract.id);
                finalmessagepage.setRedirect(true);
                return finalmessagepage; 
            }
            else{
                error = 'An Approver must be added to each approval process'; 
                return null;
            }
        }else if(currentStage == 'File Upload'){
            system.debug('at file upload stage - '+contractId);
            Integer attachments = [SELECT Count() FROM Attachment WHERE ParentId =:contractId]; 
            if(attachments>0){
                Integer Contractattachments = [SELECT Count() FROM Attachment WHERE ParentId =:contractId AND ContentType = 'Contract'];
                if(Contractattachments > 0){
                    error.remove(error);
                    return new PageReference('/apex/EdnTanikaApproval?resource=approvalbundle&entry=previewApproval#/approval/REVVY__MnContract__c/Contract%28s%29/'+contract.Name+'/'+contract.Id);
                }
                else{
                    error = 'A Contract must be attached to continue';
                }
            }
            else{
                error = 'Please upload a file to continue';
            }
        }
        else if(currentStage == 'Smart Approval'){   
            system.debug('got in to approver details');
            currentStage = 'Review and Submit';
        }
        return null;
    }
    public void isApproverAdded(){
        error='';
        Integer count=0;
        system.debug('error before checking the loop - ' + error );
        for(REVVY__MnApprovalStepInst__c StepInstance : [SELECT Id,Name,REVVY__ApprovalInst__r.Name,REVVY__StepStatus__c,createdby.Name,(SELECT Id FROM REVVY__StepInstApprovers__r)
                                                         FROM REVVY__MnApprovalStepInst__c WHERE REVVY__ApprovalInst__r.REVVY__Contract__c = :contractId
                                                         AND REVVY__StepStatus__c != 'Skipped'])
            
        {
            system.debug('approvers - '+ StepInstance);
            if(StepInstance.REVVY__StepInstApprovers__r.size() > 0){
                ApproverResultBoolean = true;
            }  
            else{
                ApproverResultBoolean = false;
                break;
            }
            count++;
        }
        system.debug('list size - '+count);
        system.debug('Approver result boolean - '+ApproverResultBoolean);
    }
    public void attachFileFunction(){
        error = '';
        if(String.isNotBlank(attachId)){
            attachmentIdList.add(attachId); 
        }
        if(attachmentIdList.size() > 0){
            if(AttachmentList.size()>0){
                AttachmentList.clear();
            }
            AttachmentList = [SELECT Id,Name,Body,ContentType,Description FROM Attachment WHERE Id IN:attachmentIdList];
        }
    }
    public PageReference previousStage(){
        error = '';
        /*if(currentStage == 'Contract Terms'){
            currentStage = 'General Information';
        }else if(currentStage == 'Contract Terms 1'){
            currentStage = 'General Information';
        }else*/ if(currentStage == 'Brief Summary'){
            currentStage = 'General Information';
        }else if(currentStage == 'Review and Submit'){
            currentStage = 'Smart Approval';
            return new PageReference('/apex/EdnTanikaApproval?resource=approvalbundle&entry=previewApproval#/approval/REVVY__MnContract__c/Contract%28s%29/'+contract.Name+'/'+contract.Id);
        }else if(currentStage == 'File Upload'){
            currentStage = 'Brief Summary';
        }else if(currentStage == 'Smart Approval'){
            currentStage = 'File Upload';
        }
        return null;
    }
    public PageReference savecontent(){
        contract.REVVY__Phase__c = 'CAF Request';
        contract.REVVY__SubPhase__c = 'Admin Review';
        contract.REVVY__Currency__c = 'USD';
        update contract;
        if(!AttachmentList.isEmpty() && AttachmentList != null && AttachmentList.size() > 0){
            List<ContentVersion> FilesList = New List<ContentVersion>();
            for(Attachment doc : AttachmentList){
                ContentVersion cv = new ContentVersion();
                cv.versionData = doc.Body;
                cv.title = doc.name;
                cv.FirstPublishLocationId = contract.id;
                cv.pathOnClient = doc.name;
                cv.Attachment_Type__c = doc.ContentType;
                cv.Description = doc.Description;
                FilesList.add(cv);
            }
            try{
                insert FilesList;
                
                Database.DeleteResult[] delRes = Database.delete(AttachmentList, false);
                if (delRes.size() > 0){
                    system.debug('delete result -- '+delRes);
                }
            }
            catch(DMLException e){
                error = e.getMessage();
            }
        }
        return null;
    }
    public PageReference saveContractRecord(){
        SavePoint sp = Database.setSavepoint();
        try{
            contract.RecordTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
            
            if(contract.Brief_Summary__c == null){
                error = 'Please input Brief Summary';
                return null;
            }else{
                //contract.Unchanged_legal_provided_template_used__c = legalcontract;
                //contract.Is_Sourcing_Contract__c= sourcingContract;
                //contract.Is_Binding_Corporate_Dev_Contract__c  = BindingCorp; '0010x00000SKHki'
                contract.REVVY__Account__c = [SELECT Id FROM Account WHERE Name=:Label.CafAccount LIMIT 1].Id;
                upsert contract;
                ContractId = contract.id;
                //return new PageReference('/apex/EmbeddedApprovals?resource=approvalbundle&entry=previewApproval#/approval/REVVY__MnContract__c/Contract%28s%29/'+contract.Name+'/'+contract.Id);
                return null;
            }
        }catch(DMLException e){
            Database.rollback(sp);
            error = e.getdmlMessage(0);
            return null;
        }catch(Exception e){
            Database.rollback(sp);
            error = e.getMessage();
            return null;
        }
    }
    public List<SelectOption> attachmentTypes
    {
        get
        {
            attachmentTypes = new List<SelectOption>();
            attachmentTypes.add(new SelectOption('', 'Select your Attachment Type'));
            attachmentTypes.add(new SelectOption('Contract', 'Contract'));
            attachmentTypes.add(new SelectOption('Justification Memo', 'Justification Memo'));
            attachmentTypes.add(new SelectOption('Legal Memo', 'Legal Memo'));
            attachmentTypes.add(new SelectOption('Other', 'Other'));
            return attachmentTypes;
        }
        set;
    }
    public List<SelectOption> BusinessUnits
    {
        get
        {
            BusinessUnits= new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = REVVY__MnContract__c.Business_Unit__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            BusinessUnits.add(new SelectOption('', '--None--'));
            for( Schema.PicklistEntry f : ple)
            {
                BusinessUnits.add(new SelectOption(f.getLabel(), f.getValue()));
            }       
            return BusinessUnits;
        }
        set;
    }
    public List<SelectOption> contractcurrency
    {
        get
        {
            contractcurrency= new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = REVVY__MnContract__c.REVVY__Currency__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
                contractcurrency.add(new SelectOption(f.getLabel(), f.getValue()));
            }       
            return contractcurrency;
        }
        set;
    }
    public void reRenderPage(){
       /* If (accountName!='' && accountName != null && accountName.length() > 25){
            accountName = accountName.substring(0,25)+'..';
        }
        If(parentName!='' && parentName!= null && parentName.length() > 25){
            parentName = parentName.substring(0,25)+'..';
        } */
        if(String.isNotBlank(contractvalue)){
            contractvalue = contractvalue.replace(',','');
            If(long.valueOf(contractvalue) < 100000){
                Lessthan100kBoolean = true;
            }else{
                Lessthan100kBoolean = false;
            }  
        }
    }
    public PageReference gotoDashboard(){
        return New PageReference('/apex/ContractRequesterDashboard');
    }
    @RemoteAction
    public static List<sObject> getLookupRecord(String likeString , String objectName,String whereClause) {
        if(String.IsNotBlank(likeString) && String.IsNotBlank(objectName)){
            likeString = '%'+likeString+'%';
            String query = '';
            query = 'SELECT Id,Name  FROM '+objectName+' WHERE name LIKE : likeString LIMIT 5';
            return database.query(query);
        }
        return null;
    }
    public class ApprovalWrapper{
        public REVVY__MnApprovalStepInst__c StepInstance{get;set;}
        public List<REVVY__MnStepApproverInst__c> ApproverNameList {get;set;}
        
        public ApprovalWrapper(REVVY__MnApprovalStepInst__c SI, List<REVVY__MnStepApproverInst__c> NameList){
            StepInstance = SI;
            ApproverNameList = NameList;
        }
    }
}