@isTest
public class ContractIntakeForm_CtrlTest{

    @isTest static void testMethod1(){
    
        Test.startTest();
        
        //Creating Group
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        //Creating QUEUE
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Contract_Intake_Form__c');
            insert testQueue;
        }
   
        Account acc = new account(name = 'test');
        insert acc;
        
        Contact con = new contact(LastName = 'Test', AccountId = acc.id);
        insert con;
        
        Case cse=new Case();
        insert cse;
 
        Contract_Intake_Form__c objCIForm = new Contract_Intake_Form__c();
        objCIForm.Description__c = 'test';
        objCIForm.Status__c = 'Assigned to Attorney';
        objCIForm.Review_Status__c = 'Accepted';
        objCIForm.Client_Name_and_business_unit__c = 'test';
        objCIForm.OwnerId = testGroup.Id;
        objCIForm.What_does_the_software_do__c = 'test';
        objCIForm.SaaS_self_hosted_Private_public_cloud__c = 'test';
        objCIForm.Integration_APIs__c = 'test';
        objCIForm.Source_code_escrow__c = 'test';
        objCIForm.premium_version_of_open_source_software__c = true;
        objCIForm.Source_code_escrow__c = 'test';
        objCIForm.provide_services_to_syndication_partners__c = 'Anticipated';
        
        insert objCIForm;
    
        ApexPages.StandardController sc = new ApexPages.StandardController(objCIForm);
        Contract_Intake_Form_Ctrl cif = new Contract_Intake_Form_Ctrl(sc);
        
        cif.attachment.Name = 'TestAttach';
        cif.attachment.Body = Blob.valueOf('Test Data');
        cif.attachment.parentId = objCIForm.Id;
        cif.save();
        
        Test.stopTest();
    
    }
}