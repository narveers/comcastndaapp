public class ApprovalStepInstanceHandler {
    Public static void updateCurrentApprover(Set<Id> StepInstSet){
        List<REVVY__MnApprovalStepInst__c> PendingApprovalStepInstList = new List<REVVY__MnApprovalStepInst__c>();
        Map<Id,List<String>> ApproverMap = new Map<Id,List<String>>();
        Map<Id,Id> ContractsMap = new Map<Id,Id>();
        List<String> ApproverList = new List<String>();
        List<REVVY__MnContract__c> UpdateContracts = new List<REVVY__MnContract__c>();
        
        if(StepInstSet.size()>0){
            system.debug('Step Inst Id - '+StepInstSet);
            for(REVVY__MnStepApproverInst__c SAI: [SELECT Id,REVVY__StepInst__c,REVVY__AssignedUser__r.Name FROM REVVY__MnStepApproverInst__c 
                                                   WHERE REVVY__Status__c IN ('Pending Approval','Not Started') AND REVVY__StepInst__c IN:StepInstSet ])
            {
                ApproverList.add(SAI.REVVY__AssignedUser__r.Name);
                ApproverMap.put(SAI.REVVY__StepInst__c,ApproverList);
                
            }
            system.debug('Approver Map-'+ApproverMap);
            for(REVVY__MnApprovalStepInst__c ASI: [SELECT Id,REVVY__ApprovalInst__r.REVVY__Contract__r.Current_Approver__c,
                                           REVVY__ApprovalInst__r.REVVY__Contract__c,
                                           REVVY__StepStatus__c FROM REVVY__MnApprovalStepInst__c 
                                           WHERE Id IN:StepInstSet AND REVVY__StepStatus__c='Pending Approval']){
                ContractsMap.put(ASI.REVVY__ApprovalInst__r.REVVY__Contract__c,ASI.Id);
            }
        }
        system.debug('ContractMap - '+ContractsMap);
        
       if(ContractsMap.size()>0){
            map<string,Procurement_User__c> procurementUserMap = new map<string,Procurement_User__c>();
            procurementUserMap = Procurement_User__c.getAll();
            
            for(REVVY__MnContract__c con: [SELECT Id,Current_Approver__c, Is_Sourcing_Contract__c, RequesterAddedPeterK__c,Procurement__c, Requester_Formula__c  FROM REVVY__MnContract__c WHERE Id IN:ContractsMap.keyset()]){
                if(ApproverMap.containskey(ContractsMap.get(con.Id))){
                    boolean userPeter = false;
                    boolean listedUser = false;
                    
                    
                    
                    for(String app: ApproverMap.get(ContractsMap.get(con.Id))){
                        if(con.Current_Approver__c != NULL){
                            con.Current_Approver__c = con.Current_Approver__c +','+ app ;
                        }else{
                            con.Current_Approver__c = app;
                        }
                        
                        if(!procurementUserMap.isEmpty() && procurementUserMap.containsKey(con.Requester_Formula__c))
                            listedUser = true;
                            
                        if(label.Procurement_User_Name == app && con.Is_Sourcing_Contract__c == 'No'){
                            
                            userPeter = true;
                        }
                        if(!procurementUserMap.isEmpty() && procurementUserMap.containsKey(app) && con.Is_Sourcing_Contract__c == 'No')
                            listedUser = true;
                    }
                    
                    con.RequesterAddedPeterK__c = 'No'; 
                    con.Procurement__c = 'No';
                    
                    if(userPeter == True){
                        con.RequesterAddedPeterK__c = 'Yes';
                    }
                    else if(listedUser == true){
                        con.Procurement__c = 'Yes';
                    }
                    else {
                        con.RequesterAddedPeterK__c = 'No';
                        con.Procurement__c = 'No';
                        }
                        
                    UpdateContracts.add(con);
                }
            }
            update UpdateContracts;
            
            system.debug('Update contracts - '+UpdateContracts);
        } 
    }
}