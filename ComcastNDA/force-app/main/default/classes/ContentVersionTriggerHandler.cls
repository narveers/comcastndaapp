public class ContentVersionTriggerHandler {
    public static String contractId;
    public static void onAfterInsert(List<ContentVersion> newList,Map<Id,ContentVersion> newMap){
        shareFilesForContracts(newList);
    }
    public static void shareFilesForContracts(List<ContentVersion> newList){
        try{
            List<Id> ContentLink = new List<Id>();
            Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
            Schema.SObjectType s = m.get('REVVY__MnContract__c') ;
            Schema.DescribeSObjectResult r = s.getDescribe() ;
            String keyPrefix = r.getKeyPrefix();
            Map<Id,Id> contentDocumentids = new Map<Id,Id>();
            List<ContentDocumentLink> linksToCreate = new List<ContentDocumentLink>();
            Set<Id> contractIds = new Set<Id>();
            Set<Id> userIds = new Set<Id>();
            for(ContentVersion cv : [SELECT Id, ContentDocumentId, FirstPublishLocationId  FROM ContentVersion WHERE Id =:newList and FirstPublishLocationId!=null]){
                if(String.valueOf(cv.FirstPublishLocationId).startsWith(keyPrefix)){
                    contractId = cv.FirstPublishLocationId  ;
                    contentDocumentids.put(cv.Id,cv.ContentDocumentId);
                    contractIds.add(cv.FirstPublishLocationId);
                }
            }
            for(REVVY__MnApprovalStepInst__c step : [select id,REVVY__Approver__r.REVVY__ApproverUser__c from REVVY__MnApprovalStepInst__c where REVVY__Approver__r.REVVY__ApproverUser__c!=null and REVVY__ApprovalInst__r.REVVY__Contract__c in:contractIds and REVVY__StepStatus__c!='Skipped']){
                userIds.add(step.REVVY__Approver__r.REVVY__ApproverUser__c);
            }
            for(REVVY__MnStepApproverInst__c approvers : [select id,REVVY__AssignedUser__c from REVVY__MnStepApproverInst__c where REVVY__AssignedUser__c!=null and REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c in:contractIds and REVVY__StepInst__r.REVVY__StepStatus__c!='Skipped' ]){
                userIds.add(approvers.REVVY__AssignedUser__c);
            }
            
            Boolean inactiveUser = false;
            for(User usr : [select id,IsActive from User where id in :userIds] ){
                if(!usr.IsActive){
                    inactiveUser = true;
                }
            }
            if(inactiveUser){
               IssueAssigningApprover();
            }else{
                
                for(ContentVersion cv : newList){
                    for(Id ids : userIds){
                        ContentDocumentLink cdl = new ContentDocumentLink();
                        cdl.ContentDocumentId = contentDocumentids.get(cv.Id);
                        cdl.LinkedEntityId = ids;
                        cdl.shareType = 'V';
                        cdl.Visibility='AllUsers';
                        linksToCreate.add(cdl);
                    }
                }
                insert linksToCreate;
            }
        }catch(Exception ex){
            system.debug(ex);
            IssueAssigningApprover();
        }
    }
    
    public static void IssueAssigningApprover(){
        contact objContact = [select id from contact where email != null limit 1];
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
        
        List<String> sendTo = new List<String>();
        sendTo.add('michael_iacovelli@comcast.com');
        mail.setToAddresses(sendTo);
        
        mail.setSubject('Issue assigning the approver');
        
        String body = 'Hi Mike,<br/>The contract '+Label.BaseLabel+'/'+contractId +' <br/>had issues assigning the approver, please check.<br/><br/>Thanks,';
        mail.setHtmlBody(body);
        
        mail.setTargetObjectId(objContact .Id);
        mail.setWhatId(contractId );
        mails.add(mail);
        Messaging.sendEmail(mails);
  
    }
}