/* 
    Name : DS_Recipe_Send_Env_Email_Controller
    Auther : Narveer S.
    Date : 08/24/2018
    Description : Docu Sign Automation class
*/
Public class DS_Recipe_Send_Env_Email_NDA_Controller {
    
    // SETTINGS
    Private static string integration_key;
    Private static string account_id; 
    Private Static string ds_server; 
    Private Static string ds_username;
    Private Static string ds_password;
     
    Public Static string NBCUsigner_email;
    Public Static string NBCUsigner_name;
    
    Public Static String CPsigner_email;
    Public Static String CPCUsigner_name;
    
    Public Static String counterpartyName;
    Public Static string email_message;
    Public Static string output;
    public Static Id NDARecordId;
    Public Static string envelope_id;
    Public Static string error_code {get;set;} // Null means no error
    Public Static string error_message {get;set;}
    public static string oid;
    Public static String NdaStatus;
    Public Static Docu_Sign__mdt objDocuSignMDT;

    Private static string trace_value = 'SFDC_004_SOAP_email_send'; // Used for tracing API calls
    Private static string trace_key = 'X-ray';
    Private Static DocuSignTK.APIServiceSoap api_sender = new DocuSignTK.APIServiceSoap();
    
    Public PageReference send() {
        NDARecordId = ApexPages.currentPage().getParameters().get('id');
        System.debug(NDARecordId);
        if(NDARecordId != Null) {
            NDA__c objNDA = [Select Id,Counterparty_Country__c,Discussions_with_this_firm__c,Is_there_intent_to_disclose_PII__c,NDA_Type__c,
                            Intent_to_pay_parties__c,Intent_to_disclose_information__c,intent_for_NBCU_to_disclose_HW_SW_Tech__c,Counterparty_Contact_Name__c,
                            Have_discussions_already_taken_place__c,Aware_of_an_existing_agreement__c,Term_Duration_years__c FROM NDA__c WHERE ID =: NDARecordId];
                
            if(objNDA != Null) {
                Blob content;
                Blob pdf;
                if(objNDA.NDA_Type__c.equals('One Way NBCU')) {
                    PageReference pr = page.OneWayPdf;
                    pr.getParameters().put('id',NDARecordId);
                    if (Test.IsRunningTest()){
                        content=Blob.valueOf('UNIT.TEST');
                    } else {
                        pdf = pr.getContentAsPDF();
                    }
                } else if(objNDA.NDA_Type__c.equals('One way Counterparty')) {
                    PageReference pr = page.OneWayCountPdf;
                    pr.getParameters().put('id',objNDA.id);
                    if (Test.IsRunningTest()){
                        content=Blob.valueOf('UNIT.TEST');
                    } else {
                        pdf = pr.getContentAsPDF();
                    }    
                } else if(objNDA.NDA_Type__c.equals('Mutual')) {
                    PageReference pr = page.MutualPdf;
                    pr.getParameters().put('id',objNDA.id);
                    if (Test.IsRunningTest()){
                        content=Blob.valueOf('UNIT.TEST');
                    } else {
                        pdf = pr.getContentAsPDF();
                    }    
                }
                    
                System.debug(pdf);
                send(objNDA.Id);
                
                Attachment NDAFormAttachment = new Attachment();
                NDAFormAttachment.Body = pdf;
                NDAFormAttachment.Name = objNDA.NDA_Type__c+ ' - Contract1.pdf';
                NDAFormAttachment.ParentId = objNDA.Id;
                
                insert NDAFormAttachment;
                
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'NDA Contarct Has been Submitted for Docu Sign');
                ApexPages.addMessage(myMsg);
                return null;       
            } else {
                System.debug('Else Part');
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'NDA Record is not Meeting Legal Review Condition');
                ApexPages.addMessage(myMsg);
                return null;
            }    
        } 
        return null;
    }
    
    public Static  PageReference doCancel() { 
        Id NDARecordId1 = ApexPages.currentPage().getParameters().get('id');
        PageReference demoPage = new pagereference('/' + NDARecordId1);
        demoPage.setRedirect(true);
        return demoPage;
    }
    
    Public static void send(Id NDARecordId) {
        oid = NDARecordId;
        ApexPages.Message myMsg;
        if( oid != null && String.isNotBlank( oid )) {
            List<Contact> oppList = new List<Contact>();
            List<NDA__c> lstNDARecrd = [SELECT Id,Comcast_Signer_Email__c,Comcast_Signer_Name__c,NBCU_Signer_Name__c,Comcast_Signer_Title__c,Custom_Template__c,
                                                           Counterparty__c,Counterparty_Signer_Email__c,Counterparty_Signer_Name__c,Counterparty_Entity_Name__c,
                                                           Counterparty_Signer_Title__c,Effective_Date__c,NDA_Type__c,Counterparty_Contact_Name__c,
                                                           Term_Duration_years__c,Termination_Date__c,Agreement_Number__c  FROM NDA__c WHERE Id =:oid LIMIT 1];
            if(lstNDARecrd.size() > 0){
                NdaStatus = lstNDARecrd[0].Custom_Template__c;
                NBCUsigner_email = lstNDARecrd[0].Comcast_Signer_Email__c;
                NBCUsigner_name = lstNDARecrd[0].NBCU_Signer_Name__c;
                CPsigner_email = lstNDARecrd[0].Counterparty_Signer_Email__c;
                CPCUsigner_name = lstNDARecrd[0].Counterparty_Contact_Name__c;
                counterpartyName = lstNDARecrd[0].Counterparty_Entity_Name__c;
                configure_sender();
                do_send(lstNDARecrd[0]);
            }else{
                error_message = 'Please select a Recipient to send Email!';
                error_code = 'INPUT_PROBLEM';
            }
            if(no_error()) {                
                NDA__c objNDA = new NDA__c(Id = NDARecordId, DocuSign_Status__c = 'Out for Signature – Counterparty');
                update objNDA;
            } 
        }
    }
    
    Private static void configure_sender() {
        
        objDocuSignMDT = [Select MasterLabel,DocuSign_URL__c,DocuSign_Account_ID__c,DocuSign_IntegratorKey__c,DocuSign_UserName__c,
                       DocuSign_Password__c from Docu_Sign__mdt WHERE MasterLabel = 'DocuSign Details'];
        if(objDocuSignMDT != Null) {
            integration_key = objDocuSignMDT.DocuSign_IntegratorKey__c;
            account_id = objDocuSignMDT.DocuSign_Account_ID__c;    
            ds_server = objDocuSignMDT.DocuSign_URL__c; 
            ds_username = objDocuSignMDT.DocuSign_UserName__c;
            ds_password = objDocuSignMDT.DocuSign_Password__c;    
        }
        
        api_sender.endpoint_x = ds_server;
        api_sender.inputHttpHeaders_x = new Map<String, String>();
        String auth =   '<DocuSignCredentials><Username>'+ ds_username +'</Username>' 
                      + '<Password>'+ ds_password +'</Password>' 
                      + '<IntegratorKey>' + integration_key + '</IntegratorKey></DocuSignCredentials>';

        api_sender.inputHttpHeaders_x.put('X-DocuSign-Authentication', auth);
    
    }
    Private static void do_send(NDA__c objNDARec) {
        List<ContentVersion> lstContent = new List<ContentVersion>();
        String emailSubject;
        // Check input
        if (String.isBlank(NBCUsigner_email) || String.isBlank(NBCUsigner_name) || !NBCUsigner_email.contains('@')) {
            error_message = 'Please Fill Contact-Role email to send email';
            error_code = 'INPUT_PROBLEM';
            return;
        }
        DocuSignTK.Recipient recipient;
        DocuSignTK.Recipient recipientNBCU;
        DocuSignTK.Document document;
        blob pdf;
        if(NdaStatus != Null) {
            if(NdaStatus.equals('Mutual Basic')) {
                System.debug('Mutual Basic');
                PageReference pr = page.MutualPdf;   
                pr.getParameters().put('id',oid);    
                if (Test.IsRunningTest()) {
                    pdf = BLOB.ValueOf('TEST');
                } else{
                    pdf = pr.getContentAsPDF();
                } 
            } else if(NdaStatus.equals('One Way Basic')) {
                System.debug('One Way Basic');
                PageReference pr = page.OneWayPdf;
                pr.getParameters().put('id',oid);
                if (Test.IsRunningTest()) {
                    pdf = BLOB.ValueOf('TEST');
                } else{
                    pdf = pr.getContentAsPDF();
                } 
            } else if(NdaStatus.equals('One Way International')) {
                System.debug('One Way International');
                PageReference pr = page.OneWayCountPdf;
                pr.getParameters().put('id',oid);    
                if (Test.IsRunningTest()) {
                    pdf = BLOB.ValueOf('TEST');
                } else{
                    pdf = pr.getContentAsPDF();
                } 
            }
            
            ContentVersion cv = new ContentVersion();
            cv.versionData = pdf;
            cv.title = objNDARec.Agreement_Number__c +' '+objNDARec.NDA_Type__c;
            cv.FirstPublishLocationId = objNDARec.id;
            cv.pathOnClient = objNDARec.Agreement_Number__c +' '+objNDARec.NDA_Type__c+'.pdf';
            cv.DocuSign_Required__c = True;
           
            lstContent.add(cv);
            
        }
        
        if(pdf != Null) {
            document = new DocuSignTK.Document();
            document.ID = 1;
            if(NdaStatus != Null && NdaStatus.equals('Mutual Basic')) {
                document.Name = 'Mutual Contract';
            } else if(NdaStatus != Null && NdaStatus.equals('One Way Basic')) {
                document.Name = 'One Way Contract';
            } else if(NdaStatus != Null && NdaStatus.equals('One Way International')) {
                document.Name = 'One Way CounterParty';
            }
            
            document.FileExtension = 'pdf';
            document.pdfBytes = EncodingUtil.base64Encode(pdf);
    
            recipient = new DocuSignTK.Recipient();
            recipient.Email = CPsigner_email;
            recipient.UserName = CPCUsigner_name;
            
            recipient.ID = 1;
            recipient.Type_x = 'Signer';
            recipient.RoutingOrder = 1;
            
            recipientNBCU = new DocuSignTK.Recipient();
            recipientNBCU.Email = NBCUsigner_email;
            recipientNBCU.UserName = NBCUsigner_name;
            
            recipientNBCU.ID = 2;
            recipientNBCU.Type_x = 'Signer';
            recipientNBCU.RoutingOrder = 2;
            
        } else{
            error_message = 'Please select a Valid Doc Attachment!';
            error_code = 'INPUT_PROBLEM';
            return;
        }
        
        DocuSignTK.Tab signHereTab5 = new DocuSignTK.Tab();
        signHereTab5.Type_x = 'Custom';
        signHereTab5.AnchorTabItem = new DocuSignTK.AnchorTab();
        signHereTab5.AnchorTabItem.AnchorTabString = 'CAddRess1';
        signHereTab5.AnchorTabItem.XOffset = -5;
        signHereTab5.AnchorTabItem.YOffset = -10;
        signHereTab5.FontSize = 'Size11';
        signHereTab5.Font = 'TimesNewRoman';
        signHereTab5.CustomTabWidth = 200;
        //signHereTab5.CustomTabHeight = 40;
        signHereTab5.CustomTabDisableAutoSize = True;
        signHereTab5.RecipientID = 1;
        signHereTab5.MaxLength = 30;
        signHereTab5.DocumentID = 1;
        signHereTab5.Name = 'Please enter Street address here';
        signHereTab5.ScaleValue = 30;
        signHereTab5.TabLabel = '{Authorized Signer’s Address1}';
        
        DocuSignTK.Tab signHereTab6 = new DocuSignTK.Tab();
        signHereTab6.Type_x = 'Custom';
        signHereTab6.AnchorTabItem = new DocuSignTK.AnchorTab();
        signHereTab6.AnchorTabItem.AnchorTabString = 'CAddRess2';
        signHereTab6.AnchorTabItem.XOffset = -5;
        signHereTab6.AnchorTabItem.YOffset = -10;
        signHereTab6.CustomTabWidth = 200;
        signHereTab6.FontSize = 'Size11';
        signHereTab6.Font = 'TimesNewRoman';
        //signHereTab6.CustomTabHeight = 40;
        signHereTab6.CustomTabDisableAutoSize = True;
        signHereTab6.RecipientID = 1;
        signHereTab6.DocumentID = 1;
        signHereTab6.MaxLength = 30;
        signHereTab6.Name = 'Please enter Street Address 2 here';
        signHereTab6.ScaleValue = 30;
        signHereTab6.TabLabel = '{Authorized Signer’s Address2}';
        
        DocuSignTK.Tab signHereTab7 = new DocuSignTK.Tab();
        signHereTab7.Type_x = 'Custom';
        signHereTab7.AnchorTabItem = new DocuSignTK.AnchorTab();
        signHereTab7.AnchorTabItem.AnchorTabString = 'CAddRess3';
        signHereTab7.FontSize = 'Size11';
        signHereTab7.Font = 'TimesNewRoman';
        signHereTab7.AnchorTabItem.XOffset = -5;
        signHereTab7.AnchorTabItem.YOffset = -10;
        signHereTab7.CustomTabWidth = 200;
        //signHereTab7.CustomTabHeight = 40;
        signHereTab7.CustomTabDisableAutoSize = True;
        signHereTab7.RecipientID = 1;
        signHereTab7.DocumentID = 1;
        signHereTab7.MaxLength = 30;
        signHereTab7.CustomTabRequired = False;
        signHereTab7.Name = 'Please enter Street Address 3 here';
        signHereTab7.ScaleValue = 30;
        signHereTab7.TabLabel = '{Authorized Signer’s Address3}';
        
        
        DocuSignTK.Tab signHereTab2 = new DocuSignTK.Tab();
        signHereTab2.Type_x = 'SignHere';
        signHereTab2.AnchorTabItem = new DocuSignTK.AnchorTab();
        signHereTab2.AnchorTabItem.AnchorTabString = 'By:C';
        signHereTab2.AnchorTabItem.XOffset = 40;
        signHereTab2.RecipientID = 1;
        signHereTab2.FontSize = 'Size11';
        signHereTab2.Font = 'TimesNewRoman';
        signHereTab2.DocumentID = 1;
        signHereTab2.MaxLength = 30;
        signHereTab2.Name = 'Please sign here';
        signHereTab2.ScaleValue = 1;
        signHereTab2.TabLabel = '{Authorized Signer’s Name}';
        
        DocuSignTK.Tab signHereTab3 = new DocuSignTK.Tab();
        signHereTab3.Type_x = 'Custom';
        signHereTab3.AnchorTabItem = new DocuSignTK.AnchorTab();
        signHereTab3.AnchorTabItem.AnchorTabString = 'Name:C';
        signHereTab3.AnchorTabItem.XOffset = 40;
        signHereTab3.AnchorTabItem.YOffset = -7;
        signHereTab3.CustomTabWidth = 200;
        signHereTab3.FontSize = 'Size11';
        signHereTab3.Font = 'TimesNewRoman';
        signHereTab3.RecipientID = 1;
        signHereTab3.MaxLength = 80;
        signHereTab3.DocumentID = 1;
        signHereTab3.Name = 'Please enter Name Here';
        signHereTab3.ScaleValue = 1;
        signHereTab3.TabLabel = '{Authorized Signer’s Name}';
        
        DocuSignTK.Tab signHereTab4 = new DocuSignTK.Tab();
        signHereTab4.Type_x = 'Custom';
        signHereTab4.AnchorTabItem = new DocuSignTK.AnchorTab();
        signHereTab4.AnchorTabItem.AnchorTabString = 'Title:C';
        signHereTab4.AnchorTabItem.XOffset = 40;
        signHereTab4.AnchorTabItem.YOffset = -7;
        signHereTab4.CustomTabWidth = 200;
        signHereTab4.FontSize = 'Size11';
        signHereTab4.Font = 'TimesNewRoman';
        signHereTab4.MaxLength = 80;
        signHereTab4.RecipientID = 1;
        signHereTab4.DocumentID = 1;
        signHereTab4.Name = 'Please enter Title here';
        signHereTab4.ScaleValue = 2;
        signHereTab4.TabLabel = '{Authorized Signer’s Title}';
        
        
        
        // The signer tab...
        DocuSignTK.Tab signHereTab = new DocuSignTK.Tab();
        signHereTab.Type_x = 'SignHere';
        signHereTab.AnchorTabItem = new DocuSignTK.AnchorTab();
        signHereTab.AnchorTabItem.AnchorTabString = 'By:N';
        signHereTab.AnchorTabItem.XOffset = 40;
        signHereTab.FontSize = 'Size11';
        signHereTab.Font = 'TimesNewRoman';
        signHereTab.RecipientID = 2;
        signHereTab.DocumentID = 1;
        signHereTab.Name = 'Please sign here';
        signHereTab.ScaleValue = 1;
        signHereTab.TabLabel = '{Authorized Signer’s Name}';
       // DocuSignTK.Tab tab2 = new DocuSignTK.Tab();
       // tab2.Type_x = 'DateSigned';
       // tab2.RecipientID = 1;
       // tab2.DocumentID = 1;
       // tab2.AnchorTabItem = new DocuSignTK.AnchorTab();
       // tab2.AnchorTabItem.AnchorTabString = 'Dated:';
       // tab2.AnchorTabItem.XOffset = 60;
        //tab2.ScaleValue = 1;
        //tab2.TabLabel = '<<Date>>';
        
        // Create an envelope and fill it in
        DocuSignTK.Envelope envelope = new DocuSignTK.Envelope();
        if(NdaStatus != Null && NdaStatus.equals('Mutual Basic')) {
            emailSubject = 'Please sign Document: Mutual Contract';
            envelope.Subject = 'Please sign Document: Mutual Contract';
        } else if(NdaStatus != Null && NdaStatus.equals('One Way Basic')) {
            emailSubject = 'Please sign Document: One Way Contract';
            envelope.Subject = 'Please sign Document: One Way Contract';
        } else if(NdaStatus != Null && NdaStatus.equals('One Way International')) {
            emailSubject = 'Please sign Document: One Way CounterParty';
            envelope.Subject = 'Please sign Document: One Way CounterParty';
        }
        envelope.AccountId  = account_id; 
        envelope.Tabs = new DocuSignTK.ArrayOfTab();
        envelope.Tabs.Tab = new DocuSignTK.Tab[7];  
        //envelope.Tabs.Tab[0] = signHereTab;        
        //envelope.Tabs.Tab[1] = tab2; 
        
        
        envelope.CustomFields = new DocuSignTK.ArrayOfCustomField();
        envelope.CustomFields.CustomField = new DocuSignTK.CustomField[1];
        DocuSignTK.CustomField env_customfield = new DocuSignTK.CustomField();
        
        env_customfield.Name = 'DSFSSourceObjectId';
        env_customfield.Show = 'false';
        env_customfield.Required = 'false';
        env_customfield.Value = oid;
        env_customfield.CustomFieldType = 'Text';
        env_customfield.ListItems = '';       
        envelope.CustomFields.CustomField[0] = env_customfield;
        
        System.debug(envelope);
        
        envelope.Tabs.Tab.add(signHereTab);
        envelope.Tabs.Tab.add(signHereTab2);
        envelope.Tabs.Tab.add(signHereTab3);
        envelope.Tabs.Tab.add(signHereTab4);
        envelope.Tabs.Tab.add(signHereTab5);
        envelope.Tabs.Tab.add(signHereTab6);
        envelope.Tabs.Tab.add(signHereTab7);
        
        envelope.Recipients = new DocuSignTK.ArrayOfRecipient();
        envelope.Recipients.Recipient = new DocuSignTK.Recipient[2];
        envelope.Recipients.Recipient.add(recipient);
        envelope.Recipients.Recipient.add(recipientNBCU);
        
        envelope.Documents = new DocuSignTK.ArrayOfDocument();
        envelope.Documents.Document = new DocuSignTK.Document[1];
        envelope.Documents.Document.add(document);
        
        if (String.isNotBlank(email_message)) {
            envelope.EmailBlurb = email_message;
        }
        
        System.debug(envelope);
        
        // Make the call
        try {
            if(!Test.isRunningTest()){
                DocuSignTK.EnvelopeStatus result = api_sender.CreateAndSendEnvelope(envelope);
                envelope_id = result.EnvelopeID;
                System.debug('@@@@@@envelope_id = '+  envelope_id);
                
                if(!lstContent.IsEmpty()) {
                    insert lstContent;
                }
                
                if(envelope_id != Null && !lstContent.isEmpty()) {
                    dsfs__DocuSign_Envelope__c objDocuSignEn = new dsfs__DocuSign_Envelope__c();
                    objDocuSignEn.dsfs__DocuSign_Email_Subject__c = emailSubject;
                    objDocuSignEn.dsfs__ChatterUpdatesEnabled__c = True;
                    objDocuSignEn.dsfs__Source_Object__c = NDARecordId;
                    objDocuSignEn.NDA__c = objNDARec.Id;
                    objDocuSignEn.dsfs__DocumentID__c = lstContent[0].id;
                    objDocuSignEn.dsfs__Document_Name__c = lstContent[0].pathOnClient;
                    if (String.isNotBlank(email_message)) {
                        objDocuSignEn.dsfs__DocuSign_Email_Message__c = email_message;
                    }
                    
                    objDocuSignEn.dsfs__DocuSign_Envelope_ID__c = envelope_id;
                    objDocuSignEn.dsfs__DocuSign_Envelope_Sent__c = System.Today();
                    
                    insert objDocuSignEn;
                    
                    dsfs__DocuSign_Envelope_Document__c objEndDoc = new dsfs__DocuSign_Envelope_Document__c();
                    objEndDoc.dsfs__Attachment_ID__c = String.ValueOf(lstContent[0].id);
                    objEndDoc.dsfs__Document_ID__c = String.ValueOf(lstContent[0].id);
                    objEndDoc.dsfs__Document_Name__c = lstContent[0].Title;
                    objEndDoc.dsfs__Document_Order__c = 1;
                    objEndDoc.dsfs__SFDocument_Type__c = 'Attachment';
                    objEndDoc.dsfs__Attachment_NameEx__c = lstContent[0].Title;
                    objEndDoc.dsfs__External_Document_Id__c = String.ValueOf(lstContent[0].Id);
                    objEndDoc.dsfs__DocuSign_EnvelopeID__c = objDocuSignEn.Id;
                    
                    insert objEndDoc;
                    
                    dsfs__DocuSign_Status__c objDocuSignStatus = new dsfs__DocuSign_Status__c();
                    objDocuSignStatus.dsfs__DocuSign_Envelope_ID__c = envelope_id;
                    objDocuSignStatus.dsfs__Sender__c = 'Comcast Legal';
                    objDocuSignStatus.NDA__c = objNDARec.Id;
                    objDocuSignStatus.dsfs__Sender_Email__c = 'legal_help@comcast.com';
                    objDocuSignStatus.dsfs__Subject__c = emailSubject;
                    objDocuSignStatus.dsfs__Envelope_Status__c = 'Draft';
                    objDocuSignStatus.File_Name__c = lstContent[0].Title;
                    
                    insert objDocuSignStatus;
                    
                    List<dsfs__DocuSign_Envelope_Recipient__c> lstEnvRecCP = new List<dsfs__DocuSign_Envelope_Recipient__c>();
                    
                    dsfs__DocuSign_Envelope_Recipient__c objEnvRecCP = new dsfs__DocuSign_Envelope_Recipient__c();
                    objEnvRecCP.dsfs__DocuSign_EnvelopeID__c = objDocuSignEn.Id;
                    objEnvRecCP.dsfs__DocuSign_Signature_Name__c = CPCUsigner_name;
                    objEnvRecCP.dsfs__Salesforce_Recipient_Type__c = 'Custom';
                    objEnvRecCP.dsfs__Routing_Order__c = Decimal.ValueOf('1');
                    objEnvRecCP.dsfs__RoleName__c = 'Signer1';
                    objEnvRecCP.dsfs__Recipient_Email__c = CPsigner_email;
                    objEnvRecCP.dsfs__DocuSign_Recipient_Role__c = 'CUSTOMER 1';
                    objEnvRecCP.dsfs__DocuSign_Signer_Type__c = 'SIGNER';
                    
                    lstEnvRecCP.add(objEnvRecCP);
                    
                    dsfs__DocuSign_Envelope_Recipient__c objEnvRecNBCU = new dsfs__DocuSign_Envelope_Recipient__c();
                    objEnvRecNBCU.dsfs__DocuSign_EnvelopeID__c = objDocuSignEn.Id;
                    objEnvRecNBCU.dsfs__DocuSign_Signature_Name__c = NBCUsigner_name;
                    objEnvRecNBCU.dsfs__Salesforce_Recipient_Type__c = 'Custom';
                    objEnvRecNBCU.dsfs__Routing_Order__c = Decimal.ValueOf('2');
                    objEnvRecNBCU.dsfs__RoleName__c = 'Signer2';
                    objEnvRecNBCU.dsfs__Recipient_Email__c = NBCUsigner_email;
                    objEnvRecNBCU.dsfs__DocuSign_Recipient_Role__c = 'CUSTOMER 2';
                    objEnvRecNBCU.dsfs__DocuSign_Signer_Type__c = 'SIGNER';
                    
                    lstEnvRecCP.add(objEnvRecNBCU);
                    
                    if(!lstEnvRecCP.IsEmpty()) {
                        insert lstEnvRecCP;
                    }
                    NDA__c objNDA = new NDA__c(Id = objNDARec.Id, DocuSign_Status__c = 'Out for Signature – Counterparty',NDA_Status__c = 'Out for Signature – Counterparty');
                    update objNDA;
                }
            }
        } catch ( CalloutException e) {
            System.debug('Exception - ' + e );
            error_code = 'Problem: ' + e;
            error_message = error_code;
        }    
    
    }
    Private static Boolean no_error() {
        return (String.isEmpty(error_code));
    }
}