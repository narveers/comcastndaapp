public class EnvelopeStatusParser{
    public class Envelopes {
        public String status;
        public String documentsUri;
        public String recipientsUri;
        public String attachmentsUri;
        public String envelopeUri;
        public String envelopeId;
        public String customFieldsUri;
        public String notificationUri;
        public String statusChangedDateTime;
        public String documentsCombinedUri;
        public String certificateUri;
        public String templatesUri;
    }

    public String resultSetSize;
    public String totalSetSize;
    public String startPosition;
    public String endPosition;
    public String nextUri;
    public String previousUri;
    public List<Envelopes> envelopes;

    
    public static EnvelopeStatusParser parse(String json) {
        return (EnvelopeStatusParser) System.JSON.deserialize(json, EnvelopeStatusParser.class);
    }
}