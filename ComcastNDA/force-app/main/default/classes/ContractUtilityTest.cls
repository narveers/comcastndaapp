/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class ContractUtilityTest {

    static testMethod void myUnitTest() {
        
        Account acc = new Account(Name='TestData-'+System.now().getTime());
        insert acc;
        
        REVVY__MnContract__c con = new REVVY__MnContract__c();
        con.Name='ContractUtilityTest'+System.now().getTime();
        con.Contract_Value__c = 100;
        con.REVVY__Account__c = acc.Id;
        con.Internal_Client_Group__c = 'Model N';
        con.Send_Email__c = True;
        con.Template_Unique_Name__c='EMnContractExpireNotif';
        con.Expiration_Date__c = System.today().addDays(30);
        con.Is_Sourcing_Contract__c = 'Yes';
        insert con;
        
        
        Map<String, Schema.SObjectField> contractFieldsMap = Schema.SobjectType.REVVY__MnContract__c.fields.getMap();
        String scontractFields = ContractUtility.getObjectFields(contractFieldsMap) + ', RecordType.DeveloperName, RecordType.Name ';
        String scontractCriteria = ' where Id = :sid';
        String contractQuery = ContractUtility.buildSQLStatement(scontractFields, 'REVVY__MnContract__c', scontractCriteria);
        
    }
}