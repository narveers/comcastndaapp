@isTest
public class ChangeOwnerExtTest{

    @isTest static void testMethod1(){
    
        Test.startTest();
        
        //Creating Group
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        //Creating QUEUE
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Contract_Intake_Form__c');
            insert testQueue;
        }
        Account acc = new account(name = 'test');
        insert acc;
        
        Contact con = new contact(LastName = 'Test', AccountId = acc.id);
        insert con;
        
        List<Contract_Intake_Form__c> lstContractIntake = new List<Contract_Intake_Form__c>();
        
        Contract_Intake_Form__c objCIForm = new Contract_Intake_Form__c();
        objCIForm.Description__c = 'test';
        objCIForm.Status__c = 'Assigned to Attorney';
        objCIForm.Review_Status__c = 'Accepted';
        objCIForm.Client_Name_and_business_unit__c = 'test';
        objCIForm.OwnerId = testGroup.Id;
        
        insert objCIForm;
        
        lstContractIntake.add(objCIForm);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objCIForm);
        ChangeOwnerExt rce = new ChangeOwnerExt(sc);
        
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstContractIntake);
        stdSetController.setSelected(lstContractIntake);
        ChangeOwnerExt objReviewContractsExt = new ChangeOwnerExt(stdSetController);
        rce.init();
        rce.ownerId = UserInfo.getUserId();
        rce.ownerName = 'test';
       // rce.valueSelection();
        rce.save();
        
        
        objReviewContractsExt.init();
        objReviewContractsExt.ownerId = UserInfo.getUserId();
        objReviewContractsExt.save();
        
        
        Test.stopTest();
    
    }
    
}