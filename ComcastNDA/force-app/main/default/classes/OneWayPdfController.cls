/* 
    Name : OneWayPdfController
    Auther : Narveer S.
    Date : 03/04/2018
    Description : One Way PDF Controller to fetch the data
*/
Public class OneWayPdfController {
    
    public List<NDA__c> lstNDARecrd{get;set;}
    public NDA__c NDARecrd{get;set;}
    public String NdaRecrdId{get;set;}
    
    public OneWayPdfController(){
        lstNDARecrd = new List<NDA__c>();
        NdaRecrdId = ApexPages.currentPage().getParameters().get('id');
        System.debug(NdaRecrdId);
        lstNDARecrd = [SELECT Id,Comcast_Signer_Email__c,Comcast_Signer_Name__c,Counterparty_Contact_Name__c,Comcast_Signer_Title__c,NBCU_Legal_Entity_Name__c,Counterparty_Entity_Name__c,
                                                           Counterparty__c,Counterparty_Signer_Email__c,Counterparty_Signer_Name__c,Purpose__c,
                                                           Counterparty_Signer_Title__c,Effective_Date__c,NDA_Type__c,NBCU_Signer_Name__c,NBCU_Signer_Title__c,
                                                           Term_Duration_years__c,Termination_Date__c FROM NDA__c WHERE Id =:NdaRecrdId LIMIT 1];    
        System.debug(lstNDARecrd);

    }
    public OneWayPdfController(ApexPages.StandardController controller) {
    
        lstNDARecrd = new List<NDA__c>();
        NdaRecrdId = ApexPages.currentPage().getParameters().get('id');
        System.debug(NdaRecrdId);
        if(NdaRecrdId  != null)
        NDARecrd = [SELECT Id,Comcast_Signer_Email__c,Comcast_Signer_Name__c,Counterparty_Contact_Name__c,Comcast_Signer_Title__c,NBCU_Legal_Entity_Name__c,Counterparty_Entity_Name__c,
                                                           Counterparty__c,Counterparty_Signer_Email__c,Counterparty_Signer_Name__c,Purpose__c,
                                                           Counterparty_Signer_Title__c,Effective_Date__c,NDA_Type__c,NBCU_Signer_Name__c,NBCU_Signer_Title__c,
                                                           Term_Duration_years__c,Termination_Date__c FROM NDA__c WHERE Id =:NdaRecrdId LIMIT 1];    
        System.debug(lstNDARecrd);
    }
}