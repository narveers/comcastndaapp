global class CMentorApprovalInboundEmailHandler implements Messaging.InboundEmailHandler {
    public String FromAddress;
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope) {
        String FirstLineBody = '';
        String ReferenceId = '';
        String Comments = '';
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        if(!String.isBlank(email.htmlBody)){
            FirstLineBody = email.htmlBody.split('<br>')[0].substringAfter('>');
            ReferenceId = email.htmlBody.split('<br>')[1].substringAfter('Reference:');
            Comments = email.htmlBody.split('<br>')[2].substringAfter('Comments:');
            if(String.isNotBlank(ReferenceId) && String.isNotBlank(FirstLineBody)){
                system.debug('First line - '+FirstLineBody);
                System.debug('Reference - '+ReferenceId);
                System.debug('Comments - '+Comments);
                system.debug('From adress - '+email.fromAddress);
                //saveApprovals(ReferenceId);
                //SendEmail(FirstLineBody,ReferenceId,Comments,email.subject);
            }
        }
        result.success = true;
        return result;
    }
    @TestVisible Private void sendEmail(String FirstLineBody, String ReferenceId, String Comments, String Subject){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //To address
        List<String> sendTo = new List<String>();
        EmailServicesAddress EmailService = [SELECT EmailDomainName,LocalPart FROM EmailServicesAddress WHERE DeveloperName ='AdvancedApprovalsEmail'];
        String ToAddress = EmailService.LocalPart+'@'+EmailService.EmailDomainName;
        SendTo.add(ToAddress);
        //SendTo.add('sandeep@cmentor.com');
        mail.setToAddresses(sendTo);
        //From address
        mail.setReplyTo('sandeep@cmentor.com');
        //bcc address
        List<String> ccTo = new List<String>();
        //ccTo.add('CMS_Notifications@comcast.com');
        ccTo.add('sandeep@cmentor.com');
        mail.setBccAddresses(ccTo);
        //subject
        mail.setSubject(Subject);
        //Body
        String Body = FirstLineBody;
        Body += '\n'+Comments+'\n ----------------------- \n If you would like to add any comments to this approval, please enter them on the line after '
            +Body+
            '.%0DPlease do not modify the reference id.\n<stepInstance>'+ReferenceId+'</stepInstance>';
        mail.setPlainTextBody(EncodingUtil.urlDecode(Body, 'UTF-8'));
        //mail.setHtmlBody(Body);
        mails.add(mail);
        Messaging.sendEmail(mails);
    }
    @TestVisible private void saveApprovals(String ReferenceId){
        List<REVVY__MnStepApproverInst__c> SAI = new List<REVVY__MnStepApproverInst__c>();
        List<REVVY__MnStepApproverInst__c> UpdateSAI = new List<REVVY__MnStepApproverInst__c>();
        SAI = [SELECT Id,REVVY__Status__c FROM REVVY__MnStepApproverInst__c WHERE REVVY__StepInst__c =:ReferenceId AND REVVY__Status__c='Pending Approval' ORDER BY CreatedDate ASC];
        if(SAI.size()>0){
            system.debug('SAI - '+SAI); 
            SAI[0].REVVY__Status__c = 'Approved';
        }
        UpdateSAI.add(SAI[0]);
        if(UpdateSAI.size()>0){
            update UpdateSAI;
        }
    } 
}