/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class CMTermSheetBatchTest {
    
        private static testMethod void myUnitTest1() {
            
        Account acc = new Account(Name='TestData'+System.now());
        insert acc;
        
        RecordType rt = [Select Id, Name, DeveloperName from RecordType where SObjectType = 'REVVY__MnContract__c' and DeveloperName='Fandango'];
       
        REVVY__MnLifeCycle__c lc = [Select Id From REVVY__MnLifeCycle__c where REVVY__RecordType__c = 'Fandango' and REVVY__Status__c='Active'];
        
        REVVY__MnContract__c con = new REVVY__MnContract__c();
        con.Name='CMTermsheetControllerTest-'+System.now().getTime();
        con.Contract_Value__c = 100;
        con.REVVY__Account__c = acc.Id;
        con.Internal_Client_Group__c = 'Model N';
        con.Template_Unique_Name__c='EMnContractExpireNotif';
        con.Expiration_Date__c = System.today().addDays(30);
        con.RecordTypeId = rt.Id;
        con.REVVY__LifecycleConfig__c = lc.Id;
        con.REVVY__Phase__c = CMTermSheetBatch.PHASE;
        con.REVVY__SubPhase__c = CMTermSheetBatch.SUBPHASE;
        insert con;
       
        Test.startTest();    
        CMTermSheetBatch  objBatc= new CMTermSheetBatch();
        Database.executeBatch(objBatc);
            
        Test.stopTest();
    } 
}