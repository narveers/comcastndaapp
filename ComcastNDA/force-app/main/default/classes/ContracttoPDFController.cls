public class ContracttoPDFController {



    public static REVVY__MnContract__c contract{get;set;}
    public List<ContentVersion> content{get;set;}
    public static Id contractId;
    
  
    
   
    //public List<REVVY__MnApprovalStepInst__c> ApprovalInstList{get;set;} Mohit Removed
    
    //Mohit Added
    public List<REVVY__MnApprovalStepInst__c> ApprovalInstListTemp{get;set;}
    
               
             

    
   // public wrapper owrapper{get;set;}
    public ContracttoPDFController(ApexPages.StandardController controller){
       contractId = controller.getId();
       content = new List<ContentVersion>();
        //contractIdList = new List<Id>();
       //ApprovalInstList = new List<REVVY__MnApprovalStepInst__c>(); Mohit Removed
      // List< REVVY__MnApprovalStepInst__c >ApprovalInstListTemp = new List<REVVY__MnApprovalStepInst__c>();Mohit Removed
        ApprovalInstListTemp = new List<REVVY__MnApprovalStepInst__c>();
         
       contract = [SELECT Id,CAF_Type__c,Requester_Formula__c,toLabel(Business_Unit__c),Name,Counterparty__c,Contract_Value__c,REVVY__Phase__c,
                   Is_Sourcing_Contract__c,Unchanged_legal_provided_template_used__c,Is_Binding_Corporate_Dev_Contract__c,
                   Brief_Summary__c, CreatedDate
                   FROM REVVY__MnContract__c WHERE Id =: contractId];
        system.debug('This is the contract - '+contract);
        content = [SELECT Title,Attachment_Type__c,Description FROM ContentVersion WHERE FirstPublishLocationId =: contractId];
         List<REVVY__MnApprovalInst__c> ApprovalProcess = [SELECT Id,Name FROM REVVY__MnApprovalInst__c WHERE REVVY__Contract__c=: contractId ORDER BY LastModifiedDate DESC LIMIT 1];
        
        
        ApprovalInstListTemp =  [SELECT Name,
                             (SELECT name,REVVY__ActionDate__c,REVVY__ApprovalComments__c,REVVY__Status__c FROM REVVY__StepInstApprovers__r) 
                             FROM REVVY__MnApprovalStepInst__c WHERE REVVY__ApprovalInst__c IN :ApprovalProcess AND REVVY__StepStatus__c != 'Skipped' ORDER BY REVVY__Step_Sequence__c ASC];
                             
       for (REVVY__MnApprovalStepInst__c sip: ApprovalInstListTemp ){
       System.debug ( 'ApprovalInstListTemp' + sip.REVVY__StepInstApprovers__r);
       
       }                      
    }
    @Future(callout=true)
    public static void saveToContractFuture (List<Id> contractId)
    {
    List<REVVY__MnContract__c> contract = new List<REVVY__MnContract__c>();
        List<ContentVersion> ContentList = new List<ContentVersion>();
        try{
            contract = [SELECT Id,Name FROM REVVY__MnContract__c WHERE Id IN:contractId]; // remove this query, if in future, name is not used
           // system.debug('id - '+contract[0].Id + ' & name - '+contract[0].Name );        // for the title of content version below
            for(REVVY__MnContract__c con:contract){
                PageReference PDF = Page.ContracttoPDF;
                PDF.getParameters().put('Id',con.Id);
                PDF.setRedirect(true);
                ContentVersion cv = new ContentVersion();
                cv.title = 'CAF Approval Summary';                  
                cv.FirstPublishLocationId = con.Id; 
                cv.pathOnClient = 'CAF Approval Summary.pdf';
                cv.Attachment_Type__c = 'Other';
                cv.Description = 'This document contains the approval history and the contract details.';                
                cv.versionData = PDF.getContent();
                
                
                ContentList.add(cv);
            }
            
 
          if(ContentList.size()>0){
                insert ContentList;
            }
        }
        catch(DmlException ex){
            system.debug('Exception - '+ex);
        }
        catch(Exception ex){
            system.debug('Exception - '+ex);
        }
    
    }
    @InvocableMethod
    public static void saveToContract(List<Id> contractId){
        saveToContractFuture(contractId);
    }
    @AuraEnabled
    public static void saveCAFSummary(String contractId){
        try{
            System.debug('before');
            REVVY__MnContract__c contract = [SELECT Id,Name FROM REVVY__MnContract__c WHERE Id =: contractId];
            System.debug('after');
            PageReference PDF = Page.ContracttoPDF;
            PDF.getParameters().put('Id',contract.Id);
            PDF.setRedirect(true);
            
            List<ContentDocumentLink> cdl = new List<ContentDocumentLink>([SELECT ContentDocumentId FROM  ContentDocumentLink WHERE LinkedEntityId = :contractId and ContentDocument.Title ='CAF Approval Summary' LIMIT 1]);
            
            if(cdl.size()>0){
                ContentVersion contentVersion = new ContentVersion(
                    ContentDocumentId = cdl[0].ContentDocumentId,
                    Title = 'CAF Approval Summary',
                    PathOnClient = 'CAF Approval Summary.pdf',
                    ReasonForChange = 'Updated CAF Summary',
                    VersionData = PDF.getContent(),
                    IsMajorVersion = true
                );
                insert contentVersion;
            }else{
                ContentVersion cv = new ContentVersion();
                cv.title = 'CAF Approval Summary';                  
                cv.FirstPublishLocationId = contract.Id; 
                cv.pathOnClient = 'CAF Approval Summary.pdf';
                cv.Attachment_Type__c = 'Other';
                cv.Description = 'This document contains the approval history and the contract details.';
                cv.versionData = PDF.getContent();
                insert cv;
            }
        }
        catch(Exception ex){
            system.debug('Exception - '+ex.getMessage());
            system.debug('Exception - '+ex.getStackTraceString());
        }
    }
   /* public class wrapper{
        public boolean ColorBoolean{get;set;}
        public List<REVVY__MnApprovalStepInst__c> ApprovalInstList{get;set;}
        public wrapper(List<REVVY__MnApprovalStepInst__c> AppList,boolean b){
            this.ApprovalInstList= AppList;
            this.ColorBoolean=b;
        }
    } */
}