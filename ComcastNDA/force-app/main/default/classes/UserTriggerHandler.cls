public class UserTriggerHandler {
    public static void onBeforeInsert(List<User> newUsers){
        addUserToProfileAndRole(newUsers);
    }
	public static void onAfterInsert(List<User> newUsers){
        assignPermissionSet(newUsers);
    }
    public static void addUserToProfileAndRole(List<User> newUsers){
        Map<String,Id> profileIds = new Map<String,Id>();
        Map<String,Id> roleIds = new Map<String,Id>();
        for(Profile prof : [select id,name from Profile]){
            profileIds.put(prof.name,prof.Id);
        } 
        for(UserRole rol : [select id,name from UserRole]){
            roleIds.put(rol.name,rol.Id);
        }
        for(User usr : newUsers){
            usr.UserPreferencesDisableAllFeedsEmail = true; // Disables notification to user via chatter API
            if(usr.profileId == null && usr.Profile_Name__c!=null && profileIds.containsKey(usr.Profile_Name__c)){
                usr.profileId = profileIds.get(usr.Profile_Name__c);
            }
            if(usr.UserRoleId == null && usr.Role_Name__c!=null && roleIds.containsKey(usr.Role_Name__c)){
                usr.UserRoleId = roleIds.get(usr.Role_Name__c);
            }
        }
    }
    public static void assignPermissionSet(List<User> newUsers){
        List<PermissionSetAssignment> assignedPermissionSets = new List<PermissionSetAssignment>();
        Map<String,Id> permissionSetIds = new Map<String,Id>();
        Map<String,String> permissionSetProfileRoleMapping = new Map<String,String>();
        for(PermissionSet ps : [select id,label from PermissionSet]){
            permissionSetIds.put(ps.label,ps.Id);
        }
        for(Profile_Role_Permission_Set_Mapping__mdt mapping : [select id,Permission_Set_Name__c,Profile_Name__c,Role_Name__c from Profile_Role_Permission_Set_Mapping__mdt]){
            if(permissionSetProfileRoleMapping.containsKey(mapping.Profile_Name__c + ';' + mapping.Role_Name__c)){
                permissionSetProfileRoleMapping.put(mapping.Profile_Name__c + ';' + mapping.Role_Name__c,permissionSetProfileRoleMapping.get(mapping.Profile_Name__c + ';' + mapping.Role_Name__c)+';'+mapping.Permission_Set_Name__c);
            }else{
                permissionSetProfileRoleMapping.put(mapping.Profile_Name__c + ';' + mapping.Role_Name__c,mapping.Permission_Set_Name__c);
            }
        }
        for(User usr : newUsers){
            if(usr.Profile_Name__c!=null && usr.Role_Name__c!=null && permissionSetProfileRoleMapping.containsKey(usr.Profile_Name__c + ';' + usr.Role_Name__c)){
                for(String str : permissionSetProfileRoleMapping.get(usr.Profile_Name__c + ';' + usr.Role_Name__c).split(';')){
                    if(permissionSetIds.containsKey(str)){
                        assignedPermissionSets.add(new PermissionSetAssignment(AssigneeId = usr.Id , PermissionSetId = permissionSetIds.get(str) ));
                    }
                }
            }
        }
        if(assignedPermissionSets.size()>0){
            insert assignedPermissionSets;
        }
    }
}