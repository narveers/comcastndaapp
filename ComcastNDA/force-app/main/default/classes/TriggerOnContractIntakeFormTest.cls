@isTest
public class TriggerOnContractIntakeFormTest {
    @isTest static void Method1() {
        
        //Creating Group
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        //Creating QUEUE
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Contract_Intake_Form__c');
            insert testQueue;
        }
        
        Contract_Intake_Form__c objCIForm = new Contract_Intake_Form__c();
        objCIForm.Description__c = 'test';
        objCIForm.Status__c = 'Assigned to Attorney';
        objCIForm.Review_Status__c = 'Accepted';
        objCIForm.Client_Name_and_business_unit__c = 'test';
        objCIForm.OwnerId = testGroup.Id;
        
        insert objCIForm;
        
        objCIForm.Review_Status__c = 'Rejected';
        
        Update objCIForm;
              
    }
}