/*
Class Name : ApprovalStepApproverInstTriggerHandler
Called From: Trigger on REVVY__MnStepApproverInst__c Object
Decsription: Manage security of the Contract by each approvers and some logic 
to set fields at the contract. 
History    : Date         Modification        Modified By 
10/01/2018    Initial Draft        Sandeep
03/26/2019    Cleanup the code     Mohit/Mahi
*/ 
public class ApprovalStepApproverInstTriggerHandler {
    
    /*
Method Name: RemoveDuplicates
Called From : After Insert Trigger
Description : Remove the duplicate approvers
*/
    public static void RemoveDuplicates(List<REVVY__MnStepApproverInst__c> newList ){
        Set<Id> stepInstanceIds = new Set<Id>();    //initializing set to store Approval Step Instance sObject Ids 
        Set<Id> contractIds = new Set<Id>();    //initializing set to store Contract sObject Ids
        Set<Id> ApproverSet = new Set<Id>();    //not using only initialized
        List<Id> ContractFilesList = new List<Id>();    //not using only initialized
        for(REVVY__MnStepApproverInst__c approvers : newList){
            if(approvers.REVVY__AssignedUser__c!=null){
                stepInstanceIds.add(approvers.REVVY__StepInst__c);
            }
        }
        if(stepInstanceIds.size()>0){   //checking size of stepInstanceIds set 
            //enhanced for loop and using soql query to fetch REVVY__MnApprovalStepInst__c records where Id in stepInstanceIds set 
            for(REVVY__MnApprovalStepInst__c step : [select id,REVVY__ApprovalInst__r.REVVY__Contract__c 
                                                     from REVVY__MnApprovalStepInst__c where id in: stepInstanceIds and REVVY__ApprovalInst__r.REVVY__Contract__c!=null]){
                                                         contractIds.add(step.REVVY__ApprovalInst__r.REVVY__Contract__c);   //adding contract Ids in contractIds
                                                     }
        }
        
        //intializing map to put duplicate REVVY__MnStepApproverInst__c records 
        Map<String, REVVY__MnStepApproverInst__c > DeleteMap = New Map<String, REVVY__MnStepApproverInst__c >();
        List< REVVY__MnStepApproverInst__c> DelApprover = new List< REVVY__MnStepApproverInst__c>();    //intializing list to perform delete operations 
        
        //enhanced for loop and using soql query to fetch REVVY__MnStepApproverInst__c records where contactId in contractIds set 
        for (REVVY__MnStepApproverInst__c aList: [ Select id, REVVY__StepInst__c, REVVY__Approver__c, REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c, REVVY__AssignedUser__c 
                                                  from REVVY__MnStepApproverInst__c
                                                  Where REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c in :contractIds]){
                                                      
                                                      // Concatenating Contract, Step and Approver
                                                      String DupStr = aList.REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c +'-'+ aList.REVVY__StepInst__c+'-'+aList.REVVY__AssignedUser__c ;                                                
                                                      
                                                      If ( DeleteMap.Containskey(DupStr)){
                                                          DelApprover.add(DeleteMap.get(DupStr)); 
                                                          
                                                      } else{
                                                          
                                                          DeleteMap.put(DupStr, aList);
                                                          
                                                      }
                                                      
                                                  }  
        //checking map size
        if (DeleteMap.size() > 0){
            Delete DelApprover; //deleting duplicate approvers records 
        } 
        
    }
    
    private static void SetContractFields(Set<Id> ContractIds, Map<id, List<String>> AMap, String CalledFrom ){
        List<REVVY__MnContract__c> UpdateContracts = new List<REVVY__MnContract__c>();
        map<string,Procurement_User__c> procurementUserMap = new map<string,Procurement_User__c>();
        procurementUserMap = Procurement_User__c.getAll();
        
        for(REVVY__MnContract__c con: [SELECT Id,Current_Approver__c,  
                                       Is_Sourcing_Contract__c, RequesterAddedPeterK__c,
                                       Procurement__c, Requester_Formula__c 
                                       FROM REVVY__MnContract__c 
                                       WHERE Id IN: ContractIds]){
                                           system.debug('---Hi I am Mahi, I am inside Loop--');
                                           if(AMap.containskey(con.Id)){
                                               boolean userPeter = false;
                                               boolean listedUser = false;
                                               
                                               for(String app: AMap.get(con.Id)){
                                                   if(!procurementUserMap.isEmpty() && ( procurementUserMap.containsKey(con.Requester_Formula__c) ||procurementUserMap.containsKey(app) ))
                                                       listedUser = true;
                                                   if(label.Procurement_User_Name == app )
                                                       userPeter = true;
                                                   
                                               }
                                               if(userPeter == True){
                                                   con.RequesterAddedPeterK__c = 'Yes';
                                               } 
                                               if(listedUser == true){
                                                   con.Procurement__c = 'Yes';
                                               } 
                                               If( CalledFrom == 'Delete' && listedUser != True   )
                                                   con.Procurement__c = 'No';
                                               If( CalledFrom == 'Delete' && userPeter != True )
                                                   con.RequesterAddedPeterK__c = 'No';   
                                               
                                               UpdateContracts.add(con);
                                           }
                                       }
        if(UpdateContracts.size()>0){
            system.debug('contracts to update - '+UpdateContracts);
            Update UpdateContracts;
        }
    }
    /*
Method Name: OnInsert
Called From : After Insert Trigger
Description : Setting Peter K and Procurement Fields on the contract
*/
    public static void onInsert(Set<id> ApproverIds){
        try{
            // Variable Declartion
            Map<Id,List<String>> ApproverMap = new Map<Id,List<String>>();
            Map<Id,Id> ContractsMap = new Map<Id,Id>();
            List<String> ApproverList = new List<String>();
            Set<Id> ConSetIds = New Set<Id>();
            
            // Getting all approvers                                      
            for(REVVY__MnStepApproverInst__c SAI: [SELECT Id,REVVY__StepInst__c,
                                                   REVVY__AssignedUser__r.Name ,
                                                   REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c  
                                                   FROM REVVY__MnStepApproverInst__c 
                                                   WHERE  id IN: ApproverIds
                                                   and REVVY__AssignmentReason__c != 'System Assignment']){
                                                       
                                                       ApproverList.add(SAI.REVVY__AssignedUser__r.Name);
                                                       ApproverMap.put(SAI.REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c,ApproverList);
                                                       
                                                   }
            
            if(ApproverMap.size()>0){
                SetContractFields(ApproverMap.keySet(),ApproverMap, 'Insert' );
                
            }
        }catch(Exception ex){
            system.debug('Exception - '+ex);
        }
    }
    
    public static void onAfterDelete(Set<id> Stepids){
        
        try{
            Map<Id,List<String>> ApproverMap = new Map<Id,List<String>>();
            Map<Id,Id> ContractsMap = new Map<Id,Id>();
            List<String> ApproverList = new List<String>();
            List<REVVY__MnContract__c> UpdateContracts = new List<REVVY__MnContract__c>();
            Set<Id> ConSetIds = New Set<Id>(); 
            
            for(REVVY__MnApprovalStepInst__c SAI: [SELECT Id, REVVY__ApprovalInst__r.REVVY__Contract__c 
                                                   FROM REVVY__MnApprovalStepInst__c  
                                                   WHERE  Id IN: Stepids]) {
                                                       ConSetIds.add(sai.REVVY__ApprovalInst__r.REVVY__Contract__c);  
                                                   }                                      
            System.debug('---Vikram2---'+ConSetIds);                                      
            for(REVVY__MnStepApproverInst__c SAI: [SELECT Id,REVVY__StepInst__c,REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c,REVVY__AssignedUser__r.Name FROM REVVY__MnStepApproverInst__c 
                                                   WHERE  REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c IN:ConSetIds and REVVY__AssignmentReason__c != 'System Assignment']) {
                                                       ApproverList.add(SAI.REVVY__AssignedUser__r.Name);
                                                       ApproverMap.put(SAI.REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c,ApproverList);
                                                   }
            
            System.debug('---Vikram3---'+ApproverMap); 
            
            if(ApproverMap.size()>0){
                SetContractFields(ApproverMap.keySet(),ApproverMap, 'Delete' );
                
            } 
            
        }catch(Exception ex){
            system.debug('Exception - '+ex);
        }
        
    }
    
    
    public static void updateApproveronContract(Set<Id> newList){
        try{
            system.debug('New Approvers Set Id - '+newList);
            Map<Id,List<String>> ApproverMap = new Map<Id,List<String>>();
            Map<Id,List<String>> goodApproverMap = new Map<Id,List<String>>();
            Map<Id,Id> ContractsMap = new Map<Id,Id>();
            List<String> ApproverList = new List<String>();
            List<String> goodApproverList = new List<String>();
            Set<id> ConSetIds = New Set<id>();
           
            List<REVVY__MnContract__c> UpdateContracts = new List<REVVY__MnContract__c>();
            for(REVVY__MnStepApproverInst__c SAI: [SELECT Id,REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c, REVVY__Status__c , REVVY__AssignedUser__r.Name FROM REVVY__MnStepApproverInst__c 
                                                   WHERE  Id IN:newList])
            {
                
                ConSetIds.add(sai.REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c);
            }
                
             for(REVVY__MnStepApproverInst__c SAI: [SELECT Id,REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c, REVVY__Status__c , REVVY__AssignedUser__r.Name FROM REVVY__MnStepApproverInst__c 
                                                   WHERE  REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c IN:ConSetIds]){
              //  ApproverList.add(SAI.REVVY__AssignedUser__r.Name);
              //  ApproverMap.put(SAI.REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c,ApproverList);
                if (SAI.REVVY__Status__c == 'Pending Approval'){
                 goodApproverList.add(SAI.REVVY__AssignedUser__r.Name); 
                 goodApproverMap.put(SAI.REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c,goodApproverList);
                }
                
            }
            system.debug('---Vikram---'+ goodApproverMap);
           
            
            for(REVVY__MnContract__c con: [SELECT Id,Current_Approver__c FROM REVVY__MnContract__c WHERE Id IN:ConSetIds]){
                    con.Current_Approver__c = ''; 
                     system.debug('---Vikram91---'+ con.Current_Approver__c);
                     system.debug('---Vikram92---'+ goodApproverMap.containskey(con.Id));
                     system.debug('---Vikram93---'+ goodApproverMap);
                      system.debug('---Vikram94---'+ con.id);
                     
                    if(goodApproverMap.containskey(con.Id)){
                        system.debug('---Vikram2---'+ goodApproverMap.get(con.Id));
                        for(String app: goodApproverMap.get(con.Id)){
                        
                            if(con.Current_Approver__c.Trim().Length() == 0 ){
                               con.Current_Approver__c = app;
                               }else{
                               system.debug('---Vikram56---'+ con.Current_Approver__c);
                                if(con.Current_Approver__c.containsOnly(app)){
                                    con.Current_Approver__c = con.Current_Approver__c.replace(app, '');
                                }else if(con.Current_Approver__c.contains(app+',')){
                                    con.Current_Approver__c = con.Current_Approver__c.replace(app+',', '');
                                }else{
                                    con.Current_Approver__c = con.Current_Approver__c +','+ app ;
                                }
                            }   
                        }
                    }
                    
                    system.debug('---Vikram56---'+ con);
                    UpdateContracts.add(con);
                }
                if(UpdateContracts.size()>0){
                    system.debug('contracts to update - '+UpdateContracts);
                    Update UpdateContracts;
             //       REVVY__MnContract__c con1 = [SELECT Id,Current_Approver_s__c FROM REVVY__MnContract__c WHERE Id IN:ContractsMap.keyset() LIMIT 1];
              //      system.debug('contract 1 - '+con1);
                }
           // }
        }catch(Exception ex){
            system.debug('Exception - '+ex);
        }
    }
    public static void manuallyShareContract(Set<Id> newList){
        try{
            List<Id> ApproverList = new List<Id>();
            List<Id> ASIList = new List<Id>();
            List<Id> ContractList = new List<Id>();
            for(REVVY__MnStepApproverInst__c SAI: [SELECT Id,REVVY__StepInst__c,REVVY__AssignedUser__c FROM REVVY__MnStepApproverInst__c 
                                                   WHERE Id IN:newList])
            {
                ApproverList.add(SAI.REVVY__AssignedUser__c);
                ASIList.add(SAI.REVVY__StepInst__c);
                
            } 
            if(ASIList.size()>0 && !ASIList.isEmpty()){
                for(REVVY__MnApprovalStepInst__c step : [select id,REVVY__ApprovalInst__r.REVVY__Contract__c,REVVY__ApprovalInst__r.REVVY__Contract__r.Exclude_In_Process_Status__c from 
                                                         REVVY__MnApprovalStepInst__c where id IN: ASIList and 
                                                         REVVY__ApprovalInst__r.REVVY__Contract__c!=null])
                {
                    ContractList.add(step.REVVY__ApprovalInst__r.REVVY__Contract__c);
                }
            }
            if(ContractList.size()>0 && !ContractList.isEmpty()){
                for(Id conid: ContractList){
                    for(Id userid: ApproverList){
                        manualShareContract(conid,userid);
                    }
                }
            }
        }catch(Exception ex){
            system.debug('Exception - '+ex);
        }
    }
    
    // we need to fix this code
    public static void manualShareContract(Id recordId, Id userOrGroupId){
        REVVY__MnContract__Share ConShr = new REVVY__MnContract__Share();
        ConShr.ParentId = recordId;
        ConShr.UserOrGroupId = userOrGroupId;
        ConShr.AccessLevel = 'Read';
        ConShr.RowCause = Schema.REVVY__MnContract__Share.RowCause.Manual;
        Database.SaveResult sr = Database.insert(ConShr,false);
        system.debug('------sr----'+sr);
        if(!sr.isSuccess()){
            Database.Error err = sr.getErrors()[0];
            if(err.getStatusCode() != StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&
               !err.getMessage().contains('AccessLevel')){
                   system.debug('Error sharing record - '+err.getMessage());
               }
        }
    }
    public static void deleteContractAccess(Set<Id> newList){
        try{
            Map<Id,String> ApproverMap = new Map<Id,String>();
            List<Id> ASIList = new List<Id>();
            List<Id> ContractList = new List<Id>();
            List<REVVY__MnContract__Share> sharesToDelete = new List<REVVY__MnContract__Share>();
            for(REVVY__MnStepApproverInst__c SAI: [SELECT Id,REVVY__StepInst__c,REVVY__AssignedUser__c,REVVY__AssignedUser__r.Name FROM REVVY__MnStepApproverInst__c 
                                                   WHERE Id IN:newList])
            {
                ApproverMap.put(SAI.REVVY__AssignedUser__c,SAI.REVVY__AssignedUser__r.Name);
                ASIList.add(SAI.REVVY__StepInst__c);
                
            } 
            if(ASIList.size()>0 && !ASIList.isEmpty()){
                for(REVVY__MnApprovalStepInst__c step : [select id,REVVY__ApprovalInst__r.REVVY__Contract__c from 
                                                         REVVY__MnApprovalStepInst__c where id IN: ASIList and 
                                                         REVVY__ApprovalInst__r.REVVY__Contract__c!=null])
                {
                    ContractList.add(step.REVVY__ApprovalInst__r.REVVY__Contract__c);
                }
            }
            for(REVVY__MnContract__Share conshr : 
                [SELECT Id,UserOrGroupId FROM REVVY__MnContract__Share WHERE ParentId IN :ContractList AND RowCause = 'Manual']){
                    if(ApproverMap.containskey(conshr.UserOrGroupId)){
                        sharesToDelete.add(conshr);
                    }            
                }
            List<REVVY__MnStepApproverInst__c> FullApproverList = [SELECT Id FROM REVVY__MnStepApproverInst__c WHERE 
                                                                   REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c IN:
                                                                   ContractList AND REVVY__AssignedUser__r.Name IN:ApproverMap.values()
                                                                   AND REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Status__c IN 
                                                                   ('Initializing','Pending Approval')];
            system.debug('Fullapproverlist - '+FullApproverList.size());
            if(!sharesToDelete.isEmpty() && FullApproverList.size() == 1){
                Database.Delete(sharesToDelete, false);
            }
        }catch(Exception ex){
            system.debug('Exception - '+ex);
        }
    }
}