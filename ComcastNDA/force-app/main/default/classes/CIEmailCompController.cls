public class CIEmailCompController {
    public Id emailContractId{get;set;}
    Public String DocId{get;set;}
    public string BaseURL{get;set;}
    public CIEmailCompController(){
        BaseURL = Label.BaseLabel;
        if(!Test.isRunningTest()){
            Document Doc = [SELECT Id,Name FROM Document WHERE DeveloperName = 'Comcast_Email_Logo_Visual_Force' LIMIT 1];
            DocId = Doc.Id;
        }
        
    }
    public Contract__c getContract(){
        List<Contract__c> contractList = New List<Contract__c>();
        if (emailContractId!=NULL){
            contractList = [SELECT Id,Name,Requester_Name__c,Requester_Email__c,Brief_Summary__c,Counterparty__c,
                            Projected_Dollar_Amount__c,Business_Unit__c,Attorney_Contact__c,LastModifiedBy.Name,
                            Assigned_Attorney__r.Name,Reason_Of_Rejection__c,Status_Update__c FROM Contract__c WHERE id=:emailContractId LIMIT 1];
            if(contractList.size() > 0){
                Contract__c contract = contractList[0];
                return contract;
            }
            else{
                return new Contract__c();
            }
        }
        else{
            return new Contract__c();
        }
    }
    public List<contentversion> getContent(){
        List<contentversion> contentList = new List<contentversion>();
        if(emailContractId!=null){
            contentList = [SELECT Id,title,Description FROM contentversion WHERE FirstPublishLocationId =:emailContractId];
            if(contentList.size()>0){
                return contentList;
            }
            else{
                return new List<contentversion>();
            }
        }
        else{
            return new List<contentversion>();
        }
    }
    public String getReviewURL(){
        String URL = '';
        if (emailContractId!=null){
            URL = BaseURL+'/apex/ReviewCableIntakeForm?Id='+emailContractId;
        }
        return URL;
    }
    
    public String getPolicyURL(){
        String URL = '';
        URL = Label.PolicyLink;
        return URL;
    }
}