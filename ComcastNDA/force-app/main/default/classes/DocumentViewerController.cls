public class DocumentViewerController {
    public ContentVersion cv{get;set;}
    public String Base64String {get;set;}
    public String RenderAs{get;set;}
    public String RenderFile {get;set;}
    public String ContentType {get;set;}
    public String Error {get;set;}
    public DocumentViewerController(){
        ContentType = '';
        RenderAs = '';
        RenderFile = '';
        Error = '';
        Id id = ApexPages.currentPage().getParameters().get('id');
        try{
            if(id != NULL){
            cv = [SELECT Id,Title, versionData, FileExtension FROM ContentVersion WHERE ID =: id];
            if(cv.FileExtension == 'pdf'){
                RenderFile = 'pdf';
                Base64String = EncodingUtil.Base64Encode(cv.versionData);
            }else{
                Error = 'Cannot display a '+cv.FileExtension+' in PDF viewer';
            }
            /*else if(cv.FileExtension == 'txt' || cv.FileExtension == 'csv' || cv.FileExtension == 'xls' 
                    || cv.FileExtension == 'xlsx'){
                RenderAs = 'pdf';
                RenderFile = 'txt';
                Blob b = cv.versionData ;
                Base64String = b.toString();
            }
            else if(cv.FileExtension == 'png' || cv.FileExtension == 'jpg'){
                RenderFile = 'img';
                Base64String = cv.Id;
            }
            else if(cv.FileExtension == 'doc' || cv.FileExtension == 'docx'){
                RenderAs = 'pdf';
                ContentType = 'application/msword';
                RenderFile = 'txt';
                Base64String = EncodingUtil.Base64Encode(cv.versionData);
                Blob b = EncodingUtil.base64Decode(Base64String);
                Base64String = b.toString();
            } */
        }
        }Catch(Exception ex){
            Error = ex.getMessage();
        }
    }
}