public class IETestCAFController03282019{
    public REVVY__MnContract__c contract{get;set;}
    //public REVVY__MnContract__c ReviewContract{get;set;}
    public String error{get;set;}
    Id contractId;
    //public string contid{get;set;}
    public List<Id> attachmentIdList;
    public Id selectedid{get;set;}
    //public Id selectedcvid{get;set;}
    public Attachment attach{get;set;}
    public String currentStage{get;set;}
    public String filename{get;set;}
    public String body{get;set;}
    public Id attachId{get;set;}
    public List<Document> docuList{get;set;}
    public List<Attachment> AttachmentList{get;set;}
    //public List<ContentVersion> ReviewDocs{get;set;}
    public String attachDescription{get;set;}
    //public String attachType{get;set;}
    //public Document doc{get;set;}
    //public String counterpartyName{get;set;}
    //public String counterpartyName1{get;set;}
    //public String parentContractName1{get;set;}
    //public String erpAccNum{get;set;}
    //public List<Account> searchedCounterparties{get;set;}
    //public REVVY__MnContract__c searchContract{get;set;}
    //public List<REVVY__MnContract__c> searchedParentsContracts{get;set;}
    //public String accountName{get;set;}
    public String ManageApprovalsText{get;set;}
    //public String parentName{get;set;}
    public Account acc{get;set;}
    public Boolean optionalMetaData{get;set;}
    public String legalcontract{get;set;}
    public String sourcingContract{get;set;}
    //public String BindingCorp{get;set;}
    public String contractvalue{get;set;}
    public String CAFTypeString{get;set;}
    public List<REVVY__MnApprovalStepInst__c> ApproverList{get;set;}
    public List<REVVY__MnApprovalInst__c> ApprovalProcess{get;set;}
    public Boolean sorted = false;
    public Boolean sortedcont = false;
    public Boolean ContentCreated = false;
    public Boolean Lessthan100kBoolean{get;set;} 
    public Boolean BindingBoolean{get;set;}
    public Boolean PSSBoolean{get;set;}
    public Boolean UnmodifiedLegalBoolean{get;set;}
    public Boolean BooCAFType{get;set;}
    public Boolean SWBoolean{get;set;}
    public Boolean NoneBoolean{get;set;}
    public Boolean ProcurementBoolean{get;set;}
    public Boolean ApproverResultBoolean{get;set;}
     public Boolean SameApprover{get;set;}
    public List<ApprovalWrapper> ApprovalWrapperList{get;set;}
    //public List<Id> CachedIds{get;set;}
    public Map<REVVY__MnApprovalStepInst__c,List<REVVY__MnStepApproverInst__c>>Approvers {get;set;}
    // Constructor - Singleton approach
    public IETestCAFController03282019(ApexPages.StandardController controller) {
        Try{
            error='';
            contractvalue = '';
            contractId = controller.getId();
            
            ApproverResultBoolean = false;
            UnmodifiedLegalBoolean = true;
            BooCAFType = False;
            SWBoolean = False;
            NoneBoolean = false;
            string stage = ApexPages.currentPage().getParameters().get('stage');
            ManageApprovalsText = ApexPages.currentPage().getParameters().get('EmailSource');
            attach = new Attachment();
            //doc= new Document();
            //CachedIds= new List<Id>();
            acc = new Account();
            docuList = new List<Document>();
            attachmentIdList = new List<Id>();
            AttachmentList = new List<Attachment>();
            //searchedCounterparties = new List<Account>();
            //searchedParentsContracts = new List<REVVY__MnContract__c>();
            //searchContract = new REVVY__MnContract__c();
            if(contractId != null){
                contract = [select Id,Counterparty__c,Less_Than_100K__c,Above_statements_applicable_to_Contract__c,REVVY__ContractId__c,Crypto_Do_any_of_the_below_apply__c ,SW_ext_any_of_the_below_true__c ,
                            Attorney__c ,Are_you_requesting_on_someone_s_behalf__c,On_Behalf_Of_Name__c,On_Behalf_Of_Email__c,Name,Business_Unit__c,REVVY__Account__r.Name,SW_extension_processed_by_Procurement__c ,
                            REVVY__Parent__r.Name,REVVY__Currency__c,Contract_Value__c,Evergreen_Contract__c,REVVY__StartDateContract__c,Expiration_Date__c,Crypto_Do_any_apply_below_question__c ,
                            Contract_Type_CAF__c,CAF_Type__c,Spend_Category__c,Is_Sourcing_Contract__c,Is_Binding_Corporate_Dev_Contract__c,Unchanged_legal_provided_template_used__c ,Payment_Terms__c,Brief_Summary__c, SW_Do_any_of_the_below_apply__c from REVVY__MnContract__c where id=: contractId];
                //ReviewContract = contract;
                CAFTypeString = contract.CAF_Type__c;
                contractvalue = contract.Contract_Value__c.format();
                reRenderPage();
                system.debug('contracts constructor - '+contract.CAF_Type__c);
                if (!string.isempty(stage)){
                    currentStage = 'File Upload';
                    for(Attachment attach :[SELECT Id FROM Attachment WHERE ParentId =:contractId]){
                        attachmentIdList.add(attach.Id);
                    }
                }else
                {
                    bringmeApproverDetails();
                    currentStage = 'Review and Submit';
                }
                AttachmentList = [SELECT Id,Name,Description,Body,ContentType FROM Attachment WHERE ParentId =:contractId];
                
                if(stage == 'Resubmit'){
                    currentStage = 'General Information';
                }
            }else{
                currentStage = 'General Information';  
                Lessthan100kBoolean = false;
                BindingBoolean = true;
                PSSBoolean = true;
                contract = new REVVY__MnContract__c();
                contract.Unchanged_legal_provided_template_used__c = '';
                contract.Is_Sourcing_Contract__c = '';
            }
            
            List<Procurement_User__c> procurementUserList = Procurement_User__c.getAll().values();
            for(Procurement_User__c objPU : procurementUserList){
                if(objPU.User_Name__c == UserInfo.getUserName()){
                    ProcurementBoolean =true;
                }
            }
        }Catch(Exception ex){
            error = ex.getMessage();
        }
    }
   // get approver details at the review and submit
    public void bringmeApproverDetails(){
        ApprovalWrapperList = new List<ApprovalWrapper>();
        ApprovalProcess = [SELECT Id,Name FROM REVVY__MnApprovalInst__c WHERE REVVY__Contract__c=: contractId ORDER BY Createddate DESC LIMIT 1];
        ApproverList = [SELECT Name,REVVY__Approver__r.Name,REVVY__StepStatus__c,REVVY__SortField__c FROM REVVY__MnApprovalStepInst__c 
                        WHERE REVVY__ApprovalInst__c IN :ApprovalProcess AND REVVY__StepStatus__c != 'Skipped' ORDER BY REVVY__Step_Sequence__c ASC];
                        
        Map<String,List<REVVY__MnStepApproverInst__c>> MnStepApproverInstMap = new Map<String,List<REVVY__MnStepApproverInst__c>>();
        for(REVVY__MnStepApproverInst__c objMnStep : [SELECT Name,REVVY__StepInst__c FROM REVVY__MnStepApproverInst__c Where REVVY__StepInst__c in : ApproverList]){
            if(!MnStepApproverInstMap.containsKey(objMnStep.REVVY__StepInst__c)){
                List<REVVY__MnStepApproverInst__c> MnStepList = new List<REVVY__MnStepApproverInst__c>();
                MnStepList.add(objMnStep);
                MnStepApproverInstMap.put(objMnStep.REVVY__StepInst__c,MnStepList);
            }else{
                List<REVVY__MnStepApproverInst__c> MnStepList = MnStepApproverInstMap.get(objMnStep.REVVY__StepInst__c);
                MnStepList.add(objMnStep);
                MnStepApproverInstMap.put(objMnStep.REVVY__StepInst__c,MnStepList);
            }
        }               
        for (REVVY__MnApprovalStepInst__c SI : ApproverList){  
            ApprovalWrapperList.add(New ApprovalWrapper(SI,MnStepApproverInstMap.get(SI.Id)));
           // ApprovalWrapperList.add(New ApprovalWrapper(SI,[SELECT Name,REVVY__StepInst__c FROM REVVY__MnStepApproverInst__c Where REVVY__StepInst__c =: SI.Id]));
            
        }
        system.debug('Approvres --- '+ApprovalWrapperList);
    }
    
  /*  public void bringmeApproverDetails(){
        ApprovalWrapperList = new List<ApprovalWrapper>();
        ApprovalProcess = [SELECT Id,Name FROM REVVY__MnApprovalInst__c WHERE REVVY__Contract__c=: contractId ORDER BY Createddate DESC LIMIT 1];
        ApproverList = [SELECT Name,REVVY__Approver__r.Name,REVVY__StepStatus__c,REVVY__SortField__c FROM REVVY__MnApprovalStepInst__c 
                        WHERE REVVY__ApprovalInst__c IN :ApprovalProcess AND REVVY__StepStatus__c != 'Skipped' ORDER BY REVVY__Step_Sequence__c ASC];
        for (REVVY__MnApprovalStepInst__c SI : ApproverList){  
            ApprovalWrapperList.add(New ApprovalWrapper(SI,[SELECT Name FROM REVVY__MnStepApproverInst__c Where REVVY__StepInst__c =: SI.Id]));
            
        }
        system.debug('Approvres --- '+ApprovalWrapperList);
    }*/
    // to delete an attachment
    public PageReference deleteAttachment(){
        Integer count=0;
        Attachment deleteDocument=null;
        try{
            for(Attachment doc:AttachmentList){
                if(doc.Id == selectedid){
                    deleteDocument = doc;
                    AttachmentList.Remove(count);
                    break;
                }
                count++;
            }
            if (deleteDocument!=null){
                delete deleteDocument;
            } 
        }
        catch(DMLException ex){
            error = ex.getMessage();
        }
        catch(exception ex){
            error = ex.getMessage();
        }
        return null;
    }
    //to move to next stage in the progression
    public PageReference nextStage(){
        system.debug('nextstage');
        error = '';
        if(currentStage == 'General Information'){
            currentStage = 'Brief Summary';
        }
        else if(currentStage == 'Brief Summary'){
            
            if(contract.Brief_Summary__c == null){
                error = 'Please input Brief Summary';
                return null;
            }else if (contract.Brief_Summary__c != null && contract.Brief_Summary__c.Trim().Length() == 0  ){
                error = 'Please input Brief Summary';
                return null;
            } else{    
                saveContractRecord();
                currentStage = 'File Upload';
            }
        }else if(currentStage == 'Review and Submit'){
            isApproverAdded();
            if(ApproverResultBoolean ){
                Contract.isFromUI__c = False;
                savecontent();
                PageReference finalmessagepage = new PageReference('/apex/RequestCAF_ThankYou');
                finalmessagepage.getParameters().put('Id', contract.id);
                finalmessagepage.setRedirect(true);
                return finalmessagepage; 
            }
            else if (!ApproverResultBoolean) {
                error = 'An Approver must be added to each approval process'; 
                return null;
            }
          
        }else if(currentStage == 'File Upload'){
            Integer attachments = [SELECT Count() FROM Attachment WHERE ParentId =:contractId];
            if(attachments>0){
                Integer Contractattachments = [SELECT Count() FROM Attachment WHERE ParentId =:contractId AND ContentType = 'Contract'];
                if(Contractattachments > 0){
                    error.remove(error);
                    return new PageReference('/apex/IETestApprovals03282019?id='+contract.Id+'&resource=approvalbundle&entry=previewApproval#/approval/REVVY__MnContract__c/Contract%28s%29/'+EncodingUtil.urlEncode(contract.Name,'UTF-8')+'/'+contract.Id);
                }
                else{
                    error = 'A Contract must be attached to continue';
                }
            }
            else{
                error = 'Please upload a file to continue';
            }
        }
        else if(currentStage == 'Smart Approval'){
           
            system.debug('got in to approver details');
            currentStage = 'Review and Submit';
        }
        return null;
    }
     // to check if approvers are inserted
    public void isApproverAdded(){
        error='';
        Integer count=0;
        system.debug('error before checking the loop - ' + error );
        ApprovalProcess = [SELECT Id,Name FROM REVVY__MnApprovalInst__c WHERE REVVY__Contract__c=: contractId ORDER BY Createddate DESC LIMIT 1];
        
        for(REVVY__MnApprovalStepInst__c StepInstance : [SELECT Id,Name,REVVY__ApprovalInst__r.Name,REVVY__StepStatus__c,createdby.Name,(SELECT Id FROM REVVY__StepInstApprovers__r)
                                                         FROM REVVY__MnApprovalStepInst__c WHERE REVVY__ApprovalInst__c IN :ApprovalProcess
                                                         AND REVVY__StepStatus__c != 'Skipped'])
            
        {
            system.debug('approvers - '+ StepInstance);
            if(StepInstance.REVVY__StepInstApprovers__r.size() > 0){
                ApproverResultBoolean = true;
            }  
            else{
                ApproverResultBoolean = false;
                break;
            }
            count++;
        }
        // Vikram added code on 01/19/2019
         Integer ApprovalsCount = [SELECT Count() FROM REVVY__MnStepApproverInst__c WHERE 
                                         REVVY__AssignedUser__c =:UserInfo.getUserId() 
                                         AND REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c =:contractId 
                                         ];
                   If  ( ApprovalsCount  > 0 ){
                     SameApprover = True;
                     
                   } else{
                      SameApprover = False;
                   }           
        system.debug('list size - '+count);
        system.debug('Approver result boolean - '+ApproverResultBoolean);
    }
    //creates a list of attachments to show on file upload stage
    public void attachFileFunction(){
        error = '';
        if(String.isNotBlank(attachId)){
            attachmentIdList.add(attachId); 
        }
        if(attachmentIdList.size() > 0){
            if(AttachmentList.size()>0){
               AttachmentList.clear();
            }
            AttachmentList = [SELECT Id,Name,Body,ContentType,Description FROM Attachment WHERE Id IN:attachmentIdList];
        }
    }
    // to go back to previous stages
    public PageReference previousStage(){
        error = '';
        /*if(currentStage == 'Contract Terms'){
            currentStage = 'General Information';
        }else if(currentStage == 'Contract Terms 1'){
            currentStage = 'General Information';
        }else*/ if(currentStage == 'Brief Summary'){
            system.debug('CAF Type -'+CAFTypeString);
            CAFTypeString = Contract.CAF_Type__c;
            currentStage = 'General Information';
        }else if(currentStage == 'Review and Submit'){
            currentStage = 'Smart Approval';
            return new PageReference('/apex/IETestApprovals?resource=approvalbundle&entry=previewApproval#/approval/REVVY__MnContract__c/Contract%28s%29/'+EncodingUtil.urlEncode(contract.Name,'UTF-8')+'/'+contract.Id);
        }else if(currentStage == 'File Upload'){
            CAFTypeString = Contract.CAF_Type__c;
            currentStage = 'Brief Summary';
        }else if(currentStage == 'Smart Approval'){
            currentStage = 'File Upload';
        }
        return null;
    }
    // creates salesforce files from attachments and deletes attachments once files are created
    public PageReference savecontent(){
       List<Id> AttachmentIds = New List<Id>();
       if ( CAFTypeString != 'LTO Test' ){ 
       
         contract.REVVY__Phase__c = 'CAF Request';
         contract.REVVY__SubPhase__c = 'Admin Review';
         contract.REVVY__Currency__c = 'USD';
         update contract;
       
       } else {
           // Code added by Vikram, Calling Model N method to sending for approval directly
            
           REVVY.CMnAdvApprovalRemoter.submit(contract.id , 'This is Automated Submittion from the form', AttachmentIds);

       
       }
       // 
         
        if(!AttachmentList.isEmpty() && AttachmentList != null && AttachmentList.size() > 0){
            List<ContentVersion> FilesList = New List<ContentVersion>();
            for(Attachment doc : AttachmentList){
                ContentVersion cv = new ContentVersion();
                cv.versionData = doc.Body;
                cv.title = doc.name;
                cv.FirstPublishLocationId = contract.id;
                cv.pathOnClient = doc.name;
                cv.Attachment_Type__c = doc.ContentType;
                cv.Description = doc.Description;
                FilesList.add(cv);
               // AttachmentIds.add(doc.id); 
            }
            try{
                insert FilesList;
         
      
          
         /*       Database.DeleteResult[] delRes = Database.delete(AttachmentList, false);
                if (delRes.size() > 0){
                    system.debug('delete result -- '+delRes);
                } */
            }
            catch(DMLException e){
                error = e.getMessage();
            }
        }
        return null;
    }
    // saves contract record after review & submit
    public PageReference saveContractRecord(){
        SavePoint sp = Database.setSavepoint();
        try{
            contract.RecordTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
            
            if(contract.Brief_Summary__c == null){
                error = 'Please input Brief Summary';
                return null;
            }else{
                //contract.Unchanged_legal_provided_template_used__c = legalcontract;
                //contract.Is_Sourcing_Contract__c= sourcingContract;
                //contract.Is_Binding_Corporate_Dev_Contract__c  = BindingCorp; '0010x00000SKHki'
                contract.REVVY__Account__c = [SELECT Id FROM Account WHERE Name=:Label.CafAccount LIMIT 1].Id;
                contract.CAF_Type__c = CAFTypeString;
                contract.isFromUI__c = True;
                upsert contract;
                ContractId = contract.id;                               
                //return new PageReference('/apex/EmbeddedApprovals?resource=approvalbundle&entry=previewApproval#/approval/REVVY__MnContract__c/Contract%28s%29/'+contract.Name+'/'+contract.Id);
                return null;
            }
        }catch(DMLException e){
            Database.rollback(sp);
            error = e.getdmlMessage(0);
            return null;
        }catch(Exception e){
            Database.rollback(sp);
            error = e.getMessage();
            return null;
        }
    }
    // available attachment types at file upload
    public List<SelectOption> attachmentTypes{
        get
        {
            attachmentTypes = new List<SelectOption>();
            attachmentTypes.add(new SelectOption('', 'Select your Attachment Type'));
            attachmentTypes.add(new SelectOption('Contract', 'Contract'));
            attachmentTypes.add(new SelectOption('Justification Memo', 'Justification Memo'));
            attachmentTypes.add(new SelectOption('Legal Memo', 'Legal Memo'));
            attachmentTypes.add(new SelectOption('Other', 'Other'));
            return attachmentTypes;
        }
        set;
    }
    //available business units for Request CAF record type
    public List<SelectOption> BusinessUnits{
        get
        {
            BusinessUnits= new List<SelectOption>();
            BusinessUnits.add(new SelectOption('', '--None--'));
            if(!PicklistValues.getValues('Business_Unit__c').isEmpty()){
                for( String f : PicklistValues.getValues('Business_Unit__c'))
                {
                    BusinessUnits.add(new SelectOption(f, f));
                }       
            }
            return BusinessUnits;
        }
        set;
    }
    //available CAF Types for Request CAF record type
    public List<SelectOption> CAFType{
        get
        {
            Schema.DescribeFieldResult fieldResult = REVVY__MnContract__c.CAF_Type__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            CAFType= new List<SelectOption>();
            CAFType.add(new SelectOption('', '--None--'));
            if(!PicklistValues.getValues('CAF_Type__c').isEmpty()){
                for( Schema.PicklistEntry f : ple)
                {
                    CAFType.add(new SelectOption(f.getValue(), f.getlabel()));
                }          
            }
            return CAFType;
        }
        set;
    }
    //available currency
    public List<SelectOption> contractcurrency{
        get
        {
            contractcurrency= new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = REVVY__MnContract__c.REVVY__Currency__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
                contractcurrency.add(new SelectOption(f.getLabel(), f.getValue()));
            }       
            return contractcurrency;
        }
        set;
    }
    // render fields based on calculations
    public void reRenderPage(){
    system.debug('---contractvalue---'+contractvalue);
        error='';
        UnmodifiedLegalBoolean = true;
        BooCAFType = False;
        SWBoolean = False;
        try{
           
            if(String.isNotBlank(contractvalue)){
               contractvalue = contractvalue.replace(',','');
               If(long.valueOf(contractvalue) >= 10000000){
               error = 'Contract value should be less than 10 million dollars. ';
               
               }
               If(long.valueOf(contractvalue) < 0){
                error = 'Contract value should be positive. ';
               
               }
            }              
         
           
            if(String.isNotBlank(CAFTypeString)){
                if(CAFTypeString == 'Cable Division' || CAFTypeString == 'Cable Division - Advertising' ||CAFTypeString  == 'Cable Division - Preferred Vendor' ){
                  BooCAFType = True; 
                  SWBoolean = True;
                    NoneBoolean = true;
                }
            }     
            
            if(String.isNotBlank(CAFTypeString)){
                    NoneBoolean = true;
                if(CAFTypeString == 'Cable Division' || CAFTypeString == 'Cable Division - Advertising'){
                    BindingBoolean = true;
                    PSSBoolean = true;
                    UnmodifiedLegalBoolean = true;
                    if(String.isNotBlank(contractvalue)){
                        contractvalue = contractvalue.replace(',','');
                        If(long.valueOf(contractvalue) < 100000){
                            Lessthan100kBoolean = true;
                        }else{
                            Lessthan100kBoolean = false;
                        }
                    }
                    else{
                        Lessthan100kBoolean = false;
                    }
                }else if(CAFTypeString == 'Cable Division - Preferred Vendor'){
                    BindingBoolean = false;
                    PSSBoolean = true;
                    UnmodifiedLegalBoolean = true;
                    if(String.isNotBlank(contractvalue)){
                        contractvalue = contractvalue.replace(',','');
                        If(long.valueOf(contractvalue) < 100000){
                            Lessthan100kBoolean = true;
                        }else{
                            Lessthan100kBoolean = false;
                        }
                    }
                    else{
                        Lessthan100kBoolean = false;
                    }
                }
                else if(CAFTypeString == 'Cable Division - Comcast India'){
                    BindingBoolean = true;
                    PSSBoolean = true;
                    UnmodifiedLegalBoolean = false;
                    Lessthan100kBoolean = false;
                }else if(CAFTypeString == 'CAF - Leisure Arts'){
                    PSSBoolean = false;
                    UnmodifiedLegalBoolean = false;
                    Lessthan100kBoolean = false;
                    BindingBoolean = true;
                }else if(CAFTypeString == 'Cable Division - Preferred Vendor'){
                    PSSBoolean = true;
                    UnmodifiedLegalBoolean = true;
                    Lessthan100kBoolean = false;
                    BindingBoolean = false;
                }
                else{
                    PSSBoolean = false;
                    UnmodifiedLegalBoolean = false;
                    Lessthan100kBoolean = false;
                    BindingBoolean = false;
                    Lessthan100kBoolean = false;
                }
            }else{
                    NoneBoolean = false;
            }
        }
        Catch(TypeException Tex){
            error = 'Only whole dollar amounts can be entered';
        }
        Catch(Exception ex){
            error = ex.getMessage();
        }
    }
    // after submitting contract, redirect to requester dashboard
    public PageReference gotoDashboard(){
       return New PageReference('/apex/ContractRequesterDashboard');
    }
    //wrapper class to wrap content to display in review & submit stage
    public class ApprovalWrapper{
        public REVVY__MnApprovalStepInst__c StepInstance{get;set;}
        public List<REVVY__MnStepApproverInst__c> ApproverNameList {get;set;}
        
        public ApprovalWrapper(REVVY__MnApprovalStepInst__c SI, List<REVVY__MnStepApproverInst__c> NameList){
            StepInstance = SI;
            ApproverNameList = NameList;
        }
    }
}