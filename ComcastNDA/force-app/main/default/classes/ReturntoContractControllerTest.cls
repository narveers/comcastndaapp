@isTest
public class ReturntoContractControllerTest {
    static testMethod void testMethod1() {
        
        Account acc = new account(name = 'test');
        insert acc;
        
        Contact con = new contact(LastName = 'Test', AccountId = acc.id);
        insert con;
        
        REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
        lifeCycle.Name = 'Test Lifecycle';
        lifeCycle.REVVY__ApplyTo__c = 'Original';
        lifeCycle.REVVY__Object__c = 'MnContract__c';
        lifeCycle.REVVY__RecordType__c = 'Request_CAF';
        lifeCycle.REVVY__Status__c = 'Draft';
        insert lifeCycle;
        
        REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
        stage.name = 'Test';
        stage.REVVY__LifeCycle__c = lifeCycle.Id;
        stage.REVVY__Phase__c = 'Draft';
        stage.REVVY__SubPhase__c = 'Draft';
        stage.REVVY__Editable__c = true;
        insert stage;
        
        lifeCycle.REVVY__InitialStage__c = stage.Id;
        lifeCycle.REVVY__Status__c = 'Active';
        update lifeCycle;
        
        Id recTypeId = Schema.SObjectType.REVVY__MnContract__c.getRecordTypeInfosByName().get('Request CAF').getRecordTypeId();
        REVVY__MnContract__c mnc = new REVVY__MnContract__c(Name='Test contract',REVVY__Account__c = acc.id, recordTypeId=recTypeId,
                                                            REVVY__Contact__c = con.id,Contract_Value__c=80000);
        insert mnc;
        
        REVVY__MnApprovalDef__c mad = new REVVY__MnApprovalDef__c(REVVY__MainObject__c = 'REVVY__MnContract__c',REVVY__Order__c = 100);
        insert mad;
        
        REVVY__MnApprovalInst__c mai = new REVVY__MnApprovalInst__c(REVVY__ApprovalDef__c = mad.id, REVVY__ApprovalObjId__c = 'Text',REVVY__Contract__c=mnc.Id);
        insert mai;
        
        REVVY__MnApprovalStepDef__c asd = new REVVY__MnApprovalStepDef__c(REVVY__ApprovalDef__c = mad.id, REVVY__StepSequence__c = 100);
        insert asd;
        
        REVVY__MnApprovalStepInst__c asi = new REVVY__MnApprovalStepInst__c(REVVY__ApprovalInst__c = mai.id, REVVY__ApprovalStepDef__c = asd.id, REVVY__Step_Sequence__c = 100,
                                                                           REVVY__StepStatus__c='Pending Approval',REVVY__IsParentStep__c = false);
        insert asi;
        
        Test.StartTest();
        PageReference pageRef = Page.RequestCAFContractVF_New;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getHeaders().put('REFERER', 'https://ccms--full--c.visualforce.com/apex/RequestCAFContractVF_New');
        ReturntoContractController RTC = new ReturntoContractController(new ApexPages.StandardController(mnc));
        Test.stopTest();
    }
}