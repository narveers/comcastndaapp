@isTest
public class IETestCAFControllerTest {
    static testMethod void testCreateContract() {
        IETestCAFController CAF = new IETestCAFController(new ApexPages.StandardController(TestData.CreateCAFRequest()));
        REVVY__MnContract__c ConNull = new REVVY__MnContract__c();
        IETestCAFController CAF1 = new IETestCAFController(new ApexPages.StandardController(ConNull));
    }
    static testMethod void testDeleteAttachment() {
        REVVY__MnContract__c testcontract = TestData.CreateCAFRequest();
        REVVY__MnApprovalDef__c mad = new REVVY__MnApprovalDef__c(REVVY__MainObject__c = 'REVVY__MnContract__c',REVVY__Order__c = 100);
        insert mad;
        
        REVVY__MnApprovalInst__c mai = new REVVY__MnApprovalInst__c(REVVY__ApprovalDef__c = mad.id, REVVY__ApprovalObjId__c = 'Text',REVVY__Contract__c= testcontract.Id);
        insert mai;
        
        REVVY__MnApprovalStepDef__c asd = new REVVY__MnApprovalStepDef__c(REVVY__ApprovalDef__c = mad.id, REVVY__StepSequence__c = 100);
        insert asd;
        
        REVVY__MnApprovalStepInst__c asi = new REVVY__MnApprovalStepInst__c(REVVY__ApprovalInst__c = mai.id, REVVY__ApprovalStepDef__c = asd.id, REVVY__Step_Sequence__c = 100,
                                                                           REVVY__StepStatus__c='Pending Approval',REVVY__IsParentStep__c = false);
        insert asi;
        
        REVVY__MnStepApproverInst__c SIA = new REVVY__MnStepApproverInst__c(REVVY__StepInst__c=asi.Id,REVVY__Status__c='Pending Approval');
        insert SIA;
        
        Attachment attach = TestData.createAttachment(testcontract.Id);
        IETestCAFController CAF2 = new IETestCAFController(new ApexPages.StandardController(testcontract));
        List<SelectOption> attachmentTypesTest = CAF2.attachmentTypes;
        List<SelectOption> BusinessUnitTest = CAF2.BusinessUnits;
        List<SelectOption> contractcurrencyTest = CAF2.contractcurrency;
        CAF2.contractvalue = '5000';
        CAF2.CAFTypeString = 'Cable Division - Comcast India';
        CAF2.reRenderPage();
        CAF2.contractvalue = '120000';
        CAF2.CAFTypeString = 'Cable Division';
        CAF2.reRenderPage();
        CAF2.CAFTypeString = 'CAF - Leisure Arts';
        CAF2.reRenderPage();
        CAF2.CAFTypeString = 'Cable Division - Preferred Vendor';
        CAF2.reRenderPage();
        CAF2.CAFTypeString = 'test';
        CAF2.reRenderPage();
        CAF2.selectedid = attach.id;
        CAF2.deleteAttachment();
        CAF2.nextStage();
        CAF2.gotoDashboard();
        //List<REVVY__MnContract__c> mnclist = RequestCAFContractVFController.getLookupRecord('Test', 'REVVY__MnContract__c', 'test');
    }
    static testMethod void testmethod3() {
        test.startTest();
        REVVY__MnContract__c mnc = TestData.CreateCAFRequest();
        
        REVVY__MnApprovalDef__c mad = new REVVY__MnApprovalDef__c(REVVY__MainObject__c = 'REVVY__MnContract__c',REVVY__Order__c = 100);
        insert mad;
        
        REVVY__MnApprovalInst__c mai = new REVVY__MnApprovalInst__c(REVVY__ApprovalDef__c = mad.id, REVVY__ApprovalObjId__c = 'Text',REVVY__Contract__c=mnc.Id);
        insert mai;
        
        REVVY__MnApprovalStepDef__c asd = new REVVY__MnApprovalStepDef__c(REVVY__ApprovalDef__c = mad.id, REVVY__StepSequence__c = 100);
        insert asd;
        
        REVVY__MnApprovalStepInst__c asi = new REVVY__MnApprovalStepInst__c(REVVY__ApprovalInst__c = mai.id, REVVY__ApprovalStepDef__c = asd.id, REVVY__Step_Sequence__c = 100,
                                                                           REVVY__StepStatus__c='Pending Approval',REVVY__IsParentStep__c = false);
        insert asi;
        
        REVVY__MnStepApproverInst__c SIA = new REVVY__MnStepApproverInst__c(REVVY__StepInst__c=asi.Id,REVVY__Status__c='Pending Approval',REVVY__AssignedUser__c = UserInfo.getUserId());
        insert SIA;
        DELETE SIA;
        UPDATE asi;
        Attachment attach = TestData.createAttachment(mnc.Id);
        IETestCAFController CAF2 = new IETestCAFController(new ApexPages.StandardController(mnc));
        CAF2.AttachmentList = [SELECT Id,Name,Description,Body,ContentType FROM Attachment WHERE ParentId =:mnc.Id];
        CAF2.attachId = attach.Id;
        CAF2.attachFileFunction();
        CAF2.nextStage();
        CAF2.currentStage = 'Brief Summary';
        CAF2.nextStage();
        test.stopTest();
        CAF2.savecontent();
        CAF2.previousStage();
        CAF2.currentStage = 'Review and Submit';
        CAF2.previousStage();
        CAF2.previousStage();
        CAF2.previousStage();
        CAF2.currentStage = 'General Information';
        CAF2.nextStage();
        CAF2.currentStage='File Upload';
        CAF2.nextStage();
        CAF2.currentStage='Smart Approval';
        CAF2.nextStage();
        List<SelectOption> TestCAFType = CAF2.CAFType;
        
        
        REVVY__MnStepApproverInst__c SIA1 = new REVVY__MnStepApproverInst__c(REVVY__StepInst__c=asi.Id,REVVY__Status__c='Approved',REVVY__AssignedUser__c = UserInfo.getUserId());
        insert SIA1;
        update SIA1;
        ApprovalStepApproverInstTriggerHandler.onBeforeDelete(new List<REVVY__MnStepApproverInst__c>{SIA1});
        
    }
}