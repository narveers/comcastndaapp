/*
    Name             : CISubmissionAttorneyCtrl
    Author           : Narveer S.
    Date             : 07/17/2018
    Description      : Controller class we are using for CI Submission for Attorney
*/
public class CISubmissionAttorneyCtrl {
    public Id emailContractId{get;set;}
    public Boolean Flag{get;set;}
    public string BaseURL{get;set;}
    public string contractnameescaped {get;set;}
    public Contract__c getContract() {
        BaseURL = Label.BaseLabel;
        List<Contract__c> contractList = New List<Contract__c>();
        Flag = False;
        if(emailContractId!=null) {//,Procurement_Contact__c
            contractList = [select ID,Attorney_Contact__c,Counterparty__c,Procurement_Contact__c,Requester_Name__c,Brief_Summary__c,Name,Projected_Dollar_Amount__c,Business_Unit__c,Attorney__c,Is_Procurement_Engaged__c,Record_URL__c FROM Contract__c where id=:emailContractId LIMIT 1];
            if(contractList.size() > 0) {
                Contract__c contract = contractList[0];
                if(contract.Is_Procurement_Engaged__c == 'Yes') {
                    Flag = True;    
                }
                contractnameescaped = EncodingUtil.urlEncode(contractList[0].Name, 'UTF-8');
                return contract;
            } else {
                return new Contract__c();
            }
        } else {
            return new Contract__c();
        }
    }
    public List<contentversion> getContent() {
        List<contentversion> contentList = new List<contentversion>();
        if(emailContractId!=null){
            contentList = [SELECT Id,title,Attachment_Type__c,Description FROM contentversion WHERE FirstPublishLocationId =:emailContractId];
            if(contentList.size()>0) {
                return contentList;
            } else {
                return new List<contentversion>();
            }
        } else {
            return new List<contentversion>();
        }
    }    
}