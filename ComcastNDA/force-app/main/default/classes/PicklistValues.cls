public class PicklistValues {
    public static List<String> getValues(String CustomFieldString){
        Id recordTypeId = [SELECT Id FROM recordtype WHERE sobjecttype='REVVY__MnContract__c' AND DeveloperName ='Request_CAF'].Id;
        String URL = 'callout:UI_API_Named_Credential/services/data/v41.0/ui-api/object-info/REVVY__MnContract__c/picklist-values/'+recordTypeId+'/'+CustomFieldString;
        List<String> ResultStringList = new List<String>();
        HttpResponse res = null;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(URL);
        req.setMethod('GET');
        Http http = new Http();
        try{
            res = http.send(req);
            if(res.getStatusCode() == 200){
                Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped((string)res.getBody());
                List<Object> ValuesList = (List<Object>)root.get('values');
                //system.debug('values-'+ValuesList);
                String Label;
                for(Object obj:ValuesList){
                    Map<string,object> valueObjMap = (Map<string,object>)obj;
                    if(valueObjMap.containsKey('label')){
                        Label = (String)valueObjMap.get('label');
                        ResultStringList.add(Label.unescapeHtml4());
                    }
                }
            }
        }
        Catch(Exception ex){
            system.debug('exception - '+ex.getStackTraceString());
            system.debug('ex - '+ex.getMessage());
        }
        //system.debug('results - '+ResultStringList);
        return ResultStringList;
    }
}