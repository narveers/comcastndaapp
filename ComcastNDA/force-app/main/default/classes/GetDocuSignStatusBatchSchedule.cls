global class GetDocuSignStatusBatchSchedule implements Schedulable{
   global void execute(SchedulableContext sc){
       Database.executeBatch(new GetDocuSignStatusBatch(),100);
   }
}