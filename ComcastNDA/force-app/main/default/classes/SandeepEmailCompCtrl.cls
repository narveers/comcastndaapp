public class SandeepEmailCompCtrl {

    public Id emailContractId{get;set;}
    public Id stepInstanceId{get;set;}
    public string contractnameescaped {get;set;}
    public string salesforceinstance {get{
        return URL.getSalesforceBaseUrl().toExternalForm()+'/apex/ReviewCAF?id='+emailContractId;
    }set;}
    
    public REVVY__MnApprovalStepInst__c ApprovalList { get{
        List<REVVY__MnApprovalStepInst__c> ToDisplay = new List<REVVY__MnApprovalStepInst__c>();
        List<REVVY__MnApprovalInst__c> tmpAppIns = [SELECT Id,Name FROM REVVY__MnApprovalInst__c WHERE REVVY__Contract__c=: emailContractId ORDER BY Createddate DESC LIMIT 1];
        ToDisplay  = [SELECT Name,(SELECT name FROM REVVY__StepInstApprovers__r) FROM REVVY__MnApprovalStepInst__c WHERE REVVY__ApprovalInst__c IN :tmpAppIns AND REVVY__StepStatus__c != 'Skipped' ORDER BY REVVY__Step_Sequence__c ASC];
        if (ToDisplay.size() > 0){
            REVVY__MnApprovalStepInst__c APSI = ToDisplay[0];
            return APSI;
        }
        else{
            return new REVVY__MnApprovalStepInst__c();
        }
    }                        
    set; }
    
    public REVVY__MnContract__c getContract(){
       List<REVVY__MnContract__c> contractList = New List<REVVY__MnContract__c>();
        //salesforceinstance = 
       if (emailContractId!=null){
         contractList = [select id,Brief_Summary__c,Name,CreatedBy.Name,REVVY__Account__r.Name,Contract_Value__c,On_Behalf_Of_Name__c,REVVY__Parent__r.Name,Business_Unit__c,REVVY__StartDateContract__c,Evergreen_Contract__c,Total_Contract_Value__c,Expiration_Date__c,Attorney__c,Is_Cable_Law_Dept_or_Corp_General_Counce__c,Record_URL__c,Contract_Type_CAF__c from REVVY__MnContract__c where id=:emailContractId LIMIT 1];
           if(contractList.size() > 0){
               REVVY__MnContract__c contract = contractList[0];
               system.debug('before -- '+contractList[0].Name);
               contractnameescaped = EncodingUtil.urlEncode(contractList[0].Name, 'UTF-8');
               system.debug('after -- '+contractList[0].Name.escapeHtml4());
               return contract;
           }
           else{
           return new REVVY__MnContract__c();
           }
       }
       else{
           return new REVVY__MnContract__c();
           }
    }
    
    public REVVY__MnApprovalStepInst__c getStepInstance(){
        List<REVVY__MnApprovalStepInst__c> StepList = new List<REVVY__MnApprovalStepInst__c>();
        if(emailContractId!=null){
            StepList = [select id from REVVY__MnApprovalStepInst__c where REVVY__StepStatus__c='Pending Approval' and REVVY__IsParentStep__c = false and REVVY__ApprovalInst__r.REVVY__Contract__c =:emailContractId];
            if(StepList.size() > 0){
                REVVY__MnApprovalStepInst__c APSI = StepList[0];
                return APSI;
            }
            else{
                return new REVVY__MnApprovalStepInst__c();
            }
        }
        else{
            return new REVVY__MnApprovalStepInst__c();
        }
    }
    public List<REVVY__MnApprovalStepInst__c> getApprover(){
        List<REVVY__MnApprovalStepInst__c> ApprovalInstList = new List<REVVY__MnApprovalStepInst__c>();
        if(emailContractId!=null){
            ApprovalInstList = [SELECT Name,(SELECT name,REVVY__ActionDate__c,REVVY__ApprovalComments__c,REVVY__Status__c FROM REVVY__StepInstApprovers__r) FROM REVVY__MnApprovalStepInst__c WHERE REVVY__ApprovalInst__r.REVVY__Contract__c = :emailContractId AND REVVY__StepStatus__c != 'Skipped' ORDER BY REVVY__Step_Sequence__c ASC];
            if(ApprovalInstList.size() > 0){
                return ApprovalInstList;
            }
            else{
                return new List<REVVY__MnApprovalStepInst__c>();
            }
        }
        else{
            return new List<REVVY__MnApprovalStepInst__c>();
        }
    }
    public List<contentversion> getContent(){
        List<contentversion> contentList = new List<contentversion>();
        if(emailContractId!=null){
            contentList = [SELECT Id,title,Attachment_Type__c,Description FROM contentversion WHERE FirstPublishLocationId =:emailContractId];
            if(contentList.size()>0){
                return contentList;
            }
            else{
                return new List<contentversion>();
            }
        }
        else{
             return new List<contentversion>();
        }
    }
}