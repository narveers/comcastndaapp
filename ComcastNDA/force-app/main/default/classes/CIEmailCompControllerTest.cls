@isTest
public class CIEmailCompControllerTest {
    @isTest static void Method1() {
        Contract__c testcontract = TestData.createContractRequest();
        
        ContentVersion cvlist = new Contentversion(); 
        cvlist.Title = 'CZDSTOU'; 
        cvlist.PathOnClient = 'test'; 
        cvlist.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body'); 
        List<ContentVersion> cvl = new List<ContentVersion>(); 
        cvl.add(cvlist); 
        insert cvl;
   /*     
        Document document;
        document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'Comcast_Email_Logo_Visual_Force';
        document.IsPublic = true;
        document.Name = 'My Document';
        document.FolderId = [select id from folder where name = 'Shared Documents'].id;
        insert document; 
   */     
        CIEmailCompController objClAss = new CIEmailCompController();
        objClAss.emailContractId = testcontract.id;
        objClAss.getContract(); 
        objClAss.getContent();
        objClAss.getReviewURL();
        
        CIEmailCompController objClAss2 = new CIEmailCompController();
        objClAss2.emailContractId = null;
        objClAss2.getContract(); 
        objClAss2.getContent();
        objClAss2.getPolicyURL();
    }
    
}