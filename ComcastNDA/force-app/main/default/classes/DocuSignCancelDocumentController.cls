/* 
    Name : DocuSignCancelDocumentController 
    Auther : Narveer S.
    Date : 04/05/2018
    Description : DocuSignCancelDocumentController for Cancel the DocuSign
*/
Public class DocuSignCancelDocumentController {
    
    public Static Id NDARecordId;
    public static string oid;
    Public static String NdaStatus;
    
    Public PageReference send() {
        NDARecordId = ApexPages.currentPage().getParameters().get('id');
        if(NDARecordId != Null) {
            NDA__c objNDA = [Select Id,DocuSign_Status__c,NDA_Status__c FROM NDA__c WHERE ID =: NDARecordId];
            if(objNDA.NDA_Status__c.equals('Closed')) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'NDA is already Expired/Voided');
                ApexPages.addMessage(myMsg);
                return null;    
            } else {
                List<dsfs__DocuSign_Status__c> lstDocuSignStatus = [Select Id,dsfs__Envelope_Status__c,NDA__c FROM dsfs__DocuSign_Status__c WHERE NDA__c =: NDARecordId AND dsfs__Envelope_Status__c = 'Sent'];
                for(dsfs__DocuSign_Status__c objDocuSignStatus: lstDocuSignStatus) {
                    objDocuSignStatus.dsfs__Envelope_Status__c = 'Voided';    
                }
                if(!lstDocuSignStatus.IsEmpty()) {
                    update lstDocuSignStatus;
                }
                objNDA.NDA_Status__c = 'Closed';
                objNDA.DocuSign_Status__c = 'Closed';
                Update objNDA;
                
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'NDA Contarcts has been Cancelled Successfully');
                ApexPages.addMessage(myMsg);
                return null;
            }
        }
        return Null;
    }
    
    public Static  PageReference doCancel() { 
        Id NDARecordId1 = ApexPages.currentPage().getParameters().get('id');
        PageReference demoPage = new pagereference('/' + NDARecordId1);
        demoPage.setRedirect(true);
        return demoPage;
    }
}