Public Class ReturntoContractController{
    
    public String URL;
    public Id contractId;
    public Boolean StandardPage = true;
    Public ReturntoContractController(ApexPages.StandardController controller){
        URL = ApexPages.currentPage().getHeaders().get('Referer');
        contractId = controller.getId();
        system.debug('This is URL - '+URL);
        system.debug('This is Id - '+contractId);
        if(URL!=null && URL!=''){
            List<String>URLList = new List<String>();
            URLList = URL.split('/apex/');
            if(URLList.size()>1){
                system.debug('This is the list of URL - '+URLList[0]);
                if(String.isNotBlank(URLList[1]) && String.isNotEmpty(URLList[1]))
                {
                    if(URLList[1].startsWith('EmbeddedApprovals')){
                        StandardPage = false;
                        system.debug('So its not a standard page call - ' + StandardPage);
                    }
                    else if(URLList[1].startsWith('EmbeddedManageApprovals')){
                        StandardPage = true;
                        system.debug('So its not a standard page call - ' + StandardPage);
                    }
                }
            }
        }   
        
        DestinationContract();   
    }
    public PageReference DestinationContract(){
        system.debug('We have landed on the destination');
        String LandingURL = System.Url.getSalesforceBaseURL().toExternalForm();
        if(StandardPage){
            LandingURL = LandingURL + '/' + contractId;
        } else{
            LandingURL = LandingURL + '/apex/RequestCAFContractVF_New?id=' + contractId;
        }
        system.debug('going to - '+ LandingURL);
        PageReference LandingPage = new PageReference(LandingURL);
        LandingPage.setRedirect(true);
        return LandingPage;
    }
}