/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class RequesterContactedEmailTest {
    
        private static testMethod void RequesterContactedEmail1() {
        
            // Create a new email, envelope object and Attachment
            Messaging.InboundEmail email = new Messaging.InboundEmail();
            Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
            
            Contract__c objContract = [Select Id,Name, Contract_Intake_Status__c From Contract__c LIMIT 1];
            
            email.subject = 'Requester Contacted ref: '+String.ValueOf(objContract.Id);
            email.fromName = 'test test';
            email.plainTextBody = 'Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
            
            Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
            Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
            binaryAttachment.Filename = 'test.txt';
            String algorithmName = 'HMacSHA1';
            Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
            Blob.valueOf('test_key'));
            binaryAttachment.Body = b;
            binaryAttachments[0] =  binaryAttachment ;
            email.binaryAttachments = binaryAttachments ;
            envelope.fromAddress = 'test@gmail.com';
            
            
            // setup controller object
            RequesterContactedEmail catcher = new RequesterContactedEmail();
            Messaging.InboundEmailResult result = catcher.handleInboundEmail(email, envelope);
            System.assertEquals( result.success  ,true);  
        
        }
        
}