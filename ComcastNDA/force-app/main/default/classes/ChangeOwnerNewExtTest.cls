@isTest
public class ChangeOwnerNewExtTest{

    @isTest static void testMethod1(){
    
        Test.startTest();
        
        //Creating Group
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        
        //Creating QUEUE
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Contract_Intake_Form__c');
            insert testQueue;
        }
        Account acc = new account(name = 'test');
        insert acc;
        
        Contact con = new contact(LastName = 'Test', AccountId = acc.id);
        insert con;
        
        List<Contract__c> lstContract = new List<Contract__c>();
        
        Contract__c objCIForm = new Contract__c();
        objCIForm.Name = 'test';
        objCIForm.Attorney__c = 'Alison Mansfield';
        objCIForm.Requester_Name__c = 'test';
        objCIForm.Business_Unit__c = 'Business Unit - HQ';
        objCIForm.Status_Update__c = 'test';
        objCIForm.Contract_Intake_Status__c = 'Pending Assignment';
        objCIForm.Counterparty__c = 'test';
        //objCIForm.OwnerId = testGroup.Id;
        
        insert objCIForm;
        
        lstContract.add(objCIForm);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objCIForm);
        ChangeOwnerNewExt rce = new ChangeOwnerNewExt(sc);
        
        //ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstContract);
        //stdSetController.setSelected(lstContract);
        //ChangeOwnerNewExt objReviewContractsExt = new ChangeOwnerNewExt(stdSetController);
        rce.ownerId = UserInfo.getUserId();
        rce.ownerName = 'test';
       // rce.valueSelection();
        rce.save();
        ChangeOwnerNewExt.getLookupRecord(rce.ownerName,'Contract__c');
        List<SelectOption> getdynamiclis = rce.getdynamiclist();
        
        //objReviewContractsExt.init();
        //objReviewContractsExt.ownerId = UserInfo.getUserId();
        //objReviewContractsExt.save();
        
        
        Test.stopTest();
    
    }
    
}