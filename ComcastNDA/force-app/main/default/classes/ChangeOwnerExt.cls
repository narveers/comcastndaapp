/*
Class Name : ChangeOwnerExt
Called From: 
Decsription: Change ownership on the Contract In-take Form

History    : Date         Modification      Modified By 
01/26/2018                Initial Draft     Vikram
*/ 

public class ChangeOwnerExt {
    private ApexPages.StandardSetController standardController;
    private ApexPages.StandardController controller;
    public List<Contract_Intake_Form__c> listContracts{get;set;}
    public Id ownerId{get;set;}
    public String ownerName{get;set;} 
    public ChangeOwnerExt(ApexPages.StandardSetController standardController){
        this.standardController = standardController;
    }
    public ChangeOwnerExt(ApexPages.StandardController controller){
        this.controller = controller;
    }
    
    /*
    Method Name: init
    Called From : Contract In-take view
    Description : Get handle to selected contracts
    */
    public PageReference init(){
        listContracts = new List<Contract_Intake_Form__c>();
        if(standardController!=null && standardController.getSelected().size()>0){
            List<Contract_Intake_Form__c> selectedContracts = [select id,name,Client_Name_and_business_unit__c,Description__c,Status__c,Review_Status__c,Ownerid,Owner.Name from Contract_Intake_Form__c where id in : standardController.getSelected()] ;
            for(Contract_Intake_Form__c contract: selectedContracts){
                listContracts.add(contract);
            }
        }else if(controller!=null && controller.getId()!=null){
            List<Contract_Intake_Form__c> selectedContracts = [select id,name,Client_Name_and_business_unit__c,Description__c,Status__c,Review_Status__c,Ownerid,Owner.Name from Contract_Intake_Form__c where id = : controller.getId()] ;
            for(Contract_Intake_Form__c contract: selectedContracts){
                listContracts.add(contract);
            }
        }
        return null;
    }
    
    // Save changes to OwnerId on the Contract
    public PageReference save(){
        for(Contract_Intake_Form__c contract: listContracts){
            contract.OwnerId = ownerId;
        }
        update listContracts;
        if(standardController!=null){
            return standardController.cancel();
        }else if(controller!=null){
            return controller.cancel();
        }else {
            return null;
        }
    }
    @RemoteAction
    public static List<sObject> getLookupRecord(String likeString , String objectName) {
        if(String.IsNotBlank(likeString) && String.IsNotBlank(objectName)){
            likeString = '%'+likeString+'%';
            String query = '';
            
            query = 'SELECT Id, Name FROM '+objectName+' WHERE Name LIKE : likeString LIMIT 50';
            return database.query(query);
        }
        return null;
    }
}