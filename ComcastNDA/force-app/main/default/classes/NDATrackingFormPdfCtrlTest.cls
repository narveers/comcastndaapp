@isTest public class NDATrackingFormPdfCtrlTest{
    @isTest static void testMethod1(){
        Test.startTest();
        
        NDA__c nda = new NDA__c();
        nda.NDA_Type__c = 'One Way NBCU';
        insert nda;
        
        Test.setCurrentPage(Page.NDATrackingFormPdf);
        
        NDATrackingFormPdfCtrl obj = new NDATrackingFormPdfCtrl();
        ApexPages.currentPage().getParameters().put('id',nda.id);
        
        Test.stopTest();
    }
}