/*
Name             : DocuSignFormCtrl
Author           : Cloud Mentor
Date             : 02/14/2019
Description      : Docu Signer Controller
*/
public class DocuSignFormCtrl {
    
    // SETTINGS
    Private string integration_key;
    Private string account_id;
    Private string ds_server; 
    Private string ds_username;
    Private string ds_password;
     
    Public string signer_email;
    Public string signer_name;
    Public String counterpartyName;
    Public string email_message;
    Public string output;
    Public string envelope_id;
    Public string error_code {get;set;} // Null means no error
    Public string error_message {get;set;}
    public string contractId;
    Public String NdaStatus;
    Public Docu_Sign__mdt objDocuSignMDT;
    
    public String signerVal {get;set;}
    public integer idx {get;set;}
    public Map<integer,boolean> signerMap{get;set;}
    public List<User> UserList{get;set;}
    public String searchString{get;set;}
    
    Public String DSEID;
    Public String SourceID;
    Public String DSEnvGUID;

    public DocuSign__c objDocuSign{get;set;}
    public List<DocuSign__c> docuSignList{get;set;}
    public String docuSignUrl{get;set;}
    
    Private static string trace_value = 'SFDC_004_SOAP_email_send'; // Used for tracing API calls
    Private static string trace_key = 'X-ray';
    Private Static DocuSignTK.APIServiceSoap api_sender = new DocuSignTK.APIServiceSoap();
    
    public Static List<ContentVersion> lstContentVersion;
    
    public DocuSignFormCtrl(ApexPages.StandardController controller) {
        lstContentVersion = new List<ContentVersion>();        
    }
    public DocuSignFormCtrl(){
        lstContentVersion = new List<ContentVersion>(); 
        objDocuSign = new DocuSign__c();
        docuSignList = new List<DocuSign__c>();
        docuSignList.add(new DocuSign__c());
        signerMap = new Map<integer,boolean>();
        signerMap.put(1,false);
        UserList = new List<User>();
        UserValues();
    }
    Public void send(Id ContractpageId) {
        
        contractId = ContractpageId;
        ApexPages.Message myMsg;
        System.debug(ContractpageId);
        if(contractId != null && String.isNotBlank(contractId)) {
            System.debug(contractId);
            lstContentVersion = [Select id,title,Attachment_Type__c,DocuSign_Status__c,Description,versiondata,FirstPublishLocationId,ContentDocumentId from ContentVersion where Id =: contractId LIMIT 1];
            System.debug(lstContentVersion);
            if(lstContentVersion.size() > 0){
            }
        }
    }
    
    public PageReference SaveRecord() {
        Id DocumentId = ApexPages.currentPage().getParameters().get('id');
        send(DocumentId);
        Integer SignerCount = 0;
        Integer RSRO = 1;
        Integer RROS = 1;
        String RC = '';
        String CCRM = '';
        String CCTM = '';
        String CRCL = '';
        String CRL = '';
        String OCO = 'Tag';
        String DST = '';
        String LA = '';
        String CEM = 'I am sending you this request for your electronic signature, please review and electronically sign by following the link below.';
        String CES = lstContentVersion[0].FirstPublishLocationId;
        String STB = '1';
        String SSB = '1';
        String SES = '1';
        String SEM = '1';
        String SRS = '1';
        String SCS = '1';
        String RES = '2';
        String CCNM = '';
        String ContractId = lstContentVersion[0].FirstPublishLocationId;
        
        for(DocuSign__c objDocuSign: docuSignList) {
            SignerCount++;    
        }
       // String pageUrl = '{!URLFOR('/apex/dsfs__DocuSign_CreateEnvelope', null, [SourceID = lstContentVersion[0].FirstPublishLocationId,CCRM = 'Decision Maker~Signer 1',CCTM = 'Decision Maker~Signer',DST = 'a2e2cd02-f11e-4c8c-a830-c9f2b6564a9e',LA = '0',LF = '0',OCO = 'Tag'])}';
       // String pageUrl = 'https://ccms--full.my.salesforce.com/apex/dsfs__DocuSign_CreateEnvelope?SourceID='+ContractId;
       // pageUrl += '&RC='+RC+'&RSL='+SignerCount+'&RSRO='+String.ValueOf(RSRO)+'&RROS='+String.ValueOf(RROS);
       // pageUrl += '&CCRM='+CCRM+'&CCTM='+CCTM+'&CRCL='+CRCL+'&CRL='+CRL+'&OCO='+OCO+'&DST='+DST+'&CCNM='+CCNM+'&LA='+LA+'&CEM='+CEM+'&CES='+CES+'&SRS='+SRS+'&STB='+STB+'&SSB='+SSB+'&SES='+SES+'&SEM='+SEM+'&SRS='+SRS+'&SCS='+SCS+'&RES='+RES;
        
        //System.debug(pageUrl);
        //PageReference pageRef = new PageReference(pageUrl);
        //pageRef.setRedirect(true);
        //return new PageReference(pageUrl);
        return null;
    }
    
    public void AddSigner(){
        docuSignList.add(new DocuSign__c());
        signerMap.put(signerMap.size()+1,false);
    }
    
    public void changeSigner(){
        if(signerVal == 'Internal Signer'){
            signerMap.put(idx,true);
        }else if(signerVal == 'External Signer'){
            signerMap.put(idx,false);
        }
    }
    
    public void UserValues(){
        searchString = '%'+searchString+'%';
        UserList = [select id,name from user where name LIKE :searchString limit 1000];
    }
    
    public void SetUser(){
        docuSignList[idx-1].Full_Name__c = searchString  ;
    }
    
    public PageReference setDocuSignUrl(){
        Id DocumentId = ApexPages.currentPage().getParameters().get('id');
        send(DocumentId);
        
        docuSignUrl = '/apex/dsfs__DocuSign_CreateEnvelope?SourceID='+lstContentVersion[0].FirstPublishLocationId+'&CRL=';
        
        Integer i = 0;
        for(DocuSign__c doc : docuSignList){
            i++;
            docuSignUrl += 'Email~'+doc.Email__c+';Role~Signer'+i+';FirstName~'+doc.Full_Name__c+';RoutingOrder~'+i+',';
        }
        
        docuSignUrl = docuSignUrl.subString(0,docuSignUrl.length()-2);
                
        docuSignUrl += ';SignNow~1,LoadDefaultContacts~1&CCRM=Decision Maker~Signer 1&CCTM=Decision Maker~Signer&DST=&LA=0&LF=0&OCO=Tag&LF=0&FILES='+lstContentVersion[0].ContentDocumentId;
        
        return new PageReference(docuSignUrl);
    }
}