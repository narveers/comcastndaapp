public class ConfidentialContractRequestController {
    public Contract__c contract{get;set;}
    public String EmailComments{get;set;}
    public String RecipientName{get;set;}
    public String error{get;set;}
    public String currentStage{get;set;}
    public String attachDescription{get;set;}
    public String ContractLink{get;set;}
    public List<Attachment> AttachmentList{get;set;}
    public Boolean ContractValueBoolean{get;set;}
    public Id selectedid{get;set;}
    public Id attachId{get;set;}
    public Static Id ContractId;
    public Boolean isFirst{get;set;}
    public static Boolean isShareable;
    public List<Id> attachmentIdList;
    public ConfidentialContractRequestController(ApexPages.StandardController controller){
        AttachmentList = new List<Attachment>();
        attachmentIdList = new List<Id>();
        ContractId = ApexPages.currentPage().getParameters().get('Id');
        System.debug('Conid-'+ContractId);
        isFirst = True;
        isShareable = False;
        if(ContractId != NULL){
            currentStage = 'Contract Details'; 
            isFirst = False;
            isShareable = True;
            contract = [select id,Assigned_Attorney__c,Attorney_Contact__c,AttorneyContact__r.Name,Existing_NDA__c,PII_and_IP_Tech_Survey__c,Business_Unit_HQ__c,Business_Unit_Division__c,Existing_Master_Agreement__c,
                        Name,Procurement_Contact__c,Requester_Email__c,Prior_Attorney_Engagement__c,Business_Unit__c,Counterparty__c,
                        Projected_Dollar_Amount__c,Requester_Name__c,Request_Description__c,Projected_due_date__c,Please_Explain__c,
                        Has_TPSA_completed_an_assessment_for_thi__c,
                        Is_Procurement_Engaged__c,Brief_Summary__c, Confidential_Submitter__c from Contract__c where id=: contractId];
            contract.Prior_Attorney_Engagement__c = 'Yes';
        }else{
            currentStage = 'Contract Request';
            contract = new Contract__c();
        }
    }
    public List<SelectOption> AttorneyList{
        get
        {
            AttorneyList = new List<SelectOption>();
            AttorneyList.add(new SelectOption('','None'));
            for( User usr: [SELECT Name, Id FROM USER WHERE Id IN (SELECT UserOrGroupId FROM GroupMember where group.DeveloperName='CI_Attorney')])
            {
                AttorneyList.add(new SelectOption(usr.Id,usr.Name));
            }       
            return AttorneyList; 
        }
        set;
    }
    public Static void callSharing(){
        if(isShareable){
            manualShareContract(ContractId, UserInfo.getUserId());
        }
    }
    // If the owner is changed the sharing will be deleted
    public static void manualShareContract(Id recordId, Id userOrGroupId){
        Contract__Share ConShr = new Contract__Share();
        ConShr.ParentId = recordId;
        ConShr.UserOrGroupId = userOrGroupId;
        ConShr.AccessLevel = 'Edit';
        ConShr.RowCause = Schema.Contract__Share.RowCause.Manual;
        Database.SaveResult sr = Database.insert(ConShr,false);
        if(!sr.isSuccess()){
            Database.Error err = sr.getErrors()[0];
            if(err.getStatusCode() != StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&
               !err.getMessage().contains('AccessLevel')){
                   system.debug('Error sharing record - '+err.getMessage());
               }
        }
    }
    public PageReference deleteAttachment(){
        Integer count=0;
        Attachment deleteDocument=null;
        try{
            for(Attachment doc:AttachmentList){
                if(doc.Id == selectedid){
                    deleteDocument = doc;
                    AttachmentList.Remove(count);
                    break;
                }
                count++;
            }
            if (deleteDocument!=null){
                delete deleteDocument;
            } 
        }
        catch(DMLException ex){
            error = ex.getMessage();
        }
        catch(exception ex){
            error = ex.getMessage();
        }
        return null;
    }
    public PageReference redirectToDashboard(){
        saveContractRecord();
        savecontent();
        return new PageReference('/apex/ContractIntakeRequesterDashboard');
    }
    public PageReference nextStage(){
        error = '';
        system.debug('current stage - '+currentStage);
        if(currentStage == 'Contract Details'){
            currentStage = 'Upload Attachment(s)';
        }else if(currentStage == 'Upload Attachment(s)'){
            currentStage = 'Review and Submit'; 
        }else if(currentStage == 'Contract Request'){
            system.debug('next stage');
            saveContractRecord();
            //sendEmail(ToEmailAddress, contract);
            //return new PageReference('/apex/ContractManagementDashboard');
        }
         return null;
    }
    public void previousStage(){
        error = '';
        if(currentStage == 'Review and Submit'){
            currentStage = 'Upload Attachment(s)';
        }else if(currentStage == 'Upload Attachment(s)'){
            currentStage = 'Contract Details';
        }
    }
    public PageReference saveContractRecord(){
        SavePoint sp = Database.setSavepoint();
        try{
            if(currentStage == 'Contract Request'){
                contract.Contract_Intake_Status__c = 'Confidential Link Sent';
                contract.RecordTypeId = Schema.SObjectType.Contract__c.getRecordTypeInfosByName().get('Cable Legal').getRecordTypeId();
                contract.Requester_Name__c = UserInfo.getName();
                contract.Requester_Email__c = UserInfo.getUserEmail();
            }else if (currentStage == 'Review and Submit' ){
                contract.Contract_Intake_Status__c = 'Pending Assignment';
                contract.Confidential_Submitter__c = UserInfo.getUserId();
            }
            contract.Is_Confidential__c = TRUE;
            contract.Assigned_Attorney__c = contract.AttorneyContact__c;
            //contract.Attorney__c = contract.Attorney_Contact__c;
            upsert contract;
            // if the user was different second time, share the record with that person
            if(contract.Confidential_Submitter__c  != null){
             manualShareContract( contract.id,contract.Confidential_Submitter__c); 
            } 
            
            system.debug('contract created - '+contract.id);
            ContractLink = contract.id;
            return null;
        }catch(DMLException e){
            Database.rollback(sp);
            for(Integer i=0 ;i < e.getNumDml() ; i++){
                error +=e.getdmlMessage(i);
            }
            return null;
        }catch(Exception e){
            Database.rollback(sp);
            error = e.getMessage();
            return null;
        }
    }
    public void savecontent(){
        if(!AttachmentList.isEmpty() && AttachmentList != null && AttachmentList.size() > 0){
            List<ContentVersion> FilesList = New List<ContentVersion>();
            for(Attachment doc : AttachmentList) {
                ContentVersion cv = new ContentVersion();
                cv.versionData = doc.Body;
                cv.title = doc.name;
                cv.FirstPublishLocationId = contract.id;
                cv.pathOnClient = doc.name;
                cv.Description = doc.Description;
                FilesList.add(cv);
            }
            Try {
                insert FilesList;
                //sendEmailtoContractManager();
                //sendEmailtoRequester();
            }
            Catch (DMLException e){
                error = e.getMessage(); 
            }
            Finally{
                if(AttachmentList.size() > 0 && !AttachmentList.isEmpty()){
                    Database.DeleteResult[] delRes = Database.delete(AttachmentList, false);
                    if (delRes.size() > 0){
                        system.debug('delete result -- '+delRes);
                    }
                }
            }
        }
    }
    /*
    public void sendEmail(string Who, Contract__c What){
        Try{
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //mail.setTemplateId(EmailTemplate);
            mail.toAddresses = new String[]{who};
                mail.plaintextbody = 'Requester Name: '+What.Requester_Name__c+'\nOther Parties: '+What.Counterparty__c+
                '\nDescription: '+What.Request_Description__c+'\n'+EmailComments+'\nContract link: '+Label.BaseLabel+'/apex/ConfidentialContractRequest?id='+What.id;
            mail.setSenderDisplayName('Legal Technology Operations');
            mail.setBccSender(false); 
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }Catch(Exception ex){
            error = ex.getMessage();
        }
    }*/
    public void attachFileFunction(){
        error = '';
        if(String.isNotBlank(attachId)){
            attachmentIdList.add(attachId); 
        }
        if(attachmentIdList.size() > 0){
            if(AttachmentList.size()>0){
                AttachmentList.clear();
            }
            AttachmentList = [SELECT Id,Name,Body,ContentType,Description FROM Attachment WHERE Id IN:attachmentIdList];
        }
    }
    public void reRenderPageFunc(){
    }
}