public class importHrUserFileBatch {
    
    public static void uploadCSv(){
        
        StaticResource static_resource = [SELECT Id, Body FROM StaticResource WHERE Name = 'HR_VPandAbove' ];
        String csvAsString = static_resource.body.toString();
        String[] csvFileLines = csvAsString.split('\n'); 
        System.debug('csvFileLines.size()'+ csvFileLines.size() );
        List<String> newCsvLines = new list<string>();
        for( Integer i = 1; i < csvFileLines.size() ; i++ ){
            String csvLine = csvFileLines[i];
            String prevLine = csvLine;
            Integer startIndex;
            Integer endIndex;
            while(csvLine.indexOf('"') > -1){
                if(startIndex == null){
                    startIndex = csvLine.indexOf('"');
                    csvLine = csvLine.substring(0, startIndex) + ':quotes:' + csvLine.substring(startIndex+1, csvLine.length());
                }else{
                    if(endIndex == null){
                        endIndex = csvLine.indexOf('"');
                        csvLine = csvLine.substring(0, endIndex) + ':quotes:' + csvLine.substring(endIndex+1, csvLine.length());
                    }
                    
                }
                if(startIndex != null && endIndex != null){
                    String sub = csvLine.substring(startIndex, endIndex);
                    sub = sub.replaceAll(',', ':comma:');
                    csvLine = csvLine.substring(0, startIndex) + sub + csvLine.substring(endIndex, csvLine.length());
                    startIndex = null;
                    endIndex = null;
                }
                
            }
            String csvNewLine = '';
            for(String column : csvLine.split(',')){
                column = column.replaceAll(':quotes:', '');
                csvNewLine += column+','; 
            }
            //System.debug('csvNewLine---->'+csvNewLine);
            newCsvLines.add(csvNewLine);
        }
        //System.debug('newCsvLines---->'+newCsvLines);  
       // System.debug('newCsvLines---->'+newCsvLines.size()); 
        
        
        double totalNoOfHrFileRec = newCsvLines.size();
        double totalNoOfEfficativeRec = 0 ;
        double checkEfficativeRecPersantage = 0 ;
        Map<String,HRFile__c> mapOfGuidAndHrRec = new Map<String,HRFile__c>();
        Map<String,User> mapOfGuidAndUsrRec = new Map<String,User>();
        Set<id> hrIds = new Set<id>();
        List<HRFile__c>   hrlist = new  List<HRFile__c>();
        List<User>   usrlist = new  List<User>();
        List<User>   usrlistUpd = new  List<User>();
        List<User>   usrlistInsert = new  List<User>();
        List<HRFile__c> allHRFilelst = [select id,GUID__c,Employee_First_Name__c,Supervisor_Name__c,Job_Function__c,Division__c,
                                        Employee_Middle_Name__c,Employee_Last_Name__c,Employee_Full_Name__c,Business_Unit_Area__c, 
                                        Employee_Email__c,Employee_Work_Phone__c,Employee_Type__c,FEIN__c,Job_Family__c,Supervisor_Email__c
                                        ,Termination_Date__c,Job_Level__c,Employee_Work_State__c,Employee_Location__c,Company__c
                                        ,Employee_Department_Descr__c,Employee_Position_Title__c from HRFile__c  ];
        
        List<User> allUserLst = [select id,FirstName,Title,Phone,FEIN__c,Job_Family__c,Job_Function__c,Business_Unit_Area__c,CompanyName,
                                 Division,Job_Level__c,Supervisor_Email__c,Supervisor_Name__c,email,LastName,GUID__c,
                                 PostalCode,State,Country from User  ];
        System.debug('allUserLst'+ allUserLst);
        for(HRFile__c hr : allHRFilelst ){
            mapOfGuidAndHrRec.put( hr.GUID__c, hr);
        }
        for(User usr : allUserLst ){
            mapOfGuidAndUsrRec.put( usr.GUID__c, usr);
        }
        
        
        
        Integer totalNumberOfUpdatedRec = 0;
        Integer totalNumberOfInsertedRec = 0;
        List<GroupMember> GMlist = new List<GroupMember>();
        
        String Emailbody='';
        String beforeUpd ='';
        String afterUpd ='';
        Profile prfil = [SELECT Id FROM Profile WHERE Name = 'Viewers' limit 1];
        UserRole cableRol  =[select id from UserRole where name =:'Cable TBA' limit 1];
        UserRole cropRol  =[select id from UserRole where name =:'Corp TBA' limit 1];
        for( Integer i = 0; i < 8 ; i++ ){
            Boolean checkEfficative = False;
            string[] csvRecordData = newCsvLines[i].split(',');
            System.debug('newCsvLines'+ newCsvLines[i]);
                if( mapOfGuidAndUsrRec.containsKey( csvRecordData[0] ) ){
                    System.debug('csvRecordData[0]'+ csvRecordData[0]);
                    System.debug('csvRecordData'+ csvRecordData);
                    System.debug('csvRecordData[1]'+ csvRecordData[1]);
                    
                    System.debug('csvRecordData[3]'+ csvRecordData[3]);
                    System.debug('csvRecordData[4]'+ csvRecordData[4].replaceAll(':comma:', ',') );
                    System.debug('csvRecordData[5]'+ csvRecordData[5]);
                    System.debug('csvRecordData[6]'+ csvRecordData[6]);
                    System.debug('csvRecordData[7]'+ csvRecordData[7]);
                    System.debug('csvRecordData[8]'+ csvRecordData[8]);
                    System.debug('csvRecordData[9]'+ csvRecordData[9]);
                    System.debug('csvRecordData[10]'+ csvRecordData[10]);
                    System.debug('csvRecordData[11]'+ csvRecordData[11].replaceAll(':comma:', ',') );
                    System.debug('csvRecordData[12]'+ csvRecordData[12]);
                    System.debug('csvRecordData[13]'+ csvRecordData[13]);
                    System.debug('csvRecordData[14]'+ csvRecordData[14]);
                    System.debug('csvRecordData[15]'+ csvRecordData[15]);
                    System.debug('csvRecordData[16]'+ csvRecordData[16]);
                    
                    User usr = mapOfGuidAndUsrRec.get( csvRecordData[0] );
                    System.debug('usr'+ usr);
                    if( csvRecordData[16] == 'Cable' ){
                        usr.UserRoleId = cableRol.Id;
                    }
                    if( csvRecordData[16] == 'Corporate' ){
                        usr.UserRoleId = cropRol.Id;
                    }
                    
                    if( usr.FirstName == csvRecordData[1] ){
                        
                    }else{
                        beforeUpd += usr.FirstName;
                        usr.FirstName = csvRecordData[1];
                        afterUpd += usr.FirstName;
                        checkEfficative = true;
                        System.debug('1');
                    }
                    
                    if( usr.LastName == csvRecordData[3]){
                        
                    }else{
                        beforeUpd += usr.LastName;
                        usr.LastName = csvRecordData[3];
                        AfterUpd += usr.LastName;
                        checkEfficative = true;
                         System.debug('2');
                    }
                   /* if( usr.Email == 'legal_help@comcast.com' ){
                        
                    }else{
                        usr.Email = 'legal_help@comcast.com' ; 
                        checkEfficative = true;
                    }*/
                    if( usr.Phone == csvRecordData[6] ||  String.isBlank( csvRecordData[6] ) ){
                        
                    }else{
                        beforeUpd += usr.Phone;
                        usr.Phone = csvRecordData[6];
                        afterUpd += usr.Phone;
                        checkEfficative = true;
                         System.debug('3');
                    }
                    
                    if( usr.Job_Function__c == csvRecordData[8] ||  String.isBlank( csvRecordData[8] )){
                        
                    }else{
                        beforeUpd += usr.Job_Function__c;
                        usr.Job_Function__c = csvRecordData[8];
                        afterUpd += usr.Job_Function__c;
                        checkEfficative = true;
                         System.debug('4');
                    }
                    if( usr.Job_Family__c == csvRecordData[9] ||  String.isBlank( csvRecordData[9] )){
                        
                    }else{
                        beforeUpd += usr.Job_Family__c;
                        usr.Job_Family__c = csvRecordData[9];
                        afterUpd += usr.Job_Family__c;
                        checkEfficative = true;
                         System.debug('5');
                    }
                    if( usr.Job_Level__c == csvRecordData[10] ||  String.isBlank( csvRecordData[10] )){
                        
                    }else{
                        beforeUpd += usr.Job_Level__c;
                        usr.Job_Level__c = csvRecordData[10];
                        afterUpd += usr.Job_Level__c;
                        checkEfficative = true;
                         System.debug('6');
                    }
                    if( usr.Title == csvRecordData[11].replaceAll(':comma:', ',') ||  String.isBlank( csvRecordData[11].replaceAll(':comma:', ',') )){
                        
                    }else{
                        beforeUpd = usr.Title;
                        usr.Title = csvRecordData[11].replaceAll(':comma:', ',');
                        afterUpd = usr.Title;
                        checkEfficative = true;
                         System.debug('11');
                    }
                    if( usr.FEIN__c == csvRecordData[13] ||  String.isBlank( csvRecordData[13] )){
                        
                    }else{
                        beforeUpd += usr.FEIN__c;
                        usr.FEIN__c = csvRecordData[13];
                        afterUpd += usr.FEIN__c;
                        checkEfficative = true;
                         System.debug('7');
                    }
                    if( usr.Business_Unit_Area__c == csvRecordData[14] ||  String.isBlank( csvRecordData[14] )){
                        
                    }else{
                        beforeUpd += usr.Business_Unit_Area__c;
                        usr.Business_Unit_Area__c = csvRecordData[14];
                        afterUpd += usr.Business_Unit_Area__c;
                        checkEfficative = true;
                         System.debug('8');
                    }
                    if( usr.Division == csvRecordData[15] ||  String.isBlank( csvRecordData[15] )){
                        
                    }else{
                        beforeUpd += usr.Division;
                        usr.Division = csvRecordData[15];
                        afterUpd += usr.Division;
                        checkEfficative = true;
                         System.debug('9');
                    }
                    if( usr.CompanyName == csvRecordData[16] ||  String.isBlank( csvRecordData[16] )){
                        
                    }else{
                        beforeUpd += usr.CompanyName;
                        usr.CompanyName = csvRecordData[16];
                        afterUpd += usr.CompanyName;
                        checkEfficative = true;
                         System.debug('10');
                    }
                    if( usr.PostalCode == csvRecordData[18] ||  String.isBlank( csvRecordData[18] )){
                        
                    }else{
                        beforeUpd += usr.PostalCode;
                        usr.PostalCode = csvRecordData[18];
                        afterUpd += usr.PostalCode;
                        checkEfficative = true;
                         System.debug('11');
                    }
                    if( usr.State == csvRecordData[19] ||  String.isBlank( csvRecordData[19] )){
                        
                    }else{
                        beforeUpd += usr.State;
                        usr.State = csvRecordData[19];
                        afterUpd += usr.State;
                        checkEfficative = true;
                         System.debug('12');
                    }
                    if( usr.Country == csvRecordData[20] ||  String.isBlank( csvRecordData[20] )){
                        
                    }else{
                        beforeUpd += usr.Country;
                        usr.Country = csvRecordData[20];
                        afterUpd += usr.Country;
                        checkEfficative = true;
                         System.debug('13');
                    }
                    if( usr.Supervisor_Name__c == csvRecordData[21].replaceAll(':comma:', ',')  ){
                        
                    }else{
                        beforeUpd += usr.Supervisor_Name__c;
                        usr.Supervisor_Name__c = csvRecordData[21].replaceAll(':comma:', ',');
                        afterUpd += usr.Supervisor_Name__c;
                        checkEfficative = true;
                         System.debug('14');
                    }
                   /* if( usr.Supervisor_Email__c == ''){
                        
                    }else{
                        usr.Supervisor_Email__c = 'legal_help@comcast.com';
                        checkEfficative = true;
                         System.debug('15');
                    }*/
                    
                    
                    
                    if( checkEfficative == true ){
                        beforeUpd += '\n';
                        afterUpd += '\n';
                        totalNoOfEfficativeRec++;
                        totalNumberOfUpdatedRec++;
                        usrlistUpd.add(usr);
                    }
                    
                }
                else{
                    User usr = new user();
                    System.debug('newCsvLines'+ newCsvLines[i] );
                    csvRecordData[4].replaceAll(':comma:', ',');
                    csvRecordData[21].replaceAll(':comma:', ',');
                    
                    usr.GUID__c = csvRecordData[0];
                    usr.FirstName = csvRecordData[1];
                    usr.LastName = csvRecordData[3];
                    usr.Alias = usr.FirstName.substring(0, 1) + usr.LastName.substring(0, 4);
                    usr.Email = '8legal_help@comcast.com' ; 
                    usr.Username = usr.Email;
                    usr.CommunityNickname = usr.Email.split('@')[0];
                    usr.ProfileId = prfil.Id;
                    usr.Phone = csvRecordData[6];
                    usr.Job_Function__c = csvRecordData[8];
                    usr.Job_Family__c = csvRecordData[9];
                    usr.Job_Level__c = csvRecordData[10];
                    usr.TimeZoneSidKey = 'America/Los_Angeles';
                    usr.EmailEncodingKey = 'UTF-8';
                    usr.LanguageLocaleKey = 'en_US';
                    usr.LocaleSidKey = 'en_US';
                    usr.FEIN__c = csvRecordData[13];
                    usr.Business_Unit_Area__c = csvRecordData[14];
                    usr.Division = csvRecordData[15];
                    usr.CompanyName = csvRecordData[16];
                    usr.PostalCode = csvRecordData[18];
                    usr.State = csvRecordData[19];
                    usr.Country = csvRecordData[20];
                    usr.Supervisor_Name__c = csvRecordData[21].replaceAll(':comma:', ',');
                    usr.Supervisor_Email__c = 'legal_help@comcast.com';
                   
                    if( csvRecordData[16] == 'Cable' ){
                        usr.UserRoleId = cableRol.Id;
                    }
                    if( csvRecordData[16] == 'Corporate' ){
                        usr.UserRoleId = cropRol.Id;
                    }

                    
                    totalNoOfEfficativeRec++;
                    totalNumberOfInsertedRec++;
                    usrlistInsert.add(usr);
                }
            }
            
        Set<id> userIds = new Set<id>();
        Decimal  checkForInsert = (totalNoOfEfficativeRec / totalNoOfHrFileRec)*100;
        if( checkForInsert <= 5 ){
            Emailbody += 'Total % Of Rec Are Change --->' + checkForInsert.setScale(2)  +'\n' ;
            Emailbody += 'Total No Of Rec--->' + totalNoOfHrFileRec +'\n' ;
            Emailbody += 'Total No Of Effective Rec--->' + totalNoOfEfficativeRec +'\n';
            Emailbody += 'Total Number Of Updated Rec--->' + totalNumberOfUpdatedRec +'\n';
            Emailbody += 'Before update--->' +beforeUpd;
            Emailbody += 'After update--->' +afterUpd;
            Emailbody += 'Total Number Of Inserted Rec--->' + totalNumberOfInsertedRec +'\n' ;
            System.debug( checkForInsert );
            
            Database.SaveResult[] srList1 = Database.update(usrlistUpd, false);
            for ( Database.SaveResult sr : srList1 ) {
                if ( sr.isSuccess() ) {
                    System.debug('Successfully inserted User. User ID: ' + sr.getId());
                    userIds.add( sr.getId() );
                    System.debug('userIds'+ userIds);
                    Emailbody += 'User Id ---> '+ sr.getId() +' is updated.'+'\n';
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                         Emailbody += 'UserException ---> '+ err.getMessage()+' \n';
                        System.debug('User fields that affected this error: ' + err.getFields());
                    }
                }
            }
           
            Database.SaveResult[] srInsertList = Database.insert(usrlistInsert, false);
            for ( Database.SaveResult sr : srInsertList ) {
                if ( sr.isSuccess() ) {
                    System.debug('Successfully inserted User. User ID: ' + sr.getId());
                    userIds.add( sr.getId() );
                    System.debug('userIds'+ userIds);
                    Emailbody += 'User Id ---> '+ sr.getId() +' is inserted.'+'\n';
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                         Emailbody += 'UserException ---> '+ err.getMessage()+' \n';
                        System.debug('User fields that affected this error: ' + err.getFields());
                    }
                }
            }
            
            
        }
        
        
        System.debug('totalNoOfRec' + totalNoOfHrFileRec );
        System.debug('totalNoOfEfficativeRec' + totalNoOfEfficativeRec ); 
        System.debug('totalNumberOfInsertedRec' + totalNumberOfInsertedRec );
        System.debug('totalNumberOfUpdatedRec' + totalNumberOfUpdatedRec );
        
        if( userIds != null && !userIds.isEmpty()  ){
            AddUserOnPublicGroup.AddToGroups(userIds);
            AddUserOnPublicGroup.sendEmailToUser(Emailbody);
        }
                
    
    }
    
}