public class ApprovalHistoryController {
    public static Id contractId;
    Public ApprovalHistoryController(ApexPages.StandardController Scontrol){
        contractId = Scontrol.getId();
    }
    @AuraEnabled
    Public static List<REVVY__MnApprovalStepInst__c> getApprovalHistory(){
        List<REVVY__MnApprovalStepInst__c> tempApprovalHistory;
        tempApprovalHistory = [SELECT Id,Name,(SELECT REVVY__AssignedUser__r.Name,REVVY__ActionDate__c,REVVY__ApprovalComments__c 
                                    FROM REVVY__StepInstApprovers__r) FROM REVVY__MnApprovalStepInst__c WHERE REVVY__StepStatus__c != 'Skipped' AND
                    				REVVY__ApprovalInst__r.REVVY__Contract__c = 'a0s0x000000XMfY' ORDER BY Lastmodifieddate DESC];
       // tempApprovalHistory = [SELECT Id,REVVY__AssignedUser__c,REVVY__ActionDate__c,	REVVY__ApprovalComments__c,REVVY__StepInst__r.Name,REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c FROM REVVY__MnStepApproverInst__c WHERE REVVY__StepInst__r.REVVY__ApprovalInst__r.REVVY__Contract__c = 'a0s0x000000XMfY' ORDER BY REVVY__ActionDate__c DESC];
            system.debug('results - '+ tempApprovalHistory);
        
        return tempApprovalHistory;
    }
}