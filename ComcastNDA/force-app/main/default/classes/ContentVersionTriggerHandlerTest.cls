@isTest
private class ContentVersionTriggerHandlerTest {
	@isTest
    Static Void CreateContentVersion(){
        Test.startTest();
        
        Account acc = new account(name = 'test');
        insert acc;
        
        String pid =[Select Id from Profile Where name ='Standard User'].Id ;
        User user = new User(Username='testaskdhjkasdj@sdjkfhsjd.com',LastName = 'asdjhgasjhd',
                            email='askdasgdhgas@sdjkfd.com',Alias='asdhagsd',TimeZoneSidKey='America/Los_Angeles',
                            LocaleSidKey='en_US',EmailEncodingKey='UTF-8',ProfileId=pid,LanguageLocaleKey='en_US');
    	insert user;
        
        User user1 = new User(Username='testaskdhjkasdj@sdjkfhsjd.com.br',LastName = 'asdjhgasjhd12',
                            email='askdasgdhgas@comcast.com',Alias='asd123',TimeZoneSidKey='America/Los_Angeles',
                            LocaleSidKey='en_US',EmailEncodingKey='UTF-8',ProfileId=pid,LanguageLocaleKey='en_US');
    	insert user1;
        
        REVVY__MnLifeCycle__c lifeCycle = new REVVY__MnLifeCycle__c();
        lifeCycle.Name = 'Test Lifecycle';
        lifeCycle.REVVY__ApplyTo__c = 'Original';
        lifeCycle.REVVY__Object__c = 'MnContract__c';
        lifeCycle.REVVY__RecordType__c = 'Request_CAF';
        lifeCycle.REVVY__Status__c = 'Draft';
        insert lifeCycle;
        
        REVVY__MnLifeCycleStage__c stage = new REVVY__MnLifeCycleStage__c();
        stage.name = 'Test';
        stage.REVVY__LifeCycle__c = lifeCycle.Id;
        stage.REVVY__Phase__c = 'Draft';
        stage.REVVY__SubPhase__c = 'Draft';
        stage.REVVY__Editable__c = true;
        insert stage;
        
        lifeCycle.REVVY__InitialStage__c = stage.Id;
        lifeCycle.REVVY__Status__c = 'Active';
        update lifeCycle;
        
        REVVY__MnContract__c contract = new REVVY__MnContract__c();
        Contract.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType='REVVY__MnContract__c' AND DeveloperName='Request_CAF' LIMIT 1].Id;
        Contract.CAF_Type__c = 'Cable Division';
        Contract.Name = 'test';
        Contract.Contract_Value__c = 85000;
        Contract.REVVY__Account__c = acc.Id;
        Contract.Unchanged_legal_provided_template_used__c = 'No';
        Contract.Is_Sourcing_Contract__c = 'No';
        Contract.Less_Than_100K__c = 'No';
        Contract.Brief_Summary__c = 'test summary';
        insert Contract;
        
        REVVY__MnApprovalDef__c mad = new REVVY__MnApprovalDef__c(REVVY__MainObject__c = 'REVVY__MnContract__c',REVVY__Order__c = 100);
        insert mad;
        
        REVVY__MnApprovalInst__c mai = new REVVY__MnApprovalInst__c(REVVY__ApprovalDef__c = mad.id, REVVY__ApprovalObjId__c = 'Text',REVVY__Contract__c = contract.Id);
        insert mai;
        
        REVVY__MnApprovalStepDef__c asd = new REVVY__MnApprovalStepDef__c(REVVY__ApprovalDef__c = mad.id, REVVY__StepSequence__c = 100);
        insert asd;
        
        REVVY__MnApprovalStepInst__c asi = new REVVY__MnApprovalStepInst__c(REVVY__ApprovalInst__c = mai.id, REVVY__ApprovalStepDef__c = asd.id, REVVY__Step_Sequence__c = 100,
                                                                           REVVY__StepStatus__c='Pending Approval',REVVY__IsParentStep__c = false);
        insert asi;
        
        REVVY__MnStepApproverInst__c SAI = new REVVY__MnStepApproverInst__c(REVVY__StepInst__c = asi.id,REVVY__AssignedUser__c=user.id,REVVY__Status__c='Not Started');
        insert SAI;
        
        ContentVersion cv = new ContentVersion();
        cv.versionData = EncodingUtil.base64Decode('test data');
        cv.title = 'test document';
        cv.FirstPublishLocationId = Contract.Id;
        cv.pathOnClient = 'test document';
        cv.Attachment_Type__c = 'contract';
        cv.Description = 'test description';
        insert cv;
        
        Test.stopTest();
        
        REVVY__MnStepApproverInst__c SAI1 = new REVVY__MnStepApproverInst__c(REVVY__StepInst__c = asi.id,REVVY__AssignedUser__c=user1.id,REVVY__Status__c='Not Started');
        insert SAI1;
        
        Delete SAI;
    }
}