<apex:page renderAs="pdf" standardController="REVVY__MnContract__c" extensions="ContracttoPDFController" sidebar="false" 
           showHeader="false" id="pg" docType="html-5.0" applyHtmlTag="false" applyBodyTag="false">
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
            <style type="text/css" media="print">
                @page {
                    padding-top: 40px;
                @top-center {
                    content: element(header);
                }
                @bottom-left {
                    content: element(footer);
                    }
                }
                div.header {
                    position: running(header);
                    margin:8px 0px 0px 8px;
                    width:98%;
                }
                div.footer {
                    display: block;
                    padding: 5px;
                    position: running(footer);
                    font-family: Arial, Helvetica, sans-serif;
                    font-size:10pt;
                    color:#808080;
                    margin-top:10%;
                }
                .pagenumber:before {
                    content: counter(page);
                }
                .pagecount:before {
                    content: counter(pages);
                }
                .main-body{
                    margin-top:1%;
                    font-family: Arial, Helvetica, sans-serif;
                }
                .general-info{
                    width:100%;
                    table-layout: fixed;
                    border-spacing: 0px;
                    border-collapse: collapse;
                }
                table{
                    page-break-inside: avoid;
                }
                td{
                    font-size:9pt;
                    height:1.95em;
                    border: 0.5px solid #000;
                    padding-left:10px;
                }
                .table-header{
                    background-color: #0070C0;
                    border: 0.5px solid #000;
                    color:#fff; 
                    height:2em; 
                    font-size:9pt;
                    width:100%;
                }
                .column-header{
                    color:#0070C0;
                }
                table.table-File td{
                    border: 0.5px solid #0070C0;
                }
                table.Approver{
                        border-collapse: collapse;
                        border-spacing: 0;
                        width:100%;
                }
                table.Approver td{
                    border: 0.5px solid #0070C0;
                    border-left:0px;
                }
            </style>
        </head>
        <body>
            <div class="header">
                <div>
                    <apex:image value="{!$Resource.ComcastLogoPdf}" alt="Comcast" height="20%" width="20%"/>
                </div>
            </div>
            <div class="footer">
                <div><apex:outputText value="{0,date,MM/dd/yy hh:mm}"><apex:param value="{!Now()-(1/6)}"/> Eastern</apex:outputText></div>
            </div>
            <div class="main-body">
                <div>
                    <p style="font-size:15pt">
                        CAF Request Summary
                    </p>
                </div>
                <div>
                    <table class="table-header">
                        <thead>
                            <th style="width:100%">General Info/Contract Terms</th>
                        </thead>
                    </table>
                    <table class="general-info">
                        <tbody>
                            <tr>
                                <td>CAF Type</td>
                                <td><apex:outputText value="{!contract.CAF_Type__c}"/></td>
                            </tr>
                            <tr>
                                <td>Requester</td>
                                <td><apex:outputText value="{!contract.Requester_Formula__c}"/></td>
                            </tr>
                            <tr>
                                <td>Request Submitted On</td>
                                <td><apex:outputText value="{0,date,MM/dd/yy}">
                                    <apex:param value="{!contract.CreatedDate}"/></apex:outputText></td>
                            </tr>
                            <tr>
                                <td>Business Unit</td>
                                <td><apex:outputText value="{!contract.Business_Unit__c}"/></td>
                            </tr>
                            <tr>
                                <td>Contract Name</td>
                                <td><apex:outputText value="{!contract.Name}"/></td>
                            </tr>
                            <tr>
                                <td>Counterparty</td>
                                <td><apex:outputText value="{!contract.Counterparty__c}"/></td>
                            </tr>
                            <tr>
                                <td>Contract Value</td>
                                <td><apex:outputField value="{!contract.Contract_Value__c}"/></td>
                            </tr>
                            <tr>
                                <td>Professional Services Sourcing Contract?</td>
                                <td><apex:outputText value="{!contract.Is_Sourcing_Contract__c}"/></td>
                            </tr>
                            <tr>
                                <td>Used an Unmodified Legal Provided Contract?</td>
                                <td><apex:outputText value="{!contract.Unchanged_legal_provided_template_used__c}"/></td>
                            </tr>
                             <tr>
                                <td>Binding Corporate Development Contract?</td>
                                <td><apex:outputText value="{!contract.Is_Binding_Corporate_Dev_Contract__c}"/></td>
                            </tr>
                        </tbody>
                    </table>
                    <br/>
                    
                    <table class="table-header">
                        <thead>
                            <th>
                                Brief Summary
                            </th>
                        </thead>
                    </table>
                    <table class="general-info">
                        <tbody>
                            <tr>
                              <td><apex:outputText escape="false" value="{!contract.Brief_Summary__c}"/></td>
                            </tr>
                        </tbody>
                    </table>
                    <!--
                    <br/>
                    <br/>
                    <table class="table-header" style="border: 0.5px solid #0070C0;">
                        <thead>
                            <th>
                                File Uploads
                            </th>
                        </thead>
                    </table>
                    <table class="general-info table-File"  width="100%"> <!-- class="general-info table-File" -->
                       <!-- <tr class="column-header">
                            <td width="55%">File Name</td>
                            <td width="20%">Attachment Type</td>
                            <td width="25%">Description</td>
                        </tr>
                        <tbody>
                            <apex:variable value="{!1}" var="j"/>
                            <apex:repeat value="{!content}" var="doc">
                                <tr style="background-color:{!IF(mod(j,2)==0,'#FFF','#DAEEFE')};" >
                                    <td width="55%" valign="top">
                                        <apex:outputText value="{!doc.Title}"/>
                                    </td>
                                    <td width="20%" valign="top">
                                        <apex:outputText value="{!doc.Attachment_Type__c}"/>
                                    </td>
                                    <td width="25%" valign="top">
                                        <apex:outputText value="{!doc.Description}"/>
                                    </td>
                                </tr>
                                <apex:variable value="{!j+1}" var="j" />
                            </apex:repeat>
                        </tbody>
                    </table> -->
                    
                    <br/>
                    <div style="page-break-inside: avoid;">
                        <table class="table-header" style="border: 0.5px solid #0070C0;">
                            <thead>
                                <th>
                                    Approver Summary
                                </th>
                            </thead>
                        </table>
                        <table class="general-info table-File" style="border-bottom:0.5px solid #0070C0">
                            <tr class="column-header">
                                <td style="width:25%">Title</td>
                                <td style="width:131px;border-bottom:0px">Name</td>
                                <td style="width:104px;border-bottom:0px">Status</td>
                                <td style="width:105px;border-bottom:0px">Datetime (Eastern)</td>
                                <td>Comment</td>
                            </tr>
                            <apex:variable value="{!1}" var="i"/>
                            
                            <apex:repeat value="{!ApprovalInstListTemp}" var="App">
                                <tr style="background-color:{!IF(mod(i,2)==0,'#FFF','#DAEEFE')};">
                                    <td style="25%">
                                        <apex:outputText value="{!App.Name}"/>
                                    </td>
                                    <td colspan="4" style="width:75%;border-collapse:collapse;border-style:none;padding:0px;border-right:0.5px solid #0070C0;"> <!-- border-bottom:0.5px solid #0070C0   -->
                                        <apex:repeat value="{!App.REVVY__StepInstApprovers__r}" var="AL">
                                            <table class="Approver" style="border-collapse:collapse;border-style:hidden;">
                                                <tr>
                                                    <td style="width:27%; padding:10px 0px 10px 10px;">
                                                        <apex:outputText value="{!AL.Name}"/>
                                                    </td>
                                                    <td style="width:22%;">
                                                        <apex:outputText value="{!AL.REVVY__Status__c}"/>
                                                    </td>
                                                    <td style="width:22%;">
                                                        <apex:outputText value="{0,date,MM/dd/yy hh:mm a}"><apex:param value="{!AL.REVVY__ActionDate__c-(1/6)}"/></apex:outputText>
                                                    </td>
                                                    <td style="border-right:0px;">
                                                        <apex:outputText value="{!AL.REVVY__ApprovalComments__c}"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </apex:repeat>
                                    </td>
                                </tr>
                                <apex:variable value="{!i+1}" var="i" />
                            </apex:repeat>
                        </table>
                    </div>
                </div>
            </div>
        </body>
    </html>
</apex:page>